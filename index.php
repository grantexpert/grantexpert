
<?php include './_components/_head.php';?>

<body class="home">

<div id="site-container">

    <?php include './_components/_header.php';?>

    <main id="site-content">
        <section class="preface">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <img src="http://grantexpert.test/assets/img/svg/grant-world.svg" alt="grant world">
                    </div>
                    <div class="col-lg-6">
                        <div class="home-right-container">
                            <h1>Spoľahlivo vás prevedieme svetom grantových výziev</h1>
                            <p class="text-primary">Pre sledovanie relevantných grantov stačí zadať váš email. My sa postaráme, aby ste boli vždy včas informovaní.</p>
                            <div class="input-animation input-with-icon">
                                <div class="input-call-to-action">
                                    <input type="text" class="form-control">
                                </div>
                                <div class="icon-container">
                                    <img class="icon" src="http://grantexpert.test/assets/img/svg/email.svg" alt="locker">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="button-scroll button-scroll--down">
                    <div class="button-border--gray"></div>
                    <div class="button-border--white">
                        <div class="icon-container"></div>
                    </div>
                </button>
            </div>
        </section>
        <section class="grant-database">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <img src="http://grantexpert.test/assets/img/svg/grant-database.svg" alt="grant database">
                                <h2 class="title-primary">Kompletná databáza aktuálnych grantových ponúk</h2>
                                <p class="text-primary">Všetky výzvy prehľadne s možnosťou filtrácie podľa oblastí záujmu na jednom mieste</p>
                            </div>
                            <div class="col-lg-6">
                                <div class="home-right-container">
                                    <div class="row header-label-container">
                                        <div class="left-part">
                                            <div class="text-small-label">Naše služby pre segment</div>
                                        </div>
                                        <div class="right-part">
                                            <div class="text-small-label">Dostupné granty</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="left-part">
                                            <div class="row-line">
                                                <a href="#" class="arrow-link">
                                                    <div class="icon-container">
                                                        <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                                    </div>
                                                    <div class="label-container">
                                                        <div class="title"><div class="vertical-center">Podnikatelia</div></div>
                                                        <div class="subtitle">Naše služby pre segment</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="row-line">
                                                <a href="#" class="arrow-link">
                                                    <div class="icon-container">
                                                        <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                                    </div>
                                                    <div class="label-container">
                                                        <div class="title"><div class="vertical-center">Mestá a obce</div></div>
                                                        <div class="subtitle">Naše služby pre segment</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="row-line">
                                                <a href="#" class="arrow-link">
                                                    <div class="icon-container">
                                                        <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                                    </div>
                                                    <div class="label-container">
                                                        <div class="title"><div class="vertical-center">Školy</div></div>
                                                        <div class="subtitle">Naše služby pre segment</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="row-line">
                                                <a href="#" class="arrow-link">
                                                    <div class="icon-container">
                                                        <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                                    </div>
                                                    <div class="label-container">
                                                        <div class="title"><div class="vertical-center">Mimovládne organizácie</div></div>
                                                        <div class="subtitle">Naše služby pre segment</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="row-line last">
                                                <a href="#" class="arrow-link">
                                                    <div class="icon-container">
                                                        <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                                    </div>
                                                    <div class="label-container">
                                                        <div class="title"><div class="vertical-center">Jednotlivci</div></div>
                                                        <div class="subtitle">Naše služby pre segment</div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="right-part">
                                            <div class="row-container">
                                                <div class="button-animation">
                                                    <button class="button-tertiary button-icon">
                                                        <div class="icon-container">
                                                            <img src="http://grantexpert.test/assets/img/svg/arrow-white.svg" alt="arrow">
                                                            <img src="http://grantexpert.test/assets/img/svg/arrow-white.svg" alt="arrow">
                                                        </div>
                                                        <div class="label-container">
                                                            59
                                                        </div>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row-container">
                                                <div class="button-animation">
                                                    <button class="button-tertiary button-icon">
                                                        <div class="icon-container">
                                                            <img src="http://grantexpert.test/assets/img/svg/arrow-white.svg" alt="arrow">
                                                            <img src="http://grantexpert.test/assets/img/svg/arrow-white.svg" alt="arrow">
                                                        </div>
                                                        <div class="label-container">
                                                            59
                                                        </div>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row-container">
                                                <div class="button-animation">
                                                    <button class="button-tertiary button-icon">
                                                        <div class="icon-container">
                                                            <img src="http://grantexpert.test/assets/img/svg/arrow-white.svg" alt="arrow">
                                                            <img src="http://grantexpert.test/assets/img/svg/arrow-white.svg" alt="arrow">
                                                        </div>
                                                        <div class="label-container">
                                                            59
                                                        </div>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row-container">
                                                <div class="button-animation">
                                                    <button class="button-tertiary button-icon">
                                                        <div class="icon-container">
                                                            <img src="http://grantexpert.test/assets/img/svg/arrow-white.svg" alt="arrow">
                                                            <img src="http://grantexpert.test/assets/img/svg/arrow-white.svg" alt="arrow">
                                                        </div>
                                                        <div class="label-container">
                                                            59
                                                        </div>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row-container">
                                                <div class="button-animation">
                                                    <button class="button-tertiary button-icon">
                                                        <div class="icon-container">
                                                            <img src="http://grantexpert.test/assets/img/svg/arrow-white.svg" alt="arrow">
                                                            <img src="http://grantexpert.test/assets/img/svg/arrow-white.svg" alt="arrow">
                                                        </div>
                                                        <div class="label-container">
                                                            59
                                                        </div>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="eligible-applicant">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <img src="http://grantexpert.test/assets/img/svg/eligible-applicant.svg" alt="eligible applicant">
                    </div>
                    <div class="col-lg-6">
                        <div class="home-right-container">
                            <h2 class="title-primary">Neviete, či ste<br> oprávneným žiadateľom?</h2>
                            <p class="text-primary">Úplne bezplatne ponúkame základné overenie oprávnenosti vašej živnosti, alebo spoločnosti</p>
                            <div class="input-animation">
                                <div class="input-call-to-action">
                                    <input type="text" class="form-control" placeholder="IČO, alebo názov spoločnosti">
                                </div>
                            </div>
                            <div class="text-small-label">Krok: 1/3</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="grant-education">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">

                        <h2 class="title-secondary text-center">Grantové vzdelávanie</h2>
                        <div class="row">
                            <div class="col-12">
                                <div class="carousel-container">
                                    <div class="tns-carousel carousel-grant-education">
                                        <div>
                                            <div class="card-event-container">
                                                <div class="card card-event">
                                                    <header>
                                                        <div class="col-container">
                                                            <a href="#" class="title-container">
                                                                <h3 class="title-secondary">Grantová podpora škôl</h3>
                                                            </a>
                                                            <div class="icon-container">
                                                                <img src="http://grantexpert.test/assets/img/svg/calendar.svg" alt="calendar">
                                                            </div>
                                                        </div>

                                                        <div class="date-time">
                                                            <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/calendar.svg" alt="calendar"></span><span class="date">28.10.</span><span class="time">10:00 - 12:00</span>
                                                        </div>


                                                    </header>
                                                    <h4>Čo sa naučíte?</h4>
                                                    <ul class="check-list text-secondary">
                                                        <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                        <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                        <li>Aktuálne a plánované výzvy</li>
                                                    </ul>
                                                    <footer>
                                                        <div class="col-container">
                                                            <div class="price-container">
                                                                <div class="tax-included">Cena s DPH</div>
                                                                <div class="title-secondary color-sun price">19,90 €</div>
                                                            </div>
                                                            <div class="more-info">
                                                                <a href="#" class="arrow-link no-hover">
                                                                    <div class="icon-container">
                                                                        <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                                                    </div>
                                                                    <div class="label-container">
                                                                        <div class="title">Viac info</div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <div class="button-container">
                                                                <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                                            </div>
                                                        </div>
                                                    </footer>
                                                </div>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="card-event-container">
                                                <div class="card card-event">
                                                    <header>
                                                        <div class="col-container">
                                                            <a href="#" class="title-container">
                                                                <h3 class="title-secondary">Cesta úspešného projektu: Podnikateľský IT projekt</h3>
                                                            </a>
                                                            <div class="icon-container">
                                                                <img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker">
                                                            </div>
                                                        </div>

                                                        <div class="preview-container col-container">
                                                            <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker"></span>
                                                            <div class="stars">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star-blank.svg" alt="star-blank">
                                                            </div>
                                                            <a href="#" class="button-preview"><img src="http://grantexpert.test/assets/img/svg/play.svg" alt="play"><span class="label">UKÁŽKA</span></a>
                                                        </div>


                                                    </header>
                                                    <h4>Čo sa naučíte?</h4>
                                                    <ul class="check-list text-secondary">
                                                        <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                        <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                        <li>Aktuálne a plánované výzvy</li>
                                                    </ul>
                                                    <footer>
                                                        <div class="col-container">
                                                            <div class="price-container">
                                                                <div class="tax-included">Cena s DPH</div>
                                                                <div class="title-secondary color-sun price">19,90 €</div>
                                                            </div>
                                                            <div class="more-info">
                                                                <a href="#" class="arrow-link no-hover">
                                                                    <div class="icon-container">
                                                                        <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                                                    </div>
                                                                    <div class="label-container">
                                                                        <div class="title">Viac info</div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <div class="button-container">
                                                                <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                                            </div>

                                                        </div>
                                                    </footer>
                                                </div>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="card-event-container">
                                                <div class="card card-event">
                                                    <header>
                                                        <div class="col-container">
                                                            <a href="#" class="title-container">
                                                                <h3 class="title-secondary">Cesta úspešného projektu: Podnikateľský IT projekt</h3>
                                                            </a>
                                                            <div class="icon-container">
                                                                <img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker">
                                                            </div>
                                                        </div>

                                                        <div class="preview-container col-container">
                                                            <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker"></span>
                                                            <div class="stars">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star-blank.svg" alt="star-blank">
                                                            </div>
                                                            <a href="#" class="button-preview"><img src="http://grantexpert.test/assets/img/svg/play.svg" alt="play"><span class="label">UKÁŽKA</span></a>
                                                        </div>


                                                    </header>
                                                    <h4>Čo sa naučíte?</h4>
                                                    <ul class="check-list text-secondary">
                                                        <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                        <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                        <li>Aktuálne a plánované výzvy</li>
                                                    </ul>
                                                    <footer>
                                                        <div class="col-container">
                                                            <div class="price-container">
                                                                <div class="tax-included">Cena s DPH</div>
                                                                <div class="title-secondary color-sun price">19,90 €</div>
                                                            </div>
                                                            <div class="more-info">
                                                                <a href="#" class="arrow-link no-hover">
                                                                    <div class="icon-container">
                                                                        <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                                                    </div>
                                                                    <div class="label-container">
                                                                        <div class="title">Viac info</div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <div class="button-container">
                                                                <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                                            </div>

                                                        </div>
                                                    </footer>
                                                </div>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="card-event-container">
                                                <div class="card card-event">
                                                    <header>
                                                        <div class="col-container">
                                                            <a href="#" class="title-container">
                                                                <h3 class="title-secondary">Cesta úspešného projektu: Podnikateľský IT projekt</h3>
                                                            </a>
                                                            <div class="icon-container">
                                                                <img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker">
                                                            </div>
                                                        </div>

                                                        <div class="preview-container col-container">
                                                            <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker"></span>
                                                            <div class="stars">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star-blank.svg" alt="star-blank">
                                                            </div>
                                                            <a href="#" class="button-preview"><img src="http://grantexpert.test/assets/img/svg/play.svg" alt="play"><span class="label">UKÁŽKA</span></a>
                                                        </div>


                                                    </header>
                                                    <h4>Čo sa naučíte?</h4>
                                                    <ul class="check-list text-secondary">
                                                        <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                        <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                        <li>Aktuálne a plánované výzvy</li>
                                                    </ul>
                                                    <footer>
                                                        <div class="col-container">
                                                            <div class="price-container">
                                                                <div class="tax-included">Cena s DPH</div>
                                                                <div class="title-secondary color-sun price">19,90 €</div>
                                                            </div>
                                                            <div class="more-info">
                                                                <a href="#" class="arrow-link no-hover">
                                                                    <div class="icon-container">
                                                                        <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                                                    </div>
                                                                    <div class="label-container">
                                                                        <div class="title">Viac info</div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <div class="button-container">
                                                                <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                                            </div>

                                                        </div>
                                                    </footer>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <button id="education-carousel-prev" class="button-scroll button-scroll--left">
                                        <div class="button-border--gray"></div>
                                        <div class="button-border--white">
                                            <div class="icon-container"></div>
                                        </div>
                                    </button>
                                    <button id="education-carousel-next" class="button-scroll button-scroll--right">
                                        <div class="button-border--gray"></div>
                                        <div class="button-border--white">
                                            <div class="icon-container"></div>
                                        </div>
                                    </button>

                                </div>
                                <div class="button-center">
                                    <a href="" class="button-primary"><div class="vertical-center">Všetky webináre</div></a>
                                    <a href="" class="button-primary"><div class="vertical-center">Všetky e-learningy</div></a>
                                    <a href="" class="button-primary"><div class="vertical-center">Školenia na mieru</div></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="blog">
            <div class="container">

                <h2 class="title-secondary text-center">Blog</h2>

                <div class="row">
                    <div class="col-12">
                        <div class="carousel-container">
                            <div class="tns-carousel carousel-blog">
                                <div>
                                    <div class="card-animation card-border-animation">
                                        <div class="card card-border">
                                            <a href="#" class="tag">AKTUALITY</a>
                                            <a href="#">
                                                <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                            </a>
                                            <p>
                                                Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="card-animation card-border-animation">
                                        <div class="card card-border">
                                            <a href="#" class="tag">AKTUALITY</a>
                                            <a href="#">
                                                <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                            </a>
                                            <p>
                                                Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="card-animation card-border-animation">
                                        <div class="card card-border">
                                            <a href="#" class="tag">AKTUALITY</a>
                                            <a href="#">
                                                <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                            </a>
                                            <p>
                                                Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="card-animation card-border-animation">
                                        <div class="card card-border">
                                            <a href="#" class="tag">AKTUALITY</a>
                                            <a href="#">
                                                <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                            </a>
                                            <p>
                                                Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <button id="blog-carousel-prev" class="button-scroll button-scroll--left">
                                <div class="button-border--gray"></div>
                                <div class="button-border--white">
                                    <div class="icon-container"></div>
                                </div>
                            </button>
                            <button id="blog-carousel-next" class="button-scroll button-scroll--right">
                                <div class="button-border--gray"></div>
                                <div class="button-border--white">
                                    <div class="icon-container"></div>
                                </div>
                            </button>
                        </div>



                        <div class="button-center">
                            <a href="" class="button-primary"><div class="vertical-center">VŠETKY BLOGY</div></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="service-packages">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">

                        <h2 class="title-secondary text-center">Vyberte si balík našich služieb podľa<br> aktuálnej potreby</h2>

                        <div class="row">
                            <div class="col-md-6 col-xl-3">
                                <a href="#">
                                    <div class="card-animation card-shadow-animation">
                                        <div class="card card-shadow">
                                            <div class="subtitle text-secondary text-center">Pre rýchlu orientáciu</div>
                                            <h3 class="title-primary">Free</h3>
                                            <div class="tax-included">CENA S DPH</div>
                                            <div class="title-secondary text-blue">0 €/rok</div>
                                            <ul class="check-list small-padding text-secondary">
                                                <li>Automatické zasielanie notifikácií o nových výzvach</li>
                                                <li>Neobmedzený prístup do databázy grantov a dotácií</li>
                                                <li>Overenie oprávnenosti</li>
                                            </ul>

                                        </div>
                                        <div class="button-secondary">OBJEDNAŤ</div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-xl-3">
                                <a href="#">
                                    <div class="card-animation card-shadow-animation">
                                        <div class="card card-shadow">
                                            <div class="subtitle text-secondary text-center">Pre prvé granty</div>
                                            <h3 class="title-primary">Basic</h3>
                                            <div class="tax-included">CENA S DPH</div>
                                            <div class="title-secondary text-blue">34,90 €/rok</div>
                                            <ul class="check-list small-padding text-secondary">
                                                <li>Automatické zasielanie notifikácií o nových výzvach</li>
                                                <li>Neobmedzený prístup do databázy grantov a dotácií</li>
                                                <li>Overenie oprávnenosti</li>
                                            </ul>

                                        </div>
                                        <div class="button-secondary">OBJEDNAŤ</div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-xl-3">
                                <a href="#">
                                    <div class="card-animation card-shadow-animation">
                                        <div class="card card-shadow">
                                            <div class="subtitle text-secondary text-center">Pre pokročilé vyhľadávanie</div>
                                            <h3 class="title-primary">Medium</h3>
                                            <div class="tax-included">CENA S DPH</div>
                                            <div class="title-secondary text-blue">69,90 €/rok</div>
                                            <ul class="check-list small-padding text-secondary">
                                                <li>Neobmedzený prístup do databázy grantov a dotácií</li>
                                                <li>Archív bezplatných webinárov</li>
                                                <li>Vyhľadanie dotácie k 1 vášmu projektu</li>
                                            </ul>

                                        </div>
                                        <div class="button-secondary">OBJEDNAŤ</div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-xl-3">
                                <a href="#">
                                    <div class="card-animation card-shadow-animation card-shadow-animation--gold">
                                        <div class="card card-shadow">
                                            <div class="subtitle text-secondary text-center">Pre aktívnych riešiteľov</div>
                                            <h3 class="title-primary">Profi</h3>
                                            <div class="tax-included">CENA S DPH</div>
                                            <div class="title-secondary text-blue">149,90 €/rok</div>
                                            <ul class="check-list small-padding text-secondary">
                                                <li>Neobmedzený prístup do databázy grantov a dotácií</li>
                                                <li>Archív bezplatných webinárov</li>
                                                <li>Vyhľadanie dotácie k 1 vášmu projektu</li>
                                            </ul>

                                        </div>
                                        <div class="button-secondary">OBJEDNAŤ</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="button-center">
                            <a href="" class="button-primary"><div class="vertical-center">POROVNAŤ BALÍKY</div></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>

    <?php include './_components/_footer.php';?>

