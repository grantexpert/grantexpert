var blogCarousel = function($){

    function init(){

        if( $('.carousel-blog').length ){

            var blogCarousel = tns({
                container: '.carousel-blog',
                items: 3,
                slideBy: 'page',
                autoplay: false,
                nav: false,
                navPosition: 'bottom',
                mouseDrag: false,
                touch: false,
                controls: true,
                nextButton: '#blog-carousel-next',
                prevButton: '#blog-carousel-prev',
                autoplayButtonOutput: false,
                gutter: 30,
                speed: 900,
                disable: true,
                responsive: {
                    1200: {
                        items: 3,
                        freezable: true,
                        touch: true
                    },
                    768: {
                        items: 2,
                        mouseDrag: false,
                        touch: false,
                        disable: false
                    }
                }
            });

        }

    }

    return{
        init : init
    };

}(jQuery);
var cartSubmenu = function($){


    function scrollToSelectedSubpage(){

        var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        var border = 4;
        var marginPadding = 60;
        if( windowWidth <= 767 ){
            marginPadding = 40;
            border = 0;
        }


        var leftElementWidth = $(".cart-submenu ul li:nth-of-type(1)").width() + marginPadding + border;
        var marginLeft = (windowWidth / 2) - (leftElementWidth / 2) + 10;
        $(".cart-submenu ul li:nth-of-type(1)").css('margin-left', marginLeft);

        var rightElementWidth = $(".cart-submenu ul li:nth-last-of-type(1)").width() + marginPadding + border;
        var marginRight = (windowWidth / 2) - (rightElementWidth / 2) + 10;
        $(".cart-submenu ul li:nth-last-of-type(1)").css('margin-right', marginRight);


        var elementWidth = $(".cart-submenu ul .active").width();
        var leftPosition = $(".cart-submenu ul .active").position().left;


        $(".cart-submenu ul").animate( { scrollLeft: leftPosition - ((windowWidth / 2) - ((elementWidth + marginPadding) / 2)) }, 100);


    }

    function init(){
        setTimeout(function() { scrollToSelectedSubpage() }, 500);
    }

    return{
        init : init
    };

}(jQuery);
var educationCarousel = function($){
     var educationCarousel = '';

    function init(){

        if( $('.carousel-grant-education').length ) {

            educationCarousel = tns({
                container: '.carousel-grant-education',
                items: 1,
                slideBy: 'page',
                autoplay: false,
                nav: false,
                navPosition: 'bottom',
                mouseDrag: true,
                controls: true,
                nextButton: '#education-carousel-next',
                prevButton: '#education-carousel-prev',
                autoplayButtonOutput: false,
                gutter: 15,
                speed: 900,
                fixedWidth: 245,
                preventScrollOnTouch: 'auto',
                /*autoWidth: false*/

                responsive: {
                    1200: {
                        items: 3,
                        /*fixedWidth: false*/
                        /*autoWidth: false*/
                    },
                    768: {
                        items: 2,
                        gutter: 30,
                        /*fixedWidth: false*/
                        /*autoWidth: false*/
                    },
                    576: {
                        items: 2,
                        gutter: 15,
                        fixedWidth: false,
                        /*autoWidth: false*/
                    }
                }
            });

            /*setTimeout(function() {
                educationCarousel.destroy();
                educationCarousel = educationCarousel.rebuild();
                console.log('rebuilded');

            }, 5000);*/

            /*$(window).on('resize', function(){
                console.log('rebuilded by resize');
                var educationTitle = $('section.grant-education h2').text();
                $('section.grant-education h2').text( educationTitle + '.')

                educationCarousel.destroy();
                educationCarousel = educationCarousel.rebuild();
                console.log('end rebuilded');

            });*/

            

        }

    }

    return{
        init : init
    };

}(jQuery);
var form = function($){


    function initInputRequired(){

        $(".input-required input").focusout(function(){

            var inputContainer = $(this).closest('.input-required');

            if($(this).val() == '') inputContainer.removeClass('input-required--filled');
            else inputContainer.addClass('input-required--filled');
        });

        $(".input-required input").focusin(function(){

            $(this).closest('.input-required').addClass('input-required--filled');

        });

    }

    function initCheckbox(){

        $(".checkbox .label").click(function () {
            var checkbox = $(this).prev();
            $(this).parent().toggleClass('checked');
            if ( checkbox.is(':checked') ) checkbox.prop('checked', false);
            else checkbox.prop('checked', true);

        });

        $(".col-select .select-all").click(function () {
            var modalWindow = $(this).closest('.modal');
            modalWindow.find('.checkbox').addClass('checked');
            modalWindow.find('.checkbox input').prop('checked', true);

        });
        $(".col-select .deselect-all").click(function () {
            var modalWindow = $(this).closest('.modal');
            modalWindow.find('.checkbox').removeClass('checked');
            modalWindow.find('.checkbox input').prop('checked', false);

        });

    }

    function initSelectbox(){
        $(".selectbox .selectbox-label").click(function () {
            $('.selectbox ul').not( $(this).next() ).fadeOut();
            $(this).next().fadeToggle();
        });

        $(".selectbox ul li").click(function () {

            var selectLabel = $(this).html();
            var selectValue = $(this).attr('data-value');
            var customSelectbox = $(this).closest('.selectbox');

            customSelectbox.find('.selectbox-label').html(selectLabel);
            customSelectbox.find('ul').fadeOut();
            customSelectbox.find('input').val(selectValue);

        });

        $(document).click(function(event) {
            var $target = $(event.target);
            if(!$target.closest('.selectbox').length &&
                $('.selectbox ul').is(":visible")) {
                $('.selectbox ul').fadeOut();
            }
        });
    }

    function init(){

        initInputRequired();
        initCheckbox();
        initSelectbox();

    }

    return{
        init : init
    };
}(jQuery);
var menuFixedBottom = function($){

    function getScrollbarWidth() {

        // Creating invisible container
        const outer = document.createElement('div');
        outer.style.visibility = 'hidden';
        outer.style.overflow = 'scroll'; // forcing scrollbar to appear
        outer.style.msOverflowStyle = 'scrollbar'; // needed for WinJS apps
        document.body.appendChild(outer);

        // Creating inner element and placing it in the container
        const inner = document.createElement('div');
        outer.appendChild(inner);

        // Calculating difference between container's full width and the child width
        const scrollbarWidth = (outer.offsetWidth - inner.offsetWidth);

        // Removing temporary elements from the DOM
        outer.parentNode.removeChild(outer);

        return scrollbarWidth;

    }

    function init(){

        //#modal-eligibility, #modal-grant-radar
        $('.modal').on('show.bs.modal', function (event) {
            var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

            if( windowWidth >= 992 ){
                var scrollWidth = getScrollbarWidth();
                $('.menu-fixed-bottom').css('margin-right', scrollWidth);
            }

        });

        //#modal-eligibility, #modal-grant-radar
        $('.modal').on('hidden.bs.modal', function (event) {

            $('.menu-fixed-bottom').css('margin-right', 0);

        });

        $(".modal-menu-fixed .page-1 .button-tertiary").click(function () {
            $(this).closest('.modal').find('.page-1').hide();
            $(this).closest('.modal').find('.page-2').show();
            $(this).closest('.modal').addClass('page-2-selected');
        });

        $(".modal-menu-fixed .page-2 .button-tertiary").click(function () {
            $(this).closest('.modal').find('.page-2').hide();
            $(this).closest('.modal').find('.page-3').show();
            $(this).closest('.modal').removeClass('page-2-selected');
            $(this).closest('.modal').addClass('page-3-selected');
        });
        $("#modal-eligibility .page-3 .button-tertiary").click(function () {
            $(this).closest('.modal').find('.page-3').hide();
            $(this).closest('.modal').find('.page-4').show();
            $(this).closest('.modal').removeClass('page-3-selected');
            $(this).closest('.modal').addClass('page-4-selected');
        });

    }

    return{
        init : init
    };

}(jQuery);
var menuMain = function($){
    var windowWidth = '';



    function setClickEvents(){
        $(".menu-main .icon").click(function () {
            $(".navbar-collapse").fadeToggle(500);
            $(".top-menu").toggleClass("top-animate");
            $(".mid-menu").toggleClass("mid-animate");
            $(".bottom-menu").toggleClass("bottom-animate");
            $('body').toggleClass('overflow-y-hidden');
        });
    }

    function init(){

        $( ".menu-main .button-tertiary" ).hover(
            function() {
                /*$(".menu-main .button-tertiary #line-1").attr("x1","32");
                $(".menu-main .button-tertiary #line-1").attr("x2","38");*/
                /*#line-1{
                    x1: 32px;
                    x2: 38px;
                }*/
            }, function() {
                /*$(".menu-main .button-tertiary #line-1").attr("x1","29.5");
                $(".menu-main .button-tertiary #line-1").attr("x2","42.5");*/
            }
        );


        menuOnScroll.init();
    }

    return{
        init : init
    };
    
}(jQuery);
var menuMobile = function($){
    var windowWidth = '';

    function setClickableResponsiveMenu(){
        $('.menu-mobile li.dropdown a.dropdown-toggle').removeClass('disabled');

        /*if (windowWidth <= 991) $('.menu-mobile li.dropdown a.dropdown-toggle').removeClass('disabled');
        else $('.menu-mobile li.dropdown a.dropdown-toggle').addClass('disabled');*/
    }

    function setClickEvents(){
        $(".menu-mobile .open-icon").click(function () {

            $('.menu-mobile .window-container').fadeIn(400);
            $('.menu-mobile .open-icon').fadeOut(200);
            $('body').toggleClass('overflow-y-hidden');
            $('.menu-mobile .window-container > .col-container').toggleClass('ejected');

        });

        $(".menu-mobile .close-icon").click(function () {
            $('.menu-mobile .open-icon').fadeIn(400);
            $('.menu-mobile .window-container').fadeOut(400);
            $('.menu-mobile .open-icon').removeClass('hide');
            $('body').toggleClass('overflow-y-hidden');
            $('.menu-mobile .window-container > .col-container').toggleClass('ejected');
        });

        $(".menu-mobile li.dropdown > a").click(function () {
            if( $(this).hasClass('opened') ){

                $(this).toggleClass('opened');
                $(this).next().fadeToggle();

            }
            else {

                $(".menu-mobile li.dropdown > a").removeClass('opened');
                $('.menu-mobile .dropdown-menu').hide();

                $(this).toggleClass('opened');
                $(this).next().fadeToggle();

            }

        });
    }

    function init(){
        windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

        setClickableResponsiveMenu();
        setClickEvents();

    }

    return{
        init : init
    };

}(jQuery);
var menuOnScroll = function($){
  	
  	var didScroll = '';
    var lastScrollTop = 0;
    var delta = 5;
    var navbarHeight = $('#site-header').outerHeight();

  	function setScroll(){

        $(window).scroll(function(event){
        	
            didScroll = true;
            if($(window).scrollTop() > 0){
                //$('#site-header').addClass('site-header--shadow_mobile');
            }
            else{
                //$('#site-header').removeClass('site-header--shadow_mobile');
            }
            if($(window).scrollTop() > 41){
                $('#site-header').addClass('site-header--small_mobile');
            }
            else{
                $('#site-header').removeClass('site-header--small_mobile');
            }
        });

        setInterval(function() {
            if (didScroll) {
                hasScrolled();
                didScroll = false;
            }
        }, 250);
    }

    function hasScrolled() {
        var st = $(this).scrollTop();
        
        // Make sure they scroll more than delta
        if(Math.abs(lastScrollTop - st) <= delta)
            return;
        
        // If they scrolled down and are past the navbar, add class .nav-up.
        // This is necessary so you never see what is "behind" the navbar.
        if (st > lastScrollTop && st > navbarHeight){
            // Scroll Down
            $('#site-header').removeClass('nav-down').addClass('nav-up');
        } else {
            // Scroll Up
            if(st + $(window).height() < $(document).height()) {
                $('#site-header').removeClass('nav-up').addClass('nav-down');
            }
        }
        
        lastScrollTop = st;
    }

    function init(){

    	if($(window).scrollTop() > 0){
            //$('#site-header').addClass('site-header--small_mobile');
            $('#site-header').addClass('site-header--shadow_mobile');
        }

        setScroll();
        
    }

    return{
        init : init
    };
    
}(jQuery);


var modalLogin = function($){


    function init(){

        $("#modal-login .page-1 .login-button").click(function () {
            $(this).closest('.modal').find('.page-1').hide();
            $(this).closest('.modal').find('.page-2').show();
            $(this).closest('.modal').addClass('page-2-selected');
        });
    }

    return{
        init : init
    };

}(jQuery);
var pageSubmenu = function($){

    /*function scrollToSelectedSubpage(){

        var leftPosition = $(".page-submenu ul .active").position().left;
        var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        var elementWidth = $(".page-submenu ul .active").width();

        console.log(elementWidth);

        $(".page-submenu ul").scrollLeft( leftPosition - ((windowWidth / 2) - ((elementWidth + 60) / 2)) );

    }*/

    function scrollToSelectedSubpage(){

        var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        var border = 4;
        var marginPadding = 60;
        if( windowWidth <= 767 ){
            marginPadding = 40;
            border = 0;
        }


        var leftElementWidth = $(".page-submenu ul li:nth-of-type(1)").width() + marginPadding + border;
        var marginLeft = (windowWidth / 2) - (leftElementWidth / 2) + 10;
        $(".page-submenu ul li:nth-of-type(1)").css('margin-left', marginLeft);

        var rightElementWidth = $(".page-submenu ul li:nth-last-of-type(1)").width() + marginPadding + border;
        var marginRight = (windowWidth / 2) - (rightElementWidth / 2) + 10;
        $(".page-submenu ul li:nth-last-of-type(1)").css('margin-right', marginRight);


        var elementWidth = $(".page-submenu ul .active").width();
        var leftPosition = $(".page-submenu ul .active").position().left;


        console.log(elementWidth);

        $(".page-submenu ul").animate( { scrollLeft: leftPosition - ((windowWidth / 2) - ((elementWidth + marginPadding) / 2)) }, 100);

        //$(".page-submenu ul").scrollLeft(leftPosition - ((windowWidth / 2) - ((elementWidth + marginPadding) / 2)));


    }

    function init(){

        setTimeout(function() { scrollToSelectedSubpage() }, 500);
    }

    return{
        init : init
    };

}(jQuery);
var rollDownList = function($){


    function init(){

        $(".roll-down-list .roll-down-item").click(function (e) {

            if( $(e.target).hasClass('col-info') ) return;
            if( $(e.target).closest('.col-info').length ) return;

            if( $(this).hasClass('opened') ){

                $(this).toggleClass('opened');
                $(this).find('.content').fadeToggle();

            }
            else {

                /*$(".roll-down-list .roll-down-item").removeClass('opened');
                $('.roll-down-list .content').hide();*/

                $(this).toggleClass('opened');
                $(this).find('.content').fadeToggle();

            }

        });

    }

    return{
        init : init
    };

}(jQuery);
var scrollToElement = function($){
  
    function init(element, duration){
        $('html, body').animate({ scrollTop: $(element).offset().top }, duration);
        return false;
    }

    return{
        init : init
    };
    
}(jQuery);


var waitForImages = function($){

    function init(){
        $('.home .bs-slider').waitForImages(function() {

            //bsSlider.init();

            //$('.page-loader').fadeOut(500);

		}, function(loaded, count, success) {
		   //console.log(loaded + ' off ' + count + ' images has ' + (success ? 'loaded' : 'failed to load') +  '.');
		}, true);
    }

    return{
        init : init
    };
}(jQuery);
var page404 = function($){

    function init(){

        blogCarousel.init();

    }

    return{
        init : init
    };
}(jQuery);
var defaultPage = function($){
    
    function getBodyClass(prefix){
        var bodyClass= "";
        if($('body').hasClass('home')) bodyClass='home';
        else{
            bodyClasses= $('body').attr('class');
            var regExpression = new RegExp(prefix + '-[^ ]*', 'g');
            if(bodyClasses.match(regExpression) != null){
                var matchedClasses = bodyClasses.match(regExpression);

                matchedClasses.forEach(function(entry) {
                    if(!entry.includes('page-template') && !entry.includes('page-id') && entry != 'page') bodyClass = entry;
                });
            }

        }

        return bodyClass;
    };

    function setSiteContentMinHeight(){
        var windowHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        var siteContentMinHeight = windowHeight - $('#site-header').height() - $('#site-footer').height();
        
        $('#site-content').css('min-height', siteContentMinHeight + 'px');
    }

    function init(){
        menuMain.init();
        menuMobile.init();
        menuFixedBottom.init();

        form.init();

        modalLogin.init();



        //setSiteContentMinHeight();


        $("#site-footer .button-scroll-top").click(function () {
            scrollToElement.init('body', 100);
        });

        $(".menu-fixed-bottom .button-close").click(function () {
            $(".menu-fixed-bottom").fadeOut();
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        $( ".page-submenu li" ).click(function() {
            var hrefURL = $(this).find('a').attr('href');
            window.location.href = hrefURL;
        });




    };

    return{
        init : init,
        getBodyClass : getBodyClass
    };

}(jQuery);

var aboutUsPage = function($){

    function setClickEvents(){
        $(".page-who-we-are section.who-we-are .arrow-link").click(function (e) {
            e.preventDefault();
            $(this).next().fadeToggle();

        });

        $("section.offer-closable .button-close--blue").click(function () {
            $('section.offer-closable').fadeOut();
        });

        $(".faq--desktop .roll-down-list .roll-down-item").click(function (e) {

            if( $(this).hasClass('opened') ){
                $(this).toggleClass('opened');
                $(this).find('.content').fadeToggle();
            }
            else {
                $(this).toggleClass('opened');
                $(this).find('.content').fadeToggle();
            }

        });

        $(".faq--mobile .roll-down-list .roll-down-item .label").click(function (e) {

            if( $(this).hasClass('opened') ){
                $(this).toggleClass('opened');
                $(this).next().fadeToggle();
            }
            else {
                $(this).toggleClass('opened');
                $(this).next().fadeToggle();
            }

        });

    }

    function init(){

        var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        if( windowWidth <= 1199 ) pageSubmenu.init();

        setClickEvents();

    }

    return{
        init : init
    };

}(jQuery);
var blogListPage = function($){


    function init(){

        $( ".input-search" ).focusin(function() {
            $( this ).parent().addClass('focused');
        });

        $( ".input-search" ).focusout(function() {
            $( this ).parent().removeClass('focused');
        });

    }

    return{
        init : init
    };
}(jQuery);
var cartPage = function($){

    function setClickEvents(){
        $(".selectbox--datepicker").click(function (e) {
            if( $(e.target).hasClass('input-datepicker') ) return;
            $(".input-datepicker").datepicker().focus();
        });

        $(".shadow-container .roll-down-arrow").click(function (e) {
            $('.roll-down').toggleClass('opened');
        });

        $(".billing-information .edit-icon").click(function (e) {
            var collumn =  $(this).closest('.col-6');

            //$(this).prev().attr('readonly', false);
            collumn.find('.text').toggleClass('hidden');
            collumn.find('input').toggleClass('editable');
            collumn.find('input').focus();
        });

        $(".billing-information input").focusout(function (e) {
            var collumn =  $(this).closest('.col-6');

            collumn.find('.text').removeClass('hidden');
            $(this).removeClass('editable');
            collumn.find('.text').text($(this).val());
            //$(this).attr('readonly', true);
        });



    }

    function initTextArea(){
        $(".textarea-char-count").keyup(function(){

            var charLength = this.value.length;
            if (charLength >= 1001) {
                this.value = this.value.substring(0, 1001);
            } else {
                $('.textarea-char-count + .char-count .char-used').text(charLength);
            }

        });
    }

    function init(){

        registrationPage.init();

        var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        if( windowWidth <= 1199 ) cartSubmenu.init();

        setClickEvents();

        initTextArea();

        $('.input-datepicker').datepicker(
            {
                language : 'sk',
                daysOfWeekHighlighted : [0]
            }
        );

    }

    return{
        init : init
    };

}(jQuery);
var elearningDetailPage = function($){


    function setClickEvents(){

        $(".button-plus-container").click(function () {

            var textContentHeight = $('.text-content').height();
            var textContainerHeight = $('.text-content-container').height();

            $('.text-content').css('height', textContentHeight);
            $('.text-content').css('max-height', 'none');

            setTimeout(function(){
                $('.text-content').css('height', textContainerHeight);
            }, 100);

            $('.text-content-overlay').fadeOut();
            $(this).fadeOut();

        });

        $(".contents-text .col-xl-4 h4").click(function () {

            $(".contents-text .contents").fadeToggle();

            $(this).toggleClass('opened');

        });

        $(document).click(function(event) {
            var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

            if(windowWidth <= 1199){
                var $target = $(event.target);
                if(!$target.closest('.contents-container').length && $('.contents-text .contents').is(":visible")) {
                    $('.contents-text .contents').fadeOut();
                    $('.contents-text .col-xl-4 h4').toggleClass('opened');
                }
            }
        });

    }

    function init(){

        setClickEvents();

    }

    return{
        init : init
    };
}(jQuery);
var filterPage = function($){

    var didScroll = '';
    var lastScrollTop = 0;
    var delta = 5;
    var navbarHeight = $('#site-header').outerHeight();

    function setScroll(){

        $(window).scroll(function(event){

            didScroll = true;

            if($(window).scrollTop() > 500){
                $('section.filter-fixed-header').fadeIn();
            }
            else{
                $('section.filter-fixed-header').fadeOut();
            }
        });

        setInterval(function() {
            if (didScroll) {
                hasScrolled();
                didScroll = false;
            }
        }, 250);
    }

    function hasScrolled() {
        var st = $(this).scrollTop();

        // Make sure they scroll more than delta
        if(Math.abs(lastScrollTop - st) <= delta)
            return;

        // If they scrolled down and are past the navbar, add class .nav-up.
        // This is necessary so you never see what is "behind" the navbar.
        if (st > lastScrollTop && st > navbarHeight){
            // Scroll Down
            $('#site-header').removeClass('nav-down').addClass('nav-up');
        } else {
            // Scroll Up
            if(st + $(window).height() < $(document).height()) {
                $('#site-header').removeClass('nav-up').addClass('nav-down');
            }
        }

        lastScrollTop = st;
    }

    function setClickEvents(){

        $("section.grant-radar .button-close--blue").click(function () {
            $('section.grant-radar').fadeOut();
        });

        $(".tag-search").click(function () {
            $(this).fadeOut();
        });

        $("section.filter-fixed-header").click(function () {
            scrollToElement.init('section.filter', 100);
        });
        $("section.filter .button-filter-open").click(function () {
            $('section.filter .filter-container').fadeToggle();
            $('section.filter .filter-selected').fadeToggle(100);

        });

    }

    function init(){

        if($(window).scrollTop() > 500){
            $('section.filter-fixed-header').fadeIn();
        }

        setScroll();

        setClickEvents();

    }

    return{
        init : init
    };
}(jQuery);
var grantDetailPage = function($){


    function carouselInit(){
        var tnsCarousel = tns({
            container: '.carousel-suppliers',
            items: 1,
            slideBy: 'page',
            autoplay: false,
            nav: false,
            navPosition: 'bottom',
            mouseDrag: true,
            controls: true,
            nextButton: '#suppliers-carousel-next',
            prevButton: '#suppliers-carousel-prev',
            autoplayButtonOutput: false,
            gutter: 0,
            speed: 900,
            fixedWidth: 245,
            responsive: {
                1200: {
                    items: 4
                },
                992: {
                    items: 3,
                },
                768: {
                    items: 2
                },
                576: {
                    items: 2,
                    fixedWidth: false
                }
            }
        });
    }

    function init(){
        carouselInit();
    }

    return{
        init : init
    };
}(jQuery);
var grantEducationPage = function($){

    function init(){
        var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        if( windowWidth <= 1199 ) pageSubmenu.init();

        $("section.offer-closable .button-close--blue").click(function () {
            $('section.offer-closable').fadeOut();
        });

        rollDownList.init();

        educationCarousel.init();
        blogCarousel.init();

    }

    return{
        init : init
    };

}(jQuery);
var grantJobsPage = function($){

    function setClickEvents(){


    }

    function init(){

        var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        if( windowWidth <= 1199 ) pageSubmenu.init();

        setClickEvents();

        $("section.offer-closable .button-close--blue").click(function () {
            $('section.offer-closable').fadeOut();
        });

    }

    return{
        init : init
    };

}(jQuery);
var grantSearchPage = function($){


    function carouselInit(){
        var tnsCarousel = tns({
            container: '.carousel-suppliers',
            items: 1,
            slideBy: 'page',
            autoplay: false,
            nav: false,
            navPosition: 'bottom',
            mouseDrag: true,
            controls: true,
            nextButton: '#suppliers-carousel-next',
            prevButton: '#suppliers-carousel-prev',
            autoplayButtonOutput: false,
            gutter: 0,
            speed: 900,
            fixedWidth: 245,
            responsive: {
                1200: {
                    items: 4
                },
                992: {
                    items: 3,
                },
                768: {
                    items: 2
                },
                576: {
                    items: 2,
                    fixedWidth: false
                }
            }
        });

        blogCarousel.init();

    }

    function init(){
        carouselInit();

        var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        if( windowWidth <= 1199 ) pageSubmenu.init();

    }

    return{
        init : init
    };
}(jQuery);
var homePage = function($){

    function init(){
        blogCarousel.init();
        educationCarousel.init();

        $( ".input-call-to-action input" ).focusin(function() {
            console.log('focusin');
            if( $(this).val() == '' ) $(this).parent().toggleClass('stop-call-to-action');
            $(this).parent().parent().toggleClass('focused');
        });
        $( ".input-call-to-action input" ).focusout(function() {
            console.log('focusout');
            if( $(this).val() == '' ) $(this).parent().toggleClass('stop-call-to-action');
            $(this).parent().parent().toggleClass('focused');
        });

        $("section.preface .button-scroll").click(function () {
            scrollToElement.init('section.grant-database', 100);
        });

    }

    return{
        init : init
    };
}(jQuery);
var howWeWorksPage = function($){

    function init(){


        $(".diagram-container .answer .button-no").click(function () {
            var answer = $(this).closest('.answer');
            if( !answer.hasClass('answer-answered') ) {
                answer.find('.diagram-arrow').removeClass('diagram-arrow--hidden');

                var dataNext = $(this).attr('data-next');
                dataNext = dataNext.replace(" ", ", .");

                $('.' + dataNext).removeClass('answer-hidden');
                $(this).closest('.answer').addClass('answer-answered');

                var dataDismiss = $(this).attr('data-dismiss');
                $('.' + dataDismiss).addClass('answer-hidden');
            }
        });

        $(".diagram-container .answer .button-yes").click(function () {
            var answer = $(this).closest('.answer');
            if( !answer.hasClass('answer-answered') ) {
                answer.find('.diagram-arrow').removeClass('diagram-arrow--hidden');

                var dataNext = $(this).attr('data-next');
                dataNext = dataNext.replace(" ", " .");

                $('.' + dataNext).removeClass('answer-hidden');
                $(this).closest('.answer').addClass('answer-answered');

                var dataDismiss = $(this).attr('data-dismiss');
                $('.' + dataDismiss).addClass('answer-hidden');
            }
        });

    }

    return{
        init : init
    };
}(jQuery);
var registrationPage = function($){


    function init(){


        $("section.billing-information").click(function (){
            $("section.billing-information").addClass('active');
            $(".container-780 .button-center").addClass('active');
            $("section.company-name").fadeOut();

        });


        $( 'section.company-name input[type="text"]').keypress(function() {
            $("section.billing-information").addClass('active');
            $(".container-780 .button-center").addClass('active');
        });

        $( 'section.company-name input[type="text"]').focusin(function() {

            $(this).closest('.card-animation').toggleClass('focused');
        });
        $( 'section.company-name input[type="text"]' ).focusout(function() {

            $(this).closest('.card-animation').toggleClass('focused');
        });

    }

    return{
        init : init
    };
}(jQuery);
var servicesPage = function($){

    function initCallToAction(){

        $( ".input-call-to-action input" ).focusin(function() {
            console.log('focusin');
            if( $(this).val() == '' ) $(this).parent().toggleClass('stop-call-to-action');
            $(this).parent().parent().toggleClass('focused');
        });

        $( ".input-call-to-action input" ).focusout(function() {
            console.log('focusout');
            if( $(this).val() == '' ) $(this).parent().toggleClass('stop-call-to-action');
            $(this).parent().parent().toggleClass('focused');
        });

    }



    function init(){
        var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        if( windowWidth <= 1199 ) pageSubmenu.init();

        initCallToAction();

        rollDownList.init();

        $("section.offer-closable .button-close--blue").click(function () {
            $('section.offer-closable').fadeOut();
        });

    }

    return{
        init : init
    };

}(jQuery);

(function($){

    defaultPage.init();

    switch(defaultPage.getBodyClass('page')){
        case 'home':
            homePage.init();
            break;
        case 'page-grant-database':
            filterPage.init();
            break;
        case 'page-grant-detail':
            grantDetailPage.init();
            break;
        case 'page-registration':
            registrationPage.init();
            break;
        case 'page-404':
            page404.init();
            break;
        default:
    }

    var bodyClasses = $('body').attr('class');

    if( bodyClasses.includes('page-e-learning-detail') ) elearningDetailPage.init();
    if( bodyClasses.includes('page-blog-list') ) blogListPage.init();
    if( bodyClasses.includes('page-job-offer') ) filterPage.init();
    if( bodyClasses.includes('page-expert-database') ) filterPage.init();
    if( bodyClasses.includes('page-how-we-works') ) howWeWorksPage.init();

    $(document).ready(function() {

        switch(defaultPage.getBodyClass('page')){

            case 'page-grant-search':
                grantSearchPage.init();
                break;
            case 'page-services':
                servicesPage.init();
                break;
            case 'page-grant-education':
                grantEducationPage.init();
                break;
            case 'page-about-us':
                aboutUsPage.init();
                break;
            case 'page-grant-jobs':
                grantJobsPage.init();
                break;
            case 'page-cart':
                cartPage.init();
                break;

            default:
        }

    });

})(jQuery);






