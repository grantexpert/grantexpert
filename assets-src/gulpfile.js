var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass')(require('sass'));
var browserSync = require('browser-sync').create();

const autoprefixer = require('gulp-autoprefixer');


gulp.task('sass', function() {
    return gulp.src('./css/*.scss')
        .pipe(sass.sync({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 version', 'chrome 4', 'ie 8', 'ie 9', 'safari 4', 'firefox 5', 'android 4', 'opera 11']
        }))
        .pipe(gulp.dest('../assets/css'))
        .pipe(browserSync.stream());
});

gulp.task('js', function() {
    return gulp.src([
        './js/components/*.js', './js/pages/*.js', './js/main.js'])
        .pipe(concat('main.js'))
        .pipe(gulp.dest('../assets/js'))
        .pipe(browserSync.stream());
});



gulp.task('server', function (done) {

    browserSync.init({
        proxy: 'http://grantexpert.test'
    });

    gulp.watch(['./css/*.scss', './css/**/*.scss', 'css/**/**/*.scss'], gulp.series('sass'));

    gulp.watch([
        './js/*.js',
        './js/pages/*.js',
        './js/components/*.js'
    ], gulp.series('js'));

    gulp.watch(['../*.php', '../_components/*.php', '../page-templates/*.php', '../bs-post-category-list/*.php', '../woocommerce/*.php'])
        .on('change', browserSync.reload);

    done();
});




gulp.task('default', gulp.series('sass', 'js', 'server'));
