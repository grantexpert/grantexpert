var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var browserSync = require('browser-sync').create();
var errorNotifier = require('gulp-error-notifier');

const autoprefixer = require('gulp-autoprefixer');

var plumberErrorHandler = { errorHandler: notify.onError({
    title: 'Gulp',
    message: 'Error: <%= error.message %>'
  })
};

gulp.task('sass', function () {
    var sassStream;

    sassStream = gulp.src('./css/*.scss')
        .pipe(errorNotifier.handleError(sass()))
        .pipe(autoprefixer({
            browsers: ['last 2 version', 'chrome 4', 'ie 8', 'ie 9', 'safari 4', 'firefox 5', 'android 4', 'opera 11']
        }))
        .pipe(gulp.dest('../assets/css'))
        .pipe(browserSync.stream());

        //.pipe(plumber(plumberErrorHandler))
});

gulp.task('js', function () {

gulp.src(['./js/components/*.js', './js/pages/*.js', './js/main.js'])
    .pipe(concat('main.js'))
    .pipe(gulp.dest('../assets/js'))
    .pipe(browserSync.stream());
});

gulp.task('watch', function() {

    browserSync.init({
        proxy: 'http://wp-bitbucket.local'
    });

    gulp.watch(['css/*.scss', 'css/**/*.scss'], ['sass']);

    gulp.watch(['./js/*.js', './js/pages/*.js', './js/components/*.js'], ['js']);

    gulp.watch(['../*.php', '../includes/*.php', '../page-templates/*.php', '../bs-post-category-list/*.php', '../woocommerce/*.php'])
        .on('change', browserSync.reload);
});

gulp.task('default', ['sass', 'js', 'watch']);
