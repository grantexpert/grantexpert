var blogListPage = function($){


    function init(){

        $( ".input-search" ).focusin(function() {
            $( this ).parent().addClass('focused');
        });

        $( ".input-search" ).focusout(function() {
            $( this ).parent().removeClass('focused');
        });

    }

    return{
        init : init
    };
}(jQuery);