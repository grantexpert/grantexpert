var defaultPage = function($){
    
    function getBodyClass(prefix){
        var bodyClass= "";
        if($('body').hasClass('home')) bodyClass='home';
        else{
            bodyClasses= $('body').attr('class');
            var regExpression = new RegExp(prefix + '-[^ ]*', 'g');
            if(bodyClasses.match(regExpression) != null){
                var matchedClasses = bodyClasses.match(regExpression);

                matchedClasses.forEach(function(entry) {
                    if(!entry.includes('page-template') && !entry.includes('page-id') && entry != 'page') bodyClass = entry;
                });
            }

        }

        return bodyClass;
    };

    function setSiteContentMinHeight(){
        var windowHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        var siteContentMinHeight = windowHeight - $('#site-header').height() - $('#site-footer').height();
        
        $('#site-content').css('min-height', siteContentMinHeight + 'px');
    }

    function init(){
        menuMain.init();
        menuMobile.init();
        menuFixedBottom.init();

        form.init();

        modalLogin.init();



        //setSiteContentMinHeight();


        $("#site-footer .button-scroll-top").click(function () {
            scrollToElement.init('body', 100);
        });

        $(".menu-fixed-bottom .button-close").click(function () {
            $(".menu-fixed-bottom").fadeOut();
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        $( ".page-submenu li" ).click(function() {
            var hrefURL = $(this).find('a').attr('href');
            window.location.href = hrefURL;
        });




    };

    return{
        init : init,
        getBodyClass : getBodyClass
    };

}(jQuery);
