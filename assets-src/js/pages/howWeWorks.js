var howWeWorksPage = function($){

    function init(){


        $(".diagram-container .answer .button-no").click(function () {
            var answer = $(this).closest('.answer');
            if( !answer.hasClass('answer-answered') ) {
                answer.find('.diagram-arrow').removeClass('diagram-arrow--hidden');

                var dataNext = $(this).attr('data-next');
                dataNext = dataNext.replace(" ", ", .");

                $('.' + dataNext).removeClass('answer-hidden');
                $(this).closest('.answer').addClass('answer-answered');

                var dataDismiss = $(this).attr('data-dismiss');
                $('.' + dataDismiss).addClass('answer-hidden');
            }
        });

        $(".diagram-container .answer .button-yes").click(function () {
            var answer = $(this).closest('.answer');
            if( !answer.hasClass('answer-answered') ) {
                answer.find('.diagram-arrow').removeClass('diagram-arrow--hidden');

                var dataNext = $(this).attr('data-next');
                dataNext = dataNext.replace(" ", " .");

                $('.' + dataNext).removeClass('answer-hidden');
                $(this).closest('.answer').addClass('answer-answered');

                var dataDismiss = $(this).attr('data-dismiss');
                $('.' + dataDismiss).addClass('answer-hidden');
            }
        });

    }

    return{
        init : init
    };
}(jQuery);