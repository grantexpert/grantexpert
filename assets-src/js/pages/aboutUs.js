var aboutUsPage = function($){

    function setClickEvents(){
        $(".page-who-we-are section.who-we-are .arrow-link").click(function (e) {
            e.preventDefault();
            $(this).next().fadeToggle();

        });

        $("section.offer-closable .button-close--blue").click(function () {
            $('section.offer-closable').fadeOut();
        });

        $(".faq--desktop .roll-down-list .roll-down-item").click(function (e) {

            if( $(this).hasClass('opened') ){
                $(this).toggleClass('opened');
                $(this).find('.content').fadeToggle();
            }
            else {
                $(this).toggleClass('opened');
                $(this).find('.content').fadeToggle();
            }

        });

        $(".faq--mobile .roll-down-list .roll-down-item .label").click(function (e) {

            if( $(this).hasClass('opened') ){
                $(this).toggleClass('opened');
                $(this).next().fadeToggle();
            }
            else {
                $(this).toggleClass('opened');
                $(this).next().fadeToggle();
            }

        });

    }

    function init(){

        var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        if( windowWidth <= 1199 ) pageSubmenu.init();

        setClickEvents();

    }

    return{
        init : init
    };

}(jQuery);