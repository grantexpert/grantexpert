var registrationPage = function($){


    function init(){


        $("section.billing-information").click(function (){
            $("section.billing-information").addClass('active');
            $(".container-780 .button-center").addClass('active');
            $("section.company-name").fadeOut();

        });


        $( 'section.company-name input[type="text"]').keypress(function() {
            $("section.billing-information").addClass('active');
            $(".container-780 .button-center").addClass('active');
        });

        $( 'section.company-name input[type="text"]').focusin(function() {

            $(this).closest('.card-animation').toggleClass('focused');
        });
        $( 'section.company-name input[type="text"]' ).focusout(function() {

            $(this).closest('.card-animation').toggleClass('focused');
        });

    }

    return{
        init : init
    };
}(jQuery);