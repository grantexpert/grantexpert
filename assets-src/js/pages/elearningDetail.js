var elearningDetailPage = function($){


    function setClickEvents(){

        $(".button-plus-container").click(function () {

            var textContentHeight = $('.text-content').height();
            var textContainerHeight = $('.text-content-container').height();

            $('.text-content').css('height', textContentHeight);
            $('.text-content').css('max-height', 'none');

            setTimeout(function(){
                $('.text-content').css('height', textContainerHeight);
            }, 100);

            $('.text-content-overlay').fadeOut();
            $(this).fadeOut();

        });

        $(".contents-text .col-xl-4 h4").click(function () {

            $(".contents-text .contents").fadeToggle();

            $(this).toggleClass('opened');

        });

        $(document).click(function(event) {
            var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

            if(windowWidth <= 1199){
                var $target = $(event.target);
                if(!$target.closest('.contents-container').length && $('.contents-text .contents').is(":visible")) {
                    $('.contents-text .contents').fadeOut();
                    $('.contents-text .col-xl-4 h4').toggleClass('opened');
                }
            }
        });

    }

    function init(){

        setClickEvents();

    }

    return{
        init : init
    };
}(jQuery);