var grantEducationPage = function($){

    function init(){
        var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        if( windowWidth <= 1199 ) pageSubmenu.init();

        $("section.offer-closable .button-close--blue").click(function () {
            $('section.offer-closable').fadeOut();
        });

        rollDownList.init();

        educationCarousel.init();
        blogCarousel.init();

    }

    return{
        init : init
    };

}(jQuery);