var homePage = function($){

    function init(){
        blogCarousel.init();
        educationCarousel.init();

        $( ".input-call-to-action input" ).focusin(function() {
            console.log('focusin');
            if( $(this).val() == '' ) $(this).parent().toggleClass('stop-call-to-action');
            $(this).parent().parent().toggleClass('focused');
        });
        $( ".input-call-to-action input" ).focusout(function() {
            console.log('focusout');
            if( $(this).val() == '' ) $(this).parent().toggleClass('stop-call-to-action');
            $(this).parent().parent().toggleClass('focused');
        });

        $("section.preface .button-scroll").click(function () {
            scrollToElement.init('section.grant-database', 100);
        });

    }

    return{
        init : init
    };
}(jQuery);