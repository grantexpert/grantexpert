var filterPage = function($){

    var didScroll = '';
    var lastScrollTop = 0;
    var delta = 5;
    var navbarHeight = $('#site-header').outerHeight();

    function setScroll(){

        $(window).scroll(function(event){

            didScroll = true;

            if($(window).scrollTop() > 500){
                $('section.filter-fixed-header').fadeIn();
            }
            else{
                $('section.filter-fixed-header').fadeOut();
            }
        });

        setInterval(function() {
            if (didScroll) {
                hasScrolled();
                didScroll = false;
            }
        }, 250);
    }

    function hasScrolled() {
        var st = $(this).scrollTop();

        // Make sure they scroll more than delta
        if(Math.abs(lastScrollTop - st) <= delta)
            return;

        // If they scrolled down and are past the navbar, add class .nav-up.
        // This is necessary so you never see what is "behind" the navbar.
        if (st > lastScrollTop && st > navbarHeight){
            // Scroll Down
            $('#site-header').removeClass('nav-down').addClass('nav-up');
        } else {
            // Scroll Up
            if(st + $(window).height() < $(document).height()) {
                $('#site-header').removeClass('nav-up').addClass('nav-down');
            }
        }

        lastScrollTop = st;
    }

    function setClickEvents(){

        $("section.grant-radar .button-close--blue").click(function () {
            $('section.grant-radar').fadeOut();
        });

        $(".tag-search").click(function () {
            $(this).fadeOut();
        });

        $("section.filter-fixed-header").click(function () {
            scrollToElement.init('section.filter', 100);
        });
        $("section.filter .button-filter-open").click(function () {
            $('section.filter .filter-container').fadeToggle();
            $('section.filter .filter-selected').fadeToggle(100);

        });

    }

    function init(){

        if($(window).scrollTop() > 500){
            $('section.filter-fixed-header').fadeIn();
        }

        setScroll();

        setClickEvents();

    }

    return{
        init : init
    };
}(jQuery);