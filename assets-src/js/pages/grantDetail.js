var grantDetailPage = function($){


    function carouselInit(){
        var tnsCarousel = tns({
            container: '.carousel-suppliers',
            items: 1,
            slideBy: 'page',
            autoplay: false,
            nav: false,
            navPosition: 'bottom',
            mouseDrag: true,
            controls: true,
            nextButton: '#suppliers-carousel-next',
            prevButton: '#suppliers-carousel-prev',
            autoplayButtonOutput: false,
            gutter: 0,
            speed: 900,
            fixedWidth: 245,
            responsive: {
                1200: {
                    items: 4
                },
                992: {
                    items: 3,
                },
                768: {
                    items: 2
                },
                576: {
                    items: 2,
                    fixedWidth: false
                }
            }
        });
    }

    function init(){
        carouselInit();
    }

    return{
        init : init
    };
}(jQuery);