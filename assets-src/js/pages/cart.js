var cartPage = function($){

    function setClickEvents(){
        $(".selectbox--datepicker").click(function (e) {
            if( $(e.target).hasClass('input-datepicker') ) return;
            $(".input-datepicker").datepicker().focus();
        });

        $(".shadow-container .roll-down-arrow").click(function (e) {
            $('.roll-down').toggleClass('opened');
        });

        $(".billing-information .edit-icon").click(function (e) {
            var collumn =  $(this).closest('.col-6');

            //$(this).prev().attr('readonly', false);
            collumn.find('.text').toggleClass('hidden');
            collumn.find('input').toggleClass('editable');
            collumn.find('input').focus();
        });

        $(".billing-information input").focusout(function (e) {
            var collumn =  $(this).closest('.col-6');

            collumn.find('.text').removeClass('hidden');
            $(this).removeClass('editable');
            collumn.find('.text').text($(this).val());
            //$(this).attr('readonly', true);
        });



    }

    function initTextArea(){
        $(".textarea-char-count").keyup(function(){

            var charLength = this.value.length;
            if (charLength >= 1001) {
                this.value = this.value.substring(0, 1001);
            } else {
                $('.textarea-char-count + .char-count .char-used').text(charLength);
            }

        });
    }

    function init(){

        registrationPage.init();

        var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        if( windowWidth <= 1199 ) cartSubmenu.init();

        setClickEvents();

        initTextArea();

        $('.input-datepicker').datepicker(
            {
                language : 'sk',
                daysOfWeekHighlighted : [0]
            }
        );

    }

    return{
        init : init
    };

}(jQuery);