var grantJobsPage = function($){

    function setClickEvents(){


    }

    function init(){

        var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        if( windowWidth <= 1199 ) pageSubmenu.init();

        setClickEvents();

        $("section.offer-closable .button-close--blue").click(function () {
            $('section.offer-closable').fadeOut();
        });

    }

    return{
        init : init
    };

}(jQuery);