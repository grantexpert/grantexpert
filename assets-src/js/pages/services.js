var servicesPage = function($){

    function initCallToAction(){

        $( ".input-call-to-action input" ).focusin(function() {
            console.log('focusin');
            if( $(this).val() == '' ) $(this).parent().toggleClass('stop-call-to-action');
            $(this).parent().parent().toggleClass('focused');
        });

        $( ".input-call-to-action input" ).focusout(function() {
            console.log('focusout');
            if( $(this).val() == '' ) $(this).parent().toggleClass('stop-call-to-action');
            $(this).parent().parent().toggleClass('focused');
        });

    }



    function init(){
        var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        if( windowWidth <= 1199 ) pageSubmenu.init();

        initCallToAction();

        rollDownList.init();

        $("section.offer-closable .button-close--blue").click(function () {
            $('section.offer-closable').fadeOut();
        });

    }

    return{
        init : init
    };

}(jQuery);