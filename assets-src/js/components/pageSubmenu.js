var pageSubmenu = function($){

    /*function scrollToSelectedSubpage(){

        var leftPosition = $(".page-submenu ul .active").position().left;
        var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        var elementWidth = $(".page-submenu ul .active").width();

        console.log(elementWidth);

        $(".page-submenu ul").scrollLeft( leftPosition - ((windowWidth / 2) - ((elementWidth + 60) / 2)) );

    }*/

    function scrollToSelectedSubpage(){

        var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        var border = 4;
        var marginPadding = 60;
        if( windowWidth <= 767 ){
            marginPadding = 40;
            border = 0;
        }


        var leftElementWidth = $(".page-submenu ul li:nth-of-type(1)").width() + marginPadding + border;
        var marginLeft = (windowWidth / 2) - (leftElementWidth / 2) + 10;
        $(".page-submenu ul li:nth-of-type(1)").css('margin-left', marginLeft);

        var rightElementWidth = $(".page-submenu ul li:nth-last-of-type(1)").width() + marginPadding + border;
        var marginRight = (windowWidth / 2) - (rightElementWidth / 2) + 10;
        $(".page-submenu ul li:nth-last-of-type(1)").css('margin-right', marginRight);


        var elementWidth = $(".page-submenu ul .active").width();
        var leftPosition = $(".page-submenu ul .active").position().left;


        console.log(elementWidth);

        $(".page-submenu ul").animate( { scrollLeft: leftPosition - ((windowWidth / 2) - ((elementWidth + marginPadding) / 2)) }, 100);

        //$(".page-submenu ul").scrollLeft(leftPosition - ((windowWidth / 2) - ((elementWidth + marginPadding) / 2)));


    }

    function init(){

        setTimeout(function() { scrollToSelectedSubpage() }, 500);
    }

    return{
        init : init
    };

}(jQuery);