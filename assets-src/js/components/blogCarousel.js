var blogCarousel = function($){

    function init(){

        if( $('.carousel-blog').length ){

            var blogCarousel = tns({
                container: '.carousel-blog',
                items: 3,
                slideBy: 'page',
                autoplay: false,
                nav: false,
                navPosition: 'bottom',
                mouseDrag: false,
                touch: false,
                controls: true,
                nextButton: '#blog-carousel-next',
                prevButton: '#blog-carousel-prev',
                autoplayButtonOutput: false,
                gutter: 30,
                speed: 900,
                disable: true,
                responsive: {
                    1200: {
                        items: 3,
                        freezable: true,
                        touch: true
                    },
                    768: {
                        items: 2,
                        mouseDrag: false,
                        touch: false,
                        disable: false
                    }
                }
            });

        }

    }

    return{
        init : init
    };

}(jQuery);