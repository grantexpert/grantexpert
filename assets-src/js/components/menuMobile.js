var menuMobile = function($){
    var windowWidth = '';

    function setClickableResponsiveMenu(){
        $('.menu-mobile li.dropdown a.dropdown-toggle').removeClass('disabled');

        /*if (windowWidth <= 991) $('.menu-mobile li.dropdown a.dropdown-toggle').removeClass('disabled');
        else $('.menu-mobile li.dropdown a.dropdown-toggle').addClass('disabled');*/
    }

    function setClickEvents(){
        $(".menu-mobile .open-icon").click(function () {

            $('.menu-mobile .window-container').fadeIn(400);
            $('.menu-mobile .open-icon').fadeOut(200);
            $('body').toggleClass('overflow-y-hidden');
            $('.menu-mobile .window-container > .col-container').toggleClass('ejected');

        });

        $(".menu-mobile .close-icon").click(function () {
            $('.menu-mobile .open-icon').fadeIn(400);
            $('.menu-mobile .window-container').fadeOut(400);
            $('.menu-mobile .open-icon').removeClass('hide');
            $('body').toggleClass('overflow-y-hidden');
            $('.menu-mobile .window-container > .col-container').toggleClass('ejected');
        });

        $(".menu-mobile li.dropdown > a").click(function () {
            if( $(this).hasClass('opened') ){

                $(this).toggleClass('opened');
                $(this).next().fadeToggle();

            }
            else {

                $(".menu-mobile li.dropdown > a").removeClass('opened');
                $('.menu-mobile .dropdown-menu').hide();

                $(this).toggleClass('opened');
                $(this).next().fadeToggle();

            }

        });
    }

    function init(){
        windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

        setClickableResponsiveMenu();
        setClickEvents();

    }

    return{
        init : init
    };

}(jQuery);