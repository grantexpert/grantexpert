var rollDownList = function($){


    function init(){

        $(".roll-down-list .roll-down-item").click(function (e) {

            if( $(e.target).hasClass('col-info') ) return;
            if( $(e.target).closest('.col-info').length ) return;

            if( $(this).hasClass('opened') ){

                $(this).toggleClass('opened');
                $(this).find('.content').fadeToggle();

            }
            else {

                /*$(".roll-down-list .roll-down-item").removeClass('opened');
                $('.roll-down-list .content').hide();*/

                $(this).toggleClass('opened');
                $(this).find('.content').fadeToggle();

            }

        });

    }

    return{
        init : init
    };

}(jQuery);