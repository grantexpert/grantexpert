var menuMain = function($){
    var windowWidth = '';



    function setClickEvents(){
        $(".menu-main .icon").click(function () {
            $(".navbar-collapse").fadeToggle(500);
            $(".top-menu").toggleClass("top-animate");
            $(".mid-menu").toggleClass("mid-animate");
            $(".bottom-menu").toggleClass("bottom-animate");
            $('body').toggleClass('overflow-y-hidden');
        });
    }

    function init(){

        $( ".menu-main .button-tertiary" ).hover(
            function() {
                /*$(".menu-main .button-tertiary #line-1").attr("x1","32");
                $(".menu-main .button-tertiary #line-1").attr("x2","38");*/
                /*#line-1{
                    x1: 32px;
                    x2: 38px;
                }*/
            }, function() {
                /*$(".menu-main .button-tertiary #line-1").attr("x1","29.5");
                $(".menu-main .button-tertiary #line-1").attr("x2","42.5");*/
            }
        );


        menuOnScroll.init();
    }

    return{
        init : init
    };
    
}(jQuery);