var educationCarousel = function($){
     var educationCarousel = '';

    function init(){

        if( $('.carousel-grant-education').length ) {

            educationCarousel = tns({
                container: '.carousel-grant-education',
                items: 1,
                slideBy: 'page',
                autoplay: false,
                nav: false,
                navPosition: 'bottom',
                mouseDrag: true,
                controls: true,
                nextButton: '#education-carousel-next',
                prevButton: '#education-carousel-prev',
                autoplayButtonOutput: false,
                gutter: 15,
                speed: 900,
                fixedWidth: 245,
                preventScrollOnTouch: 'auto',
                /*autoWidth: false*/

                responsive: {
                    1200: {
                        items: 3,
                        /*fixedWidth: false*/
                        /*autoWidth: false*/
                    },
                    768: {
                        items: 2,
                        gutter: 30,
                        /*fixedWidth: false*/
                        /*autoWidth: false*/
                    },
                    576: {
                        items: 2,
                        gutter: 15,
                        fixedWidth: false,
                        /*autoWidth: false*/
                    }
                }
            });

            /*setTimeout(function() {
                educationCarousel.destroy();
                educationCarousel = educationCarousel.rebuild();
                console.log('rebuilded');

            }, 5000);*/

            /*$(window).on('resize', function(){
                console.log('rebuilded by resize');
                var educationTitle = $('section.grant-education h2').text();
                $('section.grant-education h2').text( educationTitle + '.')

                educationCarousel.destroy();
                educationCarousel = educationCarousel.rebuild();
                console.log('end rebuilded');

            });*/

            

        }

    }

    return{
        init : init
    };

}(jQuery);