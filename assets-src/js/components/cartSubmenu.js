var cartSubmenu = function($){


    function scrollToSelectedSubpage(){

        var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        var border = 4;
        var marginPadding = 60;
        if( windowWidth <= 767 ){
            marginPadding = 40;
            border = 0;
        }


        var leftElementWidth = $(".cart-submenu ul li:nth-of-type(1)").width() + marginPadding + border;
        var marginLeft = (windowWidth / 2) - (leftElementWidth / 2) + 10;
        $(".cart-submenu ul li:nth-of-type(1)").css('margin-left', marginLeft);

        var rightElementWidth = $(".cart-submenu ul li:nth-last-of-type(1)").width() + marginPadding + border;
        var marginRight = (windowWidth / 2) - (rightElementWidth / 2) + 10;
        $(".cart-submenu ul li:nth-last-of-type(1)").css('margin-right', marginRight);


        var elementWidth = $(".cart-submenu ul .active").width();
        var leftPosition = $(".cart-submenu ul .active").position().left;


        $(".cart-submenu ul").animate( { scrollLeft: leftPosition - ((windowWidth / 2) - ((elementWidth + marginPadding) / 2)) }, 100);


    }

    function init(){
        setTimeout(function() { scrollToSelectedSubpage() }, 500);
    }

    return{
        init : init
    };

}(jQuery);