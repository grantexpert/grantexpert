var scrollToElement = function($){
  
    function init(element, duration){
        $('html, body').animate({ scrollTop: $(element).offset().top }, duration);
        return false;
    }

    return{
        init : init
    };
    
}(jQuery);

