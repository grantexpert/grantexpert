var menuFixedBottom = function($){

    function getScrollbarWidth() {

        // Creating invisible container
        const outer = document.createElement('div');
        outer.style.visibility = 'hidden';
        outer.style.overflow = 'scroll'; // forcing scrollbar to appear
        outer.style.msOverflowStyle = 'scrollbar'; // needed for WinJS apps
        document.body.appendChild(outer);

        // Creating inner element and placing it in the container
        const inner = document.createElement('div');
        outer.appendChild(inner);

        // Calculating difference between container's full width and the child width
        const scrollbarWidth = (outer.offsetWidth - inner.offsetWidth);

        // Removing temporary elements from the DOM
        outer.parentNode.removeChild(outer);

        return scrollbarWidth;

    }

    function init(){

        //#modal-eligibility, #modal-grant-radar
        $('.modal').on('show.bs.modal', function (event) {
            var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

            if( windowWidth >= 992 ){
                var scrollWidth = getScrollbarWidth();
                $('.menu-fixed-bottom').css('margin-right', scrollWidth);
            }

        });

        //#modal-eligibility, #modal-grant-radar
        $('.modal').on('hidden.bs.modal', function (event) {

            $('.menu-fixed-bottom').css('margin-right', 0);

        });

        $(".modal-menu-fixed .page-1 .button-tertiary").click(function () {
            $(this).closest('.modal').find('.page-1').hide();
            $(this).closest('.modal').find('.page-2').show();
            $(this).closest('.modal').addClass('page-2-selected');
        });

        $(".modal-menu-fixed .page-2 .button-tertiary").click(function () {
            $(this).closest('.modal').find('.page-2').hide();
            $(this).closest('.modal').find('.page-3').show();
            $(this).closest('.modal').removeClass('page-2-selected');
            $(this).closest('.modal').addClass('page-3-selected');
        });
        $("#modal-eligibility .page-3 .button-tertiary").click(function () {
            $(this).closest('.modal').find('.page-3').hide();
            $(this).closest('.modal').find('.page-4').show();
            $(this).closest('.modal').removeClass('page-3-selected');
            $(this).closest('.modal').addClass('page-4-selected');
        });

    }

    return{
        init : init
    };

}(jQuery);