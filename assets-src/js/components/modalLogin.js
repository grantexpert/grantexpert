var modalLogin = function($){


    function init(){

        $("#modal-login .page-1 .login-button").click(function () {
            $(this).closest('.modal').find('.page-1').hide();
            $(this).closest('.modal').find('.page-2').show();
            $(this).closest('.modal').addClass('page-2-selected');
        });
    }

    return{
        init : init
    };

}(jQuery);