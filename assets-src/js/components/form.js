var form = function($){


    function initInputRequired(){

        $(".input-required input").focusout(function(){

            var inputContainer = $(this).closest('.input-required');

            if($(this).val() == '') inputContainer.removeClass('input-required--filled');
            else inputContainer.addClass('input-required--filled');
        });

        $(".input-required input").focusin(function(){

            $(this).closest('.input-required').addClass('input-required--filled');

        });

    }

    function initCheckbox(){

        $(".checkbox .label").click(function () {
            var checkbox = $(this).prev();
            $(this).parent().toggleClass('checked');
            if ( checkbox.is(':checked') ) checkbox.prop('checked', false);
            else checkbox.prop('checked', true);

        });

        $(".col-select .select-all").click(function () {
            var modalWindow = $(this).closest('.modal');
            modalWindow.find('.checkbox').addClass('checked');
            modalWindow.find('.checkbox input').prop('checked', true);

        });
        $(".col-select .deselect-all").click(function () {
            var modalWindow = $(this).closest('.modal');
            modalWindow.find('.checkbox').removeClass('checked');
            modalWindow.find('.checkbox input').prop('checked', false);

        });

    }

    function initSelectbox(){
        $(".selectbox .selectbox-label").click(function () {
            $('.selectbox ul').not( $(this).next() ).fadeOut();
            $(this).next().fadeToggle();
        });

        $(".selectbox ul li").click(function () {

            var selectLabel = $(this).html();
            var selectValue = $(this).attr('data-value');
            var customSelectbox = $(this).closest('.selectbox');

            customSelectbox.find('.selectbox-label').html(selectLabel);
            customSelectbox.find('ul').fadeOut();
            customSelectbox.find('input').val(selectValue);

        });

        $(document).click(function(event) {
            var $target = $(event.target);
            if(!$target.closest('.selectbox').length &&
                $('.selectbox ul').is(":visible")) {
                $('.selectbox ul').fadeOut();
            }
        });
    }

    function init(){

        initInputRequired();
        initCheckbox();
        initSelectbox();

    }

    return{
        init : init
    };
}(jQuery);