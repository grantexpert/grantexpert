var menuOnScroll = function($){
  	
  	var didScroll = '';
    var lastScrollTop = 0;
    var delta = 5;
    var navbarHeight = $('#site-header').outerHeight();

  	function setScroll(){

        $(window).scroll(function(event){
        	
            didScroll = true;
            if($(window).scrollTop() > 0){
                //$('#site-header').addClass('site-header--shadow_mobile');
            }
            else{
                //$('#site-header').removeClass('site-header--shadow_mobile');
            }
            if($(window).scrollTop() > 41){
                $('#site-header').addClass('site-header--small_mobile');
            }
            else{
                $('#site-header').removeClass('site-header--small_mobile');
            }
        });

        setInterval(function() {
            if (didScroll) {
                hasScrolled();
                didScroll = false;
            }
        }, 250);
    }

    function hasScrolled() {
        var st = $(this).scrollTop();
        
        // Make sure they scroll more than delta
        if(Math.abs(lastScrollTop - st) <= delta)
            return;
        
        // If they scrolled down and are past the navbar, add class .nav-up.
        // This is necessary so you never see what is "behind" the navbar.
        if (st > lastScrollTop && st > navbarHeight){
            // Scroll Down
            $('#site-header').removeClass('nav-down').addClass('nav-up');
        } else {
            // Scroll Up
            if(st + $(window).height() < $(document).height()) {
                $('#site-header').removeClass('nav-up').addClass('nav-down');
            }
        }
        
        lastScrollTop = st;
    }

    function init(){

    	if($(window).scrollTop() > 0){
            //$('#site-header').addClass('site-header--small_mobile');
            $('#site-header').addClass('site-header--shadow_mobile');
        }

        setScroll();
        
    }

    return{
        init : init
    };
    
}(jQuery);

