
(function($){

    defaultPage.init();

    switch(defaultPage.getBodyClass('page')){
        case 'home':
            homePage.init();
            break;
        case 'page-grant-database':
            filterPage.init();
            break;
        case 'page-grant-detail':
            grantDetailPage.init();
            break;
        case 'page-registration':
            registrationPage.init();
            break;
        case 'page-404':
            page404.init();
            break;
        default:
    }

    var bodyClasses = $('body').attr('class');

    if( bodyClasses.includes('page-e-learning-detail') ) elearningDetailPage.init();
    if( bodyClasses.includes('page-blog-list') ) blogListPage.init();
    if( bodyClasses.includes('page-job-offer') ) filterPage.init();
    if( bodyClasses.includes('page-expert-database') ) filterPage.init();
    if( bodyClasses.includes('page-how-we-works') ) howWeWorksPage.init();

    $(document).ready(function() {

        switch(defaultPage.getBodyClass('page')){

            case 'page-grant-search':
                grantSearchPage.init();
                break;
            case 'page-services':
                servicesPage.init();
                break;
            case 'page-grant-education':
                grantEducationPage.init();
                break;
            case 'page-about-us':
                aboutUsPage.init();
                break;
            case 'page-grant-jobs':
                grantJobsPage.init();
                break;
            case 'page-cart':
                cartPage.init();
                break;

            default:
        }

    });

})(jQuery);






