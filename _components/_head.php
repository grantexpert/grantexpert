<!DOCTYPE html>
<html lang="sk-SK">
<head>
    <title>Spájame žiadateľov o eurofondy a poradcov | Grant Expert.sk</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

    <link rel="icon" type="image/png" href="http://grantexpert.test/assets/img/favicon-16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="http://grantexpert.test/assets/img/favicon-32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="http://grantexpert.test/assets/img/favicon-64.png" sizes="64x64" />

    <link rel="icon" type="image/png" href="http://grantexpert.test/assets/img/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="http://grantexpert.test/assets/img/favicon-192x192.png" sizes="192x192" />

    <link rel='stylesheet' id='normalize-css'  href='http://grantexpert.test/assets/libs/normalize/normalize.css?ver=5.8.3' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-css'  href='https://use.fontawesome.com/releases/v5.7.1/css/all.css?ver=5.8.3' type='text/css' media='all' />
    <!--<link rel='stylesheet' id='fonts-css'  href='https://fonts.googleapis.com/css?family=Roboto%3A100%2C300%2C400%2C700%2C900&#038;display=swap&#038;subset=latin-ext&#038;ver=5.8.3' type='text/css' media='all' />-->
    <link rel='stylesheet' id='font-adobe-css'  href='https://use.typekit.net/dwe7mje.css' type='text/css' media='all' />

    <!--<link rel='stylesheet' id='photoswipe-css'  href='http://grantexpert.test/assets/libs/photoswipe/photoswipe.css?ver=5.8.3' type='text/css' media='all' />
    <link rel='stylesheet' id='photoswipe-default-skin-css'  href='http://grantexpert.test/assets/libs/photoswipe/default-skin/default-skin.css?ver=5.8.3' type='text/css' media='all' />-->
    <link rel='stylesheet' id='tiny-slider-css'  href='http://grantexpert.test/assets/libs/tiny-slider/tiny-slider.css?ver=5.1.10' type='text/css' media='all' />


    <link id="datepicker-css" href="http://grantexpert.test/assets/libs/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">

    <link rel='stylesheet' id='main-css'  href='http://grantexpert.test/assets/css/main.css?ver=1' type='text/css' media='all' />

    <script type='text/javascript' src='http://grantexpert.test/assets/libs/jquery/jquery.min.js?ver=3.6.0' id='jquery-core-js'></script>


</head>
