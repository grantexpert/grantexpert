<section class="blog">
    <div class="container">

        <h2 class="title-secondary text-center">Viac o téme nájdete v našich článkoch</h2>

        <div class="row">
            <div class="col-12">
                <div class="carousel-container">
                    <div class="tns-carousel carousel-blog">
                        <div>
                            <div class="card-animation card-border-animation">
                                <div class="card card-border">
                                    <a href="#" class="tag">AKTUALITY</a>
                                    <a href="#">
                                        <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                    </a>
                                    <p>
                                        Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="card-animation card-border-animation">
                                <div class="card card-border">
                                    <a href="#" class="tag">AKTUALITY</a>
                                    <a href="#">
                                        <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                    </a>
                                    <p>
                                        Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="card-animation card-border-animation">
                                <div class="card card-border">
                                    <a href="#" class="tag">AKTUALITY</a>
                                    <a href="#">
                                        <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                    </a>
                                    <p>
                                        Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="card-animation card-border-animation">
                                <div class="card card-border">
                                    <a href="#" class="tag">AKTUALITY</a>
                                    <a href="#">
                                        <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                    </a>
                                    <p>
                                        Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>

                    <button id="blog-carousel-prev" class="button-scroll button-scroll--left">
                        <div class="button-border--gray"></div>
                        <div class="button-border--white">
                            <div class="icon-container"></div>
                        </div>
                    </button>
                    <button id="blog-carousel-next" class="button-scroll button-scroll--right">
                        <div class="button-border--gray"></div>
                        <div class="button-border--white">
                            <div class="icon-container"></div>
                        </div>
                    </button>
                </div>



                <div class="button-center">
                    <a href="" class="button-primary">VŠETKY BLOGY</a>
                </div>
            </div>
        </div>
    </div>
</section>