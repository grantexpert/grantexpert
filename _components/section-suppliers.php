<section class="suppliers">
    <div class="container-fluid">
        <div class="section-container">
            <div class="container">

                <h2 class="title-secondary text-center">Dodávatelia pre túto oblasť</h2>

                <div class="tag-container--center">
                    <a href="#" class="tag-small-rounded">Grantový expert</a>
                    <a href="#" class="tag-small-rounded">Projektant</a>
                    <a href="#" class="tag-small-rounded">Audity</a>
                    <a href="#" class="tag-small-rounded">Účtovníctvo</a>
                    <a href="#" class="tag-small-rounded">Projektový manažment</a>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="carousel-container">
                            <div class="tns-carousel carousel-suppliers">
                                <div>
                                    <div class="card-animation card-border-animation">
                                        <div class="card card-border">
                                            <div class="image-container">
                                                <img src="http://grantexpert.test/assets/img/svg/grantexpert-logo-mobile.svg" alt="" class="logo">
                                            </div>
                                            <h3 class="title-secondary">Konzultácia s grantexpertom</h3>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In turpis ligula, elementum eget mollis nec
                                            </p>
                                            <div class="more-info"><a href="#">Viac info</a></div>
                                            <div class="tax-included">Cena s DPH</div>
                                            <div class="price title-secondary">34,90 €</div>
                                        </div>
                                        <div class="button-secondary">OBJEDNAŤ</div>
                                    </div>
                                </div>
                                <div>
                                    <div class="card-animation card-border-animation card-animation--gold">
                                        <div class="card card-border">
                                            <div class="image-container">
                                                <img src="http://grantexpert.test/assets/img/profesia-logo.png" alt="" class="logo">
                                            </div>
                                            <h3 class="title-secondary">Konzultácia s grantexpertom</h3>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In turpis ligula, elementum eget mollis nec
                                            </p>
                                            <div class="more-info"><a href="#">Viac info</a></div>
                                            <div class="tax-included">Cena s DPH</div>
                                            <div class="price title-secondary">Na vyžiadanie</div>
                                        </div>
                                        <div class="button-secondary">OBJEDNAŤ</div>
                                    </div>
                                </div>
                                <div>
                                    <div class="card-animation card-border-animation">
                                        <div class="card card-border">
                                            <div class="image-container">
                                                <img src="http://grantexpert.test/assets/img/svg/grantexpert-logo-mobile.svg" alt="" class="logo">
                                            </div>
                                            <h3 class="title-secondary">Konzultácia s grantexpertom</h3>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In turpis ligula, elementum eget mollis nec
                                            </p>
                                            <div class="more-info"><a href="#">Viac info</a></div>
                                            <div class="tax-included">Cena s DPH</div>
                                            <div class="price title-secondary">34,90 €</div>
                                        </div>
                                        <div class="button-secondary">OBJEDNAŤ</div>
                                    </div>
                                </div>
                                <div>
                                    <div class="card-animation card-border-animation">
                                        <div class="card card-border">
                                            <div class="image-container">
                                                <img src="http://grantexpert.test/assets/img/svg/grantexpert-logo-mobile.svg" alt="" class="logo">
                                            </div>
                                            <h3 class="title-secondary">Konzultácia s grantexpertom</h3>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In turpis ligula, elementum eget mollis nec
                                            </p>
                                            <div class="more-info"><a href="#">Viac info</a></div>
                                            <div class="tax-included">Cena s DPH</div>
                                            <div class="price title-secondary">34,90 €</div>
                                        </div>
                                        <div class="button-secondary">OBJEDNAŤ</div>
                                    </div>
                                </div>
                                <div>
                                    <div class="card-animation card-border-animation">
                                        <div class="card card-border">
                                            <div class="image-container">
                                                <img src="http://grantexpert.test/assets/img/svg/grantexpert-logo-mobile.svg" alt="" class="logo">
                                            </div>
                                            <h3 class="title-secondary">Konzultácia s grantexpertom</h3>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In turpis ligula, elementum eget mollis nec
                                            </p>
                                            <div class="more-info"><a href="#">Viac info</a></div>
                                            <div class="tax-included">Cena s DPH</div>
                                            <div class="price title-secondary">34,90 €</div>
                                        </div>
                                        <div class="button-secondary">OBJEDNAŤ</div>
                                    </div>
                                </div>

                            </div>

                            <button id="suppliers-carousel-prev" class="button-scroll button-scroll--left">
                                <div class="button-border--gray"></div>
                                <div class="button-border--white">
                                    <div class="icon-container"></div>
                                </div>
                            </button>
                            <button id="suppliers-carousel-next" class="button-scroll button-scroll--right">
                                <div class="button-border--gray"></div>
                                <div class="button-border--white">
                                    <div class="icon-container"></div>
                                </div>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>