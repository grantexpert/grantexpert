<footer id="site-footer">
    <div class="menu-fixed-bottom">
        <svg class="button-close" xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
            <g data-name="Group 1809" transform="translate(-77)">
                <circle data-name="Ellipse 23" cx="8.5" cy="8.5" r="8.5" transform="translate(77)" style="fill:#eee"></circle>
                <g data-name="Icon feather-plus">
                    <path data-name="Path 19" d="M0 0v8.131" transform="rotate(45 37.397 109.49)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"></path>
                    <path data-name="Path 20" d="M0 0h8.131" transform="rotate(45 34.522 102.55)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"></path>
                </g>
            </g>
        </svg>
        <button class="button-radar" data-bs-toggle="modal" data-bs-target="#modal-grant-radar">
            <span class="col-container">
                <span class="col-icon">
                    <span class="icon-container">
                        <img class="icon-1" src="http://grantexpert.test/assets/img/svg/grant-radar-menu-1.svg" alt="">
                        <img class="icon-2" src="http://grantexpert.test/assets/img/svg/grant-radar-menu-2.svg" alt="">
                    </span>
                </span>
                <span class="col-label">
                    <span class="label">Grantový radar</span>
                </span>
            </span>



        </button>
        <button class="button-eligibility" data-bs-toggle="modal" data-bs-target="#modal-eligibility">
            <span class="col-container">
                <span class="col-icon">
                    <span class="icon-container">
                        <img class="icon-1" src="http://grantexpert.test/assets/img/svg/eligibility-menu-1.svg" alt="">
                        <img class="icon-2" src="http://grantexpert.test/assets/img/svg/eligibility-menu-2.svg" alt="">
                    </span>
                </span>
                 <span class="col-label">
                    <span class="label">Overenie oprávnenosti</span>
                </span>
            </span>
        </button>
    </div>

    <div class="modal fade modal-menu-fixed" id="modal-grant-radar" tabindex="-1" aria-labelledby="modal-grant-radar" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="page-1">
                        <div class="col-container icon-content-container">
                            <div class="col-icon">
                                <div class="inner-container">
                                    <div class="icon-container">
                                        <img src="http://grantexpert.test/assets/img/svg/grant-radar-icon.svg" alt="">
                                    </div>
                                    <div class="text-small-label">Objednávka služby</div>
                                    <h3 class="title-secondary">Grantový radar</h3>
                                    <div class="price-container">
                                        <div class="tax-included">cena s DPH</div>
                                        <div class="title-secondary color-sun">Zadarmo</div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-content">
                                <div class="inner-container">
                                    <p>
                                        Pre sledovanie relevantných grantov stačí zadať váš email. My vám naň pošleme každú novú výzvu, ktorá bude vyhovovať vami zvoleným kritériám.
                                    </p>

                                    <div class="text-small-label">Krok: 1/2</div>
                                    <input class="custom-form-control" type="email" placeholder="Váš email">

                                    <div class="button-center button-center--right">
                                        <button class="button-tertiary button-tertiary--padding">
                                            POKRAČOVAŤ
                                            <div class="button-scroll button-scroll--white button-scroll--right">
                                                <div class="icon-container"></div>
                                            </div>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="page-2">
                        <button class="button-arrow"><span class="arrow-left"></span></button>
                        <div class="page-container">
                            <div class="col-container">
                                <div class="col-title">
                                    <div class="steps">Krok: 2/2</div>
                                    <div class="choose-domain">Zvoľte si oblasti, ktoré vás zaujímajú</div>
                                </div>
                                <div class="col-select">
                                    <div class="select-all">Označiť všetko</div>
                                    <div class="deselect-all">Odznačiť všetko</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-6">
                                    <div class="checkbox">
                                        <input name="rozvoj-podnikania" type="checkbox">
                                        <div class="label">
                                            <div class="inline-container">
                                                <div class="col-container">
                                                    <div class="col-text">Rozvoj podnikania</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkbox">
                                        <input name="vyskum-vyvoj" type="checkbox">
                                        <div class="label">
                                            <div class="inline-container">
                                                <div class="col-container">
                                                    <div class="col-text">Výskum a vývoj</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkbox">
                                        <input name="vodne-hospodarstvo" type="checkbox">
                                        <div class="label">
                                            <div class="inline-container">
                                                <div class="col-container">
                                                    <div class="col-text">Vodné hospodárstvo a odpadové hospodárstvo</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkbox">
                                        <input name="zamestnanost" type="checkbox">
                                        <div class="label">
                                            <div class="inline-container">
                                                <div class="col-container">
                                                    <div class="col-text">Zamestnanosť a tvorba pracovných miest</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkbox">
                                        <input name="energeticka-efektivnost-budov" type="checkbox">
                                        <div class="label">
                                            <div class="inline-container">
                                                <div class="col-container">
                                                    <div class="col-text">Energetická efektívnosť budov</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkbox">
                                        <input name="" type="checkbox">
                                        <div class="label">
                                            <div class="inline-container">
                                                <div class="col-container">
                                                    <div class="col-text">Obnoviteľné zdroje energie</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkbox">
                                        <input name="" type="checkbox">
                                        <div class="label">
                                            <div class="inline-container">
                                                <div class="col-container">
                                                    <div class="col-text">Kultúra, cestovný ruch a voľnočasová infraštruktúra</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkbox">
                                        <input name="" type="checkbox">
                                        <div class="label">
                                            <div class="inline-container">
                                                <div class="col-container">
                                                    <div class="col-text">Vzdelávacie aktivity</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="checkbox">
                                        <input name="" type="checkbox">
                                        <div class="label">
                                            <div class="inline-container">
                                                <div class="col-container">
                                                    <div class="col-text">Sociálne služby a zdravotníctvo  </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkbox">
                                        <input name="" type="checkbox">
                                        <div class="label">
                                            <div class="inline-container">
                                                <div class="col-container">
                                                    <div class="col-text">Analýzy, plány, stratégie  </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkbox">
                                        <input name="" type="checkbox">
                                        <div class="label">
                                            <div class="inline-container">
                                                <div class="col-container">
                                                    <div class="col-text">Informačno-komunikačné technológie  </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkbox">
                                        <input name="" type="checkbox">
                                        <div class="label">
                                            <div class="inline-container">
                                                <div class="col-container">
                                                    <div class="col-text">Poľnohospodárstvo, rybné hospodárstvo a lesníctvo  </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkbox">
                                        <input name="" type="checkbox">
                                        <div class="label">
                                            <div class="inline-container">
                                                <div class="col-container">
                                                    <div class="col-text">Ochrana ŽP a environmentálne riziká   </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkbox">
                                        <input name="" type="checkbox">
                                        <div class="label">
                                            <div class="inline-container">
                                                <div class="col-container">
                                                    <div class="col-text">Občianska vybavenosť</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkbox">
                                        <input name="" type="checkbox">
                                        <div class="label">
                                            <div class="inline-container">
                                                <div class="col-container">
                                                    <div class="col-text">Dopravná infraštruktúra a služby  </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkbox">
                                        <input name="" type="checkbox">
                                        <div class="label">
                                            <div class="inline-container">
                                                <div class="col-container">
                                                    <div class="col-text">Zelená mobilita a cyklistická infraštruktúra  </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-select col-select--mobile">
                            <div class="select-all">Označiť všetko</div>
                            <div class="deselect-all">Odznačiť všetko</div>
                        </div>
                        <div class="button-center">
                            <button class="button-tertiary button-tertiary--padding">ODOSLAŤ</button>
                        </div>
                    </div>
                    <div class="page-3">
                        <h4>
                            Ďakujeme!<br>
                            Grantový radar máte úspešne aktivovaný
                        </h4>
                        <p>
                            Odhlásiť sa, alebo zmeniť vyhľadávacie parametre môžete kedykoľvek v nastaveniach vášho účtu
                        </p>
                        <div class="button-center">
                            <button class="button-tertiary button-tertiary--padding" data-bs-dismiss="modal">ZATVORIŤ</button>
                        </div>
                    </div>
                </div>
            </div>

    </div>

    <div class="modal fade modal-menu-fixed" id="modal-eligibility" tabindex="-1" aria-labelledby="modal-eligibility" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="page-1">
                    <div class="col-container icon-content-container">
                        <div class="col-icon">
                            <div class="inner-container">
                                <div class="icon-container">
                                    <img src="http://grantexpert.test/assets/img/svg/eligibility.svg" alt="">
                                </div>
                                <div class="text-small-label">Objednávka služby</div>
                                <h3 class="title-secondary">Overenie oprávnenosti</h3>
                                <div class="price-container">
                                    <div class="tax-included">cena s DPH</div>
                                    <div class="title-secondary color-sun">Zadarmo</div>
                                </div>
                            </div>

                        </div>
                        <div class="col-content">
                            <div class="inner-container">
                                <p>
                                    Overovač vyhodnocuje, či spĺňate základné formálne kritéria žiadateľa o granty. Čím viac týchto podmienok alebo kritérií splníte, tým lepšie je váš subjekt pripravený získať grant. Nesplnenie jednej alebo viacerých podmienok môže viesť k vylúčeniu žiadateľa z procesu hodnotenia.
                                </p>

                                <div class="text-small-label">Krok: 1/2</div>
                                <input class="custom-form-control" type="text" placeholder="IČO, alebo názov spoločnosti">

                                <div class="button-center button-center--right">
                                    <button class="button-tertiary button-tertiary--padding">
                                        POKRAČOVAŤ
                                        <div class="button-scroll button-scroll--white button-scroll--right">
                                            <div class="icon-container"></div>
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="page-2">
                    <div class="col-container icon-content-container">
                        <div class="col-icon">
                            <div class="inner-container">
                                <div class="icon-container">
                                    <img src="http://grantexpert.test/assets/img/svg/eligibility.svg" alt="">
                                </div>
                                <div class="text-small-label">Objednávka služby</div>
                                <h3 class="title-secondary">Overenie oprávnenosti</h3>
                                <div class="price-container">
                                    <div class="tax-included">cena s DPH</div>
                                    <div class="title-secondary color-sun">Zadarmo</div>
                                </div>
                            </div>

                        </div>
                        <div class="col-content">
                            <div class="inner-container">
                                <h4>Výborne, našli sme vás v dostupných databázach!</h4>
                                <p>
                                    Truben studio, s. r. o.<br/>
                                    Obchodný register Okresného súdu Bratislava I, oddiel: Sro, vložka č. 106969/B
                                </p>
                                <div class="row">
                                    <div class="col-sm-6 col-lg-6">
                                        <div class="labels">
                                            IČO:<br/>
                                            DIČ:<br/>
                                            IČ DPH:
                                        </div>
                                        <div class="values">
                                            <p>
                                                50012541<br/>
                                                2120155389
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-6">
                                        <p>
                                            Sídlo:<br/>
                                            Karpatské námestie 10<br/>
                                            831 06 Bratislava
                                        </p>
                                    </div>
                                </div>
                                

                                <div class="button-center button-center--right">
                                    <button class="button-arrow"><span class="arrow-left"></span><span class="label">Nie ste to vy?</span></button>
                                    <button class="button-tertiary button-tertiary--padding" >
                                        POKRAČOVAŤ
                                        <div class="button-scroll button-scroll--white button-scroll--right">
                                            <div class="icon-container"></div>
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page-3">
                    <div class="page-container">
                        <div class="icon-content-container">
                            <div class="col-icon">
                                <div class="inner-container">
                                    <div class="icon-container">
                                        <img src="http://grantexpert.test/assets/img/svg/eligibility.svg" alt="">
                                    </div>
                                    <div class="text-small-label">Objednávka služby</div>
                                    <h3 class="title-secondary">Overenie oprávnenosti</h3>
                                    <div class="price-container">
                                        <div class="tax-included">cena s DPH</div>
                                        <div class="title-secondary color-sun">Zadarmo</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button class="button-arrow"><span class="arrow-left"></span></button>

                        <div class="page-inner-container">
                            <div class="tabs-container">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link active" id="unregistered-user-tab" data-bs-toggle="tab" data-bs-target="#unregistered-user-pane" type="button" role="tab" aria-controls="unregistered-user-pane" aria-selected="true">Neregistrovaný užívateľ</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="registered-user-tab" data-bs-toggle="tab" data-bs-target="#registered-user-pane" type="button" role="tab" aria-controls="registered-user-pane" aria-selected="false">Registrovaný užívateľ</button>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="unregistered-user-pane" role="tabpanel" aria-labelledby="unregistered-user-tab">
                                    <div class="text-small-label">Krok: 2/2</div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="input-required error-message">
                                                <div class="input-container">
                                                    <input class="custom-form-control" type="text" placeholder="Meno">
                                                </div>
                                                <div class="error-message">Toto pole je povinné</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="input-required">
                                                <div class="input-container">
                                                    <input class="custom-form-control" type="text" placeholder="Priezvisko">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="input-required">
                                                <div class="input-container">
                                                    <input class="custom-form-control" type="text" placeholder="Telefón">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="input-required">
                                                <div class="input-container">
                                                    <input class="custom-form-control" type="email" placeholder="Email">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="input-required">
                                                <div class="input-container">
                                                    <input class="custom-form-control" type="password" placeholder="Heslo">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="input-required">
                                                <div class="input-container">
                                                    <input class="custom-form-control" type="email" placeholder="Zopakovať heslo">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-container">
                                        <div class="col-left">
                                            <div class="checkbox">
                                                <input name="" type="checkbox">
                                                <div class="label">
                                                    <div class="inline-container">
                                                        <div class="col-container">
                                                            <div class="col-text">Súhlasím so spracúvaním svojich osobných údajov na účely zasielania noviniek z oblasti grantového poradenstva v súlade so zásadami ochrany osobných údajov.</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-right">
                                            <div class="button-center">
                                                <button class="button-tertiary button-tertiary--padding">ODOSLAŤ</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="registered-user-pane" role="tabpanel" aria-labelledby="registered-user-tab">
                                    <div class="text-small-label">Krok: 2/2</div>
                                    <div class="row">
                                        <div class="offset-lg-3 col-lg-6">
                                            <div class="input-icon input-icon--user">
                                                <input class="custom-form-control" type="text" placeholder="Meno">
                                            </div>
                                            <div class="input-icon input-icon--password">
                                                <input class="custom-form-control" type="password" placeholder="Heslo">
                                            </div>
                                            <div class="button-center">
                                                <button class="button-tertiary button-tertiary--padding">ODOSLAŤ</button>
                                            </div>
                                            <div class="center-container">
                                                <a href="#" class="forgotten-password">Zabudli ste heslo?</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="page-4">
                    <h4>
                        Ďakujeme!<br>
                        Overenie oprávnenosti bolo úspešné
                    </h4>
                    <p>
                        Výsledok sme zaslali na vašu emailovú adresu, ktorú ste uviedli v predošlom kroku
                    </p>
                    <div class="button-center">
                        <button class="button-tertiary button-tertiary--padding" data-bs-dismiss="modal">ZATVORIŤ</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="container">

        <button class="button-scroll-top button-scroll">
            <div class="button-border--gray"></div>
            <div class="button-border--white">
                <div class="icon-container"></div>
            </div>
        </button>

        <div class="social-icons">
            <a href="#"><img src="http://grantexpert.test/assets/img/svg/facebook.svg" alt="facebook"></a>
            <a href="#"><img src="http://grantexpert.test/assets/img/svg/linkedin.svg" alt="linkedin"></a>
            <a href="#"><img src="http://grantexpert.test/assets/img/svg/instagram.svg" alt="instagram"></a>
            <a href="#"><img src="http://grantexpert.test/assets/img/svg/youtube.svg" alt="youtube"></a>
        </div>

        <div class="main-footer-menu">
            <div class="row">
                <div class="col">
                    <strong>Služby Grantexperta</strong>
                    <nav>
                        <ul class="menu-simple">
                            <li><a href="#">Individuálne služby</a></li>
                            <li><a href="#">Balíky služieb</a></li>
                            <li><a href="#">Konzultácia s grantexpertom</a></li>
                            <li><a href="#">Vypracovanie projektu</a></li>
                            <li><a href="#">Privátny grantový konzultant</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col">
                    <strong>Grantové vzdelávanie</strong>
                    <nav>
                        <ul class="menu-simple">
                            <li><a href="#">Kurzy a školenia</a></li>
                            <li><a href="#">Webináre</a></li>
                            <li><a href="#">Videoslovník</a></li>
                            <li><a href="#">Grantové zdroje</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">Blog</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col">
                    <strong>Granty podľa odvetví</strong>
                    <nav>
                        <ul class="menu-simple">
                            <li><a href="#">Pre podnikateľov</a></li>
                            <li><a href="#">Pre mestá a obce</a></li>
                            <li><a href="#">Pre školy</a></li>
                            <li><a href="#">Pre mimovládne organizácie</a></li>
                            <li><a href="#">Pre jednotlivcov</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col">
                    <strong>Grantové joby</strong>
                    <nav>
                        <ul class="menu-simple">
                            <li><a href="#">Pracovné ponuky</a></li>
                            <li><a href="#">Pridať pracovnú ponuku</a></li>
                            <li><a href="#">Databáza Grantových expertov</a></li>
                            <li><a href="#">Stať sa grantovým expertom</a></li>
                            <li><a href="#">Blog</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col">
                    <strong>O nás</strong>
                    <nav>
                        <ul class="menu-simple">
                            <li><a href="#">Kto sme</a></li>
                            <li><a href="#">Ako fungujeme</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">Referencie</a></li>
                            <li><a href="#">Partneri</a></li>
                            <li><a href="#">Kontakt</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <div class="contact">
            <div class="row">
                <div class="collumn-tablet order-md-1">
                    <strong>O nás</strong>
                    <nav>
                        <ul class="menu-simple">
                            <li><a href="#">Kto sme</a></li>
                            <li><a href="#">Ako fungujeme</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">Referencie</a></li>
                            <li><a href="#">Partneri</a></li>
                            <li><a href="#">Kontakt</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="collumn-1 order-3 order-md-2">
                    <nav>
                        <ul class="menu-simple">
                            <li><a href="#">Zásady ochranných údajov</a></li>
                            <li><a href="#">Všeobecné obchodné podmienky</a></li>
                            <li><a href="#">Mapa grantov</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="collumn-2 order-1 order-md-3">
                    <a href="mailto:ahoj@grantexpert.sk">ahoj@grantexpert.sk</a>
                </div>
                <div class="collumn-3 order-2 order-md-4">
                    <address>
                        Grantexpert s.r.o.<br>
                        Záhradnícka 72<br>
                        821 08 Bratislava
                    </address>
                </div>
            </div>
        </div>

        <div class="copyright">
            <div class="row">
                <div class="col-12">
                    Všetky práva vyhradené © 2021 GrantExpert.sk
                </div>
            </div>
        </div>
    </div>
</footer>
</div><!-- #wrapper -->

<script type='text/javascript' src='http://grantexpert.test/assets/libs/bootstrap/bootstrap.bundle.min.js?ver=1.0' id='bootstrap-js'></script>
<script src="http://grantexpert.test/assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="http://grantexpert.test/assets/libs/bootstrap-datepicker/locales/bootstrap-datepicker.sk.min.js" charset="UTF-8"></script>
<!--<script type='text/javascript' src='http://grantexpert.test/assets/libs/photoswipe/photoswipe.min.js?ver=1.0' id='photoswipe-js'></script>
<script type='text/javascript' src='http://grantexpert.test/assets/libs/photoswipe/photoswipe-ui-default.min.js?ver=1.0' id='photoswipe-ui-default-js'></script>-->
<script type='text/javascript' src='http://grantexpert.test/assets/libs/tiny-slider/tiny-slider.js?ver=1.0'></script>
<script type='text/javascript' src='http://grantexpert.test/assets/js/main.js?ver=1.0' id='main-js'></script>

</body>
</html>
