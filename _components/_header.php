<header id="site-header" class="site-header--small_mobile site-header--fixed_mobile">

        <div class="top-part">
            <div class="container-fluid">
                <div class="row">
                    <div class="social-media">
                        <a href="#"><img src="http://grantexpert.test/assets/img/svg/facebook-gray.svg" alt="facebook"></a>
                        <a href="#"><img src="http://grantexpert.test/assets/img/svg/linkedin-gray.svg" alt="linkedin"></a>
                        <a href="#"><img src="http://grantexpert.test/assets/img/svg/instagram-gray.svg" alt="instagram"></a>
                        <a href="#"><img src="http://grantexpert.test/assets/img/svg/youtube-gray.svg" alt="youtube"></a>
                    </div>
                    <div class="col-menu">
                        <nav>
                            <ul class="menu-top">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle">O NÁS</a>
                                    <ul role="menu" class="dropdown-menu">
                                        <li><a href="http://#">KTO SME</a></li>
                                        <li><a href="http://#">AKO FUNGUJEME</a></li>
                                        <li><a href="http://#">FAQ</a></li>
                                        <li><a href="http://#">REFERENCIE</a></li>
                                        <li><a href="http://#">PARTNERI</a></li>
                                        <li><a href="http://#">KONTAKT</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">REGISTRÁCIA</a>
                                </li>
                                <li class="item-login">
                                    <a href="#">
                                        <!--<svg xmlns="http://www.w3.org/2000/svg" width="17.408" height="22.765" viewBox="0 0 17.408 22.765">
                                            <g id="Group_1127" data-name="Group 1127" transform="translate(-345.304 -103.154)">
                                                <path id="Path_356" class="user-body" data-name="Path 356" d="M346.32,175.361q-.016-.249-.016-.5a7.7,7.7,0,0,1,15.408,0q0,.259-.017.514" transform="translate(0 -49.518)" fill="none" stroke="#008bce" stroke-linejoin="round" stroke-width="2"/>
                                                <circle id="Ellipse_340" data-name="Ellipse 340" cx="5.778" cy="5.778" r="5.778" transform="translate(348.23 104.154)" fill="none" stroke="#008bce" stroke-linejoin="round" stroke-width="2"/>
                                            </g>
                                        </svg>-->

                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             width="17.4" height="22.8" viewBox="0 0 17.4 22.8" style="enable-background:new 0 0 17.4 22.8;" xml:space="preserve">
                                            <style type="text/css">
                                                .user{fill:none;stroke:#008BCE;stroke-width:2;stroke-linejoin:round;}
                                            </style>
                                            <g>
                                                <path class="user user-body" d="M1,22.8c0-0.2,0-0.3,0-0.5c0-4.3,3.4-7.7,7.7-7.7s7.7,3.4,7.7,7.7c0,0.2,0,0.3,0,0.5" vector-effect="non-scaling-stroke"/>
                                                <path class="user" d="M8.7,1.3c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8s-5.8-2.6-5.8-5.8C2.9,3.9,5.5,1.3,8.7,1.3z"/>
                                            </g>
                                        </svg>

                                        PRIHLÁSENIE
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-part">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-logo col-5 col-lg-3">
                        <a href="http://grantexpert.test">
                            <img class="logo logo-desktop vertical-center" alt="grantexpert logo" src="http://grantexpert.test/assets/img/svg/grantexpert-logo.svg">
                            <img class="logo logo-mobile vertical-center" alt="grantexpert logo" src="http://grantexpert.test/assets/img/svg/grantexpert-logo-mobile.svg">
                        </a>
                    </div>
                    <div class="col-menu col-7 col-lg-9">
                        <div class="col-container">
                            <div class="mobile-page-title"><div class="title">Názov stránky</div></div>

                            <div class="menu-main">
                                <nav class="navbar">
                                    <ul id="menu-hlavne-menu" class="nav navbar-nav">
                                        <li class="item-1 dropdown">
                                            <a href="#" data-toggle="dropdown" class="dropdown-toggle disabled" aria-haspopup="true">GRANTOVÉ <span>VZDELÁVANIE</span></a>
                                            <ul role="menu" class="dropdown-menu">
                                                <li><a href="http://#">E-LEARNING KURZY</a></li>
                                                <li><a href="http://#">WEBINÁRE</a></li>
                                                <li><a href="http://#">ŠKOLENIA NA MIERU</a></li>
                                                <li><a href="http://#">VIDEOSLOVNÍK</a></li>
                                                <li><a href="http://#">GRANTOVÉ ZDROJE</a></li>
                                                <li><a href="http://#">BLOG</a></li>
                                            </ul>
                                        </li>
                                        <li class="item-2 dropdown">
                                            <a href="#" data-toggle="dropdown" class="dropdown-toggle disabled" aria-haspopup="true">GRANTY PODĽA <span>ODVETVÍ</span></a>
                                            <ul role="menu" class="dropdown-menu">
                                                <li><a href="http://#">PRE PODNIKATEĽOV</a></li>
                                                <li><a href="http://#">PRE MESTÁ A OBCE</a></li>
                                                <li><a href="http://#">PRE ŠKOLY</a></li>
                                                <li><a href="http://#">PRE MIMOVLÁDNE ORGANIZÁCIE</a></li>
                                                <li><a href="http://#">PRE JEDNOTLIVCOV</a></li>
                                            </ul>
                                        </li>
                                        <li class="item-3 dropdown">
                                            <a href="#" data-toggle="dropdown" class="dropdown-toggle disabled" aria-haspopup="true">Služby <span>Grantexperta</span></a>
                                            <ul role="menu" class="dropdown-menu">
                                                <li><a href="http://#">INDIVIDUÁLNE SLUŽBY</a></li>
                                                <li><a href="http://#">BALÍKY SLUŽIEB</a></li>
                                                <li><a href="http://#" class="two-lines">KONZULTÁCIA S <span>GRANTEXPERTOM</span></a></li>
                                                <li><a href="http://#" class="two-lines">VYPRACOVANIE <span>PROJEKTU</span></a></li>
                                                <li><a href="http://#" class="two-lines">PRIVÁTNY GRANTOVÝ <span>KONZULTANT</span></a></li>
                                            </ul>
                                        </li>
                                        <li class="item-4 dropdown">
                                            <a href="#" data-toggle="dropdown" class="dropdown-toggle disabled" aria-haspopup="true">Grantové <span>Joby</span></a>
                                            <ul role="menu" class="dropdown-menu">
                                                <li><a href="http://#">PRACOVNÉ PONUKY</a></li>
                                                <li><a href="http://#" class="two-lines">PRIDAŤ PRACOVNÚ <span>PONUKU</span></a></li>
                                                <li><a href="http://#" class="two-lines">DATABÁZA GRANTOVÝCH <span>EXPERTOV</span></a></li>
                                                <li><a href="http://#" class="two-lines">STAŤ SA GRANTOVÝM <span>EXPERTOM</span></a></li>
                                                <li><a href="http://#" >BLOG</a></li>
                                            </ul>
                                        </li>
                                    </ul>

                                    <a href="#" class="button-tertiary button-icon">
                                        <div class="icon-container">
                                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 width="21.1" height="26" viewBox="0 0 21.1 26" style="enable-background:new 0 0 21.1 26;" xml:space="preserve">
                                                <style type="text/css">
                                                    .st0{fill:none;stroke:#ECF6FB;stroke-width:2;}
                                                    .st1{fill:url(#Path_740_1_);stroke:#ECF6FB;stroke-width:2;}
                                                </style>
                                                <g id="Group_3960" transform="translate(-29 -16)">
                                                    <line id="line-1" class="st0" x1="33.1" y1="17" x2="46.1" y2="17"/>
                                                    <line id="line-2" class="st0" x1="33.1" y1="23" x2="46.1" y2="23"/>

                                                    <linearGradient id="Path_740_1_" gradientUnits="userSpaceOnUse" x1="14.6418" y1="42.7568" x2="14.5318" y2="44.7048" gradientTransform="matrix(13.5 0 0 -1 -157.3761 73)">
                                                        <stop  offset="0" style="stop-color:#299BD7"/>
                                                        <stop  offset="1" style="stop-color:#DE9822"/>
                                                    </linearGradient>
                                                    <path id="line-3" class="st1" d="M32.6,29h13.5"/>
                                                    <line id="line-4" class="st0" x1="33.1" y1="35" x2="46.1" y2="35"/>
                                                    <line id="line-5" class="st0" x1="33.1" y1="41" x2="46.1" y2="41"/>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="label-container">
                                            Databáza grantov
                                        </div>
                                    </a>

                                </nav>
                            </div>

                            <div class="menu-mobile">
                                <div class="open-icon">
                                    <div class="vertical-center">
                                        <div class="line"></div>
                                        <div class="line"></div>
                                        <div class="line"></div>
                                    </div>
                                </div>
                                <div class="window-container">
                                    <div class="col-container">
                                        <div class="left-part"></div>
                                        <div class="right-part">
                                            <div class="scroll-container">

                                                <header>
                                                    <div class="social-icons">
                                                        <a href="#"><img src="http://grantexpert.test/assets/img/svg/facebook.svg" alt="facebook"></a>
                                                        <a href="#"><img src="http://grantexpert.test/assets/img/svg/linkedin.svg" alt="linkedin"></a>
                                                        <a href="#"><img src="http://grantexpert.test/assets/img/svg/instagram.svg" alt="instagram"></a>
                                                        <a href="#"><img src="http://grantexpert.test/assets/img/svg/youtube.svg" alt="youtube"></a>
                                                    </div>

                                                    <img class="close-icon" src="http://grantexpert.test/assets/img/svg/plus-blue.svg" alt="">


                                                    <a href="#" class="button-tertiary button-icon">
                                                        <div class="col-container">
                                                            <div class="icon-container">
                                                                <img src="http://grantexpert.test/assets/img/svg/five-lines.svg" alt="">
                                                            </div>
                                                            <div class="label-container">
                                                                Databáza grantov
                                                            </div>
                                                        </div>
                                                    </a>
                                                </header>

                                                <div class="content-container">

                                                    <nav>
                                                        <ul>
                                                            <li class="item-1 current-menu-item dropdown">
                                                                <a href="#" data-toggle="dropdown" class="dropdown-toggle disabled" aria-haspopup="true">GRANTOVÉ <span>VZDELÁVANIE</span></a>
                                                                <ul role="menu" class="dropdown-menu">
                                                                    <li><a href="http://#">E-LEARNING KURZY</a></li>
                                                                    <li><a href="http://#">WEBINÁRE</a></li>
                                                                    <li><a href="http://#">ŠKOLENIA NA MIERU</a></li>
                                                                    <li><a href="http://#">VIDEOSLOVNÍK</a></li>
                                                                    <li><a href="http://#">GRANTOVÉ ZDROJE</a></li>
                                                                    <li><a href="http://#">BLOG</a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="item-2 dropdown">
                                                                <a href="#" data-toggle="dropdown" class="dropdown-toggle disabled" aria-haspopup="true">GRANTY PODĽA <span>ODVETVÍ</span></a>
                                                                <ul role="menu" class="dropdown-menu">
                                                                    <li><a href="http://#">PRE PODNIKATEĽOV</a></li>
                                                                    <li><a href="http://#">PRE MESTÁ A OBCE</a></li>
                                                                    <li><a href="http://#">PRE ŠKOLY</a></li>
                                                                    <li><a href="http://#">PRE MIMOVLÁDNE ORGANIZÁCIE</a></li>
                                                                    <li><a href="http://#">PRE JEDNOTLIVCOV</a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="item-3 dropdown">
                                                                <a href="#" data-toggle="dropdown" class="dropdown-toggle disabled" aria-haspopup="true">Služby <span>Grantexperta</span></a>
                                                                <ul role="menu" class="dropdown-menu">
                                                                    <li><a href="http://#">INDIVIDUÁLNE SLUŽBY</a></li>
                                                                    <li><a href="http://#">BALÍKY SLUŽIEB</a></li>
                                                                    <li><a href="http://#" class="two-lines">KONZULTÁCIA S <span>GRANTEXPERTOM</span></a></li>
                                                                    <li><a href="http://#" class="two-lines">VYPRACOVANIE <span>PROJEKTU</span></a></li>
                                                                    <li><a href="http://#" class="two-lines">PRIVÁTNY GRANTOVÝ <span>KONZULTANT</span></a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="item-4 dropdown">
                                                                <a href="#" data-toggle="dropdown" class="dropdown-toggle disabled" aria-haspopup="true">Grantové <span>Joby</span></a>
                                                                <ul role="menu" class="dropdown-menu">
                                                                    <li><a href="http://#">PRACOVNÉ PONUKY</a></li>
                                                                    <li><a href="http://#" class="two-lines">PRIDAŤ PRACOVNÚ <span>PONUKU</span></a></li>
                                                                    <li><a href="http://#" class="two-lines">DATABÁZA GRANTOVÝCH <span>EXPERTOV</span></a></li>
                                                                    <li><a href="http://#" class="two-lines">STAŤ SA GRANTOVÝM <span>EXPERTOM</span></a></li>
                                                                    <li><a href="http://#" >BLOG</a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="dropdown">
                                                                <a href="#" class="dropdown-toggle">O NÁS</a>
                                                                <ul role="menu" class="dropdown-menu">
                                                                    <li><a href="http://#">KTO SME</a></li>
                                                                    <li><a href="http://#">AKO FUNGUJEME</a></li>
                                                                    <li><a href="http://#">FAQ</a></li>
                                                                    <li><a href="http://#">REFERENCIE</a></li>
                                                                    <li><a href="http://#">PARTNERI</a></li>
                                                                    <li><a href="http://#">KONTAKT</a></li>
                                                                </ul>
                                                            </li>
                                                        </ul>

                                                    </nav>
                                                    <footer>
                                                        <ul class="menu-simple menu-top menu-simple--mobile">
                                                            <li>
                                                                <a href="#">
                                                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                                 width="17.4" height="22.8" viewBox="0 0 17.4 22.8" style="enable-background:new 0 0 17.4 22.8;" xml:space="preserve">
                                                <style type="text/css">
                                                    .user{fill:none;stroke:#008BCE;stroke-width:2;stroke-linejoin:round;}
                                                </style>
                                                                        <g>
                                                                            <path class="user user-body" d="M1,22.8c0-0.2,0-0.3,0-0.5c0-4.3,3.4-7.7,7.7-7.7s7.7,3.4,7.7,7.7c0,0.2,0,0.3,0,0.5" vector-effect="non-scaling-stroke"/>
                                                                            <path class="user" d="M8.7,1.3c3.2,0,5.8,2.6,5.8,5.8c0,3.2-2.6,5.8-5.8,5.8s-5.8-2.6-5.8-5.8C2.9,3.9,5.5,1.3,8.7,1.3z"/>
                                                                        </g>
                                            </svg>
                                                                    PRIHLÁSENIE</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">REGISTRÁCIA</a>
                                                            </li>
                                                        </ul>
                                                    </footer>

                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</header>
