
<?php include '../../_components/_head.php';?>

<body class="page-grant-consultant page-services">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li>
                    <a href="#">Individuálne služby</a>
                </li>
                <li>
                    <a href="#">Balíky služieb</a>
                </li>
                <li>
                    <a href="#">Konzultácia s grantexpertom</a>
                </li>
                <li class="active">
                    <a href="#">Vypracovanie projektu</a>
                </li>
                <li>
                    <a href="#">Privátny grantový konzultant</a>
                </li>
            </ul>
        </nav>

       <section class="description-benefit">
           <div class="container-fluid">
               <div class="section-container">
                   <div class="container">
                       <div class="row">
                           <div class="col-lg-6">
                               <div class="description">
                                   <img src="http://grantexpert.test/assets/img/svg/cogwheel.svg" alt="">
                                   <h1 class="title-primary">Privátny grantový konzultant</h1>
                                   <p class="text-primary">
                                       Rozumieme grantom a preto vieme, že agenda, ktorá sa s nimi spája môže byť náročná a neprehľadná. Aby sme ušetrili váš čas a pomohli vám využívať granty pre vašu organizáciu naplno, ponúkame službu privátneho grantového konzulanta. S ňou získavate partnera, ktorý sa v problematke orientuje a pracuje vo váš prospech.
                                   </p>

                                   <a href="#" class="button-tertiary button-tertiary--shadow">Vyžiadať ponuku</a>
                               </div>
                           </div>
                           <div class="col-lg-6">
                               <div class="card">
                                   <div class="col-container">
                                       <div class="benefit">
                                           <h3>Benefit 1</h3>
                                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse elit elit,</p>
                                       </div>
                                       <div class="benefit">
                                           <h3>Benefit 1</h3>
                                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse elit elit,</p>
                                       </div>
                                       <div class="benefit">
                                           <h3>Benefit 1</h3>
                                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse elit elit,</p>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </section>

        <section class="grant-steps">
            <div class="container">
                <h2 class="title-secondary">Kroky ktorými si spolu prejdeme</h2>

                <div class="steps">
                    <div class="step-row col-container">
                        <div class="left-part">
                            <div class="col-container">
                                <div class="col-number">
                                    <div class="number">1</div>
                                </div>
                                <div class="col-label">
                                    Analýza
                                </div>
                            </div>
                        </div>
                        <div class="right-part">
                            Prejdeme s vami vaše potreby a zámery pre najbližsie časové obdobie
                        </div>
                    </div>
                    <div class="step-row col-container">
                        <div class="left-part">
                            <div class="col-container">
                                <div class="col-number">
                                    <div class="number">2</div>
                                </div>
                                <div class="col-label">
                                    Stratégia
                                </div>
                            </div>
                        </div>
                        <div class="right-part">
                            Vypracujeme pre vás stratégiu čerpania grantov a dotácií
                        </div>
                    </div>
                    <div class="step-row col-container">
                        <div class="left-part">
                            <div class="col-container">
                                <div class="col-number">
                                    <div class="number">3</div>
                                </div>
                                <div class="col-label">
                                    Monitoring
                                </div>
                            </div>
                        </div>
                        <div class="right-part">
                            Monitorujeme za vás grantové zdroje a individuálne vás kontaktujeme vždy pri príležitostiach, ktoré zapadajú do nastavenej stratégie
                        </div>
                    </div>
                    <div class="step-row col-container">
                        <div class="left-part">
                            <div class="col-container">
                                <div class="col-number">
                                    <div class="number">4</div>
                                </div>
                                <div class="col-label">
                                    Vypracovanie
                                </div>
                            </div>
                        </div>
                        <div class="right-part">
                            Projektové žiadosti kompletne spracujú naši experti a odovzdávajú ako hotové projekty
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="achieved-results">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <header>
                            <div class="row">
                                <div class="col-lg-6">
                                    <h2 class="title-primary">Naše referencie</h2>
                                    <a href="#" class="arrow-double-link button-desktop">
                                        <div class="arrow-right"></div>
                                        <div class="label">úspešné projekty</div>
                                    </a>
                                </div>
                                <div class="col-lg-6">
                                    <p class="text-primary">
                                        Vďaka dlhoročným skúsenostiam vieme ako napísať žiadosť o grant,  aby mala čo najväčšie šance na úspech a na čo si dať pozor, aby sme sa vyhli prekážkam.
                                    </p>
                                    <a href="#" class="arrow-double-link button-mobile">
                                        <div class="arrow-right"></div>
                                        <div class="label">úspešné projekty</div>
                                    </a>
                                </div>
                            </div>
                        </header>
                        <div class="row results">
                            <div class="col-6 col-lg-3">
                                <h4>2 mil. €</h4>
                                <p class="text-primary">získaných finančných <br>prostriedkov v schválených <br>projektoch</p>
                            </div>
                            <div class="col-6 col-lg-3">
                                <h4>700+</h4>
                                <p class="text-primary">preverených nápadov</p>
                            </div>
                            <div class="col-6 col-lg-3">
                                <h4>8000+</h4>
                                <p class="text-primary">účastníkov <br>školení</p>
                            </div>
                            <div class="col-6 col-lg-3">
                                <h4>25</h4>
                                <p class="text-primary">grantových <br>profesionálov</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="offer-closable">
            <div class="container">
                <div class="background-container">
                    <button class="button-close--blue"></button>
                    <div class="grant-radar-container">
                        <div class="row">
                            <div class="col-lg-5 col-xl-3">
                                <img class="image" src="http://grantexpert.test/assets/img/svg/cogwheel-circles.svg" alt="">
                            </div>
                            <div class="col-lg-7 col-xl-6">
                                <h3 class="title-secondary">Cena, za ktorú získavate svojho externého špecialistu</h3>
                                <p>
                                    Cena privátneho experta sa bude odvíjať od vašich reálnych potrieb a cieľov, ktoré chcete prostredníctvom grantov napĺňať.
                                </p>
                            </div>
                            <div class="offset-lg-5 col-lg-7 offset-xl-0 col-xl-3">
                                <div class="price-container">
                                    <div class="tax-included">CENA S DPH/ROK</div>
                                    <div class="title-secondary color-sun price">Od 2880,00 €</div>

                                    <div class="button-container">
                                        <a href="" class="button-tertiary button-tertiary--shadow">MÁM ZÁUJEM</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>





    </main>

    <?php include '../../_components/_footer.php';?>

