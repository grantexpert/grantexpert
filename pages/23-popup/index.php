
<?php include '../../_components/_head.php';?>

<body class="page-51-54-popups">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">
        <div class="container">
            <div class="button-center">
                <button class="button-primary" data-bs-toggle="modal" data-bs-target="#webinar-locked">popup 23 - webinár</button>
            </div>

            <div class="modal fade modal-player modal-webinar" id="webinar-locked" tabindex="-1" aria-labelledby="webinar-locked" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                        <div class="player">
                            <img src="http://grantexpert.test/assets/img/video-cover.jpg" alt="">
                        </div>
                        <section class="get-access">
                            <div class="col-container">
                                <div class="col-icon">
                                    <img src="http://grantexpert.test/assets/img/svg/key.svg" alt="">
                                </div>
                                <div class="col-text">
                                    Predplaťte si prístup na celý rok nie len k tomuto videu, ale do celej našej databázy webinárov.
                                </div>
                                <div class="col-button">
                                    <a href="#" class="button-secondary">Získať prístup</a>
                                </div>
                            </div>
                        </section>
                        <footer>
                            <div class="col-container">
                                <div class="col-info">
                                    <div class="length">
                                        Dĺžka: 3h 28m
                                    </div>
                                    <div class="lecturer">
                                        Lektor: Tamás Szoke, Jozef Kovalčík
                                    </div>
                                </div>
                                <div class="col-description">
                                    <h4>Webinár: Rozvoj múzeí a galérií z FPU</h4>
                                    <p>Na webinári sme si predstavili konkértne výzvy z Fondu na podporu umenia, ktoré sa venujú rozvoju múzeí a galérií na Slovensku.</p>
                                </div>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>

            <div class="button-center">
                <button class="button-primary" data-bs-toggle="modal" data-bs-target="#webinar-unlocked">popup 23 - webinár - otvorený</button>
            </div>

            <div class="modal fade modal-player modal-webinar" id="webinar-unlocked" tabindex="-1" aria-labelledby="webinar-unlocked" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                        <div class="player">
                            <video width="100%" controls>
                                <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
                                Váš prehliadač nepodporuje HTML5 video
                            </video>
                        </div>
                        <footer>
                            <div class="col-container">
                                <div class="col-info">
                                    <div class="length">
                                        Dĺžka: 3h 28m
                                    </div>
                                    <div class="level">Úroveň: Začiatočník</div>

                                </div>
                                <div class="col-description">
                                    <h4>Webinár: Rozvoj múzeí a galérií z FPU</h4>
                                    <p>Na webinári sme si predstavili konkértne výzvy z Fondu na podporu umenia, ktoré sa venujú rozvoju múzeí a galérií na Slovensku.</p>

                                    <div class="lecturer">
                                        Lektor: Tamás Szoke, Jozef Kovalčík
                                    </div>
                                </div>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>

        </div>
    </main>

    <?php include '../../_components/_footer.php';?>

