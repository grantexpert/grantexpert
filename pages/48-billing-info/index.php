
<?php include '../../_components/_head.php';?>

<body class="page-billing-info page-cart">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <div class="container">
            <div class="cart cart--mobile">
                <div class="icon">
                    <img src="http://grantexpert.test/assets/img/svg/cart.svg" alt="">
                </div>

                <div class="order">Objednávka služby</div>
                <h3 class="title-secondary">Vypracovanie projektu</h3>
                <div class="price-container">
                    <div class="tax-included">Konečná cena s DPH</div>
                    <div class="title-secondary">100 €</div>
                </div>
            </div>
        </div>

        <nav class="cart-submenu">
            <ul>
                <li class="done">
                    <a class="step" href="#"><span class="number">1</span> Podrobnosti objednávky</a>
                </li>
                <li class="active">
                    <a class="step" href="#"><span class="number">2</span> Fakturačné údaje</a>
                </li>
                <li>
                    <span class="step"><span class="number">3</span> Platobné metódy</span>
                </li>
                <li>
                    <span class="step"><span class="number">4</span> Zhrnutie objednávky</span>
                </li>
            </ul>
        </nav>

        <section class="section-cart">
            <div class="container">
                <div class="col-container">
                    <div class="col-content">
                        <section class="company-name">
                            <div class="card-animation card-shadow-animation">
                                <div class="card card-shadow">
                                    <div class="card-container">
                                        <input class="custom-form-control input--gray" type="text" placeholder="IČO, alebo názov spoločnosti">
                                        <div class="checkbox">
                                            <input name="" type="checkbox">
                                            <div class="label">
                                                <div class="inline-container">
                                                    <div class="col-container">
                                                        <div class="col-text">Som fyzická osoba</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="billing-information">
                            <h2 class="title-secondary">Fakturačné údaje</h2>
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <div class="input-required">
                                        <div class="input-container">
                                            <input class="custom-form-control" type="text" placeholder="IČO">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="input-required">
                                        <div class="input-container">
                                            <input class="custom-form-control" type="text" placeholder="Názov spoločnosti">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="checkbox">
                                        <input name="" type="checkbox">
                                        <div class="label">
                                            <div class="inline-container">
                                                <div class="col-container">
                                                    <div class="col-text">Som fyzická osoba</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-required">
                                        <div class="input-container">
                                            <input class="custom-form-control" type="text" placeholder="Ulica">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-required">
                                        <div class="input-container">
                                            <input class="custom-form-control" type="text" placeholder="Súpisné/Orientčné číslo">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <div class="input-required">
                                        <div class="input-container">
                                            <input class="custom-form-control" type="text" placeholder="Obec">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="input-required">
                                        <div class="input-container">
                                            <input class="custom-form-control" type="text" placeholder="PSČ">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <section class="personal-info">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6">
                                        <div class="input-required">
                                            <div class="input-container">
                                                <input class="custom-form-control" type="text" placeholder="Meno">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="input-required">
                                            <div class="input-container">
                                                <input class="custom-form-control" type="text" placeholder="Priezvisko">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-md-6">
                                        <div class="input-required">
                                            <div class="input-container">
                                                <input class="custom-form-control" type="text" placeholder="Telefón">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="input-required">
                                            <div class="input-container">
                                                <input class="custom-form-control" type="email" placeholder="Email">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-md-6">
                                        <div class="input-required">
                                            <div class="input-container">
                                                <input class="custom-form-control" type="password" placeholder="Heslo">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="input-required">
                                            <div class="input-container">
                                                <input class="custom-form-control" type="password" placeholder="Zopakovať heslo">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>

                        </section>

                        <footer class="cart-footer">

                            <div class="button-center">
                                <button class="button-tertiary button-tertiary--padding button-continue">
                                    POKRAČOVAŤ
                                    <div class="button-scroll button-scroll--white button-scroll--right">
                                        <div class="icon-container"></div>
                                    </div>
                                </button>
                            </div>

                            <button class="button-arrow"><span class="arrow-left"></span><span class="label">Naspäť</span></button>

                            <div class="required-info">
                                <img src="http://grantexpert.test/assets/img/svg/required-icon.svg" alt=""> Povinný údaj
                            </div>
                        </footer>


                    </div>
                    <div class="col-cart">
                        <div class="cart cart--desktop">
                            <div class="icon">
                                <img src="http://grantexpert.test/assets/img/svg/cart.svg" alt="">
                            </div>

                            <div class="order">Objednávka služby</div>
                            <h3 class="title-secondary">Vypracovanie projektu</h3>
                            <div class="price-container">
                                <div class="tax-included">Konečná cena s DPH</div>
                                <div class="title-secondary">100 €</div>
                            </div>

                            <div class="button-center">
                                <button class="button-tertiary button-tertiary--padding button-continue">
                                    POKRAČOVAŤ
                                    <div class="button-scroll button-scroll--white button-scroll--right">
                                        <div class="icon-container"></div>
                                    </div>
                                </button>
                            </div>

                        </div>

                        <div class="required-info">
                            <img src="http://grantexpert.test/assets/img/svg/required-icon.svg" alt=""> Povinný údaj
                        </div>

                    </div>
                </div>
            </div>
        </section>



    </main>

    <?php include '../../_components/_footer.php';?>

