
<?php include '../../_components/_head.php';?>

<body class="page-404">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <section class="section-404 mt-9">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="text-container">
                            <h1 class="title-primary">Chyba 404</h1>
                            <h2 class="title-secondary">Ľutujeme. Stránka, ktorú hľadáte zdá sa neexistuje.</h2>

                            <p class="text-primary">
                                Môžete pokračovať návratom na hlavnú stránku, alebo sa pokúste nájsť hľadaný obsah prostredníctvom našich sekcií.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <img src="http://grantexpert.test/assets/img/svg/page-404.svg" alt="">
                    </div>
                </div>
            </div>
        </section>

        <section class="blog mt-9">
            <div class="container">

                <div class="row">
                    <div class="col-12">
                        <div class="carousel-container">
                            <div class="tns-carousel carousel-blog">
                                <div>
                                    <div class="card-animation card-border-animation">
                                        <div class="card card-border">
                                            <a href="#" class="tag">AKTUALITY</a>
                                            <a href="#">
                                                <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                            </a>
                                            <p>
                                                Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="card-animation card-border-animation">
                                        <div class="card card-border">
                                            <a href="#" class="tag">AKTUALITY</a>
                                            <a href="#">
                                                <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                            </a>
                                            <p>
                                                Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="card-animation card-border-animation">
                                        <div class="card card-border">
                                            <a href="#" class="tag">AKTUALITY</a>
                                            <a href="#">
                                                <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                            </a>
                                            <p>
                                                Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="card-animation card-border-animation">
                                        <div class="card card-border">
                                            <a href="#" class="tag">AKTUALITY</a>
                                            <a href="#">
                                                <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                            </a>
                                            <p>
                                                Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <button id="blog-carousel-prev" class="button-scroll button-scroll--left">
                                <div class="button-border--gray"></div>
                                <div class="button-border--white">
                                    <div class="icon-container"></div>
                                </div>
                            </button>
                            <button id="blog-carousel-next" class="button-scroll button-scroll--right">
                                <div class="button-border--gray"></div>
                                <div class="button-border--white">
                                    <div class="icon-container"></div>
                                </div>
                            </button>
                        </div>



                        <div class="button-center">
                            <a href="" class="button-primary">VŠETKY BLOGY</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </main>

    <?php include '../../_components/_footer.php';?>

