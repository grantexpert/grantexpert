
<?php include '../../_components/_head.php';?>

<body class="page-job-offer-detail page-grant-jobs">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li class="active">
                    <a href="#">Pracovné ponuky</a>
                </li>
                <li>
                    <a href="#">Pridať pracovnú ponuku</a>
                </li>
                <li>
                    <a href="#">Databáza grantových expertov</a>
                </li>
                <li>
                    <a href="#">Stať sa Grantovým expertom</a>
                </li>
                <li>
                    <a href="#">Blog</a>
                </li>
            </ul>
        </nav>


        <header class="card-title-header card-title-header--job-offer">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <div class="col-container">
                            <div class="col-card order-2 order-lg-1">
                                <div class="card-header-summary">
                                    <div class="text-small-label">Typ</div>
                                    <div class="value">Full time</div>
                                    <div class="text-small-label delimeter">Lokalita</div>
                                    <div class="value">Práca z domu</div>
                                    <div class="col-container icon-info-container">
                                        <div class="col-icon col-icon--euro">

                                        </div>
                                        <div class="col-info">
                                            <div class="text-small-label">Ohodnotenie</div>
                                            <div class="value">Od 1600 €</div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-title order-1 order-lg-2">
                                <div class="vertical-center">
                                    <h1 class="page-title title-secondary">Grantový špecialista</h1>
                                    <p class="description text-primary">
                                        Ministerstvo finnanií Slovenskej republiky
                                    </p>

                                    <div class="tag-container mt-4">
                                        <a href="#" class="tag">Financie</a>
                                        <a href="#" class="tag">Štátna správa</a>
                                        <a href="#" class="tag">Financie</a>
                                        <a href="#" class="tag">Štátna správa</a>
                                        <a href="#" class="tag">Štátna správa</a>
                                        <a href="#" class="tag">Financie</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-job-logo order-lg-3">
                                <div class="logo-container">
                                    <img src="http://grantexpert.test/assets/img/profesia-logo.jpg" alt="">
                                </div>

                                <div class="button-center button-center--right">
                                    <a href="#" class="button-tertiary button-tertiary--small-rounded button-tertiary--shadow">Mám záujem</a>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </header>


        <section class="row-line-grid">
            <div class="container">
                <div class="col-container">
                    <div class="col-title">
                        <h3>Náplň práce</h3>
                    </div>
                    <div class="col-content">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat magna vel mi congue condimentum. In dignissim ut lacus eu commodo. Nullam aliquam dui at nisl tincidunt sagittis. Pellentesque molestie nibh a arcu viverra dapibus. Praesent vestibulum velit in porta convallis. Suspendisse rhoncus nunc sit amet elit fringilla luctus. Donec efficitur odio fermentum arcu euismod, condimentum ornare risus porta. In semper eleifend nibh, nec tincidunt magna euismod eu. Praesent et sem eu lacus venenatis commodo. Fusce et diam maximus mi euismod consectetur. Fusce sit amet maximus nisl, id tincidunt arcu. Nunc ut mi vestibulum, pretium justo euismod, posuere mauris. In justo est, vestibulum non elementum a, interdum vitae est. Pellentesque tempus, sapien sed hendrerit dapibus, tellus lectus laoreet justo, vitae vestibulum orci elit vitae massa.
                        </p>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Požiadavky</h3>
                    </div>
                    <div class="col-content">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat magna vel mi congue condimentum. In dignissim ut lacus eu commodo. Nullam aliquam dui at nisl tincidunt sagittis. Pellentesque molestie nibh a arcu viverra dapibus. Praesent vestibulum velit in porta convallis. Suspendisse rhoncus nunc sit amet elit fringilla luctus. Donec efficitur odio fermentum arcu euismod, condimentum ornare risus porta. In semper eleifend nibh, nec tincidunt magna euismod eu. Praesent et sem eu lacus venenatis commodo. Fusce et diam maximus mi euismod consectetur. Fusce sit amet maximus nisl, id tincidunt arcu. Nunc ut mi vestibulum, pretium justo euismod, posuere mauris. In justo est, vestibulum non elementum a, interdum vitae est. Pellentesque tempus, sapien sed hendrerit dapibus, tellus lectus laoreet justo, vitae vestibulum orci elit vitae massa.
                        </p>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Ponúkame</h3>
                    </div>
                    <div class="col-content">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat magna vel mi congue condimentum. In dignissim ut lacus eu commodo. Nullam aliquam dui at nisl tincidunt sagittis. Pellentesque molestie nibh a arcu viverra dapibus. Praesent vestibulum velit in porta convallis. Suspendisse rhoncus nunc sit amet elit fringilla luctus. Donec efficitur odio fermentum arcu euismod, condimentum ornare risus porta. In semper eleifend nibh, nec tincidunt magna euismod eu. Praesent et sem eu lacus venenatis commodo. Fusce et diam maximus mi euismod consectetur. Fusce sit amet maximus nisl, id tincidunt arcu. Nunc ut mi vestibulum, pretium justo euismod, posuere mauris. In justo est, vestibulum non elementum a, interdum vitae est. Pellentesque tempus, sapien sed hendrerit dapibus, tellus lectus laoreet justo, vitae vestibulum orci elit vitae massa.
                        </p>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="button-center button-center--right">
                    <a href="#" class="button-tertiary button-tertiary--small-rounded button-tertiary--shadow">Mám záujem</a>
                </div>
            </div>
        </section>

        <section class="similar-offers mt-7">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <h2 class="title-secondary">Podobné ponuky</h2>


                        <div class="card-job-offer card-border-hover card-shadow-hover">
                            <div class="card-container">
                                <div class="col-container">
                                    <div class="col-left">
                                        <div class="col-container">
                                            <div class="col-image">
                                                <img src="http://grantexpert.test/assets/img/profesia-logo.jpg" alt="">
                                            </div>
                                            <div class="col-text">
                                                <a href="#"><h2>Mid / Senior Performance marketing špecialista</h2></a>
                                                <div class="subtitle">Ministerstvo financií Slovenskej republiky</div>
                                                <div class="tag-container">
                                                    <a href="#" class="tag">Financie</a>
                                                    <a href="#" class="tag">Štátna správa</a>
                                                    <a href="#" class="tag">Financie</a>
                                                    <button class="button-more"></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-right">
                                        <div class="label-value-container">
                                            <div class="label-value">
                                                <span class="label">Typ pozície:</span>
                                                <span class="value">Full time</span>
                                            </div>
                                            <div class="label-value">
                                                <span class="label">Lokalita:</span>
                                                <span class="value">Práca z domu</span>
                                            </div>
                                            <div class="label-value">
                                                <span class="label">Ohodnotenie:</span>
                                                <span class="value">od 1600 €</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-job-offer card-border-hover card-shadow-hover">
                            <div class="card-container">
                                <div class="col-container">
                                    <div class="col-left">
                                        <div class="col-container">
                                            <div class="col-image">
                                                <img src="http://grantexpert.test/assets/img/profesia-logo.jpg" alt="">
                                            </div>
                                            <div class="col-text">
                                                <a href="#"><h2>Mid / Senior Performance marketing špecialista</h2></a>
                                                <div class="subtitle">Ministerstvo financií Slovenskej republiky</div>
                                                <div class="tag-container">
                                                    <a href="#" class="tag">Financie</a>
                                                    <a href="#" class="tag">Štátna správa</a>
                                                    <a href="#" class="tag">Financie</a>
                                                    <button class="button-more"></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-right">
                                        <div class="label-value-container">
                                            <div class="label-value">
                                                <span class="label">Typ pozície:</span>
                                                <span class="value">Full time</span>
                                            </div>
                                            <div class="label-value">
                                                <span class="label">Lokalita:</span>
                                                <span class="value">Práca z domu</span>
                                            </div>
                                            <div class="label-value">
                                                <span class="label">Ohodnotenie:</span>
                                                <span class="value">od 1600 €</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-job-offer card-border-hover card-shadow-hover">
                            <div class="card-container">
                                <div class="col-container">
                                    <div class="col-left">
                                        <div class="col-container">
                                            <div class="col-image">
                                                <img src="http://grantexpert.test/assets/img/profesia-logo.jpg" alt="">
                                            </div>
                                            <div class="col-text">
                                                <a href="#"><h2>Mid / Senior Performance marketing špecialista</h2></a>
                                                <div class="subtitle">Ministerstvo financií Slovenskej republiky</div>
                                                <div class="tag-container">
                                                    <a href="#" class="tag">Financie</a>
                                                    <a href="#" class="tag">Štátna správa</a>
                                                    <a href="#" class="tag">Financie</a>
                                                    <button class="button-more"></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-right">
                                        <div class="label-value-container">
                                            <div class="label-value">
                                                <span class="label">Typ pozície:</span>
                                                <span class="value">Full time</span>
                                            </div>
                                            <div class="label-value">
                                                <span class="label">Lokalita:</span>
                                                <span class="value">Práca z domu</span>
                                            </div>
                                            <div class="label-value">
                                                <span class="label">Ohodnotenie:</span>
                                                <span class="value">od 1600 €</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="center-right mt-4">
                            <a href="#" class="arrow-double-link">
                                <div class="arrow-right"></div>
                                <div class="label">Všetky ponuky</div>
                            </a>
                        </div>




                    </div>
                </div>
            </div>

        </section>


    </main>

    <?php include '../../_components/_footer.php';?>

