
<?php include '../../_components/_head.php';?>

<body class="page-grant-database">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">
        <div class="container">
            <h1 class="page-title title-secondary">Všetky granty na jednom mieste</h1>
        </div>

        <section class="filter-fixed-header">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3">
                                <button class="button-scroll button-scroll--white">
                                    <div class="icon-container"></div>
                                </button>

                                <h3>Filter</h3>
                            </div>
                            <div class="col-lg-9">
                                <div class="text-small-label">Máte zvolené vyhľadávacie parametre</div>
                                <button class="button-primary">Vynulovať filter</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="filter">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <button class="button-filter-open"><span class="label">Filter</span><img src="http://grantexpert.test/assets/img/svg/filter.svg" alt=""></button>
                    </div>

                    <div class="container">
                        <div class="filter-container">
                            <div class="row">
                                <div class="col-xl-8">
                                    <h3>Filter</h3>
                                    <div class="input-search-container">
                                        <input class="input-search" type="text" placeholder="Zadajte hľadaný výraz">
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 col-lg-6">
                                            <div class="custom-form-control selectbox">
                                                <input type="hidden" name="grant-challenge">
                                                <div class="selectbox-label"><span class="col-container"><span class="number">113</span> <span class="text">Aktuálnych výziev</span></span></div>
                                                <ul>
                                                    <li data-value="current"><span class="col-container"><span class="number">113</span> <span class="text">Aktuálnych výziev</span></span></li>
                                                    <li data-value="planned"><span class="col-container"><span class="number">95</span> <span class="text">Plánovaných výziev</span></span></li>
                                                    <li data-value="closed"><span class="col-container"><span class="number">941</span> <span class="text">Uzatvorených výziev</span></span></li>
                                                </ul>
                                            </div>
                                            <div class="custom-form-control checkbox-popup">
                                                <div class="checkbox-label" data-bs-toggle="modal" data-bs-target="#organization-modal">Organizácia</div>
                                            </div>

                                            <div class="modal fade" id="organization-modal" tabindex="-1" aria-labelledby="organization-modal" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        <div class="content-container">
                                                            <div class="col-container">
                                                                <div class="col-title">
                                                                    <h4 class="title-secondary">Organizácia</h4>
                                                                </div>
                                                                <div class="col-select">
                                                                    <div class="select-all">Označiť všetko</div>
                                                                    <div class="deselect-all">Odznačiť všetko</div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xl-6">
                                                                    <div class="checkbox">
                                                                        <input name="podnikatelia" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Podnikatelia</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="skoly" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Školy</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Jednotlivci</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xl-6">
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Mestá a obce</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Mimovládne organizácie</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-select col-select--mobile">
                                                                <div class="select-all">Označiť všetko</div>
                                                                <div class="deselect-all">Odznačiť všetko</div>
                                                            </div>
                                                            <div class="button-center">
                                                                <button class="button-tertiary button-tertiary--padding" data-bs-dismiss="modal">POTVRDIŤ</button>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6">
                                            <div class="custom-form-control checkbox-popup">
                                                <div class="checkbox-label" data-bs-toggle="modal" data-bs-target="#areas-of-interest-modal">Oblasti záujmu</div>
                                            </div>

                                            <div class="modal fade" id="areas-of-interest-modal" tabindex="-1" aria-labelledby="areas-of-interest-modal" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        <div class="content-container">
                                                            <div class="col-container">
                                                                <div class="col-title">
                                                                    <h4 class="title-secondary">Oblasti záujmu</h4>
                                                                </div>
                                                                <div class="col-select">
                                                                    <div class="select-all">Označiť všetko</div>
                                                                    <div class="deselect-all">Odznačiť všetko</div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="checkbox">
                                                                        <input name="rozvoj-podnikania" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Rozvoj podnikania</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="vyskum-vyvoj" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Výskum a vývoj</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="vodne-hospodarstvo" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Vodné hospodárstvo a odpadové hospodárstvo</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="zamestnanost" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Zamestnanosť a tvorba pracovných miest</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="energeticka-efektivnost-budov" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Energetická efektívnosť budov</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Obnoviteľné zdroje energie</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Kultúra, cestovný ruch a voľnočasová infraštruktúra</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Vzdelávacie aktivity</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Sociálne služby a zdravotníctvo  </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Analýzy, plány, stratégie  </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Informačno-komunikačné technológie  </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Poľnohospodárstvo, rybné hospodárstvo a lesníctvo  </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Ochrana ŽP a environmentálne riziká   </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Občianska vybavenosť   </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Dopravná infraštruktúra a služby  </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Zelená mobilita a cyklistická infraštruktúra  </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-select col-select--mobile">
                                                                <div class="select-all">Označiť všetko</div>
                                                                <div class="deselect-all">Odznačiť všetko</div>
                                                            </div>
                                                            <div class="button-center">
                                                                <button class="button-tertiary button-tertiary--padding" data-bs-dismiss="modal">POTVRDIŤ</button>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="custom-form-control checkbox-popup">
                                                <div class="checkbox-label" data-bs-toggle="modal" data-bs-target="#location-modal">Lokalita</div>
                                            </div>

                                            <div class="modal fade" id="location-modal" tabindex="-1" aria-labelledby="location-modal" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        <div class="content-container">
                                                            <div class="col-container">
                                                                <div class="col-title">
                                                                    <h4 class="title-secondary">Lokalita</h4>
                                                                </div>
                                                                <div class="col-select">
                                                                    <div class="select-all">Označiť všetko</div>
                                                                    <div class="deselect-all">Odznačiť všetko</div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Banskobystrický kraj</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Bratislavský kraj</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Košický kraj </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Nitriansky kraj</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Prešovský kraj </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Trenčiansky kraj</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Trnavský kraj</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Žilinský kraj</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-select col-select--mobile">
                                                                <div class="select-all">Označiť všetko</div>
                                                                <div class="deselect-all">Odznačiť všetko</div>
                                                            </div>
                                                            <div class="button-center">
                                                                <button class="button-tertiary button-tertiary--padding" data-bs-dismiss="modal">POTVRDIŤ</button>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="checkbox checkbox--white">
                                        <input name="" type="checkbox">
                                        <div class="label">
                                            <div class="inline-container">
                                                <div class="col-container">
                                                    <div class="col-text">Zobrazit iba nové</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-xl-4">
                                    <h3>Vyhľadávané kategórie</h3>
                                    <div class="tag-search-container">
                                        <div class="tag-search-category">
                                            <div class="tag-search">
                                                Kultúra, cestovný ruch a voľnočasová infraštruktúra

                                                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
                                                    <g data-name="Group 1809" transform="translate(-77)">
                                                        <circle data-name="Ellipse 23" cx="8.5" cy="8.5" r="8.5" transform="translate(77)" style="fill:#fff"/>
                                                        <g data-name="Icon feather-plus">
                                                            <path data-name="Path 19" d="M0 0v8.131" transform="rotate(45 37.397 109.49)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                            <path data-name="Path 20" d="M0 0h8.131" transform="rotate(45 34.522 102.55)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="tag-search">
                                                Ochrana ŽP a environmentálne riziká

                                                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
                                                    <g data-name="Group 1809" transform="translate(-77)">
                                                        <circle data-name="Ellipse 23" cx="8.5" cy="8.5" r="8.5" transform="translate(77)" style="fill:#fff"/>
                                                        <g data-name="Icon feather-plus">
                                                            <path data-name="Path 19" d="M0 0v8.131" transform="rotate(45 37.397 109.49)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                            <path data-name="Path 20" d="M0 0h8.131" transform="rotate(45 34.522 102.55)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="tag-search">
                                                Sociálne služby a zdravotníctvo

                                                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
                                                    <g data-name="Group 1809" transform="translate(-77)">
                                                        <circle data-name="Ellipse 23" cx="8.5" cy="8.5" r="8.5" transform="translate(77)" style="fill:#fff"/>
                                                        <g data-name="Icon feather-plus">
                                                            <path data-name="Path 19" d="M0 0v8.131" transform="rotate(45 37.397 109.49)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                            <path data-name="Path 20" d="M0 0h8.131" transform="rotate(45 34.522 102.55)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="tag-search">
                                                Rozvoj podnikania

                                                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
                                                    <g data-name="Group 1809" transform="translate(-77)">
                                                        <circle data-name="Ellipse 23" cx="8.5" cy="8.5" r="8.5" transform="translate(77)" style="fill:#fff"/>
                                                        <g data-name="Icon feather-plus">
                                                            <path data-name="Path 19" d="M0 0v8.131" transform="rotate(45 37.397 109.49)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                            <path data-name="Path 20" d="M0 0h8.131" transform="rotate(45 34.522 102.55)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="tag-search">
                                                Vzdelávacie aktivity

                                                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
                                                    <g data-name="Group 1809" transform="translate(-77)">
                                                        <circle data-name="Ellipse 23" cx="8.5" cy="8.5" r="8.5" transform="translate(77)" style="fill:#fff"/>
                                                        <g data-name="Icon feather-plus">
                                                            <path data-name="Path 19" d="M0 0v8.131" transform="rotate(45 37.397 109.49)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                            <path data-name="Path 20" d="M0 0h8.131" transform="rotate(45 34.522 102.55)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tag-search-category">
                                        <div class="tag-search">
                                            Prešovský kraj

                                            <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
                                                <g data-name="Group 1809" transform="translate(-77)">
                                                    <circle data-name="Ellipse 23" cx="8.5" cy="8.5" r="8.5" transform="translate(77)" style="fill:#fff"/>
                                                    <g data-name="Icon feather-plus">
                                                        <path data-name="Path 19" d="M0 0v8.131" transform="rotate(45 37.397 109.49)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                        <path data-name="Path 20" d="M0 0h8.131" transform="rotate(45 34.522 102.55)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="tag-search">
                                            Trenčiansky kraj

                                            <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
                                                <g data-name="Group 1809" transform="translate(-77)">
                                                    <circle data-name="Ellipse 23" cx="8.5" cy="8.5" r="8.5" transform="translate(77)" style="fill:#fff"/>
                                                    <g data-name="Icon feather-plus">
                                                        <path data-name="Path 19" d="M0 0v8.131" transform="rotate(45 37.397 109.49)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                        <path data-name="Path 20" d="M0 0h8.131" transform="rotate(45 34.522 102.55)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="button-center">
                                        <button class="button-primary">Vynulovať filter</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="filter-selected">
                            <div class="label">Máte zvolené vyhľadávacie parametre</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="search-results">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">

                        <section class="ordering">
                            <img src="http://grantexpert.test/assets/img/svg/three-dots.svg" alt="three dots" class="three-dots">

                            <div class="row">
                                <div class="col-12">
                                    <div class="col-container">
                                        <div class="text-small-label">28 výziev</div>
                                        <div class="tab-container">
                                            <div class="order-tab"><div class="inner-container">Od najvyššej dotácie</div></div>
                                            <div class="order-tab"><div class="inner-container">Od najvzdialnejšieho termínu podania</div></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <div class="card-content-two-col card-border-hover card-shadow-hover">
                            <div class="card-container">

                                <div class="tag-container">
                                    <a href="#" class="tag-rectangle">Covid 19</a>
                                    <a href="" class="tag-rectangle"><img src="http://grantexpert.test/assets/img/svg/star-blank-white.svg" alt="star-blank"> Nová výzva</a>
                                </div>

                                <div class="row">
                                    <div class="col-xl-8">
                                        <a href="#"><h2>Podpora nových a existujúcich sociálnych služieb, sociálnoprávnej ochrany detí v zariadeniach na komunitnej úrovni (2021)</h2></a>
                                        <p>
                                            Cieľom výzvy je podporiť prechod poskytovania sociálnych služieb a zabezpečenia výkonu opatrení sociálnoprávnej ochrany detí a sociálnej kurately v zariadení z inštitucionálnej formy na komunitnú a podporiť rozvoj služieb starostlivosti o dieťa do troch rokov veku na ...
                                        </p>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="row">
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Výška dotácie
                                                        </div>
                                                        <div class="value">max. 2 000 000</div>
                                                    </div>
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Miera dotácie
                                                        </div>
                                                        <div class="value">max. 95%</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="row">
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Ostáva dní
                                                        </div>
                                                        <div class="value">48</div>
                                                    </div>
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="date">
                                                            <div class="date-from">Od 1.9.2021</div>
                                                            <div class="date-to">Do Vyčerpania financií</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="card-content-two-col card-border-hover card-shadow-hover">
                            <div class="card-container">

                                <div class="tag-container">
                                    <a href="#" class="tag-rectangle">Covid 19</a>
                                    <a href="" class="tag-rectangle"><img src="http://grantexpert.test/assets/img/svg/star-blank-white.svg" alt="star-blank"> Nová výzva</a>
                                </div>

                                <div class="row">
                                    <div class="col-xl-8">
                                        <a href="#"><h2>Podpora nových a existujúcich sociálnych služieb, sociálnoprávnej ochrany detí v zariadeniach na komunitnej úrovni (2021)</h2></a>
                                        <p>
                                            Cieľom výzvy je podporiť prechod poskytovania sociálnych služieb a zabezpečenia výkonu opatrení sociálnoprávnej ochrany detí a sociálnej kurately v zariadení z inštitucionálnej formy na komunitnú a podporiť rozvoj služieb starostlivosti o dieťa do troch rokov veku na ...
                                        </p>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="row">
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Výška dotácie
                                                        </div>
                                                        <div class="value">max. 2 000 000</div>
                                                    </div>
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Miera dotácie
                                                        </div>
                                                        <div class="value">max. 95%</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="row">
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Ostáva dní
                                                        </div>
                                                        <div class="value">48</div>
                                                    </div>
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="date">
                                                            <div class="date-from">Od 1.9.2021</div>
                                                            <div class="date-to">Do Vyčerpania financií</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <section class="pager">
                            <a href="#">
                                <div class="arrow-double-hover arrow-left"></div>
                            </a>

                            <nav>
                                <ul>
                                    <li class="page active"><a href="#">1</a></li>
                                    <li class="page"><a href="#">2</a></li>
                                    <li class="page"><a href="#">3</a></li>
                                    <li class="page mobile-invisible"><a href="#">4</a></li>
                                    <li class="page mobile-invisible"><a href="#">5</a></li>
                                    <li>...</li>
                                    <li class="page"><a href="#">12</a></li>
                                </ul>
                            </nav>
                            <a href="#">
                                <div class="arrow-double-hover arrow-right"></div>
                            </a>
                        </section>
                    </div>
                </div>
            </div>
        </section>

        <section class="grant-radar">
            <div class="container">
                <div class="background-container">
                    <button class="button-close--blue"></button>
                    <div class="grant-radar-container">
                        <div class="row">
                            <div class="col-lg-5 col-xl-3">
                                <img class="image" src="http://grantexpert.test/assets/img/svg/grant-radar.svg" alt="grant radar">
                            </div>
                            <div class="col-lg-7 col-xl-6">
                                <h3 class="title-secondary">Grantový radar</h3>
                                <p>
                                    Chcem dostávať vždy pravidelné informácie o možnostiach financovania svojich projektov.
                                </p>
                            </div>
                            <div class="offset-lg-5 col-lg-7 offset-xl-0 col-xl-3">
                                <div class="button-container">
                                    <a href="" class="button-tertiary button-tertiary--shadow">Aktivovať radar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>

    <?php include '../../_components/_footer.php';?>

