
<?php include '../../_components/_head.php';?>

<body class="page-blog-list page-grant-education">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li>
                    <a href="#">E-LEANING KURZY</a>
                </li>
                <li>
                    <a href="#">WEBINÁRE</a>
                </li>
                <li>
                    <a href="#">ŠKOLENIA NA MIERU</a>
                </li>
                <li>
                    <a href="#">VIDEOSLOVNÍK</a>
                </li>
                <li>
                    <a href="#">GRANTOVÉ ZDROJE</a>
                </li>
                <li class="active">
                    <a href="#">BLOG</a>
                </li>
            </ul>
        </nav>

        <section class="title-description">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <h1 class="title-primary">Blog</h1>
                            </div>
                            <div class="col-lg-6">
                                <p class="text-primary">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum consequat euismod. Vivamus maximus ut odio vulputate efficitur.
                                </p>
                            </div>
                        </div>

                        <div class="input-search-container">
                            <input class="input-search" type="text" placeholder="Zadajte hľadaný výraz">
                        </div>

                        <section class="tag-list">
                            <a href="#" class="tag-small-rounded tag-small-rounded--inverted active">Všetky</a>
                            <a href="#" class="tag-small-rounded tag-small-rounded--inverted">Aktuality</a>
                            <a href="#" class="tag-small-rounded tag-small-rounded--inverted">Školenia</a>
                            <a href="#" class="tag-small-rounded tag-small-rounded--inverted">Granty</a>
                            <a href="#" class="tag-small-rounded tag-small-rounded--inverted">Granové joby</a>
                        </section>

                    </div>
                </div>
            </div>
        </section>

        <section class="blog blog-list">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-lg-4">
                        <div class="card-animation card-border-animation">
                            <div class="card card-border">
                                <a href="#" class="tag">AKTUALITY</a>
                                <a href="#">
                                    <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                </a>
                                <p>
                                    Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="card-animation card-border-animation">
                            <div class="card card-border">
                                <a href="#" class="tag">AKTUALITY</a>
                                <a href="#">
                                    <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                </a>
                                <p>
                                    Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="card-animation card-border-animation">
                            <div class="card card-border">
                                <a href="#" class="tag">AKTUALITY</a>
                                <a href="#">
                                    <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                </a>
                                <p>
                                    Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-4">
                        <div class="card-animation card-border-animation">
                            <div class="card card-border">
                                <a href="#" class="tag">AKTUALITY</a>
                                <a href="#">
                                    <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                </a>
                                <p>
                                    Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="card-animation card-border-animation">
                            <div class="card card-border">
                                <a href="#" class="tag">AKTUALITY</a>
                                <a href="#">
                                    <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                </a>
                                <p>
                                    Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="card-animation card-border-animation">
                            <div class="card card-border">
                                <a href="#" class="tag">AKTUALITY</a>
                                <a href="#">
                                    <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                </a>
                                <p>
                                    Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <section class="pager">
                    <a href="#">
                        <div class="arrow-double-hover arrow-left"></div>
                    </a>

                    <nav>
                        <ul>
                            <li class="page active"><a href="#">1</a></li>
                            <li class="page"><a href="#">2</a></li>
                            <li class="page"><a href="#">3</a></li>
                            <li class="page mobile-invisible"><a href="#">4</a></li>
                            <li class="page mobile-invisible"><a href="#">5</a></li>
                            <li>...</li>
                            <li class="page"><a href="#">12</a></li>
                        </ul>
                    </nav>
                    <a href="#">
                        <div class="arrow-double-hover arrow-right"></div>
                    </a>
                </section>

            </div>
        </section>








    </main>

    <?php include '../../_components/_footer.php';?>

