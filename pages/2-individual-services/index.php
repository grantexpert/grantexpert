
<?php include '../../_components/_head.php';?>

<body class="page-individual-services page-services">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li class="active">
                    <a href="http://grantexpert.test/pages/3-service-packages">Individuálne služby</a>
                </li>
                <li>
                    <a href="#">Balíky služieb</a>
                </li>
                <li>
                    <a href="#">Konzultácia s grantexpertom</a>
                </li>
                <li>
                    <a href="#">Vypracovanie projektu</a>
                </li>
                <li>
                    <a href="#">Privátny grantový konzultant</a>
                </li>
            </ul>
        </nav>

        <section class="menu-individual-services">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <div class="col-container">
                            <div class="col-title">
                                <h1 class="title-primary">Individuálne <br>služby</h1>
                            </div>
                            <div class="col-menu-left">
                                <nav>
                                    <ul>
                                        <li>
                                            <a href="#" class="arrow-double-link">
                                                <div class="arrow-right"></div>
                                                <div class="label">Grantový radar</div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="arrow-double-link">
                                                <div class="arrow-right"></div>
                                                <div class="label">Overenie oprávnenosti</div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="arrow-double-link">
                                                <div class="arrow-right"></div>
                                                <div class="label">Konzultácia s grantexpertom</div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="arrow-double-link">
                                                <div class="arrow-right"></div>
                                                <div class="label">Webináre</div>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="col-menu-right">
                                <nav>
                                    <ul>
                                        <li>
                                            <a href="#" class="arrow-double-link">
                                                <div class="arrow-right"></div>
                                                <div class="label">Posúdenie projektového nápadu</div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="arrow-double-link">
                                                <div class="arrow-right"></div>
                                                <div class="label">Vypracovanie projektu</div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="arrow-double-link">
                                                <div class="arrow-right"></div>
                                                <div class="label">Personálne zastrešenie projektu</div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="arrow-double-link">
                                                <div class="arrow-right"></div>
                                                <div class="label">E-learningy</div>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="cards">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-animation card-shadow-animation">
                            <div class="card card-shadow">
                                <div class="card-icon-container">
                                    <img src="http://grantexpert.test/assets/img/svg/grant-radar-icon.svg" alt="" class="icon">
                                </div>

                                <h2 class="title-primary">Grantový radar</h2>
                                <p>Pre sledovanie relevantných grantov stačí zadať váš email. My sa postaráme, aby ste boli vždy včas informovaní.</p>
                                <div class="input-animation input-with-icon">
                                    <div class="input-call-to-action">
                                        <input type="text" class="form-control">
                                    </div>
                                    <div class="icon-container">
                                        <img class="icon" src="http://grantexpert.test/assets/img/svg/email.svg" alt="locker">
                                    </div>
                                </div>

                                <div class="footer-button-container">
                                    <footer class="mobile-full-width-col">
                                        <div class="col-container">
                                            <div class="price-container">
                                                <div class="tax-included">Cena s DPH</div>
                                                <div class="title-secondary color-sun price">Zadarmo</div>
                                            </div>
                                        </div>
                                    </footer>

                                    <div class="button-center">
                                        <a href="#" class="arrow-double-link">
                                            <div class="arrow-right"></div>
                                            <div class="label">Viac info</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card-animation card-shadow-animation">
                            <div class="card card-shadow">
                                <div class="card-icon-container">
                                    <img src="http://grantexpert.test/assets/img/svg/eligibility.svg" alt="" class="icon">
                                </div>

                                <h2 class="title-primary">Overenie oprávnenosti</h2>
                                <p>Úplne bezplatne ponúkame základné overenie oprávnenosti vašej živnosti, alebo spoločnosti</p>
                                <div class="input-animation">
                                    <div class="input-call-to-action">
                                        <input type="text" class="form-control" placeholder="IČO, alebo názov spoločnosti">
                                    </div>
                                </div>
                                <div class="footer-button-container">
                                    <footer class="mobile-full-width-col">
                                        <div class="col-container">
                                            <div class="price-container">
                                                <div class="tax-included">Cena s DPH</div>
                                                <div class="title-secondary color-sun price">Zadarmo</div>
                                            </div>
                                        </div>
                                    </footer>

                                    <div class="button-center">
                                        <a href="#" class="arrow-double-link">
                                            <div class="arrow-right"></div>
                                            <div class="label">Viac info</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-animation card-shadow-animation">
                            <div class="card card-shadow">
                                <div class="card-icon-container">
                                    <img src="http://grantexpert.test/assets/img/svg/consultation.svg" alt="" class="icon">
                                </div>

                                <h2 class="title-primary">Konzultácia s <br>grantexpertom</h2>
                                <p>Potrebujete sa zorientovať v konkrétnych výzvach a dotáciách? Alebo máte projekt a radi by ste vaše otázky prešli so skúseným odborníkom? Využite grantové poradenstvo formou konzultácie priamo s našimi GrantExpertmi.</p>

                                <div class="footer-button-container">
                                    <footer>
                                        <div class="col-container">
                                            <div class="price-container">
                                                <div class="tax-included">Cena s DPH</div>
                                                <div class="title-secondary color-sun price">123 €</div>
                                            </div>
                                            <div class="button-container">
                                                <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                            </div>
                                        </div>
                                    </footer>

                                    <div class="button-center">
                                        <a href="#" class="arrow-double-link">
                                            <div class="arrow-right"></div>
                                            <div class="label">Viac info</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="card-animation card-shadow-animation">
                            <div class="card card-shadow">
                                <div class="card-icon-container">
                                    <img src="http://grantexpert.test/assets/img/svg/idea.svg" alt="" class="icon">
                                </div>

                                <h2 class="title-primary">Posúdenie <br>projektového nápadu</h2>
                                <p>Máte v hlave rôzne nápady na projekt a neviete z čoho ich financovať? Môžete sa vôbec o finančný príspevok uchádzať? Zadajte vaše projektové nápady a naši experti vám na tieto otázky dajú odpoveď.</p>

                                <div class="footer-button-container">
                                    <footer>
                                        <div class="col-container">
                                            <div class="price-container">
                                                <div class="tax-included">Cena s DPH</div>
                                                <div class="title-secondary color-sun price">123 €</div>
                                            </div>
                                            <div class="button-container">
                                                <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                            </div>
                                        </div>
                                    </footer>

                                    <div class="button-center">
                                        <a href="#" class="arrow-double-link">
                                            <div class="arrow-right"></div>
                                            <div class="label">Viac info</div>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-animation card-shadow-animation">
                            <div class="card card-shadow">
                                <div class="card-icon-container">
                                    <img src="http://grantexpert.test/assets/img/svg/pencil.svg" alt="" class="icon">
                                </div>

                                <h2 class="title-primary">Vypracovanie <br>projektu</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rhoncus viverra justo tristique blandit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

                                <div class="footer-button-container">
                                    <footer class="mobile-full-width-col">
                                        <div class="col-container">
                                            <div class="price-container">
                                                <div class="tax-included">Cena s DPH</div>
                                                <div class="title-secondary color-sun price">Od projektu</div>
                                            </div>
                                            <div class="button-container">
                                                <a href="#" class="button-secondary">Vyžiadať ponuku</a>
                                            </div>
                                        </div>
                                    </footer>

                                    <div class="button-center">
                                        <a href="#" class="arrow-double-link">
                                            <div class="arrow-right"></div>
                                            <div class="label">Viac info</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="card-animation card-shadow-animation">
                            <div class="card card-shadow">
                                <div class="card-icon-container">
                                    <img src="http://grantexpert.test/assets/img/svg/hr.svg" alt="" class="icon">
                                </div>

                                <h2 class="title-primary">Personálne zastrešenie <br>projektu</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rhoncus viverra justo tristique blandit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

                                <div class="footer-button-container">
                                    <footer class="mobile-full-width-col">
                                        <div class="col-container">
                                            <div class="price-container">
                                                <div class="tax-included">Cena s DPH</div>
                                                <div class="title-secondary color-sun price">Od projektu</div>
                                            </div>
                                            <div class="button-container">
                                                <a href="#" class="button-secondary">Vyžiadať ponuku</a>
                                            </div>
                                        </div>
                                    </footer>

                                    <div class="button-center">
                                        <a href="#" class="arrow-double-link">
                                            <div class="arrow-right"></div>
                                            <div class="label">Viac info</div>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-animation card-shadow-animation footer-on-hover">
                            <div class="card card-shadow">
                                <div class="card-icon-container">
                                    <img src="http://grantexpert.test/assets/img/svg/webinars.svg" alt="" class="icon">
                                </div>

                                <h2 class="title-primary">Webináre</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rhoncus viverra justo tristique blandit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

                                <div class="footer-button-container">
                                    <footer class="mobile-full-width-col">
                                        <div class="col-container">
                                            <div class="price-container">
                                                <div class="tax-included">Cena s DPH</div>
                                                <div class="title-secondary color-sun price">Od projektu</div>
                                            </div>
                                            <div class="button-container">
                                                <a href="#" class="button-secondary">Vyžiadať ponuku</a>
                                            </div>
                                        </div>
                                    </footer>

                                    <div class="button-center">
                                        <a href="#" class="arrow-double-link">
                                            <div class="arrow-right"></div>
                                            <div class="label">Ponuka webinárov</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card-animation card-shadow-animation footer-on-hover">
                            <div class="card card-shadow">
                                <div class="card-icon-container">
                                    <img src="http://grantexpert.test/assets/img/svg/e-learning.svg" alt="" class="icon">
                                </div>

                                <h2 class="title-primary">E-learningy</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rhoncus viverra justo tristique blandit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

                                <div class="footer-button-container">
                                    <footer class="mobile-full-width-col">
                                        <div class="col-container">
                                            <div class="price-container">
                                                <div class="tax-included">Cena s DPH</div>
                                                <div class="title-secondary color-sun price">Od projektu</div>
                                            </div>
                                            <div class="button-container">
                                                <a href="#" class="button-secondary">Vyžiadať ponuku</a>
                                            </div>
                                        </div>
                                    </footer>

                                    <div class="button-center">
                                        <a href="#" class="arrow-double-link">
                                            <div class="arrow-right"></div>
                                            <div class="label">Ponuka E-learningov</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>


            </div>
        </section>

        <section class="service-packages">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">

                        <h2 class="title-secondary text-center">Potrebujete komplexnejší balík?</h2>
                        <p class="title-description">
                            Vyberte si z našich balíkov, ktoré tvoria vhodný mix pre vaše aktuálne potreby v oblasti získavania grantov.
                        </p>

                        <div class="row">
                            <div class="col-md-6 col-xl-3">
                                <a href="#">
                                    <div class="card-animation card-shadow-animation">
                                        <div class="card card-shadow">
                                            <div class="subtitle text-secondary text-center">Pre rýchlu orientáciu</div>
                                            <h3 class="title-primary">Free</h3>
                                            <div class="tax-included">CENA S DPH</div>
                                            <div class="title-secondary text-blue">0 €/rok</div>
                                            <ul class="check-list small-padding text-secondary">
                                                <li>Automatické zasielanie notifikácií o nových výzvach</li>
                                                <li>Neobmedzený prístup do databázy grantov a dotácií</li>
                                                <li>Overenie oprávnenosti</li>
                                            </ul>

                                        </div>
                                        <div class="button-secondary">OBJEDNAŤ</div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-xl-3">
                                <a href="#">
                                    <div class="card-animation card-shadow-animation">
                                        <div class="card card-shadow">
                                            <div class="subtitle text-secondary text-center">Pre prvé granty</div>
                                            <h3 class="title-primary">Basic</h3>
                                            <div class="tax-included">CENA S DPH</div>
                                            <div class="title-secondary text-blue">34,90 €/rok</div>
                                            <ul class="check-list small-padding text-secondary">
                                                <li>Automatické zasielanie notifikácií o nových výzvach</li>
                                                <li>Neobmedzený prístup do databázy grantov a dotácií</li>
                                                <li>Overenie oprávnenosti</li>
                                            </ul>

                                        </div>
                                        <div class="button-secondary">OBJEDNAŤ</div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-xl-3">
                                <a href="#">
                                    <div class="card-animation card-shadow-animation">
                                        <div class="card card-shadow">
                                            <div class="subtitle text-secondary text-center">Pre pokročilé vyhľadávanie</div>
                                            <h3 class="title-primary">Medium</h3>
                                            <div class="tax-included">CENA S DPH</div>
                                            <div class="title-secondary text-blue">69,90 €/rok</div>
                                            <ul class="check-list small-padding text-secondary">
                                                <li>Neobmedzený prístup do databázy grantov a dotácií</li>
                                                <li>Archív bezplatných webinárov</li>
                                                <li>Vyhľadanie dotácie k 1 vášmu projektu</li>
                                            </ul>

                                        </div>
                                        <div class="button-secondary">OBJEDNAŤ</div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-xl-3">
                                <a href="#">
                                    <div class="card-animation card-shadow-animation card-shadow-animation--gold">
                                        <div class="card card-shadow">
                                            <div class="subtitle text-secondary text-center">Pre aktívnych riešiteľov</div>
                                            <h3 class="title-primary">Profi</h3>
                                            <div class="tax-included">CENA S DPH</div>
                                            <div class="title-secondary text-blue">149,90 €/rok</div>
                                            <ul class="check-list small-padding text-secondary">
                                                <li>Neobmedzený prístup do databázy grantov a dotácií</li>
                                                <li>Archív bezplatných webinárov</li>
                                                <li>Vyhľadanie dotácie k 1 vášmu projektu</li>
                                            </ul>

                                        </div>
                                        <div class="button-secondary">OBJEDNAŤ</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="button-center">
                            <a href="" class="button-primary"><div class="vertical-center">POROVNAŤ BALÍKY</div></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>

    <?php include '../../_components/_footer.php';?>

