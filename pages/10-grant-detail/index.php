
<?php include '../../_components/_head.php';?>

<body class="page-grant-detail">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">


        <header class="grant-header">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <div class="col-container">
                            <div class="col-card order-2 order-lg-1">
                                <div class="card-header-summary">
                                    <div class="text-small-label">Výška dotácie</div>
                                    <div class="value">max. 2 000 000</div>
                                    <div class="text-small-label delimeter">Miera dotácie</div>
                                    <div class="value">Závisí od podprogramu</div>
                                    <div class="col-container icon-info-container">
                                        <div class="col-icon">

                                        </div>
                                        <div class="col-info">
                                            <div class="text-small-label">Ostáva dní</div>
                                            <div class="value">48</div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-title order-1 order-lg-2">
                                <div class="tag-container">
                                    <a href="#" class="tag-rectangle">Covid 19</a>
                                    <a href="" class="tag-rectangle"><img src="http://grantexpert.test/assets/img/svg/star-blank-white.svg" alt="star-blank"> Nová výzva</a>
                                </div>
                                <h1 class="page-title title-secondary">Zvyšovanie atraktivity a prepravnej kapacity cyklistickej dopravy na celkovom počte prepravených osôb</h1>
                                <div class="date date-desktop">
                                    <img src="http://grantexpert.test/assets/img/svg/calendar-wider.svg" alt="calendar">
                                    <div class="date-from">Od 1.9.2021</div>
                                    <div class="date-to">Do Vyčerpania financií</div>
                                </div>
                            </div>
                            <div class="date date-mobile order-3">
                                <img src="http://grantexpert.test/assets/img/svg/calendar-wider.svg" alt="calendar">
                                <div class="date-from">Od 1.9.2021</div>
                                <div class="date-to">Do Vyčerpania financií</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="row-line-grid premium-content">
            <div class="container">
                <div class="col-container">
                    <div class="col-title">
                        <h3>Podporované oblasti:</h3>
                    </div>
                    <div class="col-content">
                        <p>
                            Cieľom výzvy je podpora podnikov pri rozvoji inovačných zelených technológií, procesov, riešení, produktov alebo služieb a v inovovaní verejnoprospešných technológií, riešení, procesov a pomoci starším a chorým osobám.
                        </p>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Oprávnené územie:</h3>
                    </div>
                    <div class="col-content">
                        <p>
                            Trnavský kraj, Bratislavský kraj, Košický kraj, Nitriansky kraj, Banskobystrický kraj, Žilinský kraj, Prešovský kraj, Trenčiansky kraj
                        </p>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Oprávnení žiadatelia:</h3>
                    </div>
                    <div class="col-content">
                        <p>
                            Malé a stredné podniky, Veľké podniky
                        </p>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Ďalšie informácie:</h3>
                    </div>
                    <div class="col-content">
                        <h4>Oprávnení žiadatelia:</h4>
                        <p>
                            Malé, stredné a veľké podniky s menej ako 25% verejným vlastníctvom založené ako právnické
                            osoby na Slovensku.
                            Oprávnení žiadatelia musia byť zriadení minimálne tri fiškálne roky pred stanoveným dátumom
                            uzávierky na predkladanie žiadostí o projekt.
                        </p>

                        <h4>Oprávnení partneri:</h4>
                        <p>
                            Akákoľvek právnická osoba, t. j. verejný alebo súkromný subjekt, komerčný alebo nekomerčný,
                            ako aj mimovládne organizácie zriadené ako právnická osoba v Nórsku alebo na Slovensku,
                            alebo akákoľvek medzinárodná organizácia, orgán alebo agentúra aktívne zapojená a efektívne
                            prispievajúca k implementácii projektu
                        </p>
                    </div>
                </div>
                <div class="premium-content-overlay">
                    <div class="overlay-container">
                        <div class="popup">
                            <h4 class="text-primary text-center">Detail tejto grantovej výzvy patrí k prémiovému obsahu nášho portálu.</h4>
                            <div class="row">
                                <div class="col-md-6 col-lg-6">
                                    <div class="container-inner">
                                        <p>
                                            Pre jej úplné zobrazenie sa pridajte k našim predplatiteľom a získajte prístup k ďalšim službám z našej ponuky
                                        </p>
                                        <div class="button-center">
                                            <a href="#" class="button-tertiary button-tertiary--padding">Stať sa predplatiteľom</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="container-inner">
                                        <p>
                                            Ste už predplatiteľom?<br>
                                            Ďakujeme! Pre zobrazenie celej výzvy sa iba prihláste do svojho účtu
                                        </p>
                                        <div class="button-center">
                                            <a href="#" class="button-tertiary button-tertiary--padding">Prihlásiť sa</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="button-center button-center--right">
                    <a href="#" class="button-primary">Viac informácií</a>
                </div>
            </div>
        </section>

        <?php include '../../_components/section-suppliers.php';?>

        <section class="more-grants">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">

                        <h2 class="title-secondary text-center">Ďalšie podobné granty a výzvy, ktoré by vás mohli zaujímať</h2>

                        <div class="card-content-two-col card-border-hover card-shadow-hover">
                            <div class="card-container">

                                <div class="tag-container">
                                    <a href="#" class="tag-rectangle">Covid 19</a>
                                    <a href="" class="tag-rectangle"><img src="http://grantexpert.test/assets/img/svg/star-blank-white.svg" alt="star-blank"> Nová výzva</a>
                                </div>

                                <div class="row">
                                    <div class="col-xl-8">
                                        <a href="#"><h2>Podpora nových a existujúcich sociálnych služieb, sociálnoprávnej ochrany detí v zariadeniach na komunitnej úrovni (2021)</h2></a>
                                        <p>
                                            Cieľom výzvy je podporiť prechod poskytovania sociálnych služieb a zabezpečenia výkonu opatrení sociálnoprávnej ochrany detí a sociálnej kurately v zariadení z inštitucionálnej formy na komunitnú a podporiť rozvoj služieb starostlivosti o dieťa do troch rokov veku na ...
                                        </p>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="row">
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Výška dotácie
                                                        </div>
                                                        <div class="value">max. 2 000 000</div>
                                                    </div>
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Miera dotácie
                                                        </div>
                                                        <div class="value">max. 95%</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="row">
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Ostáva dní
                                                        </div>
                                                        <div class="value">48</div>
                                                    </div>
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="date">
                                                            <div class="date-from">Od 1.9.2021</div>
                                                            <div class="date-to">Do Vyčerpania financií</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="card-content-two-col card-border-hover card-shadow-hover">
                            <div class="card-container">

                                <div class="tag-container">
                                    <a href="#" class="tag-rectangle">Covid 19</a>
                                    <a href="" class="tag-rectangle"><img src="http://grantexpert.test/assets/img/svg/star-blank-white.svg" alt="star-blank"> Nová výzva</a>
                                </div>

                                <div class="row">
                                    <div class="col-xl-8">
                                        <a href="#"><h2>Podpora nových a existujúcich sociálnych služieb, sociálnoprávnej ochrany detí v zariadeniach na komunitnej úrovni (2021)</h2></a>
                                        <p>
                                            Cieľom výzvy je podporiť prechod poskytovania sociálnych služieb a zabezpečenia výkonu opatrení sociálnoprávnej ochrany detí a sociálnej kurately v zariadení z inštitucionálnej formy na komunitnú a podporiť rozvoj služieb starostlivosti o dieťa do troch rokov veku na ...
                                        </p>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="row">
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Výška dotácie
                                                        </div>
                                                        <div class="value">max. 2 000 000</div>
                                                    </div>
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Miera dotácie
                                                        </div>
                                                        <div class="value">max. 95%</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="row">
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Ostáva dní
                                                        </div>
                                                        <div class="value">48</div>
                                                    </div>
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="date">
                                                            <div class="date-from">Od 1.9.2021</div>
                                                            <div class="date-to">Do Vyčerpania financií</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="button-center">
                            <a href="#" class="button-primary">Všetky granty</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </main>

    <?php include '../../_components/_footer.php';?>

