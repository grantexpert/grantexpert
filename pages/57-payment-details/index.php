
<?php include '../../_components/_head.php';?>

<body class="page-payment-details page-cart">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <div class="container">
            <div class="cart cart--mobile">
                <div class="icon">
                    <img src="http://grantexpert.test/assets/img/svg/cart.svg" alt="">
                </div>

                <div class="order">Objednávka služby</div>
                <h3 class="title-secondary">Vypracovanie projektu</h3>
                <div class="price-container">
                    <div class="tax-included">Konečná cena s DPH</div>
                    <div class="title-secondary">100 €</div>
                </div>
            </div>
        </div>

        <nav class="cart-submenu">
            <ul>
                <li class="done">
                    <a class="step" href="#"><span class="number">1</span> Podrobnosti objednávky</a>
                </li>
                <li class="done">
                    <a class="step" href="#"><span class="number">2</span> Fakturačné údaje</a>
                </li>
                <li class="active">
                    <span class="step"><span class="number">3</span> Platobné metódy</span>
                </li>
                <li>
                    <span class="step"><span class="number">4</span> Zhrnutie objednávky</span>
                </li>
            </ul>
        </nav>

        <section class="section-cart">
            <div class="container">
                <div class="col-container">
                    <div class="col-content">

                        <div class="shadow-container border-container">
                            <div class="checkbox">
                                <input name="" type="checkbox">
                                <div class="label">
                                    <div class="inline-container">
                                        <div class="col-container">
                                            <div class="col-text text-blue">Povoliť automatické opakovanie platby</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul class="check-list">
                                <li>Z každej nasledujúcej platby vám vrátime naspäť 5%</li>
                                <li>Aktivujte si automatickú platbu tejto služby a z aktuálnej ceny Vám vrátime 5% vo forme kreditu.</li>
                                <li>Vyhnete sa nechcenej deaktivácii služby</li>
                                <li>Nemusíte myslieť na manuálne predlžovanie</li>
                                <li>Ušetríte čas aj peniaze</li>
                                <li>Automatické predlžovanie viete kedykoľvek vypnúť vo vašom profile</li>
                            </ul>
                        </div>
                        <div class="shadow-container">
                            <h2 class="title-secondary">Zvoľte platobnú metódu</h2>
                            <div class="radio-border-container">
                                <label class="radio-button-container">Platba kartou <img src="http://grantexpert.test/assets/img/card-payment-methods.jpg" alt="">
                                    <input type="radio" name="payment-method" value="">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="radio-border-container">
                                <label class="radio-button-container">Bankový prevod
                                    <input type="radio" name="payment-method" value="">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="radio-border-container">
                                <label class="radio-button-container">Vyžiadať faktúru pred platbou
                                    <input type="radio" name="payment-method" value="">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="radio-border-container">
                                <label class="radio-button-container">Iná platobná metóda
                                    <input type="radio" name="payment-method" value="">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>

                        <footer class="cart-footer">

                            <div class="button-center">
                                <button class="button-tertiary button-tertiary--padding button-continue">
                                    POKRAČOVAŤ
                                    <div class="button-scroll button-scroll--white button-scroll--right">
                                        <div class="icon-container"></div>
                                    </div>
                                </button>
                            </div>

                            <button class="button-arrow"><span class="arrow-left"></span><span class="label">Naspäť</span></button>

                            <div class="required-info">
                                <img src="http://grantexpert.test/assets/img/svg/required-icon.svg" alt=""> Povinný údaj
                            </div>
                        </footer>


                    </div>
                    <div class="col-cart">
                        <div class="cart cart--desktop">
                            <div class="icon">
                                <img src="http://grantexpert.test/assets/img/svg/cart.svg" alt="">
                            </div>

                            <div class="order">Objednávka služby</div>
                            <h3 class="title-secondary">Vypracovanie projektu</h3>
                            <div class="price-container">
                                <div class="tax-included">Konečná cena s DPH</div>
                                <div class="title-secondary">100 €</div>
                            </div>

                            <div class="button-center">
                                <button class="button-tertiary button-tertiary--padding button-continue">
                                    POKRAČOVAŤ
                                    <div class="button-scroll button-scroll--white button-scroll--right">
                                        <div class="icon-container"></div>
                                    </div>
                                </button>
                            </div>

                        </div>

                        <div class="required-info">
                            <img src="http://grantexpert.test/assets/img/svg/required-icon.svg" alt=""> Povinný údaj
                        </div>

                    </div>
                </div>
            </div>
        </section>



    </main>

    <?php include '../../_components/_footer.php';?>

