
<?php include '../../_components/_head.php';?>

<body class="page-grantexpert-consultation page-services">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li>
                    <a href="#">Individuálne služby</a>
                </li>
                <li>
                    <a href="#">Balíky služieb</a>
                </li>
                <li class="active">
                    <a href="#">Konzultácia s grantexpertom</a>
                </li>
                <li>
                    <a href="#">Vypracovanie projektu</a>
                </li>
                <li>
                    <a href="#">Privátny grantový konzultant</a>
                </li>
            </ul>
        </nav>

       <section class="description-benefit">
           <div class="container-fluid">
               <div class="section-container">
                   <div class="container">
                       <div class="row">
                           <div class="col-lg-6">
                               <div class="description">
                                   <img src="http://grantexpert.test/assets/img/svg/consultation.svg" alt="">
                                   <h1 class="title-primary">Konzultácia s grantexpertom</h1>
                                   <p class="text-primary">
                                       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rhoncus viverra justo tristique blandit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sed magna nec ex lacinia pellentesque. Maecenas ut accumsan felis, vehicula tristique felis. Etiam justo dui, dapibus ac ultricies ultrices, vestibulum nec lacus. Phasellus quis justo sagittis, sodales elit id, dignissim nibh. Nulla ac ullamcorper justo. Sed blandit leo eu sapien varius, quis fermentum purus pretium.
                                   </p>

                                   <div class="price-button-container">
                                       <div class="col-container">
                                           <div class="price-container">
                                               <div class="tax-included">Cena s DPH</div>
                                               <div class="title-secondary color-sun price">123 €</div>
                                           </div>
                                           <div class="button-container">
                                               <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <div class="col-lg-6">
                               <div class="card">
                                   <div class="col-container">
                                       <div class="benefit">
                                           <h3>Benefit 1</h3>
                                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse elit elit,</p>
                                       </div>
                                       <div class="benefit">
                                           <h3>Benefit 1</h3>
                                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse elit elit,</p>
                                       </div>
                                       <div class="benefit">
                                           <h3>Benefit 1</h3>
                                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse elit elit,</p>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </section>

        <section class="grant-steps">
            <div class="container">
                <h2 class="title-secondary">Kroky ktorými si spolu prejdeme</h2>

                <div class="steps">
                    <div class="step-row col-container">
                        <div class="left-part">
                            <div class="col-container">
                                <div class="col-number">
                                    <div class="number">1</div>
                                </div>
                                <div class="col-label">
                                    Analýza
                                </div>
                            </div>
                        </div>
                        <div class="right-part">
                            Prejdeme s vami vaše potreby a zámery pre najbližsie časové obdobie
                        </div>
                    </div>
                    <div class="step-row col-container">
                        <div class="left-part">
                            <div class="col-container">
                                <div class="col-number">
                                    <div class="number">2</div>
                                </div>
                                <div class="col-label">
                                    Stratégia
                                </div>
                            </div>
                        </div>
                        <div class="right-part">
                            Vypracujeme pre vás stratégiu čerpania grantov a dotácií
                        </div>
                    </div>
                    <div class="step-row col-container">
                        <div class="left-part">
                            <div class="col-container">
                                <div class="col-number">
                                    <div class="number">3</div>
                                </div>
                                <div class="col-label">
                                    Monitoring
                                </div>
                            </div>
                        </div>
                        <div class="right-part">
                            Monitorujeme za vás grantové zdroje a individuálne vás kontaktujeme vždy pri príležitostiach, ktoré zapadajú do nastavenej stratégie
                        </div>
                    </div>
                    <div class="step-row col-container">
                        <div class="left-part">
                            <div class="col-container">
                                <div class="col-number">
                                    <div class="number">4</div>
                                </div>
                                <div class="col-label">
                                    Vypracovanie
                                </div>
                            </div>
                        </div>
                        <div class="right-part">
                            Projektové žiadosti kompletne spracujú naši experti a odovzdávajú ako hotové projekty
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="achieved-results">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <header>
                            <div class="row">
                                <div class="col-lg-6">
                                    <h2 class="title-primary">Naše referencie</h2>
                                    <a href="#" class="arrow-double-link button-desktop">
                                        <div class="arrow-right"></div>
                                        <div class="label">úspešné projekty</div>
                                    </a>
                                </div>
                                <div class="col-lg-6">
                                    <p class="text-primary">
                                        Vďaka dlhoročným skúsenostiam vieme ako napísať žiadosť o grant,  aby mala čo najväčšie šance na úspech a na čo si dať pozor, aby sme sa vyhli prekážkam.
                                    </p>
                                    <a href="#" class="arrow-double-link button-mobile">
                                        <div class="arrow-right"></div>
                                        <div class="label">úspešné projekty</div>
                                    </a>
                                </div>
                            </div>
                        </header>
                        <div class="row results">
                            <div class="col-6 col-lg-3">
                                <h4>2 mil. €</h4>
                                <p class="text-primary">získaných finančných <br>prostriedkov v schválených <br>projektoch</p>
                            </div>
                            <div class="col-6 col-lg-3">
                                <h4>700+</h4>
                                <p class="text-primary">preverených nápadov</p>
                            </div>
                            <div class="col-6 col-lg-3">
                                <h4>8000+</h4>
                                <p class="text-primary">účastníkov <br>školení</p>
                            </div>
                            <div class="col-6 col-lg-3">
                                <h4>25</h4>
                                <p class="text-primary">grantových <br>profesionálov</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>

    <?php include '../../_components/_footer.php';?>

