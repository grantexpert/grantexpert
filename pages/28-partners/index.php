
<?php include '../../_components/_head.php';?>

<body class="page-partners page-about-us">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li>
                    <a href="#">Kto sme</a>
                </li>
                <li>
                    <a href="#">Ako fungujeme</a>
                </li>
                <li>
                    <a href="#">FAQ</a>
                </li>
                <li>
                    <a href="#">Referencie</a>
                </li>
                <li class="active">
                    <a href="#">Partneri</a>
                </li>
                <li>
                    <a href="#">Kontakt</a>
                </li>
            </ul>
        </nav>

        <section class="title-description">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <h1 class="title-primary">Naši Partneri</h1>
                            </div>
                            <div class="col-lg-6">
                                <p class="text-primary">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porta molestie lacus, eget feugiat nulla. Maecenas pulvinar sodales dolor, a cursus lacus hendrerit quis. Maecenas viverra lorem ac ex porta luctus.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="partner-list">
            <div class="container">

                <h2>Názov sekcie partnerov 1</h2>

                <div class="row">
                    <div class="col-sm-6 col-lg-4">
                        <div class="card-logo">
                            <img src="http://grantexpert.test/assets/img/profesia-logo.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-4">
                        <div class="card-logo">
                            <img src="http://grantexpert.test/assets/img/profesia-logo.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-4">
                        <div class="card-logo">
                            <img src="http://grantexpert.test/assets/img/profesia-logo.jpg" alt="">
                        </div>
                    </div>
                </div>

                <h2>Názov sekcie partnerov 2</h2>

                <div class="row">
                    <div class="col-sm-6 col-lg-4">
                        <div class="card-logo">
                            <img src="http://grantexpert.test/assets/img/profesia-logo.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-4">
                        <div class="card-logo">
                            <img src="http://grantexpert.test/assets/img/profesia-logo.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-4">
                        <div class="card-logo">
                            <img src="http://grantexpert.test/assets/img/profesia-logo.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>










    </main>

    <?php include '../../_components/_footer.php';?>

