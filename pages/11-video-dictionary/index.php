
<?php include '../../_components/_head.php';?>

<body class="page-video-dictionary page-grant-education">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li>
                    <a href="#">E-LEANING KURZY</a>
                </li>
                <li>
                    <a href="#">WEBINÁRE</a>
                </li>
                <li>
                    <a href="#">ŠKOLENIA NA MIERU</a>
                </li>
                <li class="active">
                    <a href="#">VIDEOSLOVNÍK</a>
                </li>
                <li>
                    <a href="#">GRANTOVÉ ZDROJE</a>
                </li>
                <li>
                    <a href="#">BLOG</a>
                </li>
            </ul>
        </nav>

        <section class="title-description">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <h1 class="title-primary">Videoslovník</h1>
                            </div>
                            <div class="col-lg-6">
                                <p class="text-primary">
                                    Zoznámte sa so základnými pojmami, s ktorými sa budete stretávať nie len na našej stránke, ale aj v celej Grantovej terminológii.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="dictionary">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-lg-4">
                        <div class="word" data-bs-toggle="modal" data-bs-target="#modal-video-dictionary-1">
                            <div class="col-container">
                                <div class="col-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="128" height="118" viewBox="0 0 128 118">
                                        <g id="Group_1921" data-name="Group 1921" transform="translate(-728.574 -292.654)">
                                            <path id="Path_892" data-name="Path 892" d="M855.574,392.154h-126V302.219h54l9-.065,9-.13h54Z" fill="none" stroke="#008bce" stroke-linejoin="round" stroke-width="2"/>
                                            <path id="Path_893" data-name="Path 893" d="M819.574,383.154v27l-9-9-9,9v-27" fill="#fff" stroke="#008bce" stroke-linejoin="round" stroke-width="1"/>
                                            <path id="Path_894" data-name="Path 894" d="M774.574,383.154h-36v-90h45l9,9,9-9h45v90h-45" fill="#fff" stroke="#008bce" stroke-linejoin="round" stroke-width="1"/>
                                            <path id="Path_895" class="play-button" data-name="Path 895" d="M786.973,355.67a3.283,3.283,0,0,1-4.989-2.838l.131-12.711-.3-12.708a3.283,3.283,0,0,1,4.952-2.9l10.943,6.469,11.153,6.1a3.283,3.283,0,0,1,.037,5.74l-11.073,6.242Z" fill="none" stroke="#008bce" stroke-linejoin="round" stroke-width="2" vector-effect="non-scaling-stroke"/>
                                            <line id="Line_3166" data-name="Line 3166" y2="9" transform="translate(792.574 302.154)" fill="none" stroke="#008bce" stroke-linejoin="round" stroke-width="1"/>
                                        </g>
                                    </svg>
                                </div>
                                <div class="col-label">
                                    <div class="subtitle"><span class="count">1/15</span> Čo je to?</div>
                                    <h4>Výzva</h4>
                                </div>
                            </div>
                        </div>

                        <div class="modal modal-player modal-dictionary fade" id="modal-video-dictionary-1" tabindex="-1" aria-labelledby="modal-video-dictionary-1" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                                    <div class="player">
                                        <video width="100%" controls>
                                            <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
                                            Váš prehliadač nepodporuje HTML5 video
                                        </video>
                                    </div>
                                    <footer>
                                        <div class="col-container">
                                            <div class="col-prev">
                                                <div class="prev-container">
                                                    <div class="col-container">
                                                        <div class="col-button">
                                                            <div class="arrow-double-link button-mobile">
                                                                <div class="arrow-left"></div>
                                                            </div>
                                                            <button class="button-scroll button-scroll--left button-desktop">
                                                                <div class="button-border--gray"></div>
                                                                <div class="button-border--white">
                                                                    <div class="icon-container"></div>
                                                                </div>
                                                            </button>
                                                        </div>
                                                        <div class="col-text">
                                                            <div class="text-small">
                                                                Predošlé video
                                                            </div>
                                                            <div class="subtitle"><span class="count">1/15</span> Čo je to?</div>
                                                            <h4>Výzva</h4>
                                                        </div>
                                                        <div class="col-text-mobile">
                                                            <h4><span class="count">1/15</span>Výzva</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-next">
                                                <div class="next-container">
                                                    <div class="col-container">
                                                        <div class="col-text">
                                                            <div class="text-small">
                                                                Nasledujúce video
                                                            </div>
                                                            <div class="subtitle"><span class="count">1/15</span> Čo je to?</div>
                                                            <h4>Výzva</h4>
                                                        </div>
                                                        <div class="col-button">
                                                            <div class="arrow-double-link button-mobile">
                                                                <div class="arrow-right"></div>
                                                            </div>
                                                            <button class="button-scroll button-scroll--right button-desktop">
                                                                <div class="button-border--gray"></div>
                                                                <div class="button-border--white">
                                                                    <div class="icon-container"></div>
                                                                </div>
                                                            </button>
                                                        </div>
                                                        <div class="col-text-mobile">
                                                            <h4><span class="count">1/15</span>Výzva</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </footer>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-4">
                        <div class="word" data-bs-toggle="modal" data-bs-target="#modal-video-dictionary-2">
                            <div class="col-container">
                                <div class="col-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="128" height="118" viewBox="0 0 128 118">
                                        <g id="Group_1921" data-name="Group 1921" transform="translate(-728.574 -292.654)">
                                            <path id="Path_892" data-name="Path 892" d="M855.574,392.154h-126V302.219h54l9-.065,9-.13h54Z" fill="none" stroke="#008bce" stroke-linejoin="round" stroke-width="2"/>
                                            <path id="Path_893" data-name="Path 893" d="M819.574,383.154v27l-9-9-9,9v-27" fill="#fff" stroke="#008bce" stroke-linejoin="round" stroke-width="1"/>
                                            <path id="Path_894" data-name="Path 894" d="M774.574,383.154h-36v-90h45l9,9,9-9h45v90h-45" fill="#fff" stroke="#008bce" stroke-linejoin="round" stroke-width="1"/>
                                            <path id="Path_895" class="play-button" data-name="Path 895" d="M786.973,355.67a3.283,3.283,0,0,1-4.989-2.838l.131-12.711-.3-12.708a3.283,3.283,0,0,1,4.952-2.9l10.943,6.469,11.153,6.1a3.283,3.283,0,0,1,.037,5.74l-11.073,6.242Z" fill="none" stroke="#008bce" stroke-linejoin="round" stroke-width="2" vector-effect="non-scaling-stroke"/>
                                            <line id="Line_3166" data-name="Line 3166" y2="9" transform="translate(792.574 302.154)" fill="none" stroke="#008bce" stroke-linejoin="round" stroke-width="1"/>
                                        </g>
                                    </svg>
                                </div>
                                <div class="col-label">
                                    <div class="subtitle"><span class="count">1/15</span> Čo je to?</div>
                                    <h4>Výzva</h4>
                                </div>
                            </div>
                        </div>

                        <div class="modal modal-player modal-dictionary fade" id="modal-video-dictionary-2" tabindex="-1" aria-labelledby="modal-video-dictionary-2" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                                    <div class="player">
                                        <video width="100%" controls>
                                            <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
                                            Váš prehliadač nepodporuje HTML5 video
                                        </video>
                                    </div>
                                    <footer>
                                        <div class="col-container">
                                            <div class="col-prev">
                                                <div class="prev-container">
                                                    <div class="col-container">
                                                        <div class="col-button">
                                                            <div class="arrow-double-link button-mobile">
                                                                <div class="arrow-left"></div>
                                                            </div>
                                                            <button class="button-scroll button-scroll--left button-desktop">
                                                                <div class="button-border--gray"></div>
                                                                <div class="button-border--white">
                                                                    <div class="icon-container"></div>
                                                                </div>
                                                            </button>
                                                        </div>
                                                        <div class="col-text">
                                                            <div class="text-small">
                                                                Predošlé video
                                                            </div>
                                                            <div class="subtitle"><span class="count">1/15</span> Čo je to?</div>
                                                            <h4>Výzva</h4>
                                                        </div>
                                                        <div class="col-text-mobile">
                                                            <h4><span class="count">1/15</span>Výzva</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-next">
                                                <div class="next-container">
                                                    <div class="col-container">
                                                        <div class="col-text">
                                                            <div class="text-small">
                                                                Nasledujúce video
                                                            </div>
                                                            <div class="subtitle"><span class="count">1/15</span> Čo je to?</div>
                                                            <h4>Výzva</h4>
                                                        </div>
                                                        <div class="col-button">
                                                            <div class="arrow-double-link button-mobile">
                                                                <div class="arrow-right"></div>
                                                            </div>
                                                            <button class="button-scroll button-scroll--right button-desktop">
                                                                <div class="button-border--gray"></div>
                                                                <div class="button-border--white">
                                                                    <div class="icon-container"></div>
                                                                </div>
                                                            </button>
                                                        </div>
                                                        <div class="col-text-mobile">
                                                            <h4><span class="count">1/15</span>Výzva</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </footer>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-4">
                        <div class="word" data-bs-toggle="modal" data-bs-target="#modal-video-dictionary-3">
                            <div class="col-container">
                                <div class="col-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="128" height="118" viewBox="0 0 128 118">
                                        <g id="Group_1921" data-name="Group 1921" transform="translate(-728.574 -292.654)">
                                            <path id="Path_892" data-name="Path 892" d="M855.574,392.154h-126V302.219h54l9-.065,9-.13h54Z" fill="none" stroke="#008bce" stroke-linejoin="round" stroke-width="2"/>
                                            <path id="Path_893" data-name="Path 893" d="M819.574,383.154v27l-9-9-9,9v-27" fill="#fff" stroke="#008bce" stroke-linejoin="round" stroke-width="1"/>
                                            <path id="Path_894" data-name="Path 894" d="M774.574,383.154h-36v-90h45l9,9,9-9h45v90h-45" fill="#fff" stroke="#008bce" stroke-linejoin="round" stroke-width="1"/>
                                            <path id="Path_895" class="play-button" data-name="Path 895" d="M786.973,355.67a3.283,3.283,0,0,1-4.989-2.838l.131-12.711-.3-12.708a3.283,3.283,0,0,1,4.952-2.9l10.943,6.469,11.153,6.1a3.283,3.283,0,0,1,.037,5.74l-11.073,6.242Z" fill="none" stroke="#008bce" stroke-linejoin="round" stroke-width="2" vector-effect="non-scaling-stroke"/>
                                            <line id="Line_3166" data-name="Line 3166" y2="9" transform="translate(792.574 302.154)" fill="none" stroke="#008bce" stroke-linejoin="round" stroke-width="1"/>
                                        </g>
                                    </svg>
                                </div>
                                <div class="col-label">
                                    <div class="subtitle"><span class="count">1/15</span> Čo je to?</div>
                                    <h4>Výzva</h4>
                                </div>
                            </div>
                        </div>

                        <div class="modal modal-player modal-dictionary fade" id="modal-video-dictionary-3" tabindex="-1" aria-labelledby="modal-video-dictionary-3" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                                    <div class="player">
                                        <video width="100%" controls>
                                            <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
                                            Váš prehliadač nepodporuje HTML5 video
                                        </video>
                                    </div>
                                    <footer>
                                        <div class="col-container">
                                            <div class="col-prev">
                                                <div class="prev-container">
                                                    <div class="col-container">
                                                        <div class="col-button">
                                                            <div class="arrow-double-link button-mobile">
                                                                <div class="arrow-left"></div>
                                                            </div>
                                                            <button class="button-scroll button-scroll--left button-desktop">
                                                                <div class="button-border--gray"></div>
                                                                <div class="button-border--white">
                                                                    <div class="icon-container"></div>
                                                                </div>
                                                            </button>
                                                        </div>
                                                        <div class="col-text">
                                                            <div class="text-small">
                                                                Predošlé video
                                                            </div>
                                                            <div class="subtitle"><span class="count">1/15</span> Čo je to?</div>
                                                            <h4>Výzva</h4>
                                                        </div>
                                                        <div class="col-text-mobile">
                                                            <h4><span class="count">1/15</span>Výzva</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-next">
                                                <div class="next-container">
                                                    <div class="col-container">
                                                        <div class="col-text">
                                                            <div class="text-small">
                                                                Nasledujúce video
                                                            </div>
                                                            <div class="subtitle"><span class="count">1/15</span> Čo je to?</div>
                                                            <h4>Výzva</h4>
                                                        </div>
                                                        <div class="col-button">
                                                            <div class="arrow-double-link button-mobile">
                                                                <div class="arrow-right"></div>
                                                            </div>
                                                            <button class="button-scroll button-scroll--right button-desktop">
                                                                <div class="button-border--gray"></div>
                                                                <div class="button-border--white">
                                                                    <div class="icon-container"></div>
                                                                </div>
                                                            </button>
                                                        </div>
                                                        <div class="col-text-mobile">
                                                            <h4><span class="count">1/15</span>Výzva</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </footer>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-6 col-lg-4">
                        <div class="word" data-bs-toggle="modal" data-bs-target="#modal-video-dictionary-4">
                            <div class="col-container">
                                <div class="col-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="128" height="118" viewBox="0 0 128 118">
                                        <g id="Group_1921" data-name="Group 1921" transform="translate(-728.574 -292.654)">
                                            <path id="Path_892" data-name="Path 892" d="M855.574,392.154h-126V302.219h54l9-.065,9-.13h54Z" fill="none" stroke="#008bce" stroke-linejoin="round" stroke-width="2"/>
                                            <path id="Path_893" data-name="Path 893" d="M819.574,383.154v27l-9-9-9,9v-27" fill="#fff" stroke="#008bce" stroke-linejoin="round" stroke-width="1"/>
                                            <path id="Path_894" data-name="Path 894" d="M774.574,383.154h-36v-90h45l9,9,9-9h45v90h-45" fill="#fff" stroke="#008bce" stroke-linejoin="round" stroke-width="1"/>
                                            <path id="Path_895" class="play-button" data-name="Path 895" d="M786.973,355.67a3.283,3.283,0,0,1-4.989-2.838l.131-12.711-.3-12.708a3.283,3.283,0,0,1,4.952-2.9l10.943,6.469,11.153,6.1a3.283,3.283,0,0,1,.037,5.74l-11.073,6.242Z" fill="none" stroke="#008bce" stroke-linejoin="round" stroke-width="2" vector-effect="non-scaling-stroke"/>
                                            <line id="Line_3166" data-name="Line 3166" y2="9" transform="translate(792.574 302.154)" fill="none" stroke="#008bce" stroke-linejoin="round" stroke-width="1"/>
                                        </g>
                                    </svg>
                                </div>
                                <div class="col-label">
                                    <div class="subtitle"><span class="count">1/15</span> Čo je to?</div>
                                    <h4>Výzva</h4>
                                </div>
                            </div>
                        </div>

                        <div class="modal modal-player modal-dictionary fade" id="modal-video-dictionary-4" tabindex="-1" aria-labelledby="modal-video-dictionary-4" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                                    <div class="player">
                                        <video width="100%" controls>
                                            <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
                                            Váš prehliadač nepodporuje HTML5 video
                                        </video>
                                    </div>
                                    <footer>
                                        <div class="col-container">
                                            <div class="col-prev">
                                                <div class="prev-container">
                                                    <div class="col-container">
                                                        <div class="col-button">
                                                            <div class="arrow-double-link button-mobile">
                                                                <div class="arrow-left"></div>
                                                            </div>
                                                            <button class="button-scroll button-scroll--left button-desktop">
                                                                <div class="button-border--gray"></div>
                                                                <div class="button-border--white">
                                                                    <div class="icon-container"></div>
                                                                </div>
                                                            </button>
                                                        </div>
                                                        <div class="col-text">
                                                            <div class="text-small">
                                                                Predošlé video
                                                            </div>
                                                            <div class="subtitle"><span class="count">1/15</span> Čo je to?</div>
                                                            <h4>Výzva</h4>
                                                        </div>
                                                        <div class="col-text-mobile">
                                                            <h4><span class="count">1/15</span>Výzva</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-next">
                                                <div class="next-container">
                                                    <div class="col-container">
                                                        <div class="col-text">
                                                            <div class="text-small">
                                                                Nasledujúce video
                                                            </div>
                                                            <div class="subtitle"><span class="count">1/15</span> Čo je to?</div>
                                                            <h4>Výzva</h4>
                                                        </div>
                                                        <div class="col-button">
                                                            <div class="arrow-double-link button-mobile">
                                                                <div class="arrow-right"></div>
                                                            </div>
                                                            <button class="button-scroll button-scroll--right button-desktop">
                                                                <div class="button-border--gray"></div>
                                                                <div class="button-border--white">
                                                                    <div class="icon-container"></div>
                                                                </div>
                                                            </button>
                                                        </div>
                                                        <div class="col-text-mobile">
                                                            <h4><span class="count">1/15</span>Výzva</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </footer>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-4">
                        <div class="word" data-bs-toggle="modal" data-bs-target="#modal-video-dictionary-5">
                            <div class="col-container">
                                <div class="col-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="128" height="118" viewBox="0 0 128 118">
                                        <g id="Group_1921" data-name="Group 1921" transform="translate(-728.574 -292.654)">
                                            <path id="Path_892" data-name="Path 892" d="M855.574,392.154h-126V302.219h54l9-.065,9-.13h54Z" fill="none" stroke="#008bce" stroke-linejoin="round" stroke-width="2"/>
                                            <path id="Path_893" data-name="Path 893" d="M819.574,383.154v27l-9-9-9,9v-27" fill="#fff" stroke="#008bce" stroke-linejoin="round" stroke-width="1"/>
                                            <path id="Path_894" data-name="Path 894" d="M774.574,383.154h-36v-90h45l9,9,9-9h45v90h-45" fill="#fff" stroke="#008bce" stroke-linejoin="round" stroke-width="1"/>
                                            <path id="Path_895" class="play-button" data-name="Path 895" d="M786.973,355.67a3.283,3.283,0,0,1-4.989-2.838l.131-12.711-.3-12.708a3.283,3.283,0,0,1,4.952-2.9l10.943,6.469,11.153,6.1a3.283,3.283,0,0,1,.037,5.74l-11.073,6.242Z" fill="none" stroke="#008bce" stroke-linejoin="round" stroke-width="2" vector-effect="non-scaling-stroke"/>
                                            <line id="Line_3166" data-name="Line 3166" y2="9" transform="translate(792.574 302.154)" fill="none" stroke="#008bce" stroke-linejoin="round" stroke-width="1"/>
                                        </g>
                                    </svg>
                                </div>
                                <div class="col-label">
                                    <div class="subtitle"><span class="count">1/15</span> Čo je to?</div>
                                    <h4>Výzva</h4>
                                </div>
                            </div>
                        </div>

                        <div class="modal modal-player modal-dictionary fade" id="modal-video-dictionary-5" tabindex="-1" aria-labelledby="modal-video-dictionary-5" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                                    <div class="player">
                                        <video width="100%" controls>
                                            <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
                                            Váš prehliadač nepodporuje HTML5 video
                                        </video>
                                    </div>
                                    <footer>
                                        <div class="col-container">
                                            <div class="col-prev">
                                                <div class="prev-container">
                                                    <div class="col-container">
                                                        <div class="col-button">
                                                            <div class="arrow-double-link button-mobile">
                                                                <div class="arrow-left"></div>
                                                            </div>
                                                            <button class="button-scroll button-scroll--left button-desktop">
                                                                <div class="button-border--gray"></div>
                                                                <div class="button-border--white">
                                                                    <div class="icon-container"></div>
                                                                </div>
                                                            </button>
                                                        </div>
                                                        <div class="col-text">
                                                            <div class="text-small">
                                                                Predošlé video
                                                            </div>
                                                            <div class="subtitle"><span class="count">1/15</span> Čo je to?</div>
                                                            <h4>Výzva</h4>
                                                        </div>
                                                        <div class="col-text-mobile">
                                                            <h4><span class="count">1/15</span>Výzva</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-next">
                                                <div class="next-container">
                                                    <div class="col-container">
                                                        <div class="col-text">
                                                            <div class="text-small">
                                                                Nasledujúce video
                                                            </div>
                                                            <div class="subtitle"><span class="count">1/15</span> Čo je to?</div>
                                                            <h4>Výzva</h4>
                                                        </div>
                                                        <div class="col-button">
                                                            <div class="arrow-double-link button-mobile">
                                                                <div class="arrow-right"></div>
                                                            </div>
                                                            <button class="button-scroll button-scroll--right button-desktop">
                                                                <div class="button-border--gray"></div>
                                                                <div class="button-border--white">
                                                                    <div class="icon-container"></div>
                                                                </div>
                                                            </button>
                                                        </div>
                                                        <div class="col-text-mobile">
                                                            <h4><span class="count">1/15</span>Výzva</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </footer>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-4">
                        <div class="word" data-bs-toggle="modal" data-bs-target="#modal-video-dictionary-6">
                            <div class="col-container">
                                <div class="col-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="128" height="118" viewBox="0 0 128 118">
                                        <g id="Group_1921" data-name="Group 1921" transform="translate(-728.574 -292.654)">
                                            <path id="Path_892" data-name="Path 892" d="M855.574,392.154h-126V302.219h54l9-.065,9-.13h54Z" fill="none" stroke="#008bce" stroke-linejoin="round" stroke-width="2"/>
                                            <path id="Path_893" data-name="Path 893" d="M819.574,383.154v27l-9-9-9,9v-27" fill="#fff" stroke="#008bce" stroke-linejoin="round" stroke-width="1"/>
                                            <path id="Path_894" data-name="Path 894" d="M774.574,383.154h-36v-90h45l9,9,9-9h45v90h-45" fill="#fff" stroke="#008bce" stroke-linejoin="round" stroke-width="1"/>
                                            <path id="Path_895" class="play-button" data-name="Path 895" d="M786.973,355.67a3.283,3.283,0,0,1-4.989-2.838l.131-12.711-.3-12.708a3.283,3.283,0,0,1,4.952-2.9l10.943,6.469,11.153,6.1a3.283,3.283,0,0,1,.037,5.74l-11.073,6.242Z" fill="none" stroke="#008bce" stroke-linejoin="round" stroke-width="2" vector-effect="non-scaling-stroke"/>
                                            <line id="Line_3166" data-name="Line 3166" y2="9" transform="translate(792.574 302.154)" fill="none" stroke="#008bce" stroke-linejoin="round" stroke-width="1"/>
                                        </g>
                                    </svg>
                                </div>
                                <div class="col-label">
                                    <div class="subtitle"><span class="count">1/15</span> Čo je to?</div>
                                    <h4>Výzva</h4>
                                </div>
                            </div>
                        </div>

                        <div class="modal modal-player modal-dictionary fade" id="modal-video-dictionary-6" tabindex="-1" aria-labelledby="modal-video-dictionary-6" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                                    <div class="player">
                                        <video width="100%" controls>
                                            <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
                                            Váš prehliadač nepodporuje HTML5 video
                                        </video>
                                    </div>
                                    <footer>
                                        <div class="col-container">
                                            <div class="col-prev">
                                                <div class="prev-container">
                                                    <div class="col-container">
                                                        <div class="col-button">
                                                            <div class="arrow-double-link button-mobile">
                                                                <div class="arrow-left"></div>
                                                            </div>
                                                            <button class="button-scroll button-scroll--left button-desktop">
                                                                <div class="button-border--gray"></div>
                                                                <div class="button-border--white">
                                                                    <div class="icon-container"></div>
                                                                </div>
                                                            </button>
                                                        </div>
                                                        <div class="col-text">
                                                            <div class="text-small">
                                                                Predošlé video
                                                            </div>
                                                            <div class="subtitle"><span class="count">1/15</span> Čo je to?</div>
                                                            <h4>Výzva</h4>
                                                        </div>
                                                        <div class="col-text-mobile">
                                                            <h4><span class="count">1/15</span>Výzva</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-next">
                                                <div class="next-container">
                                                    <div class="col-container">
                                                        <div class="col-text">
                                                            <div class="text-small">
                                                                Nasledujúce video
                                                            </div>
                                                            <div class="subtitle"><span class="count">1/15</span> Čo je to?</div>
                                                            <h4>Výzva</h4>
                                                        </div>
                                                        <div class="col-button">
                                                            <div class="arrow-double-link button-mobile">
                                                                <div class="arrow-right"></div>
                                                            </div>
                                                            <button class="button-scroll button-scroll--right button-desktop">
                                                                <div class="button-border--gray"></div>
                                                                <div class="button-border--white">
                                                                    <div class="icon-container"></div>
                                                                </div>
                                                            </button>
                                                        </div>
                                                        <div class="col-text-mobile">
                                                            <h4><span class="count">1/15</span>Výzva</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </footer>
                                </div>
                            </div>
                        </div>
                    </div>




            </div>
        </section>

        <section class="offer-closable">
            <div class="container">
                <div class="background-container">
                    <button class="button-close--blue"></button>
                    <div class="grant-radar-container">
                        <div class="row">
                            <div class="col-lg-5 col-xl-3">
                                <img class="image" src="http://grantexpert.test/assets/img/svg/youtube-illustration.svg" alt="">
                            </div>
                            <div class="col-lg-7 col-xl-6">
                                <h3 class="title-secondary">Viac našich vzdelávacích videí nájdete na našom YouTube kanály</h3>
                            </div>
                            <div class="offset-lg-5 col-lg-7 offset-xl-0 col-xl-3">
                                <div class="button-container">
                                    <a href="" class="button-tertiary button-tertiary--shadow">Zobraziť</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>





    </main>

    <?php include '../../_components/_footer.php';?>

