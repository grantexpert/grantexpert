
<?php include '../../_components/_head.php';?>

<body class="page-who-we-are page-about-us">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li class="active">
                    <a href="#">Kto sme</a>
                </li>
                <li>
                    <a href="#">Ako fungujeme</a>
                </li>
                <li>
                    <a href="#">FAQ</a>
                </li>
                <li>
                    <a href="#">Referencie</a>
                </li>
                <li>
                    <a href="#">Partneri</a>
                </li>
                <li>
                    <a href="#">Kontakt</a>
                </li>
            </ul>
        </nav>

        <section class="title-description">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <h1 class="title-primary">Kto sme</h1>
                            </div>
                            <div class="col-lg-6">
                                <p class="text-primary">
                                    Ako experti na grantové poradenstvo sme roky pracovali na množstve úspešných projektov veľkého rozsahu. Pri práci v tejto oblasti sme si uvedomili, že sa často zabúda na malé projekty, ktorých hodnota pre rozvoj podnikov, verejných priestranstiev, či občianskej vybavenosti nijako nezostávala za väčšími zámermi. Vtedy sme si povedali, že prečo nepomáhať aj menším projektom získať dotácie pre ich uskutočnenie.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="who-we-are">
            <div class="container">
                <div class="row">
                    <div class="col-image col-md-6 col-lg-6">
                        <img src="http://grantexpert.test/assets/img/who-we-are-photo.jpg" alt="">
                    </div>
                    <div class="col-description col-md-6 col-lg-6">
                        <div class="col-description-container">
                            <header class="col-container">
                                <div class="col-name">
                                    <h2 class="title-secondary">Tamás Szőke</h2>
                                    <div class="position">Zakladateľ portálu</div>
                                </div>
                                <div class="col-linkedin">
                                    <a href="#" class="social-media-icon linkedin-icon">
                                        <img src="http://grantexpert.test/assets/img/svg/linkedin-black.svg" alt="">
                                    </a>
                                </div>
                            </header>

                            <div class="email">email@grantexpert.sk</div>

                            <a href="#" class="arrow-link no-hover">
                                <div class="icon-container">
                                    <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                </div>
                                <div class="label-container">
                                    <div class="title">Viac info</div>
                                </div>
                            </a>
                            <div class="description">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vestibulum suscipit turpis vel vulputate. Cras malesuada volutpat tellus. Sed blandit, quam id facilisis gravida, ipsum risus euismod quam, aliquam volutpat dolor tortor ac augue. Maecenas aliquam luctus dignissim. Proin purus justo, placerat in lobortis quis, semper id justo. Pellentesque faucibus lorem nec lorem ullamcorper, vitae fermentum turpis sagittis. Morbi lorem erat, mattis vel ipsum nec, ullamcorper eleifend quam. Suspendisse lorem dui, sollicitudin et interdum sit amet, sagittis a ex. Cras id varius urna, fringilla suscipit ex.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-image col-md-6 col-lg-6">
                        <img src="http://grantexpert.test/assets/img/who-we-are-photo.jpg" alt="">
                    </div>
                    <div class="col-description col-md-6 col-lg-6">
                        <div class="col-description-container">
                            <header class="col-container">
                                <div class="col-name">
                                    <h2 class="title-secondary">Tamás Szőke</h2>
                                    <div class="position">Zakladateľ portálu</div>
                                </div>
                                <div class="col-linkedin">
                                    <a href="#" class="social-media-icon linkedin-icon">
                                        <img src="http://grantexpert.test/assets/img/svg/linkedin-black.svg" alt="">
                                    </a>
                                </div>
                            </header>

                            <div class="email">email@grantexpert.sk</div>

                            <a href="#" class="arrow-link no-hover">
                                <div class="icon-container">
                                    <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                </div>
                                <div class="label-container">
                                    <div class="title">Viac info</div>
                                </div>
                            </a>
                            <div class="description">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vestibulum suscipit turpis vel vulputate. Cras malesuada volutpat tellus. Sed blandit, quam id facilisis gravida, ipsum risus euismod quam, aliquam volutpat dolor tortor ac augue. Maecenas aliquam luctus dignissim. Proin purus justo, placerat in lobortis quis, semper id justo. Pellentesque faucibus lorem nec lorem ullamcorper, vitae fermentum turpis sagittis. Morbi lorem erat, mattis vel ipsum nec, ullamcorper eleifend quam. Suspendisse lorem dui, sollicitudin et interdum sit amet, sagittis a ex. Cras id varius urna, fringilla suscipit ex.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-image col-md-6 col-lg-6">
                        <img src="http://grantexpert.test/assets/img/who-we-are-photo.jpg" alt="">
                    </div>
                    <div class="col-description col-md-6 col-lg-6">
                        <div class="col-description-container">
                            <header class="col-container">
                                <div class="col-name">
                                    <h2 class="title-secondary">Tamás Szőke</h2>
                                    <div class="position">Zakladateľ portálu</div>
                                </div>
                                <div class="col-linkedin">
                                    <a href="#" class="social-media-icon linkedin-icon">
                                        <img src="http://grantexpert.test/assets/img/svg/linkedin-black.svg" alt="">
                                    </a>
                                </div>
                            </header>

                            <div class="email">email@grantexpert.sk</div>

                            <a href="#" class="arrow-link no-hover">
                                <div class="icon-container">
                                    <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                </div>
                                <div class="label-container">
                                    <div class="title">Viac info</div>
                                </div>
                            </a>
                            <div class="description">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vestibulum suscipit turpis vel vulputate. Cras malesuada volutpat tellus. Sed blandit, quam id facilisis gravida, ipsum risus euismod quam, aliquam volutpat dolor tortor ac augue. Maecenas aliquam luctus dignissim. Proin purus justo, placerat in lobortis quis, semper id justo. Pellentesque faucibus lorem nec lorem ullamcorper, vitae fermentum turpis sagittis. Morbi lorem erat, mattis vel ipsum nec, ullamcorper eleifend quam. Suspendisse lorem dui, sollicitudin et interdum sit amet, sagittis a ex. Cras id varius urna, fringilla suscipit ex.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>








    </main>

    <?php include '../../_components/_footer.php';?>

