
<?php include '../../_components/_head.php';?>

<body class="page-blog-detail page-grant-education">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li>
                    <a href="#">E-LEANING KURZY</a>
                </li>
                <li>
                    <a href="#">WEBINÁRE</a>
                </li>
                <li>
                    <a href="#">ŠKOLENIA NA MIERU</a>
                </li>
                <li>
                    <a href="#">VIDEOSLOVNÍK</a>
                </li>
                <li>
                    <a href="#">GRANTOVÉ ZDROJE</a>
                </li>
                <li class="active">
                    <a href="#">BLOG</a>
                </li>
            </ul>
        </nav>

        <article class="blog-article">
            <div class="container">
                <div class="col-container">
                    <div class="article-info col-left-part">
                        <div class="col-container">
                            <div class="col-image">
                                <img src="http://grantexpert.test/assets/img/blog-profile-photo.jpg" alt="" class="profile-photo">
                                <div class="profile-link profile-link--tablet">
                                    <a href="#" class="arrow-double-link">
                                        <div class="arrow-right"></div>
                                        <div class="label">Profil</div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-description">
                                <div class="author">Tamás Szoke</div>
                                <div class="description">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum consequat euismod. Vivamus maximus ut odio vulputate efficitur. Aliquam tristique, risus eleifend malesuada tempus, lacus eros commodo lectus, quis vehicula nisl erat ac elit. Fusce quis vulputate ex. Pellentesque et vulputate purus. Nunc accumsan condimentum velit id sodales. Suspendisse elit diam, scelerisque in nulla euismod, auctor porttitor eros. Duis dictum facilisis tristique. Mauris lacinia eros vitae arcu lacinia sagittis.</p>
                                </div>
                                <div class="profile-link profile-link--desktop">
                                    <a href="#" class="arrow-double-link">
                                        <div class="arrow-right"></div>
                                        <div class="label">Profil</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-right-part">
                        <header>
                            <a href="#" class="tag">AKTUALITY</a>
                            <div class="date">
                                28.9.2021
                            </div>
                        </header>

                        <h1 class="title-primary">Výhody a riziká medzinárodných projektov</h1>
                        <div class="perex">
                            <p>Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické medzinárodné projekty? Čítajte ďalej a zistite viac </p>
                        </div>

                        <h2>Čo je to medzinárodný projekt?</h2>
                        <p>
                            V skratke možno povedať, že sú to tie projekty, na ktorých sa zúčastňujú partneri z minimálne 2 rôznych krajín, alebo/aj tie projekty, ktorých výstupy sa týkajú minimálne 2 krajín. V drvivej väčšine sa teda jedná o partnerské projekty. Ich základné delenie podľa zdroja financovania je nasledovné:
                        </p>
                        <p>
                            Cezhraničné: napr. Interreg SK-HU, Interreg SK-CZ a pod.
                            Nadnárodné: napr. Interreg Central Europe, Program Dunaj a pod.
                            Komunitárne:napr. Erasmus plus, Horizont 2020, Kreatívna Európa, Európa pre občanov
                            Skôr, ako sa môže žiadateľ zapojiť do medzinárodného projektu, musí nájsť vhodnú výzvu alebo program, ktorý podporuje medzinárodné projekty. Samotné programy majú zväčša svoje webové stránky, kde informujú o aktuálnych grantových výzvach. Tie môžete nájsť aj v databáze Grantexpert.sk, kde sú k dispozícii aktuálne a plánované výzvy zo všemožných zdrojov.
                        </p>
                        <p>
                            Do partnerského projektu sa môžete zapojiť dvomi spôsobmi. Môžete to spraviť aktívne, čo znamená, že budete sami vyhľadávať a oslovovať potenciálnych partnerov. Zapojiť sa však môžete aj tak, že vás niekto osloví. V tomto prípade hovoríme o pasívnom zapojení. K osloveniu potenciálneho partnera do projektu môže prísť na základe referencií alebo aj cieleným hľadaním. Keďže partneri často hľadajú organizácie alebo inštitúcie v rámci podobne oblasti, je dobré mať informácie o vás zverejnené napríklad na webovej stránke.
                        </p>

                        <img class="blog-image" src="http://grantexpert.test/assets/img/blog-image.jpg" alt="">

                        <p>
                            Fungovanie partnerstva je založené na tzv. Partnership Agreement, ktoré podpisujú všetci partneri (buď ako celé konzorcium alebo bilaterálne). V rámci medzinárodných projektov rozlišujeme troch kľúčových účastníkov celého procesu:
                        </p>

                        <p>
                            donor – poskytuje financovanie projektu
                            hlavný partner – zodpovedá za celé riadenie projektu, podpisuje zmluvu s donorom, a partnerské zmluvy s ostatnými partnermi, predkladá správy za projekt
                            partner – zodpovedá za svoju časť projektu,  je spoluzodpovedný za celkový úspech projektu.
                            Počas celého trvania projektu je dôležitá vzájomná komunikácia, dodržiavanie termínov a dodávanie kvalitných výstupov. V prípade, že nastanú problémy, vždy ich treba riešiť včas. Najväčšiu zodpovednosť v celom procese nesie hlavný partner, ktorý by mal jednak kontrolovať samotné výstupu, dbať na dodržiavanie termínov a v prípade potreby vedieť pomôcť ostatným partnerom projektu.
                        </p>
                    </div>
                    <div class="col-left-part">
                        <div class="quote">
                            <p>
                                Treba mať na pamäti tiež, že väčšina projektov funguje prostredníctvom refundácie nákladov, na ktoré sa zväčša čaká aj niekoľko mesiacov.
                            </p>
                        </div>
                    </div>
                    <div class="col-right-part">
                        <p>
                            Počas celého trvania projektu je dôležitá vzájomná komunikácia, dodržiavanie termínov a dodávanie kvalitných výstupov. V prípade, že nastanú problémy, vždy ich treba riešiť včas. Najväčšiu zodpovednosť v celom procese nesie hlavný partner, ktorý by mal jednak kontrolovať samotné výstupu, dbať na dodržiavanie termínov a v prípade potreby vedieť pomôcť ostatným partnerom projektu.
                        </p>

                        <p>
                            Treba mať na pamäti tiež, že väčšina projektov funguje prostredníctvom refundácie nákladov, na ktoré sa zväčša čaká aj niekoľko mesiacov. V rámci cash flowu by sa teda malo rátať s určitou rezervou v prípade čakania na prefinancovanie nákladov. Dôležitá je komunikácia s donorom, ktorý je často zvedavý či sa všetko dodržuje tak ako má a kontroluje výstupy. S donorom komunikuje vždy iba hlavný partner.
                        </p>

                        <h2>Financovanie projektu a rozpočet</h2>
                        <p>
                            Financovanie projektu môže mať najčastejšie podobu zálohovej platby alebo refundácie. Pri zálohových platbách je presne definované kedy a ako vám bude poskytnutá záloha. Často to býva pomerom 40/40/20, teda na začiatku, v priebehu a na konci celého proces. Pri refundácii treba mať správne naplánovaný cash flow, pretože samotné preplatenie nákladov môže trvať aj niekoľko mesiacov a treba mať dostatočne zabezpečené, aby ste mali z čoho projekt financovať.
                        </p>
                        <p>
                            Na začiatku je vhodné správnym spôsobom komunikovať sledovanie a dokladovanie financií.  Zmeny v projektoch sú možné a bežné. Po dohode medzi partnermi a odsúhlasení donorom je možné rozpočet upravovať. Ak je však rozpočet odsúhlasený, jeho maximálna výška je finálna. Žiaden donor totiž neposkytuje viac peňazí ako bolo schválené.
                        </p>

                        <button class="button-tertiary button-tertiary--reverse button-icon--auto-width">
                            <div class="col-container">
                                <div class="icon-container">
                                    <img src="http://grantexpert.test/assets/img/svg/share.svg" alt="">
                                </div>
                                <div class="label-container">
                                    Zdieľať
                                </div>
                            </div>
                        </button>

                    </div>
                </div>
            </div>
        </article>

        <section class="share-article">
            <div class="container">
                <button class="button-tertiary button-tertiary--reverse button-icon--auto-width">
                    <div class="col-container">
                        <div class="icon-container">
                            <img src="http://grantexpert.test/assets/img/svg/share.svg" alt="">
                        </div>
                        <div class="label-container">
                            Zdieľať
                        </div>
                    </div>
                </button>
            </div>
        </section>


        <section class="blog">
            <div class="container">

                <h2 class="title-primary">Príbuzné články</h2>

                <div class="row">
                    <div class="col-12">
                        <div class="carousel-container">
                            <div class="tns-carousel carousel-blog">
                                <div>
                                    <div class="card-animation card-border-animation">
                                        <div class="card card-border">
                                            <a href="#" class="tag">AKTUALITY</a>
                                            <a href="#">
                                                <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                            </a>
                                            <p>
                                                Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="card-animation card-border-animation">
                                        <div class="card card-border">
                                            <a href="#" class="tag">AKTUALITY</a>
                                            <a href="#">
                                                <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                            </a>
                                            <p>
                                                Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="card-animation card-border-animation">
                                        <div class="card card-border">
                                            <a href="#" class="tag">AKTUALITY</a>
                                            <a href="#">
                                                <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                            </a>
                                            <p>
                                                Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="card-animation card-border-animation">
                                        <div class="card card-border">
                                            <a href="#" class="tag">AKTUALITY</a>
                                            <a href="#">
                                                <h3 class="title-secondary">Výhody a riziká medzinárodných projektov</h3>
                                            </a>
                                            <p>
                                                Máte nápad na projekt, ktorý by ste radi realizovali s partnermi zo zahraničia? Alebo máte ponuku sa zapojiť? Čím sú špecifické ...
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <button id="blog-carousel-prev" class="button-scroll button-scroll--left">
                                <div class="button-border--gray"></div>
                                <div class="button-border--white">
                                    <div class="icon-container"></div>
                                </div>
                            </button>
                            <button id="blog-carousel-next" class="button-scroll button-scroll--right">
                                <div class="button-border--gray"></div>
                                <div class="button-border--white">
                                    <div class="icon-container"></div>
                                </div>
                            </button>
                        </div>



                        <div class="button-center">
                            <a href="" class="button-primary">VŠETKY BLOGY</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>



    </main>

    <?php include '../../_components/_footer.php';?>

