
<?php include '../../_components/_head.php';?>

<body class="page-webinar-detail page-grant-education">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li>
                    <a href="#">E-LEANING KURZY</a>
                </li>
                <li class="active">
                    <a href="#">WEBINÁRE</a>
                </li>
                <li>
                    <a href="#">ŠKOLENIA NA MIERU</a>
                </li>
                <li>
                    <a href="#">VIDEOSLOVNÍK</a>
                </li>
                <li>
                    <a href="#">GRANTOVÉ ZDROJE</a>
                </li>
                <li>
                    <a href="#">BLOG</a>
                </li>
            </ul>
        </nav>


        <header class="card-title-header card-title-header--with-price">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <div class="col-container">
                            <div class="col-card order-2 order-lg-1">
                                <div class="card-header-summary card-color-sun">
                                    <div class="text-small-label">Podujatie bude prebiehať</div>
                                    <div class="value">Online</div>
                                    <div class="text-small-label delimeter">Termín</div>
                                    <div class="value">16.12.2021</div>
                                    <div class="col-container icon-info-container">
                                        <div class="col-icon">

                                        </div>
                                        <div class="col-info">
                                            <div class="text-small-label">Čas</div>
                                            <div class="value">10:00 - 11:30</div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-title order-1 order-lg-2">
                                <h1 class="page-title title-secondary">1-2-3 Granty</h1>
                                <p class="description text-primary">
                                    Radi by ste využili granty a dotácie pre financovanie vašich projektov, ale neviete sa zorientovať v spleti informácií?
                                </p>

                                <div class="price-button price-button--white price-button--desktop">
                                    <div class="col-container">
                                        <div class="price-container">
                                            <div class="vertical-center">
                                                <div class="tax-included">Cena s DPH</div>
                                                <div class="title-secondary color-sun price">29 €</div>
                                            </div>
                                        </div>
                                        <div class="button-container">
                                            <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                        </div>

                                    </div>
                                </div>

                            </div>

                            <div class="price-button price-button--white price-button--mobile order-3">
                                <div class="col-container">
                                    <div class="price-container">
                                        <div class="vertical-center">
                                            <div class="tax-included">Cena s DPH</div>
                                            <div class="title-secondary color-sun price">29 €</div>
                                        </div>
                                    </div>
                                    <div class="button-container">
                                        <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="row-line-grid grid-color-sun">
            <div class="container">
                <div class="col-container">
                    <div class="col-title">
                        <h3>Čo vás naučíme?</h3>
                    </div>
                    <div class="col-content">
                        <video width="100%" controls>
                            <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
                            Váš prehliadač nepodporuje HTML5 video
                        </video>
                        <p>
                            Neziskové organizácie, samosprávy, ale aj súkromné spoločnosti sa majú každoročne možnosť uchádzať o desiatky miliónov eur z eurofondov a iných grantových zdrojov určených na financovanie rozvojových projektov. Ale viete kde a ako hľadať informácie o dotačných zdrojoch a ich vhodnosti pre vaše zámery? Tento kurz vám pomôže odhaliť taje nenávratného financovania tým, že vás vybaví teoretickými znalosťami, ktoré sú potrebné na porozumenie „eurofondštiny“ a lepšie pochopenie procesu získavania nenávratnej finančnej pomoci z verejných zdrojov.
                        </p>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Témy webináru</h3>
                    </div>
                    <div class="col-content">
                        <p>
                            Porozumieť základnej terminológií v oblasti nenávratného financovania<br>
                            Zorientovať sa v dostupných dotačných zdrojoch<br>
                            Aké sú základné podmienky, ktoré musíte spĺňať pre čerpanie nenávratného financovania<br>
                            Čo je to projekt a aké sú jeho základné obsahové prvky<br>
                            Ako prebieha proces prípravy, výberu a realizácie projektov.
                        </p>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Trvanie kurzu</h3>
                    </div>
                    <div class="col-content">
                        <p>
                            Kurz je rozdelený na päť lekcií. V jednotlivých lekciách postupne získate znalosti potrebné na pochopenie celkového kontextu grantového financovania, jeho pravidiel a postupov.
                        </p>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Pre koho je kurz určený</h3>
                    </div>
                    <div class="col-content">
                        <p>Pre laickú verejnosť z verejného, súkromného aj neziskového sektora. Medzi predchádzajúcimi účastníkmi prezenčnej formy tohto kurzu boli, zamestnanci samospráv, neziskových organizácií, ako aj podnikatelia.</p>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Kvalifikačné predpoklady</h3>
                    </div>
                    <div class="col-content">
                        <p>Nevyžadujú sa žiadne predchádzajúce skúsenosti s nenávratnými zdrojmi financovania, avšak všeobecný právny rozhľad je potrebný.</p>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Lektor</h3>
                    </div>
                    <div class="col-content">
                        <div class="col-container">
                            <div class="col-lecturer">
                                <p>
                                    Tamás Szőke<br>
                                    Expert na eurofondy
                                </p>
                            </div>
                            <div class="col-profile">
                                <a href="#" class="arrow-double-link profile-link">
                                    <div class="arrow-right"></div>
                                    <div class="label">Profil</div>
                                </a>
                                <a href="#" class="social-media-icon linkedin-icon">
                                    <img src="http://grantexpert.test/assets/img/svg/linkedin-black.svg" alt="">
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Cena kurzu</h3>
                    </div>
                    <div class="col-content">
                        <div class="price-button">
                            <div class="col-container">
                                <div class="price-container">
                                    <div class="vertical-center">
                                        <div class="tax-included">Cena s DPH</div>
                                        <div class="title-secondary color-sun price">29 €</div>
                                    </div>
                                </div>
                                <div class="button-container">
                                    <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                </div>

                            </div>
                        </div>
                        <div class="mt-1 center-right">
                            Po zakúpení nájdete kurz vo vašom profile
                        </div>
                    </div>
                </div>
            </div>

        </section>


        <section class="grant-education mt-8">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">

                        <h2 class="title-secondary">Ďalšie webináre</h2>

                        <div class="row">
                            <div class="col-12">
                                <div class="carousel-container">
                                    <div class="tns-carousel carousel-grant-education">
                                        <div>
                                            <div class="card-event-container">
                                                <div class="card card-event">
                                                    <header>
                                                        <div class="col-container">
                                                            <a href="#" class="title-container">
                                                                <h3 class="title-secondary">Grantová podpora škôl</h3>
                                                            </a>
                                                            <div class="icon-container">
                                                                <img src="http://grantexpert.test/assets/img/svg/calendar.svg" alt="calendar">
                                                            </div>
                                                        </div>

                                                        <div class="date-time">
                                                            <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/calendar.svg" alt="calendar"></span><span class="date">28.10.</span><span class="time">10:00 - 12:00</span>
                                                        </div>


                                                    </header>
                                                    <h4>Čo sa naučíte?</h4>
                                                    <ul class="check-list text-secondary">
                                                        <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                        <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                        <li>Aktuálne a plánované výzvy</li>
                                                    </ul>
                                                    <footer>
                                                        <div class="col-container">
                                                            <div class="price-container">
                                                                <div class="tax-included">Cena s DPH</div>
                                                                <div class="title-secondary color-sun price">19,90 €</div>
                                                            </div>
                                                            <div class="more-info">
                                                                <a href="#" class="arrow-link no-hover">
                                                                    <div class="icon-container">
                                                                        <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                                                    </div>
                                                                    <div class="label-container">
                                                                        <div class="title">Viac info</div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <div class="button-container">
                                                                <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                                            </div>
                                                        </div>
                                                    </footer>
                                                </div>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="card-event-container">
                                                <div class="card card-event">
                                                    <header>
                                                        <div class="col-container">
                                                            <a href="#" class="title-container">
                                                                <h3 class="title-secondary">Cesta úspešného projektu: Podnikateľský IT projekt</h3>
                                                            </a>
                                                            <div class="icon-container">
                                                                <img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker">
                                                            </div>
                                                        </div>

                                                        <div class="preview-container col-container">
                                                            <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker"></span>
                                                            <div class="stars">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star-blank.svg" alt="star-blank">
                                                            </div>
                                                            <a href="#" class="button-preview"><img src="http://grantexpert.test/assets/img/svg/play.svg" alt="play"><span class="label">UKÁŽKA</span></a>
                                                        </div>


                                                    </header>
                                                    <h4>Čo sa naučíte?</h4>
                                                    <ul class="check-list text-secondary">
                                                        <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                        <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                        <li>Aktuálne a plánované výzvy</li>
                                                    </ul>
                                                    <footer>
                                                        <div class="col-container">
                                                            <div class="price-container">
                                                                <div class="tax-included">Cena s DPH</div>
                                                                <div class="title-secondary color-sun price">19,90 €</div>
                                                            </div>
                                                            <div class="more-info">
                                                                <a href="#" class="arrow-link no-hover">
                                                                    <div class="icon-container">
                                                                        <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                                                    </div>
                                                                    <div class="label-container">
                                                                        <div class="title">Viac info</div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <div class="button-container">
                                                                <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                                            </div>

                                                        </div>
                                                    </footer>
                                                </div>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="card-event-container">
                                                <div class="card card-event">
                                                    <header>
                                                        <div class="col-container">
                                                            <a href="#" class="title-container">
                                                                <h3 class="title-secondary">Cesta úspešného projektu: Podnikateľský IT projekt</h3>
                                                            </a>
                                                            <div class="icon-container">
                                                                <img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker">
                                                            </div>
                                                        </div>

                                                        <div class="preview-container col-container">
                                                            <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker"></span>
                                                            <div class="stars">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star-blank.svg" alt="star-blank">
                                                            </div>
                                                            <a href="#" class="button-preview"><img src="http://grantexpert.test/assets/img/svg/play.svg" alt="play"><span class="label">UKÁŽKA</span></a>
                                                        </div>


                                                    </header>
                                                    <h4>Čo sa naučíte?</h4>
                                                    <ul class="check-list text-secondary">
                                                        <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                        <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                        <li>Aktuálne a plánované výzvy</li>
                                                    </ul>
                                                    <footer>
                                                        <div class="col-container">
                                                            <div class="price-container">
                                                                <div class="tax-included">Cena s DPH</div>
                                                                <div class="title-secondary color-sun price">19,90 €</div>
                                                            </div>
                                                            <div class="more-info">
                                                                <a href="#" class="arrow-link no-hover">
                                                                    <div class="icon-container">
                                                                        <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                                                    </div>
                                                                    <div class="label-container">
                                                                        <div class="title">Viac info</div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <div class="button-container">
                                                                <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                                            </div>

                                                        </div>
                                                    </footer>
                                                </div>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="card-event-container">
                                                <div class="card card-event">
                                                    <header>
                                                        <div class="col-container">
                                                            <a href="#" class="title-container">
                                                                <h3 class="title-secondary">Cesta úspešného projektu: Podnikateľský IT projekt</h3>
                                                            </a>
                                                            <div class="icon-container">
                                                                <img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker">
                                                            </div>
                                                        </div>

                                                        <div class="preview-container col-container">
                                                            <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker"></span>
                                                            <div class="stars">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star-blank.svg" alt="star-blank">
                                                            </div>
                                                            <a href="#" class="button-preview"><img src="http://grantexpert.test/assets/img/svg/play.svg" alt="play"><span class="label">UKÁŽKA</span></a>
                                                        </div>


                                                    </header>
                                                    <h4>Čo sa naučíte?</h4>
                                                    <ul class="check-list text-secondary">
                                                        <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                        <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                        <li>Aktuálne a plánované výzvy</li>
                                                    </ul>
                                                    <footer>
                                                        <div class="col-container">
                                                            <div class="price-container">
                                                                <div class="tax-included">Cena s DPH</div>
                                                                <div class="title-secondary color-sun price">19,90 €</div>
                                                            </div>
                                                            <div class="more-info">
                                                                <a href="#" class="arrow-link no-hover">
                                                                    <div class="icon-container">
                                                                        <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                                                    </div>
                                                                    <div class="label-container">
                                                                        <div class="title">Viac info</div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <div class="button-container">
                                                                <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                                            </div>

                                                        </div>
                                                    </footer>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <button id="education-carousel-prev" class="button-scroll button-scroll--left">
                                        <div class="button-border--gray"></div>
                                        <div class="button-border--white">
                                            <div class="icon-container"></div>
                                        </div>
                                    </button>
                                    <button id="education-carousel-next" class="button-scroll button-scroll--right">
                                        <div class="button-border--gray"></div>
                                        <div class="button-border--white">
                                            <div class="icon-container"></div>
                                        </div>
                                    </button>

                                </div>
                                <div class="button-center">
                                    <a href="" class="button-primary"><div class="vertical-center">Všetky webináre</div></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </main>

    <?php include '../../_components/_footer.php';?>

