
<?php include '../../_components/_head.php';?>

<body class="page-order-summary page-cart">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <div class="container">
            <div class="cart cart--mobile">
                <div class="icon">
                    <img src="http://grantexpert.test/assets/img/svg/cart.svg" alt="">
                </div>

                <div class="order">Objednávka služby</div>
                <h3 class="title-secondary">Vypracovanie projektu</h3>
                <div class="price-container">
                    <div class="tax-included">Konečná cena s DPH</div>
                    <div class="title-secondary">100 €</div>
                </div>
            </div>
        </div>

        <nav class="cart-submenu">
            <ul>
                <li class="done">
                    <a class="step" href="#"><span class="number">1</span> Podrobnosti objednávky</a>
                </li>
                <li class="done">
                    <a class="step" href="#"><span class="number">2</span> Fakturačné údaje</a>
                </li>
                <li class="done">
                    <a class="step" href="#"><span class="number">3</span> Platobné metódy</a>
                </li>
                <li class="active">
                    <span class="step"><span class="number">4</span> Zhrnutie objednávky</span>
                </li>
            </ul>
        </nav>

        <section class="section-cart">
            <div class="container">
                <div class="col-container">
                    <div class="col-content">

                        <div class="shadow-container">
                            <div class="subtitle">Objednávka služby</div>
                            <h1 class="title-secondary color-sun">Vypracovanie projektu</h1>

                            <div class="row-container">
                                <div class="row">
                                    <div class="col-6">Oblasť:</div>
                                    <div class="col-6">Zvolená oblasť</div>
                                </div>
                                <div class="row">
                                    <div class="col-6">Preferovaný termín:</div>
                                    <div class="col-6">12.04.2022</div>
                                </div>
                                <div class="row">
                                    <div class="col-6">Preferovaný čas:</div>
                                    <div class="col-6">12:00 - 13:00</div>
                                </div>
                                <div class="row">
                                    <div class="col-6">opis predmetu konzultácie:</div>
                                    <div class="col-12">
                                        <img class="roll-down-arrow" src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="">
                                        <div class="roll-down">
                                            Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <h2>Fakturačné údaje</h2>
                            <div class="billing-information row-container">
                                <div class="row">
                                    <div class="col-6">IČO:</div>
                                    <div class="col-6"><div class="text">XXXXXXXXXX</div><input type="text" value="XXXXXXXXXX"name=""> <span class="edit-icon"></span></div>
                                </div>
                                <div class="row">
                                    <div class="col-6">Názov spoločnosti:</div>
                                    <div class="col-6"><div class="text">Truben studio</div><input type="text" value="Truben studio" name=""> <span class="edit-icon"></span></div>
                                </div>
                                <div class="row">
                                    <div class="col-6">Ulica:</div>
                                    <div class="col-6"><div class="text">Karpatské námestie</div><input type="text" value="Karpatské námestie" name=""> <span class="edit-icon"></span></div>
                                </div>
                                <div class="row">
                                    <div class="col-6">Súpisné / Orientčné číslo</div>
                                    <div class="col-6"><div class="text">10</div><input type="text" value="10" name=""> <span class="edit-icon"></span></div>
                                </div>
                                <div class="row">
                                    <div class="col-6">Obec</div>
                                    <div class="col-6"><div class="text">Bratislava</div><input type="text" value="Bratislava" name=""> <span class="edit-icon"></span></div>
                                </div>
                                <div class="row">
                                    <div class="col-6">PSČ:</div>
                                    <div class="col-6"><div class="text">98654</div><input type="text" value="98654" name=""> <span class="edit-icon"></span></div>
                                </div>
                                <div class="row">
                                    <div class="col-6">Meno:</div>
                                    <div class="col-6"><div class="text">Pavol</div><input type="text" value="Pavol" name=""> <span class="edit-icon"></span></div>
                                </div>
                                <div class="row">
                                    <div class="col-6">Priezvisko</div>
                                    <div class="col-6"><div class="text">Truben</div><input type="text" value="Truben" name=""> <span class="edit-icon"></span></div>
                                </div>
                                <div class="row">
                                    <div class="col-6">Tel. č.</div>
                                    <div class="col-6"><div class="text">+421123 123 123</div><input type="text" value="+421123 123 123" name=""> <span class="edit-icon"></span></div>
                                </div>
                                <div class="row">
                                    <div class="col-6">Email:</div>
                                    <div class="col-6"><div class="text">email@gmail.co</div><input type="email" value="email@gmail.com" name=""> <span class="edit-icon"></span></div>
                                </div>
                            </div>

                            <h2>Platobná metóda</h2>
                            <div class="row-container">
                                <div class="row">
                                    <div class="col-6">Platobná metóda</div>
                                    <div class="col-6">Platba kartou</div>
                                </div>
                                <div class="row">
                                    <div class="col-6">Automatické opakovanie platby</div>
                                    <div class="col-6">Nie</div>
                                </div>
                            </div>

                        </div>

                        <footer class="cart-footer">

                            <div class="button-center">
                                <div class="checkbox checkbox--text-light">
                                    <input name="" type="checkbox">
                                    <div class="label">
                                        <div class="inline-container">
                                            Súhlasím so spracúvaním svojich osobných údajov na účely zasielania noviniek z oblasti grantového poradenstva v súlade so zásadami ochrany osobných údajov.
                                        </div>
                                    </div>
                                </div>

                                <div class="checkbox checkbox--text-light">
                                    <input name="" type="checkbox">
                                    <div class="label">
                                        <div class="inline-container">
                                            Súhlasím s obchodnými podmienkami portálu
                                        </div>
                                    </div>
                                </div>

                                <button class="button-secondary">Zaplatiť</button>

                            </div>

                            <button class="button-arrow"><span class="arrow-left"></span><span class="label">Naspäť</span></button>
                            <button class="button-primary button-primary--small-padding">Zrušiť objednávku</button>
                        </footer>


                    </div>
                    <div class="col-cart">
                        <div class="cart cart--desktop">
                            <div class="icon">
                                <img src="http://grantexpert.test/assets/img/svg/cart.svg" alt="">
                            </div>

                            <div class="order">Objednávka služby</div>
                            <h3 class="title-secondary">Vypracovanie projektu</h3>
                            <div class="price-container">
                                <div class="tax-included">Konečná cena s DPH</div>
                                <div class="title-secondary">100 €</div>
                            </div>

                            <div class="checkbox checkbox--text-light">
                                <input name="" type="checkbox">
                                <div class="label">
                                    <div class="inline-container">
                                        Súhlasím so spracúvaním svojich osobných údajov na účely zasielania noviniek z oblasti grantového poradenstva v súlade so zásadami ochrany osobných údajov.
                                    </div>
                                </div>
                            </div>

                            <div class="checkbox checkbox--text-light">
                                <input name="" type="checkbox">
                                <div class="label">
                                    <div class="inline-container">
                                        Súhlasím s obchodnými podmienkami portálu
                                    </div>
                                </div>
                            </div>




                            <div class="button-center">
                                <button class="button-secondary">Zaplatiť</button>
                            </div>

                        </div>



                    </div>
                </div>
            </div>
        </section>



    </main>

    <?php include '../../_components/_footer.php';?>

