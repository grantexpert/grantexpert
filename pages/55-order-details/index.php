
<?php include '../../_components/_head.php';?>

<body class="page-payment-details page-cart">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <div class="container">
            <div class="cart cart--mobile">
                <div class="icon">
                    <img src="http://grantexpert.test/assets/img/svg/cart.svg" alt="">
                </div>

                <div class="order">Objednávka služby</div>
                <h3 class="title-secondary">Vypracovanie projektu</h3>
                <div class="price-container">
                    <div class="tax-included">Konečná cena s DPH</div>
                    <div class="title-secondary">100 €</div>
                </div>
            </div>
        </div>

        <nav class="cart-submenu">
            <ul>
                <li class="active">
                    <a class="step" href="#"><span class="number">1</span> Podrobnosti objednávky</a>
                </li>
                <li>
                    <span class="step"><span class="number">2</span> Fakturačné údaje</span>
                </li>
                <li>
                    <span class="step"><span class="number">3</span> Platobné metódy</span>
                </li>
                <li>
                    <span class="step"><span class="number">4</span> Zhrnutie objednávky</span>
                </li>
            </ul>
        </nav>

        <form action="">
            <section class="section-cart">
                <div class="container">
                    <div class="col-container">
                        <div class="col-content">

                            <div class="shadow-container">
                                <h2 class="title-secondary">Podrobnosti objednávky</h2>
                                <div class="form-label form-label--required">
                                    <div class="col-container">
                                        <div class="col-text">
                                            Vyberte oblasť, ktorej sa požiadavka bude týkať
                                        </div>
                                        <div class="col-required"></div>
                                    </div>
                                </div>
                                <div class="custom-form-control selectbox selectbox--blue-shadow selectbox--small">
                                    <input type="hidden" name="">
                                    <div class="selectbox-label">Oblasť expertízy</div>
                                    <ul>
                                        <li data-value="current">Oblasť expertízy 1</li>
                                        <li data-value="planned">Oblasť expertízy 2</li>
                                        <li data-value="closed">Oblasť expertízy 3</li>
                                    </ul>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">

                                        <div class="form-label form-label--required mt-4">
                                            <div class="col-container">
                                                <div class="col-text">
                                                    Preferovaný dátum konzultácie
                                                </div>
                                                <div class="col-required"></div>
                                            </div>
                                        </div>
                                        <div class="custom-form-control selectbox selectbox--datepicker selectbox--blue-shadow selectbox--small selectbox--text-center">
                                            <div class="selectbox-label">
                                                <input type="text" class="input-datepicker">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-label form-label--required mt-4">
                                            <div class="col-container">
                                                <div class="col-text">
                                                    Preferovaný čas konzultácie
                                                </div>
                                                <div class="col-required"></div>
                                            </div>

                                        </div>
                                        <div class="custom-form-control selectbox selectbox--blue-shadow selectbox--small selectbox--text-center">
                                            <input type="hidden" name="">
                                            <div class="selectbox-label"></div>
                                            <ul>
                                                <li data-value="current">9:00 - 10:00</li>
                                                <li data-value="planned">10:00 - 11:00</li>
                                                <li data-value="closed">11:00 - 12:00</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-label form-label--required mt-4">
                                    <div class="col-container">
                                        Stručný opis predmetu konzultácie
                                    </div>
                                </div>
                                <textarea class="textarea-char-count" name=""></textarea>
                                <div class="char-count"><span class="char-used">0</span>/1000 znakov</div>

                            </div>

                            <footer class="cart-footer">

                                <div class="button-center">
                                    <button class="button-tertiary button-tertiary--padding button-continue">
                                        POKRAČOVAŤ
                                        <div class="button-scroll button-scroll--white button-scroll--right">
                                            <div class="icon-container"></div>
                                        </div>
                                    </button>
                                </div>

                                <div class="required-info">
                                    <img src="http://grantexpert.test/assets/img/svg/required-icon.svg" alt=""> Povinný údaj
                                </div>
                            </footer>


                        </div>
                        <div class="col-cart">
                            <div class="cart cart--desktop">
                                <div class="icon">
                                    <img src="http://grantexpert.test/assets/img/svg/cart.svg" alt="">
                                </div>

                                <div class="order">Objednávka služby</div>
                                <h3 class="title-secondary">Vypracovanie projektu</h3>
                                <div class="price-container">
                                    <div class="tax-included">Konečná cena s DPH</div>
                                    <div class="title-secondary">100 €</div>
                                </div>

                                <div class="button-center">
                                    <button class="button-tertiary button-tertiary--padding button-continue">
                                        POKRAČOVAŤ
                                        <div class="button-scroll button-scroll--white button-scroll--right">
                                            <div class="icon-container"></div>
                                        </div>
                                    </button>
                                </div>

                            </div>

                            <div class="required-info">
                                <img src="http://grantexpert.test/assets/img/svg/required-icon.svg" alt=""> Povinný údaj
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </form>


    </main>

    <?php include '../../_components/_footer.php';?>

