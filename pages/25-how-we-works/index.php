
<?php include '../../_components/_head.php';?>

<body class="page-how-we-works page-about-us">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li>
                    <a href="#">Kto sme</a>
                </li>
                <li class="active">
                    <a href="#">Ako fungujeme</a>
                </li>
                <li>
                    <a href="#">FAQ</a>
                </li>
                <li>
                    <a href="#">Referencie</a>
                </li>
                <li >
                    <a href="#">Partneri</a>
                </li>
                <li>
                    <a href="#">Kontakt</a>
                </li>
            </ul>
        </nav>

        <section class="how-we-works title-description">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <h1 class="title-primary">Ako fungujeme</h1>
                            </div>
                            <div class="col-lg-6">
                                <p class="text-primary">
                                    Vďaka dlhoročným skúsenostiam vieme ako napísať žiadosť o grant,  aby mala čo najväčšie šance na úspech a na čo si dať pozor, aby sme sa vyhli prekážkam.
                                </p>
                            </div>
                        </div>

                        <div class="row mt-5">
                            <div class="col-lg-4">
                                <div class="card">
                                    <h3 class="title-secondary">Grantové vzdelávanie</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vestibulum suscipit turpis vel vulputate. Cras malesuada volutpat tellus. </p>
                                </div>
                                <div class="text-center">
                                    <a href="#" class="arrow-double-link">
                                        <div class="arrow-right"></div>
                                        <div class="label">Naše školenia</div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card card--sun">
                                    <h3 class="title-secondary">Grantové vyhľadávanie</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vestibulum suscipit turpis vel vulputate. Cras malesuada volutpat tellus. </p>
                                </div>
                                <div class="text-center">
                                    <a href="#" class="arrow-double-link">
                                        <div class="arrow-right"></div>
                                        <div class="label">Databáza grantov</div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card">
                                    <h3 class="title-secondary">Vypracovávanie grantov</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vestibulum suscipit turpis vel vulputate. Cras malesuada volutpat tellus. </p>
                                </div>
                                <div class="text-center">
                                    <a href="#" class="arrow-double-link">
                                        <div class="arrow-right"></div>
                                        <div class="label">Zoznam expertov</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="achieved-results title-description mt-0">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <header>
                            <div class="row">
                                <div class="col-lg-6">
                                    <h2 class="title-primary">Naše referencie</h2>

                                </div>
                                <div class="col-lg-6">
                                    <p class="text-primary">
                                        Vďaka dlhoročným skúsenostiam vieme ako napísať žiadosť o grant,  aby mala čo najväčšie šance na úspech a na čo si dať pozor, aby sme sa vyhli prekážkam.
                                    </p>
                                </div>
                            </div>
                        </header>
                        <div class="row results">
                            <div class="col-6 col-lg-3">
                                <h4>2 mil. €</h4>
                                <p class="text-primary">získaných finančných <br>prostriedkov v schválených <br>projektoch</p>
                            </div>
                            <div class="col-6 col-lg-3">
                                <h4>700+</h4>
                                <p class="text-primary">preverených nápadov</p>
                            </div>
                            <div class="col-6 col-lg-3">
                                <h4>8000+</h4>
                                <p class="text-primary">účastníkov <br>školení</p>
                            </div>
                            <div class="col-6 col-lg-3">
                                <h4>25</h4>
                                <p class="text-primary">grantových <br>profesionálov</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="process">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h2 class="title-primary">Aký je postup?</h2>
                    </div>
                    <div class="col-lg-6">
                        <p class="text-primary">
                            Rozumieme individuálnym potrebám každého žiadateľa o grant. Preto sú naše služby dynamické tak, aby sme dokázali pomôcť v ktorejkoľvek fáze riešenia vášho projektu.
                        </p>
                    </div>
                </div>

                <div class="diagram-container diagram-container--desktop">
                    <img class="diagram" src="http://grantexpert.test/assets/img/diagram-how-we-work.jpg" alt="">

                    <div class="button-line-container">
                        <a href="#" class="button-tertiary button-tertiary--small-rounded button-tertiary--shadow">
                            <div class="vertical-center">Vypracovanie projektu</div>
                        </a>
                    </div>
                    <img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="" class="arrow">
                    <div class="button-line-container">
                        <a href="#" class="button-tertiary button-tertiary--small-rounded button-tertiary--shadow">
                            <div class="vertical-center">Konzultácia s grantexpertom</div>
                        </a>
                        <a href="#" class="button-tertiary button-tertiary--small-rounded button-tertiary--shadow">
                            <div class="vertical-center">Overenie oprávnenosti</div>
                        </a>
                    </div>
                    <div class="button-line-container">
                        <a href="#" class="button-tertiary button-tertiary--reverse button-tertiary--small-rounded button-tertiary--shadow">
                            <div class="vertical-center">Webináre</div>
                        </a>
                        <a href="#" class="button-tertiary button-tertiary--reverse button-tertiary--small-rounded button-tertiary--shadow">
                            <div class="vertical-center">E-Learning</div>
                        </a>
                    </div>
                    <div class="button-line-container">
                        <a href="#" class="button-tertiary button-tertiary--reverse button-tertiary--small-rounded button-tertiary--shadow">
                            <div class="vertical-center">Blog</div>
                        </a>
                        <a href="#" class="button-tertiary button-tertiary--reverse button-tertiary--small-rounded button-tertiary--shadow">
                            <div class="vertical-center">Videoslovník</div>
                        </a>
                        <a href="#" class="button-tertiary button-tertiary--reverse button-tertiary--small-rounded button-tertiary--shadow">
                            <div class="vertical-center">1. 2. 3. granty</div>
                        </a>
                    </div>
                </div>

                <div class="diagram-container diagram-container--mobile">
                    <div class="answer">
                        <div class="question-yes-no">
                            <div class="question">Je pre vás téma grantov nová?</div>
                            <div class="col-container">
                                <div class="answer-no"><button class="button-no" data-next="answer-1 answer-7">Nie</button></div>
                                <div class="answer-yes"><button class="button-yes" data-next="answer-9">Áno</button></div>
                            </div>
                        </div>
                    </div>
                    <div class="answer answer-1 answer-hidden">
                        <div class="diagram-arrow diagram-arrow--no"></div>
                        <div class="question-yes-no">
                            <div class="question">Máte nápad na projekt?</div>
                            <div class="col-container">
                                <div class="answer-no"><button>Nie</button></div>
                                <div class="answer-yes"><button class="button-yes" data-next="answer-2" data-dismiss="answer-7">Áno</button></div>
                            </div>
                        </div>
                    </div>

                    <div class="answer answer-2 answer-hidden">
                        <div class="diagram-arrow diagram-arrow--yes"></div>
                        <div class="question-yes-no">
                            <div class="question">Našli ste vhodný grant?</div>
                            <div class="col-container">
                                <div class="answer-no"><button class="button-no" data-next="answer-6">Nie</button></div>
                                <div class="answer-yes"><button class="button-yes" data-next="answer-3">Áno</button></div>
                            </div>
                        </div>
                    </div>
                    <div class="answer answer-3 answer-hidden">
                        <div class="diagram-arrow diagram-arrow--yes"></div>
                        <div class="question-yes-no">
                            <div class="question">Ste si istý, že váš nápad je v poriadku a ste oprávneným žiadateľom?</div>
                            <div class="col-container">
                                <div class="answer-no"><button class="button-no" data-next="answer-5">Nie</button></div>
                                <div class="answer-yes"><button class="button-yes" data-next="answer-4">Áno</button></div>
                            </div>
                        </div>
                    </div>
                    <div class="answer answer-4 answer-hidden">
                        <div class="diagram-arrow diagram-arrow--yes"></div>
                        <div class="button-line-container">
                            <a href="#" class="button-tertiary button-tertiary--small-rounded button-tertiary--shadow">
                                <div class="vertical-center">Vypracovanie projektu</div>
                            </a>
                        </div>
                    </div>
                    <div class="answer answer-5 answer-hidden">
                        <div class="diagram-arrow diagram-arrow--no"></div>
                        <div class="button-line-container">
                            <a href="#" class="button-tertiary button-tertiary--small-rounded button-tertiary--shadow">
                                <div class="vertical-center">Konzultácia s grantexpertom</div>
                            </a>
                            <a href="#" class="button-tertiary button-tertiary--small-rounded button-tertiary--shadow">
                                <div class="vertical-center">Overenie oprávnenosti</div>
                            </a>
                        </div>
                    </div>
                    <div class="answer answer-6 answer-hidden">
                        <div class="diagram-arrow diagram-arrow--no"></div>
                        <div class="button-line-container">
                            <a href="#" class="button-tertiary button-tertiary--reverse button-tertiary--small-rounded button-tertiary--shadow">
                                <div class="vertical-center">Blog</div>
                            </a>
                            <a href="#" class="button-tertiary button-tertiary--reverse button-tertiary--small-rounded button-tertiary--shadow">
                                <div class="vertical-center">Videoslovník</div>
                            </a>
                            <a href="#" class="button-tertiary button-tertiary--reverse button-tertiary--small-rounded button-tertiary--shadow">
                                <div class="vertical-center">1. 2. 3. granty</div>
                            </a>
                        </div>
                    </div>

                    <div class="answer answer-7 answer-hidden mt-3">
                        <div class="diagram-arrow diagram-arrow--no diagram-arrow--hidden"></div>
                        <div class="question-yes-no">
                            <div class="question">Radi by ste sa vzdelávali v oblasti grantov?</div>
                            <div class="col-container">
                                <div class="answer-no"><button>Nie</button></div>
                                <div class="answer-yes"><button class="button-yes" data-next="answer-8" data-dismiss="answer-1">Áno</button></div>
                            </div>
                        </div>
                    </div>
                    <div class="answer answer-8 answer-hidden">
                        <div class="diagram-arrow diagram-arrow--yes"></div>
                        <div class="button-line-container">
                            <a href="#" class="button-tertiary button-tertiary--reverse button-tertiary--small-rounded button-tertiary--shadow">
                                <div class="vertical-center">Webináre</div>
                            </a>
                            <a href="#" class="button-tertiary button-tertiary--reverse button-tertiary--small-rounded button-tertiary--shadow">
                                <div class="vertical-center">E-Learning</div>
                            </a>
                        </div>
                    </div>

                    <div class="answer answer-9 answer-hidden">
                        <div class="diagram-arrow diagram-arrow--yes"></div>
                        <div class="question-yes-no">
                            <div class="question">Chceli by ste sa lepšie zorientovať?</div>
                            <div class="col-container">
                                <div class="answer-no"><button>Nie</button></div>
                                <div class="answer-yes"><button class="button-yes" data-next="answer-10">Áno</button></div>
                            </div>
                        </div>
                    </div>

                    <div class="answer answer-10 answer-hidden">
                        <div class="diagram-arrow diagram-arrow--yes"></div>
                        <div class="button-line-container">
                            <a href="#" class="button-tertiary button-tertiary--reverse button-tertiary--small-rounded button-tertiary--shadow">
                                <div class="vertical-center">Blog</div>
                            </a>
                            <a href="#" class="button-tertiary button-tertiary--reverse button-tertiary--small-rounded button-tertiary--shadow">
                                <div class="vertical-center">Videoslovník</div>
                            </a>
                            <a href="#" class="button-tertiary button-tertiary--reverse button-tertiary--small-rounded button-tertiary--shadow">
                                <div class="vertical-center">1. 2. 3. granty</div>
                            </a>
                        </div>
                    </div>





            </div>
        </section>


        <section class="service-packages">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">

                        <h2 class="title-secondary text-center">Potrebujete komplexnejší balík?</h2>
                        <p class="title-description">
                            Vyberte si z našich balíkov, ktoré tvoria vhodný mix pre vaše aktuálne potreby v oblasti získavania grantov.
                        </p>

                        <div class="row">
                            <div class="col-md-6 col-xl-3">
                                <a href="#">
                                    <div class="card-animation card-shadow-animation">
                                        <div class="card card-shadow">
                                            <div class="subtitle text-secondary text-center">Pre rýchlu orientáciu</div>
                                            <h3 class="title-primary">Free</h3>
                                            <div class="tax-included">CENA S DPH</div>
                                            <div class="title-secondary text-blue">0 €/rok</div>
                                            <ul class="check-list small-padding text-secondary">
                                                <li>Automatické zasielanie notifikácií o nových výzvach</li>
                                                <li>Neobmedzený prístup do databázy grantov a dotácií</li>
                                                <li>Overenie oprávnenosti</li>
                                            </ul>

                                        </div>
                                        <div class="button-secondary">OBJEDNAŤ</div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-xl-3">
                                <a href="#">
                                    <div class="card-animation card-shadow-animation">
                                        <div class="card card-shadow">
                                            <div class="subtitle text-secondary text-center">Pre prvé granty</div>
                                            <h3 class="title-primary">Basic</h3>
                                            <div class="tax-included">CENA S DPH</div>
                                            <div class="title-secondary text-blue">34,90 €/rok</div>
                                            <ul class="check-list small-padding text-secondary">
                                                <li>Automatické zasielanie notifikácií o nových výzvach</li>
                                                <li>Neobmedzený prístup do databázy grantov a dotácií</li>
                                                <li>Overenie oprávnenosti</li>
                                            </ul>

                                        </div>
                                        <div class="button-secondary">OBJEDNAŤ</div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-xl-3">
                                <a href="#">
                                    <div class="card-animation card-shadow-animation">
                                        <div class="card card-shadow">
                                            <div class="subtitle text-secondary text-center">Pre pokročilé vyhľadávanie</div>
                                            <h3 class="title-primary">Medium</h3>
                                            <div class="tax-included">CENA S DPH</div>
                                            <div class="title-secondary text-blue">69,90 €/rok</div>
                                            <ul class="check-list small-padding text-secondary">
                                                <li>Neobmedzený prístup do databázy grantov a dotácií</li>
                                                <li>Archív bezplatných webinárov</li>
                                                <li>Vyhľadanie dotácie k 1 vášmu projektu</li>
                                            </ul>

                                        </div>
                                        <div class="button-secondary">OBJEDNAŤ</div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-xl-3">
                                <a href="#">
                                    <div class="card-animation card-shadow-animation card-shadow-animation--gold">
                                        <div class="card card-shadow">
                                            <div class="subtitle text-secondary text-center">Pre aktívnych riešiteľov</div>
                                            <h3 class="title-primary">Profi</h3>
                                            <div class="tax-included">CENA S DPH</div>
                                            <div class="title-secondary text-blue">149,90 €/rok</div>
                                            <ul class="check-list small-padding text-secondary">
                                                <li>Neobmedzený prístup do databázy grantov a dotácií</li>
                                                <li>Archív bezplatných webinárov</li>
                                                <li>Vyhľadanie dotácie k 1 vášmu projektu</li>
                                            </ul>

                                        </div>
                                        <div class="button-secondary">OBJEDNAŤ</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="button-center">
                            <a href="" class="button-primary"><div class="vertical-center">POROVNAŤ BALÍKY</div></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>





    </main>

    <?php include '../../_components/_footer.php';?>

