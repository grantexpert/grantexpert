
<?php include '../../_components/_head.php';?>

<body class="page-references page-about-us">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li>
                    <a href="#">Kto sme</a>
                </li>
                <li>
                    <a href="#">Ako fungujeme</a>
                </li>
                <li>
                    <a href="#">FAQ</a>
                </li>
                <li class="active">
                    <a href="#">Referencie</a>
                </li>
                <li>
                    <a href="#">Partneri</a>
                </li>
                <li>
                    <a href="#">Kontakt</a>
                </li>
            </ul>
        </nav>

        <section class="achieved-results title-description mt-0">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <header>
                            <div class="row">
                                <div class="col-lg-6">
                                    <h1 class="title-primary">Naše referencie</h1>

                                </div>
                                <div class="col-lg-6">
                                    <p class="text-primary">
                                        Vďaka dlhoročným skúsenostiam vieme ako napísať žiadosť o grant,  aby mala čo najväčšie šance na úspech a na čo si dať pozor, aby sme sa vyhli prekážkam.
                                    </p>
                                </div>
                            </div>
                        </header>
                        <div class="row results">
                            <div class="col-6 col-lg-3">
                                <h4>2 mil. €</h4>
                                <p class="text-primary">získaných finančných <br>prostriedkov v schválených <br>projektoch</p>
                            </div>
                            <div class="col-6 col-lg-3">
                                <h4>700+</h4>
                                <p class="text-primary">preverených nápadov</p>
                            </div>
                            <div class="col-6 col-lg-3">
                                <h4>8000+</h4>
                                <p class="text-primary">účastníkov <br>školení</p>
                            </div>
                            <div class="col-6 col-lg-3">
                                <h4>25</h4>
                                <p class="text-primary">grantových <br>profesionálov</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="project-list">
            <div class="container">
                <h2 class="title-primary">Príklady úspešných projektov, ktoré sme realizovali</h2>

                <header>
                    <div class="row">
                        <div class="col-md-6 col-lg-3">
                            <div class="vertical-center">
                                Žiadateľ
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="vertical-center">
                                Grantový zdroj
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="vertical-center">
                                Región
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="vertical-center">
                                Schválená celková výška
                                oprávnených výdavkov
                            </div>
                        </div>
                    </div>
                </header>
            </div>
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <a href="#" class="card-project">
                            <div class="card-project-container">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            Jajneken s.r.o.
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            Integrovaný regionálny
                                            operačný program
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            Bratislavský kraj
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-price col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            256 308,49 €
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>

                        <a href="#" class="card-project">
                            <div class="card-project-container">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            Jajneken s.r.o.
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            Integrovaný regionálny
                                            operačný program
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            Bratislavský kraj
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-price col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            256 308,49 €
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>

                        <a href="#" class="card-project">
                            <div class="card-project-container">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            Jajneken s.r.o.
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            Integrovaný regionálny
                                            operačný program
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            Bratislavský kraj
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-price col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            256 308,49 €
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>








                    </div>
                </div>
            </div>
        </section>


    </main>

    <?php include '../../_components/_footer.php';?>

