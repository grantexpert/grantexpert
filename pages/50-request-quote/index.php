
<?php include '../../_components/_head.php';?>

<body class="page-request-quote">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">
        <form action="">
            <section class="request-quote">
                <div class="container">
                    <div class="row">
                        <div class="offset-lg-1 col-lg-10">
                            <header>
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="order">Žiadosť o vypracovanie  ponuky</div>
                                        <h3 class="title-secondary">Služba: Vypracovanie projektu</h3>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="icon">
                                            <img src="http://grantexpert.test/assets/img/calc.png" width="35" alt="">
                                        </div>
                                    </div>
                                </div>
                            </header>
                            <div class="shadow-container">

                                <div class="form-part">
                                    <h2 class="title-secondary">Financovanie projektu</h2>

                                    <div class="form-label form-label--required">
                                        <div class="col-container">
                                            <div class="col-text">
                                                Uveďte krátky, predbežný názov projektu
                                            </div>
                                            <div class="col-required"></div>
                                        </div>
                                    </div>
                                    <input class="custom-form-control" type="text">
                                </div>
                                <div class="form-part">
                                    <div class="form-label form-label--required">
                                        <div class="col-container">
                                            <div class="col-text">
                                                Zdroj spolufinancovania vlastného podielu z celkového rozpočtu projektu
                                            </div>
                                            <div class="col-required"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="radio-button-container">vlastné zdroje
                                                <input type="radio" name="payment-method" value="">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="radio-button-container">bankový úver
                                                <input type="radio" name="payment-method" value="">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-part">
                                    <div class="form-label form-label--required">
                                        <div class="col-container">
                                            <div class="col-text">
                                                Odhadovaný celkový rozpočet projektu (cudzie + vlastné zdroje)
                                            </div>
                                            <div class="col-required"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="radio-button-container"> do 100 000 €
                                                <input type="radio" name="payment-method" value="">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="radio-button-container"> 301 000 - 1 000 000 €
                                                <input type="radio" name="payment-method" value="">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="radio-button-container">101 000 € - 300 000 €
                                                <input type="radio" name="payment-method" value="">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="radio-button-container">nad 1 000 000 €
                                                <input type="radio" name="payment-method" value="">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-part">
                                    <div class="form-label form-label--required">
                                        <div class="col-container">
                                            <div class="col-text">
                                                Požadovaný podiel grantu z celkového rozpočtu projektu
                                            </div>
                                            <div class="col-required"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="radio-button-container">do 50%
                                                <input type="radio" name="payment-method" value="">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="radio-button-container">75% - 95%
                                                <input type="radio" name="payment-method" value="">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="radio-button-container">51% - 75%
                                                <input type="radio" name="payment-method" value="">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-part mb-5">
                                    <div class="form-label form-label--required">
                                        <div class="col-container">
                                            <div class="col-text">
                                                Kraj, v ktorom sa budú odohrávať aktivity vášho projektu
                                            </div>
                                            <div class="col-required"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox--text-light">
                                                <input name="" type="checkbox">
                                                <div class="label">
                                                    <div class="inline-container">
                                                        <div class="col-container">
                                                            <div class="col-text">Trnavský kraj</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="checkbox checkbox--text-light">
                                                <input name="" type="checkbox">
                                                <div class="label">
                                                    <div class="inline-container">
                                                        <div class="col-container">
                                                            <div class="col-text">Bratislavský kraj</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="checkbox checkbox--text-light">
                                                <input name="" type="checkbox">
                                                <div class="label">
                                                    <div class="inline-container">
                                                        <div class="col-container">
                                                            <div class="col-text">Košický kraj</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="checkbox checkbox--text-light">
                                                <input name="" type="checkbox">
                                                <div class="label">
                                                    <div class="inline-container">
                                                        <div class="col-container">
                                                            <div class="col-text">Nitriansky kraj</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="checkbox checkbox--text-light">
                                                <input name="" type="checkbox">
                                                <div class="label">
                                                    <div class="inline-container">
                                                        <div class="col-container">
                                                            <div class="col-text">Banskobystrický kraj</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox--text-light">
                                                <input name="" type="checkbox">
                                                <div class="label">
                                                    <div class="inline-container">
                                                        <div class="col-container">
                                                            <div class="col-text">Žilinský kraj</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="checkbox checkbox--text-light">
                                                <input name="" type="checkbox">
                                                <div class="label">
                                                    <div class="inline-container">
                                                        <div class="col-container">
                                                            <div class="col-text">Prešovský kraj</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="checkbox checkbox--text-light">
                                                <input name="" type="checkbox">
                                                <div class="label">
                                                    <div class="inline-container">
                                                        <div class="col-container">
                                                            <div class="col-text">Trenčiansky kraj</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="checkbox checkbox--text-light">
                                                <input name="" type="checkbox">
                                                <div class="label">
                                                    <div class="inline-container">
                                                        <div class="col-container">
                                                            <div class="col-text">Celá Slovenská republika</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="checkbox checkbox--text-light">
                                                <input name="" type="checkbox">
                                                <div class="label">
                                                    <div class="inline-container">
                                                        <div class="col-container">
                                                            <div class="col-text">Zahraničie</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-part form-part--gray form-part--no-border">
                                    <h2 class="title-secondary">Informácie o projekte</h2>

                                    <div class="form-label form-label--required">
                                        <div class="col-container">
                                            <div class="col-text">
                                                Zadajte názov výzvy, do ktorej sa chcete zapojiť *
                                            </div>
                                            <div class="col-required"></div>
                                        </div>
                                    </div>
                                    <input class="custom-form-control" type="text">

                                    <div class="grant-search">
                                        <div class="col-container">
                                            <div class="col-text">
                                                Nemáte pre váš projekt nájdenú výzvu?
                                            </div>
                                            <div class="col-button">
                                                <button class="button-tertiary button-tertiary--small-padding">Vyhľadať grant</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-part form-part--gray">
                                    <div class="form-label form-label--required">
                                        <div class="col-container">
                                            <div class="col-text">
                                                Vyberte oblasť, ktorej sa požiadavka bude týkať
                                            </div>
                                            <div class="col-required"></div>
                                        </div>
                                    </div>
                                    <div class="custom-form-control selectbox selectbox--blue-shadow selectbox--small">
                                        <input type="hidden" name="">
                                        <div class="selectbox-label">Oblasť expertízy</div>
                                        <ul>
                                            <li data-value="current">Oblasť expertízy 1</li>
                                            <li data-value="planned">Oblasť expertízy 2</li>
                                            <li data-value="closed">Oblasť expertízy 3</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-part form-part--gray">
                                    <div class="form-label form-label--required mt-4">
                                        <div class="col-container">
                                            Stručný opis predmetu konzultácie
                                        </div>
                                    </div>
                                    <textarea class="textarea-char-count" name=""></textarea>
                                    <div class="char-count"><span class="char-used">0</span>/1000 znakov</div>
                                </div>

                                <div class="form-part form-part--no-border mb-5">
                                    <h2 class="title-secondary">Príloha</h2>

                                    <div class="attachment">
                                        <div class="file">
                                            <div class="col-container">
                                                <div class="col-filename">
                                                    Názov nahratej prílohy č.1
                                                </div>
                                                <div class="col-remove">
                                                    <button>Zmazať <img src="http://grantexpert.test/assets/img/svg/close-button-blue.svg" alt=""></button>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="file">
                                            <div class="col-container">
                                                <div class="col-filename">
                                                    Názov nahratej prílohy č.1
                                                </div>
                                                <div class="col-remove">
                                                    <button>Zmazať <img src="http://grantexpert.test/assets/img/svg/close-button-blue.svg" alt=""></button>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="file">
                                            <div class="col-container">
                                                <div class="col-filename">
                                                    Názov nahratej prílohy č.1
                                                </div>
                                                <div class="col-remove">
                                                    <button>Zmazať <img src="http://grantexpert.test/assets/img/svg/close-button-blue.svg" alt=""></button>

                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <button class="button-primary button-primary--small-padding">Nahrať prílohu</button>

                                </div>

                                <div class="billing-information form-part form-part--gray form-part--no-border">
                                    <h2 class="title-secondary">Kontaktné údaje</h2>

                                    <div class="row">
                                        <div class="offset-xl-6 col-xl-6">
                                            <div class="card-animation card--login">
                                                <div class="card">
                                                    <p>Ak ste registrovaný užívateľ, prihláste sa</p>
                                                    <div class="button-center">
                                                        <button class="login-button button-tertiary button-icon">
                                                            <div class="col-container">
                                                                <div class="icon-container">
                                                                    <img src="http://grantexpert.test/assets/img/svg/user-white.svg" alt="">
                                                                </div>
                                                                <div class="label-container">
                                                                    Prihlásiť sa
                                                                </div>
                                                            </div>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6 col-md-6">
                                            <div class="input-required">
                                                <div class="input-container">
                                                    <input class="custom-form-control" type="text" placeholder="IČO">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <div class="input-required">
                                                <div class="input-container">
                                                    <input class="custom-form-control" type="text" placeholder="Názov spoločnosti">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="checkbox checkbox--text-light">
                                                <input name="" type="checkbox">
                                                <div class="label">
                                                    <div class="inline-container">
                                                        <div class="col-container">
                                                            <div class="col-text">Som fyzická osoba</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-md-6">
                                            <div class="input-required">
                                                <div class="input-container">
                                                    <input class="custom-form-control" type="text" placeholder="Meno">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <div class="input-required error-message">
                                                <div class="input-container">
                                                    <input class="custom-form-control" type="text" placeholder="Priezvisko">
                                                </div>
                                                <div class="error-message">Toto pole je povinné</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-md-6">
                                            <div class="input-required">
                                                <div class="input-container">
                                                    <input class="custom-form-control" type="text" placeholder="Telefón">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <div class="input-required">
                                                <div class="input-container">
                                                    <input class="custom-form-control" type="email" placeholder="Email">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <footer>
                                <div class="col-container">
                                    <div class="col-privacy-policy">
                                        <div class="checkbox checkbox--text-light">
                                            <input name="" type="checkbox">
                                            <div class="label">
                                                <div class="inline-container">
                                                    <div class="col-container">
                                                        <div class="col-text">Súhlasím so spracúvaním svojich osobných údajov na účely zasielania noviniek z oblasti grantového poradenstva v súlade so zásadami ochrany osobných údajov.</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-required">
                                        <div class="required-info">
                                            <img src="http://grantexpert.test/assets/img/svg/required-icon.svg" alt=""> Povinný údaj
                                        </div>
                                    </div>

                                </div>
                            </footer>


                        </div>
                    </div>
                </div>

            </section>
        </form>



    </main>

    <?php include '../../_components/_footer.php';?>

