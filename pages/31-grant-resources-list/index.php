
<?php include '../../_components/_head.php';?>

<body class="page-grant-resources-list page-grant-education">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li>
                    <a href="#">E-LEANING KURZY</a>
                </li>
                <li>
                    <a href="#">WEBINÁRE</a>
                </li>
                <li>
                    <a href="#">ŠKOLENIA NA MIERU</a>
                </li>
                <li>
                    <a href="#">VIDEOSLOVNÍK</a>
                </li>
                <li class="active">
                    <a href="#">GRANTOVÉ ZDROJE</a>
                </li>
                <li>
                    <a href="#">BLOG</a>
                </li>
            </ul>
        </nav>

        <section class="title-description">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <h1 class="title-primary">Grantové zdroje</h1>
                            </div>
                            <div class="col-lg-6">
                                <p class="text-primary">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porta molestie lacus, eget feugiat nulla. Maecenas pulvinar sodales dolor, a cursus lacus hendrerit quis. Maecenas viverra lorem ac ex porta luctus.
                                </p>
                            </div>
                        </div>

                        <div class="roll-down-list grant-resources-list mt-5 mt-lg-6">
                            <div class="roll-down-item">
                                <div class="roll-down-item-container">
                                    <div class="label">
                                        <div class="col-container">
                                            <div class="col-info">
                                                <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                                    <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                                        <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                                        <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                                        <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                                    </svg>
                                                </span>
                                            </div>
                                            <h2>
                                                Bruselské granty
                                            </h2>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <a href="#">
                                            <div class="card-grant-resource">
                                                <div class="card-grant-resource-container">
                                                    <div class="col-image">
                                                        <img src="http://grantexpert.test/assets/img/europa-pre-obcanov-logo.jpg" alt="">
                                                    </div>
                                                    <div class="col-arrow">
                                                        <img src="http://grantexpert.test/assets/img/svg/double-arrow-blue.svg" alt="">
                                                    </div>
                                                    <div class="col-text">
                                                        <h3>Európa pre občanov 2014-2020</h3>
                                                        <div class="description">
                                                            <p>
                                                                Celková alokácia sedemročného komunitárneho programu EÚ „Európa pre občanov 2014 – 2020“ predstavuje takmer 185,5 mil. eur. Hlavným cieľom je zlepšovať podmienky pre občiansku participáciu v Európskej únii.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>

                                        <a href="#">
                                            <div class="card-grant-resource">
                                                <div class="card-grant-resource-container">
                                                    <div class="col-image">
                                                        <img src="http://grantexpert.test/assets/img/europa-pre-obcanov-logo.jpg" alt="">
                                                    </div>
                                                    <div class="col-arrow">
                                                        <img src="http://grantexpert.test/assets/img/svg/double-arrow-blue.svg" alt="">
                                                    </div>
                                                    <div class="col-text">
                                                        <h3>Európa pre občanov 2014-2020</h3>
                                                        <div class="description">
                                                            <p>
                                                                Celková alokácia sedemročného komunitárneho programu EÚ „Európa pre občanov 2014 – 2020“ predstavuje takmer 185,5 mil. eur. Hlavným cieľom je zlepšovať podmienky pre občiansku participáciu v Európskej únii.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>

                                        <a href="#">
                                            <div class="card-grant-resource">
                                                <div class="card-grant-resource-container">
                                                    <div class="col-image">
                                                        <img src="http://grantexpert.test/assets/img/europa-pre-obcanov-logo.jpg" alt="">
                                                    </div>
                                                    <div class="col-arrow">
                                                        <img src="http://grantexpert.test/assets/img/svg/double-arrow-blue.svg" alt="">
                                                    </div>
                                                    <div class="col-text">
                                                        <h3>Európa pre občanov 2014-2020</h3>
                                                        <div class="description">
                                                            <p>
                                                                Celková alokácia sedemročného komunitárneho programu EÚ „Európa pre občanov 2014 – 2020“ predstavuje takmer 185,5 mil. eur. Hlavným cieľom je zlepšovať podmienky pre občiansku participáciu v Európskej únii.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="roll-down-item">
                                <div class="roll-down-item-container">
                                    <div class="label">
                                        <div class="col-container">
                                            <div class="col-info">
                                                <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                                    <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                                        <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                                        <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                                        <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                                    </svg>
                                                </span>
                                            </div>
                                            <h2>
                                                Bruselské granty
                                            </h2>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <a href="#">
                                            <div class="card-grant-resource">
                                                <div class="card-grant-resource-container">
                                                    <div class="col-image">
                                                        <img src="http://grantexpert.test/assets/img/europa-pre-obcanov-logo.jpg" alt="">
                                                    </div>
                                                    <div class="col-arrow">
                                                        <img src="http://grantexpert.test/assets/img/svg/double-arrow-blue.svg" alt="">
                                                    </div>
                                                    <div class="col-text">
                                                        <h3>Európa pre občanov 2014-2020</h3>
                                                        <div class="description">
                                                            <p>
                                                                Celková alokácia sedemročného komunitárneho programu EÚ „Európa pre občanov 2014 – 2020“ predstavuje takmer 185,5 mil. eur. Hlavným cieľom je zlepšovať podmienky pre občiansku participáciu v Európskej únii.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>

                                        <a href="#">
                                            <div class="card-grant-resource">
                                                <div class="card-grant-resource-container">
                                                    <div class="col-image">
                                                        <img src="http://grantexpert.test/assets/img/europa-pre-obcanov-logo.jpg" alt="">
                                                    </div>
                                                    <div class="col-arrow">
                                                        <img src="http://grantexpert.test/assets/img/svg/double-arrow-blue.svg" alt="">
                                                    </div>
                                                    <div class="col-text">
                                                        <h3>Európa pre občanov 2014-2020</h3>
                                                        <div class="description">
                                                            <p>
                                                                Celková alokácia sedemročného komunitárneho programu EÚ „Európa pre občanov 2014 – 2020“ predstavuje takmer 185,5 mil. eur. Hlavným cieľom je zlepšovať podmienky pre občiansku participáciu v Európskej únii.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>

                                        <a href="#">
                                            <div class="card-grant-resource">
                                                <div class="card-grant-resource-container">
                                                    <div class="col-image">
                                                        <img src="http://grantexpert.test/assets/img/europa-pre-obcanov-logo.jpg" alt="">
                                                    </div>
                                                    <div class="col-arrow">
                                                        <img src="http://grantexpert.test/assets/img/svg/double-arrow-blue.svg" alt="">
                                                    </div>
                                                    <div class="col-text">
                                                        <h3>Európa pre občanov 2014-2020</h3>
                                                        <div class="description">
                                                            <p>
                                                                Celková alokácia sedemročného komunitárneho programu EÚ „Európa pre občanov 2014 – 2020“ predstavuje takmer 185,5 mil. eur. Hlavným cieľom je zlepšovať podmienky pre občiansku participáciu v Európskej únii.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="roll-down-item">
                                <div class="roll-down-item-container">
                                    <div class="label">
                                        <div class="col-container">
                                            <div class="col-info">
                                                <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                                    <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                                        <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                                        <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                                        <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                                    </svg>
                                                </span>
                                            </div>
                                            <h2>
                                                Bruselské granty
                                            </h2>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <a href="#">
                                            <div class="card-grant-resource">
                                                <div class="card-grant-resource-container">
                                                    <div class="col-image">
                                                        <img src="http://grantexpert.test/assets/img/europa-pre-obcanov-logo.jpg" alt="">
                                                    </div>
                                                    <div class="col-arrow">
                                                        <img src="http://grantexpert.test/assets/img/svg/double-arrow-blue.svg" alt="">
                                                    </div>
                                                    <div class="col-text">
                                                        <h3>Európa pre občanov 2014-2020</h3>
                                                        <div class="description">
                                                            <p>
                                                                Celková alokácia sedemročného komunitárneho programu EÚ „Európa pre občanov 2014 – 2020“ predstavuje takmer 185,5 mil. eur. Hlavným cieľom je zlepšovať podmienky pre občiansku participáciu v Európskej únii.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>

                                        <a href="#">
                                            <div class="card-grant-resource">
                                                <div class="card-grant-resource-container">
                                                    <div class="col-image">
                                                        <img src="http://grantexpert.test/assets/img/europa-pre-obcanov-logo.jpg" alt="">
                                                    </div>
                                                    <div class="col-arrow">
                                                        <img src="http://grantexpert.test/assets/img/svg/double-arrow-blue.svg" alt="">
                                                    </div>
                                                    <div class="col-text">
                                                        <h3>Európa pre občanov 2014-2020</h3>
                                                        <div class="description">
                                                            <p>
                                                                Celková alokácia sedemročného komunitárneho programu EÚ „Európa pre občanov 2014 – 2020“ predstavuje takmer 185,5 mil. eur. Hlavným cieľom je zlepšovať podmienky pre občiansku participáciu v Európskej únii.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>

                                        <a href="#">
                                            <div class="card-grant-resource">
                                                <div class="card-grant-resource-container">
                                                    <div class="col-image">
                                                        <img src="http://grantexpert.test/assets/img/europa-pre-obcanov-logo.jpg" alt="">
                                                    </div>
                                                    <div class="col-arrow">
                                                        <img src="http://grantexpert.test/assets/img/svg/double-arrow-blue.svg" alt="">
                                                    </div>
                                                    <div class="col-text">
                                                        <h3>Európa pre občanov 2014-2020</h3>
                                                        <div class="description">
                                                            <p>
                                                                Celková alokácia sedemročného komunitárneho programu EÚ „Európa pre občanov 2014 – 2020“ predstavuje takmer 185,5 mil. eur. Hlavným cieľom je zlepšovať podmienky pre občiansku participáciu v Európskej únii.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </section>


    </main>

    <?php include '../../_components/_footer.php';?>

