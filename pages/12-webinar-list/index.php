
<?php include '../../_components/_head.php';?>

<body class="page-webinar-list page-grant-education">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li>
                    <a href="#">E-LEANING KURZY</a>
                </li>
                <li class="active">
                    <a href="#">WEBINÁRE</a>
                </li>
                <li>
                    <a href="#">ŠKOLENIA NA MIERU</a>
                </li>
                <li>
                    <a href="#">VIDEOSLOVNÍK</a>
                </li>
                <li>
                    <a href="#">GRANTOVÉ ZDROJE</a>
                </li>
                <li>
                    <a href="#">BLOG</a>
                </li>
            </ul>
        </nav>

        <section class="page-section-container">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <section class="title-description">
                            <div class="row">
                                <div class="col-lg-6">
                                    <h1 class="title-primary">Webináre</h1>
                                </div>
                                <div class="col-lg-6">
                                    <p class="text-primary">
                                        Pravidelne organizujeme webináre na najrôznejšie témy z oblasti grantov. Naši predplatitelia balíkov MEDIUM a PROFI majú prístup ku všetkým našim webinárom zadarmo.
                                    </p>
                                </div>
                            </div>
                        </section>

                        <section class="tag-list">
                            <a href="#" class="tag-small-rounded tag-small-rounded--inverted active">Všetky</a>
                            <a href="#" class="tag-small-rounded tag-small-rounded--inverted">Nadchádzajúce</a>
                            <a href="#" class="tag-small-rounded tag-small-rounded--inverted">Archív</a>
                            <a href="#" class="tag-small-rounded tag-small-rounded--inverted">Zakúpené</a>
                        </section>

                        <section class="card-event-list">
                            <div class="row">
                                <div class="col-md-6 col-xl-4">
                                    <div class="card-event-container">
                                        <div class="card card-event">
                                            <header>
                                                <div class="col-container">
                                                    <a href="#" class="title-container">
                                                        <h3 class="title-secondary">Grantová podpora škôl</h3>
                                                    </a>
                                                    <div class="icon-container">
                                                        <img src="http://grantexpert.test/assets/img/svg/calendar.svg" alt="calendar">
                                                    </div>
                                                </div>

                                                <div class="date-time">
                                                    <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/calendar.svg" alt="calendar"></span><span class="date">28.10.</span><span class="time">10:00 - 12:00</span>
                                                </div>


                                            </header>
                                            <h4>Čo sa naučíte?</h4>
                                            <ul class="check-list">
                                                <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                <li>Aktuálne a plánované výzvy</li>
                                            </ul>
                                            <footer>
                                                <div class="col-container">
                                                    <div class="price-container">
                                                        <div class="tax-included">Cena s DPH</div>
                                                        <div class="title-secondary color-sun price">19,90 €</div>
                                                    </div>
                                                    <div class="button-container">
                                                        <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                                    </div>
                                                </div>
                                            </footer>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-4">
                                    <div class="card-event-container">
                                        <div class="card card-event">
                                            <header>
                                                <div class="col-container">
                                                    <a href="#" class="title-container">
                                                        <h3 class="title-secondary">Cesta úspešného projektu: Podnikateľský IT projekt</h3>
                                                    </a>
                                                    <div class="icon-container">
                                                        <img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker">
                                                    </div>
                                                </div>

                                                <div class="preview-container col-container">
                                                    <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker"></span>
                                                    <div class="stars">
                                                        <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                        <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                        <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                        <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                        <img src="http://grantexpert.test/assets/img/svg/star-blank.svg" alt="star-blank">
                                                    </div>
                                                    <a href="#" class="button-preview"><img src="http://grantexpert.test/assets/img/svg/play.svg" alt="play"><span class="label">UKÁŽKA</span></a>
                                                </div>


                                            </header>
                                            <h4>Čo sa naučíte?</h4>
                                            <ul class="check-list">
                                                <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                <li>Aktuálne a plánované výzvy</li>
                                            </ul>
                                            <footer>
                                                <div class="col-container">
                                                    <div class="price-container">
                                                        <div class="tax-included">Cena s DPH</div>
                                                        <div class="title-secondary color-sun price">19,90 €</div>
                                                    </div>
                                                    <div class="button-container">
                                                        <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                                    </div>

                                                </div>
                                            </footer>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-xl-4">
                                    <div class="card-event-container event-watching">
                                        <div class="card card-event">
                                            <header>
                                                <div class="col-container">
                                                    <a href="#" class="title-container">
                                                        <h3 class="title-secondary">Cesta úspešného projektu: Podnikateľský IT projekt</h3>
                                                    </a>
                                                    <div class="icon-container">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="12.25" height="14.168" viewBox="0 0 12.25 14.168">
                                                            <path id="Path_884" data-name="Path 884" d="M578.222,129.352l-8.778-5.121a1.559,1.559,0,0,1,0-2.58l8.778-5.121c.822-.479,1.789.218,1.789,1.29v10.243C580.011,129.134,579.044,129.831,578.222,129.352Z" transform="translate(580.511 130.025) rotate(180)" fill="none" stroke="#fff" stroke-linejoin="bevel" stroke-width="1"/>
                                                        </svg>
                                                    </div>
                                                </div>

                                                <div class="preview-container col-container">
                                                    <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker"></span>
                                                    <div class="stars">
                                                        <img src="http://grantexpert.test/assets/img/svg/star-white.svg" alt="star">
                                                        <img src="http://grantexpert.test/assets/img/svg/star-white.svg" alt="star">
                                                        <img src="http://grantexpert.test/assets/img/svg/star-white.svg" alt="star">
                                                        <img src="http://grantexpert.test/assets/img/svg/star-white.svg" alt="star">
                                                        <img src="http://grantexpert.test/assets/img/svg/star-blank-white.svg" alt="star-blank">
                                                    </div>

                                                </div>


                                            </header>
                                            <h4>Čo sa naučíte?</h4>
                                            <ul class="check-list">
                                                <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                <li>Aktuálne a plánované výzvy</li>
                                            </ul>
                                            <footer>
                                                <div class="col-container">
                                                    <div class="price-container">
                                                        <div class="col-container">
                                                            <div class="col-icon">
                                                                <div class="circle-progressbar">
                                                                    <div class="pie-wrapper progress-50 style-2">
                                                                        <div class="pie">
                                                                            <div class="left-side half-circle"></div>
                                                                            <div class="right-side half-circle"></div>
                                                                        </div>
                                                                        <div class="pie-wrapper-shadow"></div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="col-text">
                                                                <div class="text-small-label-secondary">
                                                                    Stav
                                                                </div>
                                                                <div class="title-secondary">
                                                                    50%
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="button-container">
                                                        <a href="#" class="button-preview">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="12.25" height="14.168" viewBox="0 0 12.25 14.168">
                                                                <path id="Path_884" data-name="Path 884" d="M578.222,129.352l-8.778-5.121a1.559,1.559,0,0,1,0-2.58l8.778-5.121c.822-.479,1.789.218,1.789,1.29v10.243C580.011,129.134,579.044,129.831,578.222,129.352Z" transform="translate(580.511 130.025) rotate(180)" fill="none" stroke="#fff" stroke-linejoin="bevel" stroke-width="1"/>
                                                            </svg>
                                                            <span class="label">Pokračovať</span>
                                                        </a>
                                                    </div>

                                                </div>
                                            </footer>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-xl-4">
                                    <div class="card-event-container">
                                        <div class="card card-event">
                                            <header>
                                                <div class="col-container">
                                                    <a href="#" class="title-container">
                                                        <h3 class="title-secondary">Grantová podpora škôl</h3>
                                                    </a>
                                                    <div class="icon-container">
                                                        <img src="http://grantexpert.test/assets/img/svg/calendar.svg" alt="calendar">
                                                    </div>
                                                </div>

                                                <div class="date-time">
                                                    <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/calendar.svg" alt="calendar"></span><span class="date">28.10.</span><span class="time">10:00 - 12:00</span>
                                                </div>


                                            </header>
                                            <h4>Čo sa naučíte?</h4>
                                            <ul class="check-list">
                                                <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                <li>Aktuálne a plánované výzvy</li>
                                            </ul>
                                            <footer>
                                                <div class="col-container">
                                                    <div class="price-container">
                                                        <div class="tax-included">Cena s DPH</div>
                                                        <div class="title-secondary color-sun price">19,90 €</div>
                                                    </div>
                                                    <div class="button-container">
                                                        <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                                    </div>
                                                </div>
                                            </footer>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-4">
                                    <div class="card-event-container">
                                        <div class="card card-event">
                                            <header>
                                                <div class="col-container">
                                                    <a href="#" class="title-container">
                                                        <h3 class="title-secondary">Cesta úspešného projektu: Podnikateľský IT projekt</h3>
                                                    </a>
                                                    <div class="icon-container">
                                                        <img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker">
                                                    </div>
                                                </div>

                                                <div class="preview-container col-container">
                                                    <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker"></span>
                                                    <div class="stars">
                                                        <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                        <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                        <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                        <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                        <img src="http://grantexpert.test/assets/img/svg/star-blank.svg" alt="star-blank">
                                                    </div>
                                                    <a href="#" class="button-preview"><img src="http://grantexpert.test/assets/img/svg/play.svg" alt="play"><span class="label">UKÁŽKA</span></a>
                                                </div>


                                            </header>
                                            <h4>Čo sa naučíte?</h4>
                                            <ul class="check-list">
                                                <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                <li>Aktuálne a plánované výzvy</li>
                                            </ul>
                                            <footer>
                                                <div class="col-container">
                                                    <div class="price-container">
                                                        <div class="tax-included">Cena s DPH</div>
                                                        <div class="title-secondary color-sun price">19,90 €</div>
                                                    </div>
                                                    <div class="button-container">
                                                        <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                                    </div>

                                                </div>
                                            </footer>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-xl-4">
                                    <div class="card-event-container event-watching">
                                        <div class="card card-event">
                                            <header>
                                                <div class="col-container">
                                                    <a href="#" class="title-container">
                                                        <h3 class="title-secondary">Cesta úspešného projektu: Podnikateľský IT projekt</h3>
                                                    </a>
                                                    <div class="icon-container">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="12.25" height="14.168" viewBox="0 0 12.25 14.168">
                                                            <path id="Path_884" data-name="Path 884" d="M578.222,129.352l-8.778-5.121a1.559,1.559,0,0,1,0-2.58l8.778-5.121c.822-.479,1.789.218,1.789,1.29v10.243C580.011,129.134,579.044,129.831,578.222,129.352Z" transform="translate(580.511 130.025) rotate(180)" fill="none" stroke="#fff" stroke-linejoin="bevel" stroke-width="1"/>
                                                        </svg>
                                                    </div>
                                                </div>

                                                <div class="preview-container col-container">
                                                    <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker"></span>
                                                    <div class="stars">
                                                        <img src="http://grantexpert.test/assets/img/svg/star-white.svg" alt="star">
                                                        <img src="http://grantexpert.test/assets/img/svg/star-white.svg" alt="star">
                                                        <img src="http://grantexpert.test/assets/img/svg/star-white.svg" alt="star">
                                                        <img src="http://grantexpert.test/assets/img/svg/star-white.svg" alt="star">
                                                        <img src="http://grantexpert.test/assets/img/svg/star-blank-white.svg" alt="star-blank">
                                                    </div>

                                                </div>


                                            </header>
                                            <h4>Čo sa naučíte?</h4>
                                            <ul class="check-list">
                                                <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                <li>Aktuálne a plánované výzvy</li>
                                            </ul>
                                            <footer>
                                                <div class="col-container">
                                                    <div class="price-container">
                                                        <div class="col-container">
                                                            <div class="col-icon">
                                                                <div class="circle-progressbar">
                                                                    <div class="pie-wrapper progress-20 style-2">
                                                                        <div class="pie">
                                                                            <div class="left-side half-circle"></div>
                                                                            <div class="right-side half-circle"></div>
                                                                        </div>
                                                                        <div class="pie-wrapper-shadow"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-text">
                                                                <div class="text-small-label-secondary">
                                                                    Stav
                                                                </div>
                                                                <div class="title-secondary">
                                                                    20%
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="button-container">
                                                        <a href="#" class="button-preview">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="12.25" height="14.168" viewBox="0 0 12.25 14.168">
                                                                <path id="Path_884" data-name="Path 884" d="M578.222,129.352l-8.778-5.121a1.559,1.559,0,0,1,0-2.58l8.778-5.121c.822-.479,1.789.218,1.789,1.29v10.243C580.011,129.134,579.044,129.831,578.222,129.352Z" transform="translate(580.511 130.025) rotate(180)" fill="none" stroke="#fff" stroke-linejoin="bevel" stroke-width="1"/>
                                                            </svg>
                                                            <span class="label">Pokračovať</span>
                                                        </a>
                                                    </div>

                                                </div>
                                            </footer>
                                        </div>
                                    </div>
                                </div>



                            </div> <!-- row -->
                        </section>


                        <section class="pager">
                            <a href="#">
                                <div class="arrow-double-hover arrow-left"></div>
                            </a>

                            <nav>
                                <ul>
                                    <li class="page active"><a href="#">1</a></li>
                                    <li class="page"><a href="#">2</a></li>
                                    <li class="page"><a href="#">3</a></li>
                                    <li class="page mobile-invisible"><a href="#">4</a></li>
                                    <li class="page mobile-invisible"><a href="#">5</a></li>
                                    <li>...</li>
                                    <li class="page"><a href="#">12</a></li>
                                </ul>
                            </nav>
                            <a href="#">
                                <div class="arrow-double-hover arrow-right"></div>
                            </a>
                        </section>
                    </div>
                </div>
            </div>
        </section>



    </main>

    <?php include '../../_components/_footer.php';?>

