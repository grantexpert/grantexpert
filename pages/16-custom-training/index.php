
<?php include '../../_components/_head.php';?>

<body class="page-custom-training page-grant-education">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li>
                    <a href="#">E-LEANING KURZY</a>
                </li>
                <li>
                    <a href="#">WEBINÁRE</a>
                </li>
                <li class="active">
                    <a href="#">ŠKOLENIA NA MIERU</a>
                </li>
                <li>
                    <a href="#">VIDEOSLOVNÍK</a>
                </li>
                <li>
                    <a href="#">GRANTOVÉ ZDROJE</a>
                </li>
                <li>
                    <a href="#">BLOG</a>
                </li>
            </ul>
        </nav>

       <section class="description-benefit">
           <div class="container-fluid">
               <div class="section-container">
                   <div class="container">
                       <div class="row">
                           <div class="col-lg-6">
                               <div class="description">
                                   <img src="http://grantexpert.test/assets/img/svg/custom-training.svg" alt="">
                                   <h1 class="title-primary">Školenia na mieru</h1>
                                   <p class="text-primary">
                                       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rhoncus viverra justo tristique blandit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sed magna nec ex lacinia pellentesque. Maecenas ut accumsan felis, vehicula tristique felis. Etiam justo dui, dapibus ac ultricies ultrices, vestibulum nec lacus. Phasellus quis justo sagittis, sodales elit id, dignissim nibh. Nulla ac ullamcorper justo. Sed blandit leo eu sapien varius, quis fermentum purus pretium.
                                   </p>

                                   <a href="#" class="button-tertiary button-tertiary--shadow">Vyžiadať ponuku</a>
                               </div>
                           </div>
                           <div class="col-lg-6">
                               <div class="card">
                                   <div class="col-container">
                                       <div class="benefit">
                                           <h3>Benefit 1</h3>
                                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse elit elit,</p>
                                       </div>
                                       <div class="benefit">
                                           <h3>Benefit 1</h3>
                                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse elit elit,</p>
                                       </div>
                                       <div class="benefit">
                                           <h3>Benefit 1</h3>
                                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse elit elit,</p>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </section>

        <section class="grant-steps">
            <div class="container">
                <h2 class="title-secondary">Kroky ktorými si spolu prejdeme</h2>

                <div class="steps">
                    <div class="step-row col-container">
                        <div class="left-part">
                            <div class="col-container">
                                <div class="col-number">
                                    <div class="number">1</div>
                                </div>
                                <div class="col-label">
                                    Vaša potreba
                                </div>
                            </div>
                        </div>
                        <div class="right-part">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vel consequat ligula.
                        </div>
                    </div>
                    <div class="step-row col-container">
                        <div class="left-part">
                            <div class="col-container">
                                <div class="col-number">
                                    <div class="number">2</div>
                                </div>
                                <div class="col-label">
                                    Naša ponuka
                                </div>
                            </div>
                        </div>
                        <div class="right-part">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vel consequat ligula.
                        </div>
                    </div>
                    <div class="step-row col-container">
                        <div class="left-part">
                            <div class="col-container">
                                <div class="col-number">
                                    <div class="number">3</div>
                                </div>
                                <div class="col-label">
                                    Školenie
                                </div>
                            </div>
                        </div>
                        <div class="right-part">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vel consequat ligula.
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="offer-closable">
            <div class="container">
                <div class="background-container">
                    <button class="button-close--blue"></button>
                    <div class="grant-radar-container">
                        <div class="row">
                            <div class="col-lg-5 col-xl-3">
                                <img class="image" src="http://grantexpert.test/assets/img/svg/eurofond-calc.svg" alt="">
                            </div>
                            <div class="col-lg-7 col-xl-6">
                                <h3 class="title-secondary">Lorem ipsum dolor sit amet</h3>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vel consequat ligula. Fusce nunc ante, fermentum in varius at, porta congue dui. Mauris laoreet pharetra sem.
                                </p>
                            </div>
                            <div class="offset-lg-5 col-lg-7 offset-xl-0 col-xl-3">
                                <div class="button-container">
                                    <a href="" class="button-tertiary button-tertiary--shadow">Vyžiadať ponuku</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="achieved-results">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <header>
                            <div class="row">
                                <div class="col-lg-6">
                                    <h2 class="title-primary">Naše referencie</h2>
                                    <a href="#" class="arrow-double-link button-desktop">
                                        <div class="arrow-right"></div>
                                        <div class="label">úspešné projekty</div>
                                    </a>
                                </div>
                                <div class="col-lg-6">
                                    <p class="text-primary">
                                        Vďaka dlhoročným skúsenostiam vieme ako napísať žiadosť o grant,  aby mala čo najväčšie šance na úspech a na čo si dať pozor, aby sme sa vyhli prekážkam.
                                    </p>
                                    <a href="#" class="arrow-double-link button-mobile">
                                        <div class="arrow-right"></div>
                                        <div class="label">úspešné projekty</div>
                                    </a>
                                </div>
                            </div>
                        </header>
                        <div class="row results">
                            <div class="col-6 col-lg-3">
                                <h4>2 mil. €</h4>
                                <p class="text-primary">získaných finančných <br>prostriedkov v schválených <br>projektoch</p>
                            </div>
                            <div class="col-6 col-lg-3">
                                <h4>700+</h4>
                                <p class="text-primary">preverených nápadov</p>
                            </div>
                            <div class="col-6 col-lg-3">
                                <h4>8000+</h4>
                                <p class="text-primary">účastníkov <br>školení</p>
                            </div>
                            <div class="col-6 col-lg-3">
                                <h4>25</h4>
                                <p class="text-primary">grantových <br>profesionálov</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="grant-education">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">

                        <h2 class="title-secondary">Ďalšie webináre</h2>

                        <div class="row">
                            <div class="col-12">
                                <div class="carousel-container">
                                    <div class="tns-carousel carousel-grant-education">
                                        <div>
                                            <div class="card-event-container">
                                                <div class="card card-event">
                                                    <header>
                                                        <div class="col-container">
                                                            <a href="#" class="title-container">
                                                                <h3 class="title-secondary">Grantová podpora škôl</h3>
                                                            </a>
                                                            <div class="icon-container">
                                                                <img src="http://grantexpert.test/assets/img/svg/calendar.svg" alt="calendar">
                                                            </div>
                                                        </div>

                                                        <div class="date-time">
                                                            <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/calendar.svg" alt="calendar"></span><span class="date">28.10.</span><span class="time">10:00 - 12:00</span>
                                                        </div>


                                                    </header>
                                                    <h4>Čo sa naučíte?</h4>
                                                    <ul class="check-list text-secondary">
                                                        <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                        <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                        <li>Aktuálne a plánované výzvy</li>
                                                    </ul>
                                                    <footer>
                                                        <div class="col-container">
                                                            <div class="price-container">
                                                                <div class="tax-included">Cena s DPH</div>
                                                                <div class="title-secondary color-sun price">19,90 €</div>
                                                            </div>
                                                            <div class="more-info">
                                                                <a href="#" class="arrow-link no-hover">
                                                                    <div class="icon-container">
                                                                        <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                                                    </div>
                                                                    <div class="label-container">
                                                                        <div class="title">Viac info</div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <div class="button-container">
                                                                <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                                            </div>
                                                        </div>
                                                    </footer>
                                                </div>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="card-event-container">
                                                <div class="card card-event">
                                                    <header>
                                                        <div class="col-container">
                                                            <a href="#" class="title-container">
                                                                <h3 class="title-secondary">Cesta úspešného projektu: Podnikateľský IT projekt</h3>
                                                            </a>
                                                            <div class="icon-container">
                                                                <img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker">
                                                            </div>
                                                        </div>

                                                        <div class="preview-container col-container">
                                                            <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker"></span>
                                                            <div class="stars">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star-blank.svg" alt="star-blank">
                                                            </div>
                                                            <a href="#" class="button-preview"><img src="http://grantexpert.test/assets/img/svg/play.svg" alt="play"><span class="label">UKÁŽKA</span></a>
                                                        </div>


                                                    </header>
                                                    <h4>Čo sa naučíte?</h4>
                                                    <ul class="check-list text-secondary">
                                                        <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                        <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                        <li>Aktuálne a plánované výzvy</li>
                                                    </ul>
                                                    <footer>
                                                        <div class="col-container">
                                                            <div class="price-container">
                                                                <div class="tax-included">Cena s DPH</div>
                                                                <div class="title-secondary color-sun price">19,90 €</div>
                                                            </div>
                                                            <div class="more-info">
                                                                <a href="#" class="arrow-link no-hover">
                                                                    <div class="icon-container">
                                                                        <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                                                    </div>
                                                                    <div class="label-container">
                                                                        <div class="title">Viac info</div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <div class="button-container">
                                                                <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                                            </div>

                                                        </div>
                                                    </footer>
                                                </div>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="card-event-container">
                                                <div class="card card-event">
                                                    <header>
                                                        <div class="col-container">
                                                            <a href="#" class="title-container">
                                                                <h3 class="title-secondary">Cesta úspešného projektu: Podnikateľský IT projekt</h3>
                                                            </a>
                                                            <div class="icon-container">
                                                                <img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker">
                                                            </div>
                                                        </div>

                                                        <div class="preview-container col-container">
                                                            <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker"></span>
                                                            <div class="stars">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star-blank.svg" alt="star-blank">
                                                            </div>
                                                            <a href="#" class="button-preview"><img src="http://grantexpert.test/assets/img/svg/play.svg" alt="play"><span class="label">UKÁŽKA</span></a>
                                                        </div>


                                                    </header>
                                                    <h4>Čo sa naučíte?</h4>
                                                    <ul class="check-list text-secondary">
                                                        <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                        <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                        <li>Aktuálne a plánované výzvy</li>
                                                    </ul>
                                                    <footer>
                                                        <div class="col-container">
                                                            <div class="price-container">
                                                                <div class="tax-included">Cena s DPH</div>
                                                                <div class="title-secondary color-sun price">19,90 €</div>
                                                            </div>
                                                            <div class="more-info">
                                                                <a href="#" class="arrow-link no-hover">
                                                                    <div class="icon-container">
                                                                        <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                                                    </div>
                                                                    <div class="label-container">
                                                                        <div class="title">Viac info</div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <div class="button-container">
                                                                <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                                            </div>

                                                        </div>
                                                    </footer>
                                                </div>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="card-event-container">
                                                <div class="card card-event">
                                                    <header>
                                                        <div class="col-container">
                                                            <a href="#" class="title-container">
                                                                <h3 class="title-secondary">Cesta úspešného projektu: Podnikateľský IT projekt</h3>
                                                            </a>
                                                            <div class="icon-container">
                                                                <img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker">
                                                            </div>
                                                        </div>

                                                        <div class="preview-container col-container">
                                                            <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker"></span>
                                                            <div class="stars">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star-blank.svg" alt="star-blank">
                                                            </div>
                                                            <a href="#" class="button-preview"><img src="http://grantexpert.test/assets/img/svg/play.svg" alt="play"><span class="label">UKÁŽKA</span></a>
                                                        </div>


                                                    </header>
                                                    <h4>Čo sa naučíte?</h4>
                                                    <ul class="check-list text-secondary">
                                                        <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                        <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                        <li>Aktuálne a plánované výzvy</li>
                                                    </ul>
                                                    <footer>
                                                        <div class="col-container">
                                                            <div class="price-container">
                                                                <div class="tax-included">Cena s DPH</div>
                                                                <div class="title-secondary color-sun price">19,90 €</div>
                                                            </div>
                                                            <div class="more-info">
                                                                <a href="#" class="arrow-link no-hover">
                                                                    <div class="icon-container">
                                                                        <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                                                    </div>
                                                                    <div class="label-container">
                                                                        <div class="title">Viac info</div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <div class="button-container">
                                                                <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                                            </div>

                                                        </div>
                                                    </footer>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <button id="education-carousel-prev" class="button-scroll button-scroll--left">
                                        <div class="button-border--gray"></div>
                                        <div class="button-border--white">
                                            <div class="icon-container"></div>
                                        </div>
                                    </button>
                                    <button id="education-carousel-next" class="button-scroll button-scroll--right">
                                        <div class="button-border--gray"></div>
                                        <div class="button-border--white">
                                            <div class="icon-container"></div>
                                        </div>
                                    </button>

                                </div>
                                <div class="button-center">
                                    <a href="" class="button-primary"><div class="vertical-center">Všetky webináre</div></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>

    <?php include '../../_components/_footer.php';?>

