
<?php include '../../_components/_head.php';?>

<body class="page-service-packages page-services">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li>
                    <a href="#">Individuálne služby</a>
                </li>
                <li class="active">
                    <a href="#">Balíky služieb</a>
                </li>
                <li>
                    <a href="#">Konzultácia s grantexpertom</a>
                </li>
                <li>
                    <a href="#">Vypracovanie projektu</a>
                </li>
                <li>
                    <a href="#">Privátny grantový konzultant</a>
                </li>
            </ul>
        </nav>

        <section class="service-packages">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <h2 class="title-primary">Balíky služieb</h2>
                            </div>
                            <div class="col-lg-6">
                                <p class="text-primary">
                                    Vyberte si z našich balíkov, ktoré tvoria vhodný mix pre vaše aktuálne potreby v oblasti získavania grantov.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row row-cards">
                        <div class="col-md-6 col-xl-3">
                            <a href="#">
                                <div class="card-animation card-shadow-animation">
                                    <div class="card card-shadow">
                                        <div class="subtitle text-secondary text-center">Pre rýchlu orientáciu</div>
                                        <h3 class="title-primary">Free</h3>
                                        <div class="tax-included">CENA S DPH</div>
                                        <div class="title-secondary text-blue">0 €/rok</div>

                                    </div>
                                    <div class="button-secondary">OBJEDNAŤ</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-xl-3">
                            <a href="#">
                                <div class="card-animation card-shadow-animation">
                                    <div class="card card-shadow">
                                        <div class="subtitle text-secondary text-center">Pre prvé granty</div>
                                        <h3 class="title-primary">Basic</h3>
                                        <div class="tax-included">CENA S DPH</div>
                                        <div class="title-secondary text-blue">34,90 €/rok</div>

                                    </div>
                                    <div class="button-secondary">OBJEDNAŤ</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-xl-3">
                            <a href="#">
                                <div class="card-animation card-shadow-animation">
                                    <div class="card card-shadow">
                                        <div class="subtitle text-secondary text-center">Pre pokročilé vyhľadávanie</div>
                                        <h3 class="title-primary">Medium</h3>
                                        <div class="tax-included">CENA S DPH</div>
                                        <div class="title-secondary text-blue">69,90 €/rok</div>

                                    </div>
                                    <div class="button-secondary">OBJEDNAŤ</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-xl-3">
                            <a href="#">
                                <div class="card-animation card-shadow-animation card-shadow-animation--gold">
                                    <div class="card card-shadow">
                                        <div class="subtitle text-secondary text-center">Pre aktívnych riešiteľov</div>
                                        <h3 class="title-primary">Profi</h3>
                                        <div class="tax-included">CENA S DPH</div>
                                        <div class="title-secondary text-blue">149,90 €/rok</div>

                                    </div>
                                    <div class="button-secondary">OBJEDNAŤ</div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="button-center">
                        <a href="#" class="button-primary"><div class="vertical-center">POROVNAŤ BALÍKY</div></a>
                    </div>
                </div>
            </div>
        </section>


        <section class="package-comparison">

                <div class="container-fluid">
                    <div class="section-container">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6">
                                    <h2 class="title-primary">Porovnanie <br>balíkov</h2>
                                </div>
                                <div class="col-lg-6">
                                    <p class="text-primary">
                                        Porovnajte si výhody jednotlivých balíkov a rozhodnite sa pre ten, ktorý najviac vyhovuje vašej aktuálnej potrebe. Balík môžete kedykoľvek zmeniť.
                                    </p>
                                </div>
                            </div>

                            <div class="package-comparison--desktop">
                            <div class="col-container row-header">
                                <div class="col-description">

                                </div>
                                <div class="col-package">
                                    <h3 class="title-secondary">Free</h3>
                                    <div class="tax-included">CENA S DPH</div>
                                    <div class="title-secondary text-blue">0 €/rok</div>
                                    <div class="subtitle text-secondary text-center">Pre rýchlu orientáciu</div>
                                </div>
                                <div class="col-package">
                                    <h3 class="title-secondary">Basic</h3>
                                    <div class="tax-included">CENA S DPH</div>
                                    <div class="title-secondary text-blue">34,90 €/rok</div>
                                    <div class="subtitle text-secondary text-center">Pre prvé granty</div>
                                </div>
                                <div class="col-package">
                                    <h3 class="title-secondary">Medium</h3>
                                    <div class="tax-included">CENA S DPH</div>
                                    <div class="title-secondary text-blue">69,90 €/rok</div>
                                    <div class="subtitle text-secondary text-center">Pre pokročilé vyhľadávanie</div>
                                </div>
                                <div class="col-package">
                                    <h3 class="title-secondary">Profi</h3>
                                    <div class="tax-included">CENA S DPH</div>
                                    <div class="title-secondary color-sun">149,90 €/rok</div>
                                    <div class="subtitle text-secondary text-center">Pre aktívnych riešiteľov</div>
                                </div>
                            </div>
                            <div class="col-container">
                                <div class="col-description">
                                    <div class="col-container">
                                        <div class="col-text">
                                            Automatické zasielanie notifikácií o nových výzvach
                                        </div>
                                        <div class="col-info">
                                        <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                            <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                                <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                                <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                                <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                            </svg>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-description">
                                    <div class="col-container">
                                        <div class="col-text">
                                            Neobmedzený prístup do databázy grantov a dotácií
                                        </div>
                                        <div class="col-info">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-package">

                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-description">
                                    <div class="col-container">
                                        <div class="col-text">
                                            Overenie oprávnenosti
                                        </div>
                                        <div class="col-info">
                                        <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                            <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                                <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                                <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                                <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                            </svg>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-description">
                                    <div class="col-container">
                                        <div class="col-text">
                                            Interaktívne štatistiky úspešnosti žiadateľov o eurofondy
                                        </div>
                                        <div class="col-info">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-package">

                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-description">
                                    <div class="col-container">
                                        <div class="col-text">
                                            Vyhľadanie dotácie k projektu
                                        </div>
                                        <div class="col-info">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-package">

                                </div>
                                <div class="col-package">
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-description">
                                    <div class="col-container">
                                        <div class="col-text">
                                            Archív webinárov
                                        </div>
                                        <div class="col-info">
                                        <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                            <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                                <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                                <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                                <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                            </svg>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-package">

                                </div>
                                <div class="col-package">
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-description">
                                    <div class="col-container">
                                        <div class="col-text">
                                            30 minútová konzultácia s grantexpertom
                                        </div>
                                        <div class="col-info">
                                        <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                            <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                                <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                                <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                                <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                            </svg>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-package">

                                </div>
                                <div class="col-package">
                                </div>
                                <div class="col-package">
                                </div>
                                <div class="col-package">
                                    <div class="count">1x</div>
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-description">
                                    <div class="col-container">
                                        <div class="col-text">
                                            Posúdenie projektového nápadu
                                        </div>
                                        <div class="col-info">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-package">

                                </div>
                                <div class="col-package">
                                </div>
                                <div class="col-package">
                                    <div class="count">1x</div>
                                </div>
                                <div class="col-package">
                                    <div class="count">3x</div>
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-description">
                                    <div class="col-container">
                                        <div class="col-text">
                                            VIP rozhranie s možnosťou chatu
                                        </div>
                                        <div class="col-info">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-package">

                                </div>
                                <div class="col-package">
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-description">
                                    <div class="col-container">
                                    </div>
                                </div>
                                <div class="col-package">
                                    <a href="#" class="button-secondary">OBJEDNAŤ</a>
                                </div>
                                <div class="col-package">
                                    <a href="#" class="button-secondary">OBJEDNAŤ</a>
                                </div>
                                <div class="col-package">
                                    <a href="#" class="button-secondary">OBJEDNAŤ</a>
                                </div>
                                <div class="col-package">
                                    <a href="#" class="button-secondary">OBJEDNAŤ</a>
                                </div>
                            </div>

                        </div>

                        </div>
                    </div>
                </div>


            <div class="package-comparison--mobile">
                <div class="container">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="package-free-tab" data-bs-toggle="tab" data-bs-target="#package-free-pane" type="button" role="tab" aria-controls="package-free-pane" aria-selected="true">FREE</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="package-basic-tab" data-bs-toggle="tab" data-bs-target="#package-basic-pane" type="button" role="tab" aria-controls="package-basic-pane" aria-selected="false">BASIC</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="package-medium-tab" data-bs-toggle="tab" data-bs-target="#package-medium-pane" type="button" role="tab" aria-controls="package-medium-pane" aria-selected="false">MEDIUM</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="package-profi-tab" data-bs-toggle="tab" data-bs-target="#package-profi-pane" type="button" role="tab" aria-controls="package-profi-pane" aria-selected="false">PROFI</button>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade" id="package-free-pane" role="tabpanel" aria-labelledby="package-free-tab">
                            <h3 class="title-secondary">Free</h3>
                            <p class="title-description">Pre rýchlu orientáciu</p>

                            <div class="col-container">
                                <div class="col-text">
                                    Automatické zasielanie notifikácií o nových výzvach
                                </div>
                                <div class="col-info">
                                    <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                        <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                            <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                            <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                            <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                        </svg>
                                    </span>
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-text">
                                    Neobmedzený prístup do databázy grantov a dotácií
                                </div>
                                <div class="col-info">

                                </div>
                                <div class="col-package">

                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-text">
                                    Overenie oprávnenosti
                                </div>
                                <div class="col-info">
                                    <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                        <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                            <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                            <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                            <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                        </svg>
                                    </span>
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-text">
                                    Interaktívne štatistiky úspešnosti žiadateľov o eurofondy
                                </div>
                                <div class="col-info">

                                </div>
                                <div class="col-package">
                                </div>
                            </div>
                            <div class="col-container">
                                <div class="col-text">
                                    Vyhľadanie dotácie k projektu
                                </div>
                                <div class="col-info">

                                </div>
                                <div class="col-package">
                                </div>
                            </div>
                            <div class="col-container">
                                <div class="col-text">
                                    Archív webinárov
                                </div>
                                <div class="col-info">
                                    <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                        <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                            <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                            <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                            <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                        </svg>
                                    </span>
                                </div>
                                <div class="col-package">
                                </div>
                            </div>
                            <div class="col-container">
                                <div class="col-text">
                                    30 minútová konzultácia s grantexpertom
                                </div>
                                <div class="col-info">

                                </div>
                                <div class="col-package">
                                </div>
                            </div>
                            <div class="col-container">
                                <div class="col-text">
                                    Posúdenie projektového nápadu
                                </div>
                                <div class="col-info">

                                </div>
                                <div class="col-package">
                                </div>
                            </div>
                            <div class="col-container">
                                <div class="col-text">
                                    VIP rozhranie s možnosťou chatu
                                </div>
                                <div class="col-info">
                                    <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                        <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                            <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                            <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                            <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                        </svg>
                                    </span>
                                </div>
                                <div class="col-package">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-text">
                                    <div class="tax-included">CENA S DPH</div>
                                    <div class="title-secondary text-blue">0 €/rok</div>
                                </div>
                                <div class="col-package">
                                    <div class="button-secondary">OBJEDNAŤ</div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="package-basic-pane" role="tabpanel" aria-labelledby="package-basic-tab">
                            <h3 class="title-secondary">Basic</h3>
                            <p class="title-description">Pre prvé granty</p>

                            <div class="col-container">
                                <div class="col-text">
                                    Automatické zasielanie notifikácií o nových výzvach
                                </div>
                                <div class="col-info">
                                    <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                        <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                            <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                            <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                            <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                        </svg>
                                    </span>
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-text">
                                    Neobmedzený prístup do databázy grantov a dotácií
                                </div>
                                <div class="col-info">

                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-text">
                                    Overenie oprávnenosti
                                </div>
                                <div class="col-info">
                                    <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                        <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                            <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                            <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                            <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                        </svg>
                                    </span>
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-text">
                                    Interaktívne štatistiky úspešnosti žiadateľov o eurofondy
                                </div>
                                <div class="col-info">

                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>
                            <div class="col-container">
                                <div class="col-text">
                                    Vyhľadanie dotácie k projektu
                                </div>
                                <div class="col-info">

                                </div>
                                <div class="col-package">
                                </div>
                            </div>
                            <div class="col-container">
                                <div class="col-text">
                                    Archív webinárov
                                </div>
                                <div class="col-info">
                                    <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                        <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                            <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                            <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                            <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                        </svg>
                                    </span>
                                </div>
                                <div class="col-package">
                                </div>
                            </div>
                            <div class="col-container">
                                <div class="col-text">
                                    30 minútová konzultácia s grantexpertom
                                </div>
                                <div class="col-info">

                                </div>
                                <div class="col-package">
                                </div>
                            </div>
                            <div class="col-container">
                                <div class="col-text">
                                    Posúdenie projektového nápadu
                                </div>
                                <div class="col-info">

                                </div>
                                <div class="col-package">
                                </div>
                            </div>
                            <div class="col-container">
                                <div class="col-text">
                                    VIP rozhranie s možnosťou chatu
                                </div>
                                <div class="col-info">
                                    <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                        <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                            <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                            <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                            <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                        </svg>
                                    </span>
                                </div>
                                <div class="col-package">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-text">
                                    <div class="tax-included">CENA S DPH</div>
                                    <div class="title-secondary text-blue">34,90 €/rok</div>
                                </div>
                                <div class="col-package">
                                    <div class="button-secondary">OBJEDNAŤ</div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="package-medium-pane" role="tabpanel" aria-labelledby="package-medium-tab">
                            <h3 class="title-secondary">Medium</h3>
                            <p class="title-description">Pre pokročilé vyhľadávanie</p>

                            <div class="col-container">
                                <div class="col-text">
                                    Automatické zasielanie notifikácií o nových výzvach
                                </div>
                                <div class="col-info">
                                    <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                        <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                            <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                            <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                            <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                        </svg>
                                    </span>
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-text">
                                    Neobmedzený prístup do databázy grantov a dotácií
                                </div>
                                <div class="col-info">

                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-text">
                                    Overenie oprávnenosti
                                </div>
                                <div class="col-info">
                                    <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                        <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                            <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                            <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                            <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                        </svg>
                                    </span>
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-text">
                                    Interaktívne štatistiky úspešnosti žiadateľov o eurofondy
                                </div>
                                <div class="col-info">

                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>
                            <div class="col-container">
                                <div class="col-text">
                                    Vyhľadanie dotácie k projektu
                                </div>
                                <div class="col-info">

                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>
                            <div class="col-container">
                                <div class="col-text">
                                    Archív webinárov
                                </div>
                                <div class="col-info">
                                    <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                        <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                            <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                            <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                            <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                        </svg>
                                    </span>
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>
                            <div class="col-container">
                                <div class="col-text">
                                    30 minútová konzultácia s grantexpertom
                                </div>
                                <div class="col-info">

                                </div>
                                <div class="col-package">
                                </div>
                            </div>
                            <div class="col-container">
                                <div class="col-text">
                                    Posúdenie projektového nápadu
                                </div>
                                <div class="col-info">

                                </div>
                                <div class="col-package">
                                    <div class="count">1x</div>
                                </div>
                            </div>
                            <div class="col-container">
                                <div class="col-text">
                                    VIP rozhranie s možnosťou chatu
                                </div>
                                <div class="col-info">
                                    <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                        <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                            <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                            <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                            <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                        </svg>
                                    </span>
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-text">
                                    <div class="tax-included">CENA S DPH</div>
                                    <div class="title-secondary text-blue">69,90 €/rok</div>
                                </div>
                                <div class="col-package">
                                    <div class="button-secondary">OBJEDNAŤ</div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade show active" id="package-profi-pane" role="tabpanel" aria-labelledby="package-profi-tab">
                            <h3 class="title-secondary">Profi</h3>
                            <p class="title-description">Pre aktívnych riešiteľov</p>

                            <div class="col-container">
                                <div class="col-text">
                                    Automatické zasielanie notifikácií o nových výzvach
                                </div>
                                <div class="col-info">
                                    <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                        <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                            <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                            <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                            <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                        </svg>
                                    </span>
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-text">
                                    Neobmedzený prístup do databázy grantov a dotácií
                                </div>
                                <div class="col-info">

                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-text">
                                    Overenie oprávnenosti
                                </div>
                                <div class="col-info">
                                    <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                        <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                            <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                            <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                            <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                        </svg>
                                    </span>
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-text">
                                    Interaktívne štatistiky úspešnosti žiadateľov o eurofondy
                                </div>
                                <div class="col-info">

                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>
                            <div class="col-container">
                                <div class="col-text">
                                    Vyhľadanie dotácie k projektu
                                </div>
                                <div class="col-info">

                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>
                            <div class="col-container">
                                <div class="col-text">
                                    Archív webinárov
                                </div>
                                <div class="col-info">
                                    <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                        <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                            <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                            <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                            <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                        </svg>
                                    </span>
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>
                            <div class="col-container">
                                <div class="col-text">
                                    30 minútová konzultácia s grantexpertom
                                </div>
                                <div class="col-info">

                                </div>
                                <div class="col-package">
                                    <div class="count">1x</div>
                                </div>
                            </div>
                            <div class="col-container">
                                <div class="col-text">
                                    Posúdenie projektového nápadu
                                </div>
                                <div class="col-info">

                                </div>
                                <div class="col-package">
                                    <div class="count">3x</div>
                                </div>
                            </div>
                            <div class="col-container">
                                <div class="col-text">
                                    VIP rozhranie s možnosťou chatu
                                </div>
                                <div class="col-info">
                                    <span data-toggle="tooltip" title="Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu.">
                                        <svg data-name="Component 120 – 1" xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                            <circle class="bg" data-name="Ellipse 608" cx="13.5" cy="13.5" r="13.5" style="fill:#eee"/>
                                            <path class="symbol-path" data-name="Line 2954" transform="translate(13.5 12.5)" style="fill:none;stroke:#a2a2a2;stroke-linecap:round;stroke-width:3px" d="M0 0v7"/>
                                            <circle class="symbol-circle" data-name="Ellipse 607" cx="2" cy="2" r="2" transform="translate(11.5 4.75)" style="fill:#a2a2a2"/>
                                        </svg>
                                    </span>
                                </div>
                                <div class="col-package">
                                    <img src="http://grantexpert.test/assets/img/svg/check.svg" alt="check">
                                </div>
                            </div>

                            <div class="col-container">
                                <div class="col-text">
                                    <div class="tax-included">CENA S DPH</div>
                                    <div class="title-secondary color-sun">149,90 €/rok</div>
                                </div>
                                <div class="col-package">
                                    <div class="button-secondary">OBJEDNAŤ</div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </section>

        <section class="roll-down">
            <div class="container-780">
                <h2 class="title-secondary">Časté otázky</h2>

                <div class="roll-down-list">
                    <div class="roll-down-item">
                        <div class="roll-down-item-container">
                            <div class="label">
                                Je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                            </div>
                            <div class="content">
                                <p>
                                    Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="roll-down-item">
                        <div class="roll-down-item-container">
                            <div class="label">
                                Je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                            </div>
                            <div class="content">
                                <p>
                                    Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="roll-down-item">
                        <div class="roll-down-item-container">
                            <div class="label">
                                Je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                            </div>
                            <div class="content">
                                <p>
                                    Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="roll-down-item">
                        <div class="roll-down-item-container">
                            <div class="label">
                                Je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                            </div>
                            <div class="content">
                                <p>
                                    Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

    </main>

    <?php include '../../_components/_footer.php';?>

