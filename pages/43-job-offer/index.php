
<?php include '../../_components/_head.php';?>

<body class="page-job-offer page-grant-jobs">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li class="active">
                    <a href="#">Pracovné ponuky</a>
                </li>
                <li>
                    <a href="#">Pridať pracovnú ponuku</a>
                </li>
                <li>
                    <a href="#">Databáza grantových expertov</a>
                </li>
                <li>
                    <a href="#">Stať sa Grantovým expertom</a>
                </li>
                <li>
                    <a href="#">Blog</a>
                </li>
            </ul>
        </nav>

        <div class="container">
            <h1 class="page-title title-secondary">Pracovné ponuky v oblasti Grantov</h1>
        </div>

        <section class="filter-fixed-header filter-fixed-header--sun">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3">
                                <button class="button-scroll button-scroll--white">
                                    <div class="icon-container"></div>
                                </button>

                                <h3>Filter</h3>
                            </div>
                            <div class="col-lg-9">
                                <div class="text-small-label">Máte zvolené vyhľadávacie parametre</div>
                                <button class="button-primary">Vynulovať filter</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="filter filter--sun">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <button class="button-filter-open"><span class="label">Filter</span><img src="http://grantexpert.test/assets/img/svg/filter.svg" alt=""></button>
                    </div>

                    <div class="container">
                        <div class="filter-container">
                            <div class="row">
                                <div class="col-xl-8">
                                    <h3>Filter</h3>

                                    <div class="row">

                                        <div class="col-md-6 col-lg-6">
                                            <div class="custom-form-control checkbox-popup">
                                                <div class="checkbox-label" data-bs-toggle="modal" data-bs-target="#position-modal">Pozícia</div>
                                            </div>

                                            <div class="modal fade" id="position-modal" tabindex="-1" aria-labelledby="position-modal" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        <div class="content-container">
                                                            <div class="col-container">
                                                                <div class="col-title">
                                                                    <h4 class="title-secondary">Pozícia</h4>
                                                                </div>
                                                                <div class="col-select">
                                                                    <div class="select-all">Označiť všetko</div>
                                                                    <div class="deselect-all">Odznačiť všetko</div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="checkbox">
                                                                        <input name="rozvoj-podnikania" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Rozvoj podnikania</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="vyskum-vyvoj" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Výskum a vývoj</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="vodne-hospodarstvo" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Vodné hospodárstvo a odpadové hospodárstvo</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="zamestnanost" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Zamestnanosť a tvorba pracovných miest</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="energeticka-efektivnost-budov" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Energetická efektívnosť budov</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Obnoviteľné zdroje energie</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Kultúra, cestovný ruch a voľnočasová infraštruktúra</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Vzdelávacie aktivity</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Sociálne služby a zdravotníctvo  </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Analýzy, plány, stratégie  </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Informačno-komunikačné technológie  </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Poľnohospodárstvo, rybné hospodárstvo a lesníctvo  </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Ochrana ŽP a environmentálne riziká   </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Občianska vybavenosť   </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Dopravná infraštruktúra a služby  </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Zelená mobilita a cyklistická infraštruktúra  </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-select col-select--mobile">
                                                                <div class="select-all">Označiť všetko</div>
                                                                <div class="deselect-all">Odznačiť všetko</div>
                                                            </div>
                                                            <div class="button-center">
                                                                <button class="button-tertiary button-tertiary--padding" data-bs-dismiss="modal">POTVRDIŤ</button>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-lg-6">
                                            <div class="custom-form-control checkbox-popup">
                                                <div class="checkbox-label" data-bs-toggle="modal" data-bs-target="#location-modal">Lokalita</div>
                                            </div>

                                            <div class="modal fade" id="location-modal" tabindex="-1" aria-labelledby="location-modal" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        <div class="content-container">
                                                            <div class="col-container">
                                                                <div class="col-title">
                                                                    <h4 class="title-secondary">Lokalita</h4>
                                                                </div>
                                                                <div class="col-select">
                                                                    <div class="select-all">Označiť všetko</div>
                                                                    <div class="deselect-all">Odznačiť všetko</div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Banskobystrický kraj</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Bratislavský kraj</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Košický kraj </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Nitriansky kraj</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Prešovský kraj </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Trenčiansky kraj</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Trnavský kraj</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="checkbox">
                                                                        <input name="" type="checkbox">
                                                                        <div class="label">
                                                                            <div class="inline-container">
                                                                                <div class="col-container">
                                                                                    <div class="col-number">15</div><div class="col-text">Žilinský kraj</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-select col-select--mobile">
                                                                <div class="select-all">Označiť všetko</div>
                                                                <div class="deselect-all">Odznačiť všetko</div>
                                                            </div>
                                                            <div class="button-center">
                                                                <button class="button-tertiary button-tertiary--padding" data-bs-dismiss="modal">POTVRDIŤ</button>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <section class="tag-list">
                                        <button class="tag-small-rounded tag-small-rounded--inverted active">Všetky</button>
                                        <button class="tag-small-rounded tag-small-rounded--inverted">Na projekt</button>
                                        <button class="tag-small-rounded tag-small-rounded--inverted">Full time</button>
                                    </section>

                                    <div class="checkbox checkbox--white">
                                        <input name="" type="checkbox">
                                        <div class="label">
                                            <div class="inline-container">
                                                <div class="col-container">
                                                    <div class="col-text">Zobrazit iba nové</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-xl-4">
                                    <h3>Vyhľadávané kategórie</h3>
                                    <div class="tag-search-container">
                                        <div class="tag-search-category">
                                            <div class="tag-search">
                                                Kultúra, cestovný ruch a voľnočasová infraštruktúra

                                                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
                                                    <g data-name="Group 1809" transform="translate(-77)">
                                                        <circle data-name="Ellipse 23" cx="8.5" cy="8.5" r="8.5" transform="translate(77)" style="fill:#fff"/>
                                                        <g data-name="Icon feather-plus">
                                                            <path data-name="Path 19" d="M0 0v8.131" transform="rotate(45 37.397 109.49)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                            <path data-name="Path 20" d="M0 0h8.131" transform="rotate(45 34.522 102.55)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="tag-search">
                                                Ochrana ŽP a environmentálne riziká

                                                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
                                                    <g data-name="Group 1809" transform="translate(-77)">
                                                        <circle data-name="Ellipse 23" cx="8.5" cy="8.5" r="8.5" transform="translate(77)" style="fill:#fff"/>
                                                        <g data-name="Icon feather-plus">
                                                            <path data-name="Path 19" d="M0 0v8.131" transform="rotate(45 37.397 109.49)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                            <path data-name="Path 20" d="M0 0h8.131" transform="rotate(45 34.522 102.55)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="tag-search">
                                                Sociálne služby a zdravotníctvo

                                                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
                                                    <g data-name="Group 1809" transform="translate(-77)">
                                                        <circle data-name="Ellipse 23" cx="8.5" cy="8.5" r="8.5" transform="translate(77)" style="fill:#fff"/>
                                                        <g data-name="Icon feather-plus">
                                                            <path data-name="Path 19" d="M0 0v8.131" transform="rotate(45 37.397 109.49)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                            <path data-name="Path 20" d="M0 0h8.131" transform="rotate(45 34.522 102.55)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="tag-search">
                                                Rozvoj podnikania

                                                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
                                                    <g data-name="Group 1809" transform="translate(-77)">
                                                        <circle data-name="Ellipse 23" cx="8.5" cy="8.5" r="8.5" transform="translate(77)" style="fill:#fff"/>
                                                        <g data-name="Icon feather-plus">
                                                            <path data-name="Path 19" d="M0 0v8.131" transform="rotate(45 37.397 109.49)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                            <path data-name="Path 20" d="M0 0h8.131" transform="rotate(45 34.522 102.55)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="tag-search">
                                                Vzdelávacie aktivity

                                                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
                                                    <g data-name="Group 1809" transform="translate(-77)">
                                                        <circle data-name="Ellipse 23" cx="8.5" cy="8.5" r="8.5" transform="translate(77)" style="fill:#fff"/>
                                                        <g data-name="Icon feather-plus">
                                                            <path data-name="Path 19" d="M0 0v8.131" transform="rotate(45 37.397 109.49)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                            <path data-name="Path 20" d="M0 0h8.131" transform="rotate(45 34.522 102.55)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tag-search-category">
                                        <div class="tag-search">
                                            Prešovský kraj

                                            <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
                                                <g data-name="Group 1809" transform="translate(-77)">
                                                    <circle data-name="Ellipse 23" cx="8.5" cy="8.5" r="8.5" transform="translate(77)" style="fill:#fff"/>
                                                    <g data-name="Icon feather-plus">
                                                        <path data-name="Path 19" d="M0 0v8.131" transform="rotate(45 37.397 109.49)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                        <path data-name="Path 20" d="M0 0h8.131" transform="rotate(45 34.522 102.55)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="tag-search">
                                            Trenčiansky kraj

                                            <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
                                                <g data-name="Group 1809" transform="translate(-77)">
                                                    <circle data-name="Ellipse 23" cx="8.5" cy="8.5" r="8.5" transform="translate(77)" style="fill:#fff"/>
                                                    <g data-name="Icon feather-plus">
                                                        <path data-name="Path 19" d="M0 0v8.131" transform="rotate(45 37.397 109.49)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                        <path data-name="Path 20" d="M0 0h8.131" transform="rotate(45 34.522 102.55)" style="fill:none;stroke:#145070;stroke-linecap:round;stroke-linejoin:round"/>
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="button-center">
                                        <button class="button-primary">Vynulovať filter</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="filter-selected">
                            <div class="label">Máte zvolené vyhľadávacie parametre</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="search-results">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">

                        <section class="ordering">
                            <img src="http://grantexpert.test/assets/img/svg/three-dots.svg" alt="three dots" class="three-dots">

                            <div class="row">
                                <div class="col-12">
                                    <div class="col-container">
                                        <div class="text-small-label mb-3">28 ponúk</div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <div class="card-job-offer card-border-hover card-shadow-hover">
                            <div class="card-container">
                                <div class="col-container">
                                    <div class="col-left">
                                        <div class="col-container">
                                            <div class="col-image">
                                                <img src="http://grantexpert.test/assets/img/profesia-logo.jpg" alt="">
                                            </div>
                                            <div class="col-text">
                                                <a href="#"><h2>Mid / Senior Performance marketing špecialista</h2></a>
                                                <div class="subtitle">Ministerstvo financií Slovenskej republiky</div>
                                                <div class="tag-container">
                                                    <a href="#" class="tag">Financie</a>
                                                    <a href="#" class="tag">Štátna správa</a>
                                                    <a href="#" class="tag">Financie</a>
                                                    <button class="button-more"></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-right">
                                        <div class="label-value-container">
                                            <div class="label-value">
                                                <span class="label">Typ pozície:</span>
                                                <span class="value">Full time</span>
                                            </div>
                                            <div class="label-value">
                                                <span class="label">Lokalita:</span>
                                                <span class="value">Práca z domu</span>
                                            </div>
                                            <div class="label-value">
                                                <span class="label">Ohodnotenie:</span>
                                                <span class="value">od 1600 €</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-job-offer card-border-hover card-shadow-hover">
                            <div class="card-container">
                                <div class="col-container">
                                    <div class="col-left">
                                        <div class="col-container">
                                            <div class="col-image">
                                                <img src="http://grantexpert.test/assets/img/profesia-logo.jpg" alt="">
                                            </div>
                                            <div class="col-text">
                                                <a href="#"><h2>Mid / Senior Performance marketing špecialista</h2></a>
                                                <div class="subtitle">Ministerstvo financií Slovenskej republiky</div>
                                                <div class="tag-container">
                                                    <a href="#" class="tag">Financie</a>
                                                    <a href="#" class="tag">Štátna správa</a>
                                                    <a href="#" class="tag">Financie</a>
                                                    <button class="button-more"></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-right">
                                        <div class="label-value-container">
                                            <div class="label-value">
                                                <span class="label">Typ pozície:</span>
                                                <span class="value">Full time</span>
                                            </div>
                                            <div class="label-value">
                                                <span class="label">Lokalita:</span>
                                                <span class="value">Práca z domu</span>
                                            </div>
                                            <div class="label-value">
                                                <span class="label">Ohodnotenie:</span>
                                                <span class="value">od 1600 €</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-job-offer card-border-hover card-shadow-hover">
                            <div class="card-container">
                                <div class="col-container">
                                    <div class="col-left">
                                        <div class="col-container">
                                            <div class="col-image">
                                                <img src="http://grantexpert.test/assets/img/profesia-logo.jpg" alt="">
                                            </div>
                                            <div class="col-text">
                                                <a href="#"><h2>Mid / Senior Performance marketing špecialista</h2></a>
                                                <div class="subtitle">Ministerstvo financií Slovenskej republiky</div>
                                                <div class="tag-container">
                                                    <a href="#" class="tag">Financie</a>
                                                    <a href="#" class="tag">Štátna správa</a>
                                                    <a href="#" class="tag">Financie</a>
                                                    <button class="button-more"></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-right">
                                        <div class="label-value-container">
                                            <div class="label-value">
                                                <span class="label">Typ pozície:</span>
                                                <span class="value">Full time</span>
                                            </div>
                                            <div class="label-value">
                                                <span class="label">Lokalita:</span>
                                                <span class="value">Práca z domu</span>
                                            </div>
                                            <div class="label-value">
                                                <span class="label">Ohodnotenie:</span>
                                                <span class="value">od 1600 €</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <section class="pager">
                            <a href="#">
                                <div class="arrow-double-hover arrow-left"></div>
                            </a>

                            <nav>
                                <ul>
                                    <li class="page active"><a href="#">1</a></li>
                                    <li class="page"><a href="#">2</a></li>
                                    <li class="page"><a href="#">3</a></li>
                                    <li class="page mobile-invisible"><a href="#">4</a></li>
                                    <li class="page mobile-invisible"><a href="#">5</a></li>
                                    <li>...</li>
                                    <li class="page"><a href="#">12</a></li>
                                </ul>
                            </nav>
                            <a href="#">
                                <div class="arrow-double-hover arrow-right"></div>
                            </a>
                        </section>
                    </div>
                </div>
            </div>
        </section>

        <section class="offer-closable">
            <div class="container">
                <div class="background-container">
                    <button class="button-close--blue"></button>
                    <div class="grant-radar-container">
                        <div class="row">
                            <div class="col-lg-5 col-xl-3">
                                <img class="image" src="http://grantexpert.test/assets/img/svg/eurofond-calc.svg" alt="">
                            </div>
                            <div class="col-lg-7 col-xl-6">
                                <h3 class="title-secondary">Chcete pridať vašu pracovnú ponuku?</h3>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                </p>
                            </div>
                            <div class="offset-lg-5 col-lg-7 offset-xl-0 col-xl-3">
                                <div class="button-container">
                                    <a href="" class="button-tertiary button-tertiary--shadow">Pridať ponuku</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>

    <?php include '../../_components/_footer.php';?>

