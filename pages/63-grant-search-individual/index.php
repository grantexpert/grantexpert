
<?php include '../../_components/_head.php';?>

<body class="page-search-individual page-grant-search">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li>
                    <a href="#">Pre podnikateľov</a>
                </li>
                <li>
                    <a href="#">Pre mestá a obce</a>
                </li>
                <li>
                    <a href="#">Pre školy</a>
                </li>
                <li>
                    <a href="#">Pre mimovládne organizácie</a>
                </li>
                <li class="active">
                    <a href="#">Pre jednotlivcov</a>
                </li>
            </ul>
        </nav>



        <section class="preface">
            <div class="container-full-width">
                <div class="title-container">
                    <h3>Grantové vyhľadávanie</h3>
                    <h1 class="title-primary">PRE JEDNOTLIVCOV</h1>
                    <p>
                        Mestá, obce a kraje sú z dotácií podporované v mnohých oblastiach. Výrazný podiel na investíciách do samospráv majú eurofondy. V najbližšom období bude balík európskych peňazí podstatne navýšený. Vďaka portálu Grantexpert.sk máte k dispozícii všetky aktuálne možnosti podpory vášho mesta či obce.
                    </p>
                </div>
            </div>
        </section>


        <section class="arrow-section">
            <div class="container">
                <h2 class="title-secondary">Ako získať dotáciu?</h2>

                <div class="text-container">
                    <div class="text">Váš projektový nápad</div>
                </div>

                <img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="">

                <div class="text-container">
                    <div class="text">Vyhľadanie vhodnej výzvy</div>
                </div>

                <img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="">

                <div class="text-container">
                    <div class="text">Overenie oprávnenosti</div>
                </div>

                <img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="">

                <div class="text-container">
                    <div class="text">Vypracovanie projektovej žiadosti</div>
                </div>

                <img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="">

                <div class="text-container">
                    <div class="text">Realizácia projektu</div>
                </div>
            </div>
        </section>

        <section class="grant-search-content">
            <div class="container">
                <h3>Aktuálne granty, dotácie a eurofondy pre obce a mestá</h3>
                <p>
                    Podpora regiónov a samospráv z eurofondov by v najbližších rokoch mala zohrávať dôležitú úlohu. Skôr než budú k dispozícii grantové výzvy z programového obdobia 2021 – 2027, zostáva Slovensku vyčerpať značné množstvo peňazí z toho predošlého. K tomu treba zohľadniť aj granty a dotácie z „protikrízového“ balíka REACT EU či z Plánu obnovy, v rámci ktorého je pre Slovensko vyčlenený veľký obnos peňazí. Mestá a obce na Slovensku majú momentálne viacero možností získať dotácie na rôzne účely. V roku 2021 bolo vyhlásených viacero grantových výziev. Či už je to dotácia na obnovu kultúrnych domov, galérií alebo modernizácia nových cyklotrás, práve teraz je vhodný čas žiadať o financie z eurofondov a ďalších zdrojov.
                </p>
            </div>
        </section>

        <section class="grants-in-the-field">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">

                        <h2 class="title-secondary text-center">Granty a výzvy vo zvolenej oblasti</h2>

                        <div class="card-content-two-col card-border-hover card-shadow-hover">
                            <div class="card-container">

                                <div class="tag-container">
                                    <a href="#" class="tag-rectangle">Covid 19</a>
                                    <a href="" class="tag-rectangle"><img src="http://grantexpert.test/assets/img/svg/star-blank-white.svg" alt="star-blank"> Nová výzva</a>
                                </div>

                                <div class="row">
                                    <div class="col-xl-8">
                                        <a href="#"><h2>Podpora nových a existujúcich sociálnych služieb, sociálnoprávnej ochrany detí v zariadeniach na komunitnej úrovni (2021)</h2></a>
                                        <p>
                                            Cieľom výzvy je podporiť prechod poskytovania sociálnych služieb a zabezpečenia výkonu opatrení sociálnoprávnej ochrany detí a sociálnej kurately v zariadení z inštitucionálnej formy na komunitnú a podporiť rozvoj služieb starostlivosti o dieťa do troch rokov veku na ...
                                        </p>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="row">
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Výška dotácie
                                                        </div>
                                                        <div class="value">max. 2 000 000</div>
                                                    </div>
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Miera dotácie
                                                        </div>
                                                        <div class="value">max. 95%</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="row">
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Ostáva dní
                                                        </div>
                                                        <div class="value">48</div>
                                                    </div>
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="date">
                                                            <div class="date-from">Od 1.9.2021</div>
                                                            <div class="date-to">Do Vyčerpania financií</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="card-content-two-col card-border-hover card-shadow-hover">
                            <div class="card-container">

                                <div class="tag-container">
                                    <a href="#" class="tag-rectangle">Covid 19</a>
                                    <a href="" class="tag-rectangle"><img src="http://grantexpert.test/assets/img/svg/star-blank-white.svg" alt="star-blank"> Nová výzva</a>
                                </div>

                                <div class="row">
                                    <div class="col-xl-8">
                                        <a href="#"><h2>Podpora nových a existujúcich sociálnych služieb, sociálnoprávnej ochrany detí v zariadeniach na komunitnej úrovni (2021)</h2></a>
                                        <p>
                                            Cieľom výzvy je podporiť prechod poskytovania sociálnych služieb a zabezpečenia výkonu opatrení sociálnoprávnej ochrany detí a sociálnej kurately v zariadení z inštitucionálnej formy na komunitnú a podporiť rozvoj služieb starostlivosti o dieťa do troch rokov veku na ...
                                        </p>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="row">
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Výška dotácie
                                                        </div>
                                                        <div class="value">max. 2 000 000</div>
                                                    </div>
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Miera dotácie
                                                        </div>
                                                        <div class="value">max. 95%</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="row">
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Ostáva dní
                                                        </div>
                                                        <div class="value">48</div>
                                                    </div>
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="date">
                                                            <div class="date-from">Od 1.9.2021</div>
                                                            <div class="date-to">Do Vyčerpania financií</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-content-two-col card-border-hover card-shadow-hover">
                            <div class="card-container">

                                <div class="tag-container">
                                    <a href="#" class="tag-rectangle">Covid 19</a>
                                    <a href="" class="tag-rectangle"><img src="http://grantexpert.test/assets/img/svg/star-blank-white.svg" alt="star-blank"> Nová výzva</a>
                                </div>

                                <div class="row">
                                    <div class="col-xl-8">
                                        <a href="#"><h2>Podpora nových a existujúcich sociálnych služieb, sociálnoprávnej ochrany detí v zariadeniach na komunitnej úrovni (2021)</h2></a>
                                        <p>
                                            Cieľom výzvy je podporiť prechod poskytovania sociálnych služieb a zabezpečenia výkonu opatrení sociálnoprávnej ochrany detí a sociálnej kurately v zariadení z inštitucionálnej formy na komunitnú a podporiť rozvoj služieb starostlivosti o dieťa do troch rokov veku na ...
                                        </p>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="row">
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Výška dotácie
                                                        </div>
                                                        <div class="value">max. 2 000 000</div>
                                                    </div>
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Miera dotácie
                                                        </div>
                                                        <div class="value">max. 95%</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="row">
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Ostáva dní
                                                        </div>
                                                        <div class="value">48</div>
                                                    </div>
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="date">
                                                            <div class="date-from">Od 1.9.2021</div>
                                                            <div class="date-to">Do Vyčerpania financií</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="count-of-grants">
                            <span class="number">+81</span>
                            <span class="text">Ďalších grantových výziev v tejto oblasti</span>
                        </div>
                        <div class="button-center">
                            <a href="#" class="button-primary">Zobraziť všetky</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php include '../../_components/section-suppliers.php';?>

        <?php include '../../_components/section-blog.php';?>

    </main>

    <?php include '../../_components/_footer.php';?>

