
<?php include '../../_components/_head.php';?>

<body class="page-reference-detail page-about-us">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li>
                    <a href="#">Kto sme</a>
                </li>
                <li>
                    <a href="#">Ako fungujeme</a>
                </li>
                <li>
                    <a href="#">FAQ</a>
                </li>
                <li class="active">
                    <a href="#">Referencie</a>
                </li>
                <li>
                    <a href="#">Partneri</a>
                </li>
                <li>
                    <a href="#">Kontakt</a>
                </li>
            </ul>
        </nav>


        <header class="card-title-header">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <div class="col-container">
                            <div class="col-card col-card--logo order-2 order-lg-1">
                                <div class="card-header-summary">
                                    <img src="http://grantexpert.test/assets/img/profesia-logo.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-title order-1 order-lg-2">
                                <div class="vertical-center">
                                    <h1 class="page-title title-primary">Naťa a Maťo s.r.o.</h1>
                                    <p class="description title-secondary title-secondary--light">
                                        Spoločnosť žiadala nenávratný finančný príspevok vo výške 91 867,68 € a ten aj získala.
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="row-line-grid">
            <div class="container">
                <div class="col-container col-section-title">
                    <div class="col-title">
                        <h2 class="title-secondary">Grantový zdroj</h2>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Operačný program</h3>
                    </div>
                    <div class="col-content">
                        <p>
                            Integrovaný regionálny operačný program
                        </p>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Prioritná os:</h3>
                    </div>
                    <div class="col-content">
                        <p>
                            3. Mobilizácia kreatívneho potenciálu v regiónoch
                        </p>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Špecifický cieľ</h3>
                    </div>
                    <div class="col-content">
                        <p>
                            3.1 Stimulovanie podpory udržateľnej zamestnanosti a tvorby pracovných miest v kultúrnom a kreatívnom priemysle prostredníctvom vytvorenia priaznivého prostredia pre rozvoj kreatívneho talentu, netechnologických inovácií
                        </p>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Kód a názov výzvy</h3>
                    </div>
                    <div class="col-content">
                        <p>
                            IROP-PO3-SC31-2016-5 Podpora prístupu k hmotným a nehmotným aktívam MSP v kultúrnom a kreatívnom sektore pre účely tvorby pracovných miest (decentralizovaná podpora)
                        </p>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Vyhlasovateľ výzvy:</h3>
                    </div>
                    <div class="col-content">
                        <p>
                            <strong>Ministerstvo kultúry Slovenskej republiky – Sprostredkovateľský orgán IROP – MKSR</strong>
                        </p>
                    </div>
                </div>
                <div class="col-container col-section-title">
                    <div class="col-title">
                        <h2 class="title-secondary">Stručne o projekte</h2>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Popis projektu</h3>
                    </div>
                    <div class="col-content">
                        <p>
                            Projekt s názvom „Zvýšenie kvality a výrobných kapacít vo firme Naťa a Maťo s.r.o.“ je zameraný na inováciu výrobného procesu – produktov a služieb – prostredníctvom technologických a netechnologických inovácií.
                            Technologické inovácie sa dosiahnu prostredníctvom obstarania moderných technológií (DTG tlačiareň a sušiaci tunel) a netechnologické inovácie budú dosiahnuté prostredníctvom marketingovej kampane na podporu spoločnosti a zároveň prostredníctvom zmeny organizačnej štruktúry spoločnosti vytvorením 2 nových pracovných miest (1 pracovné miesto bude vytvorené zo skupiny neaktívnych občanov).
                        </p>
                        <p>
                            Cieľom projektu a predkladaného podnikateľského plánu je – „Zlepšiť postavenie spoločnosti Naťa a Maťo, s.r.o. na súčasnom trhu prostredníctvom technologických a netechnologických inovácií.“
                        </p>
                        <p>
                            Dosiahnutie tohto cieľa bude realizované prostredníctvom dvoch hlavných aktivít v rámci realizácie projektu:
                        </p>
                        <p>
                            Obstaranie inovatívnych technológií – v rámci tejto aktivity bude obstaraná DTG tlačiareň na priamu tlač textilu a sušiaci tunel.
                            Marketingová podpora spoločnosti – v rámci tejto aktivity bude zabezpečená marketingová podpora spoločnosti prostredníctvom netechnologickej inovácie marketingovej kampane.
                            Miesto realizácie projektu bude v meste Podolínec, v Prešovskom samosprávnom kraji.
                        </p>
                    </div>
                </div>
                <div class="col-container col-section-title">
                    <div class="col-title">
                        <h2 class="title-secondary">Výška finančných prostriedkov projektu</h2>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Schválená celková výška oprávnených výdavkov : </h3>
                    </div>
                    <div class="col-content">
                        <p>
                            99 964,83 €
                        </p>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Schválená celková výška oprávnených výdavkov pre projekty generujúce príjem</h3>
                    </div>
                    <div class="col-content">
                        <p>
                            99 964,83 €
                        </p>
                    </div>
                </div>
            </div>

        </section>

        <section class="offer-closable">
            <div class="container">
                <div class="background-container">
                    <button class="button-close--blue"></button>
                    <div class="grant-radar-container">
                        <div class="row">
                            <div class="col-lg-5 col-xl-3">
                                <img class="image" src="http://grantexpert.test/assets/img/svg/grant-expert-illustration.svg" alt="">
                            </div>
                            <div class="col-lg-7 col-xl-6">
                                <h3 class="title-secondary">Grantoví experti</h3>
                                <p>
                                    Máte záujem o vypracovanie vlastného projektového nápadu?  Pozrite si zoznam našich grantových expertov
                                </p>
                            </div>
                            <div class="offset-lg-5 col-lg-7 offset-xl-0 col-xl-3">
                                <div class="button-container">
                                    <a href="" class="button-tertiary button-tertiary--shadow">Zobraziť</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </main>

    <?php include '../../_components/_footer.php';?>

