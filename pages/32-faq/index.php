
<?php include '../../_components/_head.php';?>

<body class="page-faq page-about-us">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li>
                    <a href="#">Kto sme</a>
                </li>
                <li>
                    <a href="#">Ako fungujeme</a>
                </li>
                <li class="active">
                    <a href="#">FAQ</a>
                </li>
                <li>
                    <a href="#">Referencie</a>
                </li>
                <li >
                    <a href="#">Partneri</a>
                </li>
                <li>
                    <a href="#">Kontakt</a>
                </li>
            </ul>
        </nav>

        <section class="faq-section">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <h1 class="title-primary">Najčastejšie otázky</h1>

                        <div class="faq--desktop">
                            <div class="row">
                                <div class="col-lg-4">
                                    <ul class="nav faq-nav-tab" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link active" id="faq-group-1-tab" data-bs-toggle="tab" data-bs-target="#faq-group-1-pane" type="button" role="tab" aria-controls="faq-group-1-pane" aria-selected="true">Ako portál funguje?</button>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link" id="faq-group-2-tab" data-bs-toggle="tab" data-bs-target="#faq-group-2-pane" type="button" role="tab" aria-controls="faq-group-2-pane" aria-selected="false">Pomoc pre klientov</button>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-8">
                                    <div class="tab-content">
                                        <div class="tab-pane fade show active" id="faq-group-1-pane" role="tabpanel" aria-labelledby="faq-group-1-tab">
                                            <div class="roll-down-list">
                                                <div class="roll-down-item">
                                                    <div class="roll-down-item-container">
                                                        <div class="label">
                                                            Je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                                                        </div>
                                                        <div class="content">
                                                            <p>
                                                                Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="roll-down-item">
                                                    <div class="roll-down-item-container">
                                                        <div class="label">
                                                            Je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                                                        </div>
                                                        <div class="content">
                                                            <p>
                                                                Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="roll-down-item">
                                                    <div class="roll-down-item-container">
                                                        <div class="label">
                                                            Je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                                                        </div>
                                                        <div class="content">
                                                            <p>
                                                                Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="roll-down-item">
                                                    <div class="roll-down-item-container">
                                                        <div class="label">
                                                            Je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                                                        </div>
                                                        <div class="content">
                                                            <p>
                                                                Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane fade" id="faq-group-2-pane" role="tabpanel" aria-labelledby="faq-group-2-tab">
                                            <div class="roll-down-list">
                                                <div class="roll-down-item">
                                                    <div class="roll-down-item-container">
                                                        <div class="label">
                                                            Ako je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                                                        </div>
                                                        <div class="content">
                                                            <p>
                                                                Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="roll-down-item">
                                                    <div class="roll-down-item-container">
                                                        <div class="label">
                                                            Je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                                                        </div>
                                                        <div class="content">
                                                            <p>
                                                                Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="roll-down-item">
                                                    <div class="roll-down-item-container">
                                                        <div class="label">
                                                            Je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                                                        </div>
                                                        <div class="content">
                                                            <p>
                                                                Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="roll-down-item">
                                                    <div class="roll-down-item-container">
                                                        <div class="label">
                                                            Je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                                                        </div>
                                                        <div class="content">
                                                            <p>
                                                                Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="faq--mobile">
                            <div class="roll-down-list">
                                <div class="roll-down-item">
                                    <div class="roll-down-item-container">
                                        <div class="label">
                                            Ako portál funguje?
                                        </div>
                                        <div class="content">
                                            <div class="roll-down-list">
                                                <div class="roll-down-item">
                                                    <div class="roll-down-item-container">
                                                        <div class="label">
                                                            Je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                                                        </div>
                                                        <div class="content">
                                                            <p>
                                                                Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="roll-down-item">
                                                    <div class="roll-down-item-container">
                                                        <div class="label">
                                                            Je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                                                        </div>
                                                        <div class="content">
                                                            <p>
                                                                Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="roll-down-item">
                                                    <div class="roll-down-item-container">
                                                        <div class="label">
                                                            Je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                                                        </div>
                                                        <div class="content">
                                                            <p>
                                                                Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="roll-down-item">
                                                    <div class="roll-down-item-container">
                                                        <div class="label">
                                                            Je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                                                        </div>
                                                        <div class="content">
                                                            <p>
                                                                Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="roll-down-item">
                                    <div class="roll-down-item-container">
                                        <div class="label">
                                            Pomoc pre klientov
                                        </div>
                                        <div class="content">
                                            <div class="roll-down-list">
                                                <div class="roll-down-item">
                                                    <div class="roll-down-item-container">
                                                        <div class="label">
                                                            Ako je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                                                        </div>
                                                        <div class="content">
                                                            <p>
                                                                Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="roll-down-item">
                                                    <div class="roll-down-item-container">
                                                        <div class="label">
                                                            Je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                                                        </div>
                                                        <div class="content">
                                                            <p>
                                                                Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="roll-down-item">
                                                    <div class="roll-down-item-container">
                                                        <div class="label">
                                                            Je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                                                        </div>
                                                        <div class="content">
                                                            <p>
                                                                Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="roll-down-item">
                                                    <div class="roll-down-item-container">
                                                        <div class="label">
                                                            Je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                                                        </div>
                                                        <div class="content">
                                                            <p>
                                                                Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="roll-down-item">
                                    <div class="roll-down-item-container">
                                        <div class="label">
                                            Pomoc pre grantových expertov
                                        </div>
                                        <div class="content">
                                            <div class="roll-down-list">
                                                <div class="roll-down-item">
                                                    <div class="roll-down-item-container">
                                                        <div class="label">
                                                            Ako je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                                                        </div>
                                                        <div class="content">
                                                            <p>
                                                                Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="roll-down-item">
                                                    <div class="roll-down-item-container">
                                                        <div class="label">
                                                            Je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                                                        </div>
                                                        <div class="content">
                                                            <p>
                                                                Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="roll-down-item">
                                                    <div class="roll-down-item-container">
                                                        <div class="label">
                                                            Je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                                                        </div>
                                                        <div class="content">
                                                            <p>
                                                                Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="roll-down-item">
                                                    <div class="roll-down-item-container">
                                                        <div class="label">
                                                            Je možné predplatné kedykoľvek zrušiť, alebo zmeniť?
                                                        </div>
                                                        <div class="content">
                                                            <p>
                                                                Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenený. Spopularizovaný bol v 60-tych rokoch 20.storočia, vydaním hárkov Letraset, ktoré obsahovali pasáže Lorem Ipsum, a neskôr aj publikačným softvérom ako Aldus PageMaker, ktorý obsahoval verzie Lorem Ipsum.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </section>

    </main>

    <?php include '../../_components/_footer.php';?>

