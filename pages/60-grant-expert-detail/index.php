
<?php include '../../_components/_head.php';?>

<body class="page-grant-expert-detail page-grant-jobs">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li>
                    <a href="#">Pracovné ponuky</a>
                </li>
                <li>
                    <a href="#">Pridať pracovnú ponuku</a>
                </li>
                <li class="active">
                    <a href="#">Databáza grantových expertov</a>
                </li>
                <li>
                    <a href="#">Stať sa Grantovým expertom</a>
                </li>
                <li>
                    <a href="#">Blog</a>
                </li>
            </ul>
        </nav>


        <header class="card-title-header">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <div class="col-container">
                            <div class="col-card col-card--logo order-2 order-lg-1">
                                <div class="card-header-summary">
                                    <img src="http://grantexpert.test/assets/img/profesia-logo.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-title order-1 order-lg-2">
                                <div class="vertical-center">
                                    <h1 class="page-title title-secondary">Názov subjektu alebo Grantového špecialistu</h1>
                                    <p class="description text-primary">
                                        Košický kraj
                                    </p>

                                    <div class="tag-container mt-4">
                                        <a href="#" class="tag">Financie</a>
                                        <a href="#" class="tag">Štátna správa</a>
                                        <a href="#" class="tag">Financie</a>
                                        <a href="#" class="tag">Štátna správa</a>
                                        <a href="#" class="tag">Financie</a>
                                        <a href="#" class="tag">Štátna správa</a>
                                        <a href="#" class="tag">Financie</a>
                                        <a href="#" class="tag">Štátna správa</a>
                                    </div>
                                    <div class="tag-container">
                                        <a href="#" class="tag tag--sun">Štátna správa</a>
                                        <a href="#" class="tag tag--sun">Financie</a>
                                        <a href="#" class="tag tag--sun">Štátna správa</a>
                                        <a href="#" class="tag tag--sun">Financie</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="row-line-grid">
            <div class="container">
                <div class="col-container">
                    <div class="col-title">
                        <h3>O nás</h3>
                    </div>
                    <div class="col-content">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat magna vel mi congue condimentum. In dignissim ut lacus eu commodo. Nullam aliquam dui at nisl tincidunt sagittis. Pellentesque molestie nibh a arcu viverra dapibus. Praesent vestibulum velit in porta convallis. Suspendisse rhoncus nunc sit amet elit fringilla luctus. Donec efficitur odio fermentum arcu euismod, condimentum ornare risus porta. In semper eleifend nibh, nec tincidunt magna euismod eu. Praesent et sem eu lacus venenatis commodo. Fusce et diam maximus mi euismod consectetur. Fusce sit amet maximus nisl, id tincidunt arcu. Nunc ut mi vestibulum, pretium justo euismod, posuere mauris. In justo est, vestibulum non elementum a, interdum vitae est. Pellentesque tempus, sapien sed hendrerit dapibus, tellus lectus laoreet justo, vitae vestibulum orci elit vitae massa.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="row-line-grid mt-5">
            <div class="container">
                <div class="col-container col-section-title">
                    <div class="col-title">
                        <h2 class="title-secondary text-blue">Služby experta</h2>
                    </div>
                    <div class="col-content">
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3 class="title-18">Názov služby</h3>
                    </div>
                    <div class="col-content">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce porta mi sed interdum lobortis. Sed id egestas arcu, in eleifend ligula. Mauris viverra, nulla placerat tempor commodo, est ipsum pretium elit, in vestibulum quam orci at lectus. Nam dictum a enim nec placerat. Quisque sapien leo, tempus non euismod et, condimentum at mauris. Aenean dictum luctus purus, nec efficitur justo volutpat id. Nunc convallis ullamcorper orci.
                        </p>
                        <div class="price-button price-button--margin-bottom">
                            <div class="col-container">
                                <div class="price-container">
                                    <div class="vertical-center">
                                        <div class="tax-included">Cena s DPH</div>
                                        <div class="title-secondary color-sun price">123 €</div>
                                    </div>
                                </div>
                                <div class="button-container">
                                    <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3 class="title-18">Názov služby</h3>
                    </div>
                    <div class="col-content">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce porta mi sed interdum lobortis. Sed id egestas arcu, in eleifend ligula. Mauris viverra, nulla placerat tempor commodo, est ipsum pretium elit, in vestibulum quam orci at lectus. Nam dictum a enim nec placerat. Quisque sapien leo, tempus non euismod et, condimentum at mauris. Aenean dictum luctus purus, nec efficitur justo volutpat id. Nunc convallis ullamcorper orci.
                        </p>
                        <div class="price-button price-button--margin-bottom">
                            <div class="col-container">
                                <div class="price-container">
                                    <div class="vertical-center">
                                        <div class="tax-included">Cena s DPH</div>
                                        <div class="title-secondary color-sun price">Na vyžiadanie</div>
                                    </div>
                                </div>
                                <div class="button-container">
                                    <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <section class="expert-references project-list mt-2 mt-lg-7">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <h2 class="title-secondary pt-5">Referencie experta</h2>

                        <header>
                            <div class="row">
                                <div class="col-md-6 col-lg-3">
                                    <div class="vertical-center">
                                        Žiadateľ
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-3">
                                    <div class="vertical-center">
                                        Grantový zdroj
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-3">
                                    <div class="vertical-center">
                                        Región
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-3">
                                    <div class="vertical-center">
                                        Schválená celková výška
                                        oprávnených výdavkov
                                    </div>
                                </div>
                            </div>
                        </header>
                        <a href="#" class="card-project">
                            <div class="card-project-container">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            Jajneken s.r.o.
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            Integrovaný regionálny
                                            operačný program
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            Bratislavský kraj
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-price col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            256 308,49 €
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>

                        <a href="#" class="card-project">
                            <div class="card-project-container">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            Jajneken s.r.o.
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            Integrovaný regionálny
                                            operačný program
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            Bratislavský kraj
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-price col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            256 308,49 €
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>

                        <a href="#" class="card-project">
                            <div class="card-project-container">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            Jajneken s.r.o.
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            Integrovaný regionálny
                                            operačný program
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            Bratislavský kraj
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-price col-md-6 col-lg-3">
                                        <div class="vertical-center">
                                            256 308,49 €
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

        </section>


    </main>

    <?php include '../../_components/_footer.php';?>

