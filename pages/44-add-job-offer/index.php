
<?php include '../../_components/_head.php';?>

<body class="page-add-job-offer page-grant-jobs">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li>
                    <a href="#">Pracovné ponuky</a>
                </li>
                <li class="active">
                    <a href="#">Pridať pracovnú ponuku</a>
                </li>
                <li>
                    <a href="#">Databáza grantových expertov</a>
                </li>
                <li>
                    <a href="#">Stať sa Grantovým expertom</a>
                </li>
                <li>
                    <a href="#">Blog</a>
                </li>
            </ul>
        </nav>

       <section class="description-benefit">
           <div class="container-fluid">
               <div class="section-container">
                   <div class="container">
                       <div class="row">
                           <div class="col-lg-6">
                               <div class="description">
                                   <img src="http://grantexpert.test/assets/img/svg/briefcase-plus.svg" alt="">
                                   <h1 class="title-primary">Pridať pracovnú ponuku</h1>
                                   <p class="text-primary">
                                       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla suscipit accumsan urna eu ultricies. Quisque mauris erat, porta lacinia libero vitae, dapibus commodo augue.
                                   </p>

                                   <a href="#" class="button-tertiary button-tertiary--shadow">Pridať ponuku</a>
                               </div>
                           </div>
                           <div class="col-lg-6">
                               <div class="card">
                                   <div class="col-container">
                                       <div class="benefit">
                                           <h3>Benefit 1</h3>
                                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse elit elit,</p>
                                       </div>
                                       <div class="benefit">
                                           <h3>Benefit 1</h3>
                                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse elit elit,</p>
                                       </div>
                                       <div class="benefit">
                                           <h3>Benefit 1</h3>
                                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse elit elit,</p>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>

                       <section class="advertising mt-5 mt-lg-9">
                           <div class="row">
                               <div class="col-lg-4">
                                   <div class="card-advertising card-advertising--sun">
                                       <h3 class="title-primary">Basic</h3>
                                       <div class="price-container">
                                           <div class="tax-included">Cena s DPH</div>
                                           <div class="title-secondary">123 €/inzerát</div>
                                       </div>
                                       <ul class="check-list check-list--black-check text-secondary">
                                           <li>Neobmedzený čas ponuky</li>
                                           <li>Logo spoločnosti</li>
                                       </ul>
                                   </div>
                                   <div class="button-secondary">OBJEDNAŤ</div>
                               </div>
                               <div class="col-lg-4">
                                   <div class="card-advertising card-advertising--recommendation">
                                       <div class="card-recommendation">
                                           Najefektívnejší spôsob inzerovania
                                       </div>
                                       <h3 class="title-primary">Medium</h3>
                                       <div class="price-container">
                                           <div class="tax-included">Cena s DPH</div>
                                           <div class="title-secondary">123 €/inzerát</div>
                                       </div>
                                       <ul class="check-list check-list--black-check text-secondary">
                                           <li>Neobmedzený čas ponuky</li>
                                           <li>Prémiové zobrazenie 14 dní</li>
                                           <li>Facebook post</li>
                                           <li>Logo spoločnosti</li>
                                           <li>PPC kampaň v hodnote 10€</li>
                                       </ul>
                                   </div>
                                   <div class="button-secondary">OBJEDNAŤ</div>
                               </div>
                               <div class="col-lg-4">
                                   <div class="card-advertising card-advertising--sun">
                                       <h3 class="title-primary">Expert</h3>
                                       <div class="price-container">
                                           <div class="tax-included">Cena s DPH</div>
                                           <div class="title-secondary">123 €/inzerát</div>
                                       </div>
                                       <ul class="check-list check-list--black-check text-secondary">
                                           <li>Neobmedzený čas ponuky</li>
                                           <li>Prémiové zobrazenie 14 dní</li>
                                           <li>Facebook post</li>
                                           <li>Logo spoločnosti</li>
                                           <li>PPC kampaň v hodnote 10€</li>
                                           <li>Databáza životopisov</li>
                                       </ul>
                                   </div>
                                   <div class="button-secondary">OBJEDNAŤ</div>
                               </div>
                           </div>

                       </section>

                   </div>
               </div>
           </div>
       </section>






    </main>

    <?php include '../../_components/_footer.php';?>

