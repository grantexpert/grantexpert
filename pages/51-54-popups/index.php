
<?php include '../../_components/_head.php';?>

<body class="page-51-54-popups">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">
        <div class="container">
            <div class="button-center">
                <button class="button-primary" data-bs-toggle="modal" data-bs-target="#modal-login">popup 51, 54 - prihlásiť sa</button>
            </div>

            <div class="modal fade" id="modal-login" tabindex="-1" aria-labelledby="modal-login" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        <div class="page-1">
                            <div class="col-container">
                                <div class="col-left">
                                    <div class="inner-container">
                                        <h4 class="title-secondary">Existujúci zákazník</h4>
                                        <p>Pre dokončenie objednávky je potrebné prihlásenie</p>
                                        <div class="button-center">
                                            <button class="login-button button-tertiary button-icon">
                                                <div class="col-container">
                                                    <div class="icon-container">
                                                        <img src="http://grantexpert.test/assets/img/svg/user-white.svg" alt="">
                                                    </div>
                                                    <div class="label-container">
                                                        Prihlásiť sa
                                                    </div>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-right">
                                    <div class="inner-container">
                                        <h4 class="title-secondary">Nový zákazník</h4>
                                        <p>Ak ste u nás ešte nenakupovali, vytvorte si prosím nový účet</p>
                                        <div class="button-center">
                                            <a href="#" class="button-tertiary">Registrovať sa</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="page-2">
                            <div class="col-container">
                                <div class="col-login-form">

                                    <div class="input-icon input-icon--user">
                                        <input class="custom-form-control" type="text" placeholder="Meno">
                                    </div>
                                    <div class="input-icon input-icon--password">
                                        <input class="custom-form-control" type="password" placeholder="Heslo">
                                    </div>
                                    <div class="button-center">
                                        <button class="button-tertiary button-tertiary--padding">PRIHLÁSIŤ</button>
                                    </div>
                                    <div class="center-container">
                                        <a href="#" class="forgotten-password">Zabudli ste heslo?</a>
                                    </div>

                                </div>
                                <div class="col-register">
                                    <h4>Ešte nie som zaregistrovaný</h4>
                                    <p>Registrácia Vám zaberie len pár sekúnd. Kliknite na Registrácia a systém Vás prevedie registráciou.</p>
                                    <div class="button-center">
                                        <a href="#" class="button-tertiary button-tertiary--reverse">Registrácia</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="button-center">
                <button class="button-primary" data-bs-toggle="modal" data-bs-target="#modal-webinars">popup 52 - webináre</button>
            </div>

            <div class="modal fade" id="modal-webinars" tabindex="-1" aria-labelledby="modal-webinars" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        <div class="header">
                            <h4>Ďakujeme, že si plánujete zakúpiť náš webinár</h4>
                            <p>Radi by sme vám pripomenuli, že pri predplatení balíku našich služieb MEDIUM, alebo PROFI získate prístup ku všetkým webinárom v našom archíve.</p>
                        </div>
                        <div class="col-container">
                            <div class="col-left">
                                <div class="inner-container">
                                    <p>Chcem získať prístup do celého archívu webinárov</p>
                                    <div class="button-center">
                                        <a href="#" class="button-tertiary">Zobraziť ponuku balíkov</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-right">
                                <div class="inner-container">
                                    <p>Stačí mi zvolený webinár, nepotrebujem prístup ku všetkým webinárom</p>
                                    <div class="button-center">
                                        <a href="#" class="button-tertiary">Zaplatiť 69 €</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="button-center">
                <button class="button-primary" data-bs-toggle="modal" data-bs-target="#modal-order-cancellation">popup 53 - zrušenie objednávky</button>
            </div>

            <div class="modal fade" id="modal-order-cancellation" tabindex="-1" aria-labelledby="modal-order-cancellation" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                        <div class="inner-container">

                            <h4 class="title-secondary">Prečo ste sa rozhodli zrušiť objednávku?</h4>

                            <label class="radio-button-container">Chcel som objednať niečo iné
                                <input type="radio" name="radio" value="nieco-ine">
                                <span class="checkmark"></span>
                            </label>
                            <label class="radio-button-container">Neviem čo dostanem za svoje peniaze
                                <input type="radio" name="radio" value="neviem">
                                <span class="checkmark"></span>
                            </label>
                            <label class="radio-button-container">Mám problém s platobnou bránou - napíšte nám prosím na ahoj@grantexpert.sk
                                <input type="radio" name="radio" value="platobna">
                                <span class="checkmark"></span>
                            </label>
                            <label class="radio-button-container">Iný dôvod:
                                <input type="radio" name="radio" value="iny">
                                <span class="checkmark"></span>
                            </label>
                            <input class="custom-form-control" type="text">

                            <div class="col-container">
                                <div class="col-left">
                                    <p>Naozaj chcete zrušiť objednávku?</p>
                                </div>
                                <div class="col-right">
                                    <button class="button-primary">ÁNO</button>
                                    <button class="button-primary button-primary--reverse">NIE</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>

    <?php include '../../_components/_footer.php';?>

