
<?php include '../../_components/_head.php';?>

<body class="page-registration">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">
        <div class="container-780">
            <section class="company-name">
                <div class="card-animation card-shadow-animation">
                    <div class="card card-shadow">
                        <div class="card-container">
                            <input class="custom-form-control input--gray" type="text" placeholder="IČO, alebo názov spoločnosti">
                            <div class="checkbox">
                                <input name="" type="checkbox">
                                <div class="label">
                                    <div class="inline-container">
                                        <div class="col-container">
                                            <div class="col-text">Som fyzická osoba</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="billing-information">
                <h2 class="title-secondary">Fakturačné údaje</h2>
                <div class="row">
                    <div class="col-sm-6 col-md-6">
                        <div class="input-required">
                            <div class="input-container">
                                <input class="custom-form-control" type="text" placeholder="IČO">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                        <div class="input-required">
                            <div class="input-container">
                                <input class="custom-form-control" type="text" placeholder="Názov spoločnosti">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="checkbox">
                            <input name="" type="checkbox">
                            <div class="label">
                                <div class="inline-container">
                                    <div class="col-container">
                                        <div class="col-text">Som fyzická osoba</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-required">
                            <div class="input-container">
                                <input class="custom-form-control" type="text" placeholder="Ulica">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-required">
                            <div class="input-container">
                                <input class="custom-form-control" type="text" placeholder="Súpisné/Orientčné číslo">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-6">
                        <div class="input-required">
                            <div class="input-container">
                                <input class="custom-form-control" type="text" placeholder="Obec">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                        <div class="input-required">
                            <div class="input-container">
                                <input class="custom-form-control" type="text" placeholder="PSČ">
                            </div>
                        </div>
                    </div>
                </div>
                <section class="personal-info">
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                            <div class="input-required">
                                <div class="input-container">
                                    <input class="custom-form-control" type="text" placeholder="Meno">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="input-required">
                                <div class="input-container">
                                    <input class="custom-form-control" type="text" placeholder="Priezvisko">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                            <div class="input-required">
                                <div class="input-container">
                                    <input class="custom-form-control" type="text" placeholder="Telefón">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="input-required">
                                <div class="input-container">
                                    <input class="custom-form-control" type="email" placeholder="Email">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                            <div class="input-required">
                                <div class="input-container">
                                    <input class="custom-form-control" type="password" placeholder="Heslo">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="input-required">
                                <div class="input-container">
                                    <input class="custom-form-control" type="password" placeholder="Zopakovať heslo">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </section>

            <div class="button-center">
                <button class="button-tertiary button-tertiary--padding">Registrovať</button>
            </div>
        </div>

    </main>

    <?php include '../../_components/_footer.php';?>

