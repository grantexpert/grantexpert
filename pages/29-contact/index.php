
<?php include '../../_components/_head.php';?>

<body class="page-contact page-about-us">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li>
                    <a href="#">Kto sme</a>
                </li>
                <li>
                    <a href="#">Ako fungujeme</a>
                </li>
                <li>
                    <a href="#">FAQ</a>
                </li>
                <li>
                    <a href="#">Referencie</a>
                </li>
                <li >
                    <a href="#">Partneri</a>
                </li>
                <li class="active">
                    <a href="#">Kontakt</a>
                </li>
            </ul>
        </nav>

        <section>
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-6">
                                <h1 class="title-primary">Kontakt</h1>
                            </div>
                            <div class="col-xl-6">
                                <div class="social-icons">
                                    <a href="#"><img src="http://grantexpert.test/assets/img/svg/facebook.svg" alt="facebook"></a>
                                    <a href="#"><img src="http://grantexpert.test/assets/img/svg/linkedin.svg" alt="linkedin"></a>
                                    <a href="#"><img src="http://grantexpert.test/assets/img/svg/instagram.svg" alt="instagram"></a>
                                    <a href="#"><img src="http://grantexpert.test/assets/img/svg/youtube.svg" alt="youtube"></a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-xl-3">
                                <h2>Naša adresa</h2>
                                <p class="address">
                                    Grantexpert s.r.o.<br>
                                    Záhradnícka 72<br>
                                    821 08 Bratislava
                                </p>
                                <p class="text-small">
                                    V prípade problémov s platbou nás kontaktujte na číslach:<br>
                                    +421 911 107 306<br>
                                    +421-2-5010 9870
                                </p>
                                <p class="text-small">
                                    Všetky ďalšie otázky radi zodpovieme na emailovej adrese:<br>
                                    ahoj@grantexpert.sk
                                </p>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <h2>Fakturačné údaje</h2>
                                <p class="invoice-info">
                                    IČO: 47454890<br>
                                    DIČ: 2023906786<br>
                                    IČ DPH: SK2023906786
                                </p>
                                <p class="text-small">
                                    Obchodný register Okr. súd BA I, odd. Sro, vl. č. 93256/B (zo dňa 17. 10. 2013)
                                </p>
                                <p class="text-small">
                                    Inšpektorát SOI pre BSK, Prievozská 32, P.O.Box 5, 820 07 Bratislava 27
                                </p>
                            </div>
                            <div class="col-12 col-xl-6">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2661.6502874370253!2d17.138882715759948!3d48.155546957888035!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x476c8932dc88ceb7%3A0x3eb1667dcf0638bb!2sGrantexpert%20s.%20r.%20o.!5e0!3m2!1ssk!2ssk!4v1646649195073!5m2!1ssk!2ssk" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>

    <?php include '../../_components/_footer.php';?>

