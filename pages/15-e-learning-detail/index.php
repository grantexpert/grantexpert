
<?php include '../../_components/_head.php';?>

<body class="page-e-learning-detail page-grant-education">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li class="active">
                    <a href="#">E-LEANING KURZY</a>
                </li>
                <li>
                    <a href="#">WEBINÁRE</a>
                </li>
                <li>
                    <a href="#">ŠKOLENIA NA MIERU</a>
                </li>
                <li>
                    <a href="#">VIDEOSLOVNÍK</a>
                </li>
                <li>
                    <a href="#">GRANTOVÉ ZDROJE</a>
                </li>
                <li>
                    <a href="#">BLOG</a>
                </li>
            </ul>
        </nav>


        <header class="card-title-header card-title-header--with-price">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <div class="col-container">
                            <div class="col-card order-2 order-lg-1">
                                <div class="card-header-summary">
                                    <div class="text-small-label">Rozsah</div>
                                    <div class="value">22 tém + 4 testy</div>
                                    <div class="text-small-label delimeter">Určené pre</div>
                                    <div class="value">Začiatočník</div>
                                    <div class="col-container icon-info-container">
                                        <div class="col-icon">

                                        </div>
                                        <div class="col-info">
                                            <div class="text-small-label">Dĺžka spolu</div>
                                            <div class="value">8 hodín</div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-title order-1 order-lg-2">
                                <div class="stars">
                                    <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                    <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                    <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                    <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                    <img src="http://grantexpert.test/assets/img/svg/star-blank.svg" alt="star-blank">
                                </div>
                                <h1 class="page-title title-secondary">1-2-3 Granty</h1>
                                <p class="description text-primary">
                                    Radi by ste využili granty a dotácie pre financovanie vašich projektov, ale neviete sa zorientovať v spleti informácií?
                                </p>

                                <div class="price-button price-button--white price-button--desktop">
                                    <div class="col-container">
                                        <div class="price-container">
                                            <div class="vertical-center">
                                                <div class="tax-included">Cena s DPH</div>
                                                <div class="title-secondary color-sun price">29 €</div>
                                            </div>
                                        </div>
                                        <div class="button-container">
                                            <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                        </div>

                                    </div>
                                </div>

                            </div>

                            <div class="price-button price-button--white price-button--mobile order-3">
                                <div class="col-container">
                                    <div class="price-container">
                                        <div class="vertical-center">
                                            <div class="tax-included">Cena s DPH</div>
                                            <div class="title-secondary color-sun price">29 €</div>
                                        </div>
                                    </div>
                                    <div class="button-container">
                                        <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="e-learning-advantages">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-lg-4">
                        <div class="col-container">
                            <div class="col-image">
                                <img src="http://grantexpert.test/assets/img/svg/globe.svg" alt="">
                            </div>
                            <div class="col-text">
                                <h4>100% online</h4>
                                <p>
                                    Začnite okamžite a učte sa
                                    vlastným tempom.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="col-container">
                            <div class="col-image">
                                <img src="http://grantexpert.test/assets/img/svg/e-learning.svg" alt="">
                            </div>
                            <div class="col-text">
                                <h4>Video lekcie</h4>
                                <p>
                                    Učte sa prostredníctvom krátkych videí.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="col-container">
                            <div class="col-image">
                                <img src="http://grantexpert.test/assets/img/svg/certificate.svg" alt="">
                            </div>
                            <div class="col-text">
                                <h4>Certifikát</h4>
                                <p>
                                    Získajte certifikát o absolvovaní kurzu.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="contents-text">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4">
                        <div class="contents-container">
                            <h4>Obsah kurzu</h4>
                            <div class="completion">
                                Celková dokončenosť
                                <span class="number">50%</span>
                            </div>

                            <div class="contents">

                                <div class="col-container section-title">
                                    <div class="col-number"><div class="number-container">1</div></div>
                                    <div class="col-title">Vitajte na kurze 1-2-3 granty!</div>
                                </div>
                                <nav>
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <div class="col-container">
                                                    <div class="col-number">1/1</div>
                                                    <div class="col-title">Zoznámte sa s lektorom</div>
                                                    <div class="col-checked"><img src="http://grantexpert.test/assets/img/svg/check.svg" alt=""></div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="col-container">
                                                    <div class="col-number">2/2</div>
                                                    <div class="col-title">Praktické informácie na úvod</div>
                                                    <div class="col-checked"><img src="http://grantexpert.test/assets/img/svg/check.svg" alt=""></div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>



                                <div class="col-container section-title">
                                    <div class="col-number"><div class="number-container">2</div></div>
                                    <div class="col-title">Základné pojmy v oblasti grantového financovania</div>
                                </div>
                                <nav>
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <div class="col-container">
                                                    <div class="col-number">1/7</div>
                                                    <div class="col-title">Program výzva projekt</div>
                                                    <div class="col-checked"><img src="http://grantexpert.test/assets/img/svg/check.svg" alt=""></div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="col-container">
                                                    <div class="col-number">2/7</div>
                                                    <div class="col-title">Typy projektov</div>
                                                    <div class="col-checked"><img src="http://grantexpert.test/assets/img/svg/check.svg" alt=""></div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="col-container">
                                                    <div class="col-number">1/7</div>
                                                    <div class="col-title">Program výzva projekt</div>
                                                    <div class="col-checked"><img src="http://grantexpert.test/assets/img/svg/check.svg" alt=""></div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="active">
                                            <a href="#">
                                                <div class="col-container">
                                                    <div class="col-number">1/7</div>
                                                    <div class="col-title">Typy projektov</div>
                                                    <div class="col-checked"><img src="http://grantexpert.test/assets/img/svg/circle.svg" alt=""></div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="col-container">
                                                    <div class="col-number"><img src="http://grantexpert.test/assets/img/svg/quiz.svg" alt=""></div>
                                                    <div class="col-title">Kvíz - Základné pojmy v oblasti grantového financovania</div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>

                                <div class="col-container section-title">
                                    <div class="col-number"><div class="number-container">3</div></div>
                                    <div class="col-title">Základné pojmy v oblasti grantového financovania</div>
                                </div>
                                <nav>
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <div class="col-container">
                                                    <div class="col-number">1/7</div>
                                                    <div class="col-title">Program výzva projekt</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="col-container">
                                                    <div class="col-number">2/7</div>
                                                    <div class="col-title">Typy projektov</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="col-container">
                                                    <div class="col-number">1/7</div>
                                                    <div class="col-title">Program výzva projekt</div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>

                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8">
                        <header>
                            <div class="col-container">
                                <div class="col-section-number">
                                    <div class="number-container">3</div>
                                </div>
                                <div class="col-count">
                                    1/7
                                </div>
                                <div class="col-title">
                                    <h3>Program výzva projekt</h3>
                                </div>
                                <div class="col-length">
                                    <img src="http://grantexpert.test/assets/img/svg/clock-simple.svg" alt="">
                                    2 minúty 28 sekúnd
                                </div>
                            </div>
                        </header>

                        <div class="text-content">
                            <div class="text-content-container">
                                <video width="100%" controls>
                                    <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
                                    Váš prehliadač nepodporuje HTML5 video
                                </video>

                                <h4>Prepis videa</h4>
                                <p>
                                    Oblasť grantov a dotácií je veľmi rozmanitá a taká je aj terminológia, ktorú používame na označenie rôznych aspektov programového a projektového cyklu. Pôvod týchto pojmov a skratiek treba hľadať v slovenskej a európskej legislatíve a v rôznych manuáloch a iných dokumentoch donorských organizácií. Aby ste sa vedeli úspešne orientovať vo svete grantov a dotácií je nevyhnutné poznať základnú terminológiu, ktorú vám postupne vysvetlím vo videách prvej lekcie. Poďme ale pekne po poriadku a na úvod sa pozrime na to, čo sú granty, dotácie a eurofondy a aký je medzi nimi rozdiel.
                                </p>
                                <p>
                                    Podľa údajov Google Trends za posledných 5 rokov je z týchto 3 výrazov na Slovensku najvyhľadávanejším slovo dotácia/dotácie, ktoré predstavuje viac ako 70% vyhľadávaní, nasledujú eurofondy s 20% a napokon granty s 10%.
                                </p>
                                <p>
                                    Podľa údajov Google Trends za posledných 5 rokov je z týchto 3 výrazov na Slovensku najvyhľadávanejším slovo dotácia/dotácie, ktoré predstavuje viac ako 70% vyhľadávaní, nasledujú eurofondy s 20% a napokon granty s 10%.
                                </p>
                                <p>
                                    Podľa údajov Google Trends za posledných 5 rokov je z týchto 3 výrazov na Slovensku najvyhľadávanejším slovo dotácia/dotácie, ktoré predstavuje viac ako 70% vyhľadávaní, nasledujú eurofondy s 20% a napokon granty s 10%.
                                </p>
                                <p>
                                    Podľa údajov Google Trends za posledných 5 rokov je z týchto 3 výrazov na Slovensku najvyhľadávanejším slovo dotácia/dotácie, ktoré predstavuje viac ako 70% vyhľadávaní, nasledujú eurofondy s 20% a napokon granty s 10%.
                                </p>
                                <p>
                                    Podľa údajov Google Trends za posledných 5 rokov je z týchto 3 výrazov na Slovensku najvyhľadávanejším slovo dotácia/dotácie, ktoré predstavuje viac ako 70% vyhľadávaní, nasledujú eurofondy s 20% a napokon granty s 10%.
                                </p>

                                <div class="text-content-overlay"></div>
                                <div class="button-plus-container">
                                    <button class="button-plus">
                                        <div class="button-border--gray"></div>
                                        <div class="button-border--white">
                                            <div class="icon-container"></div>
                                        </div>
                                    </button>
                                    <div class="label">Zobraziť celý text</div>
                                </div>
                            </div>
                        </div>

                        <footer>
                            <div class="col-container">
                                <div class="col-prev">
                                    <a href="#">
                                        <div class="col-container">
                                            <div class="col-count">
                                                1/2
                                            </div>
                                            <div class="col-arrow">
                                                <button class="button-scroll button-scroll--left">
                                                    <div class="button-border--gray"></div>
                                                    <div class="button-border--white">
                                                        <div class="icon-container"></div>
                                                    </div>
                                                </button>
                                            </div>
                                            <div class="col-label">
                                                Predchádzajúca <br>lekcia
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-next">
                                    <a href="#">
                                        <div class="col-container">
                                            <div class="col-label">
                                                Nasledujúca <br>lekcia
                                            </div>
                                            <div class="col-arrow">
                                                <button class="button-scroll button-scroll--right">
                                                    <div class="button-border--gray"></div>
                                                    <div class="button-border--white">
                                                        <div class="icon-container"></div>
                                                    </div>
                                                </button>
                                            </div>
                                            <div class="col-count">
                                                2/2
                                            </div>

                                        </div>
                                    </a>
                                </div>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </section>


        <section class="grant-education mt-8">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">

                        <h2 class="title-secondary">Ďalšie e-learningy</h2>

                        <div class="row">
                            <div class="col-12">
                                <div class="carousel-container">
                                    <div class="tns-carousel carousel-grant-education">
                                        <div>
                                            <div class="card-event-container card-event-container--blue">
                                                <div class="card card-event">
                                                    <header>
                                                        <div class="col-container">
                                                            <a href="#" class="title-container">
                                                                <h3 class="title-secondary">Cesta úspešného projektu: Podnikateľský IT projekt</h3>
                                                            </a>
                                                            <div class="icon-container">
                                                                <img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker">
                                                            </div>
                                                        </div>

                                                        <div class="preview-container col-container">
                                                            <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker"></span>
                                                            <div class="stars">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star-blank.svg" alt="star-blank">
                                                            </div>
                                                            <a href="#" class="button-preview"><img src="http://grantexpert.test/assets/img/svg/play.svg" alt="play"><span class="label">UKÁŽKA</span></a>
                                                        </div>

                                                        <div class="length-scope-container">
                                                            <div class="col-container">
                                                                <div class="col-length">
                                                                    <span class="label">Dĺžka:</span>3h 28m
                                                                </div>
                                                                <div class="col-scope">
                                                                    <span class="label">Rozsah:</span>22 tém + 4 testy
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </header>
                                                    <h4>Čo sa naučíte?</h4>
                                                    <ul class="check-list text-secondary">
                                                        <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                        <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                        <li>Aktuálne a plánované výzvy</li>
                                                    </ul>
                                                    <footer>
                                                        <div class="col-container">
                                                            <div class="price-container">
                                                                <div class="tax-included">Cena s DPH</div>
                                                                <div class="title-secondary color-sun price">19,90 €</div>
                                                            </div>
                                                            <div class="more-info">
                                                                <a href="#" class="arrow-link no-hover">
                                                                    <div class="icon-container">
                                                                        <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                                                    </div>
                                                                    <div class="label-container">
                                                                        <div class="title">Viac info</div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <div class="button-container">
                                                                <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                                            </div>

                                                        </div>
                                                    </footer>
                                                </div>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="card-event-container card-event-container--blue">
                                                <div class="card card-event">
                                                    <header>
                                                        <div class="col-container">
                                                            <a href="#" class="title-container">
                                                                <h3 class="title-secondary">Cesta úspešného projektu: Podnikateľský IT projekt</h3>
                                                            </a>
                                                            <div class="icon-container">
                                                                <img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker">
                                                            </div>
                                                        </div>

                                                        <div class="preview-container col-container">
                                                            <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker"></span>
                                                            <div class="stars">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star-blank.svg" alt="star-blank">
                                                            </div>
                                                            <a href="#" class="button-preview"><img src="http://grantexpert.test/assets/img/svg/play.svg" alt="play"><span class="label">UKÁŽKA</span></a>
                                                        </div>

                                                        <div class="length-scope-container">
                                                            <div class="col-container">
                                                                <div class="col-length">
                                                                    <span class="label">Dĺžka:</span>3h 28m
                                                                </div>
                                                                <div class="col-scope">
                                                                    <span class="label">Rozsah:</span>22 tém + 4 testy
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </header>
                                                    <h4>Čo sa naučíte?</h4>
                                                    <ul class="check-list text-secondary">
                                                        <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                        <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                        <li>Aktuálne a plánované výzvy</li>
                                                    </ul>
                                                    <footer>
                                                        <div class="col-container">
                                                            <div class="price-container">
                                                                <div class="tax-included">Cena s DPH</div>
                                                                <div class="title-secondary color-sun price">19,90 €</div>
                                                            </div>
                                                            <div class="more-info">
                                                                <a href="#" class="arrow-link no-hover">
                                                                    <div class="icon-container">
                                                                        <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                                                    </div>
                                                                    <div class="label-container">
                                                                        <div class="title">Viac info</div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <div class="button-container">
                                                                <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                                            </div>

                                                        </div>
                                                    </footer>
                                                </div>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="card-event-container card-event-container--blue">
                                                <div class="card card-event">
                                                    <header>
                                                        <div class="col-container">
                                                            <a href="#" class="title-container">
                                                                <h3 class="title-secondary">Cesta úspešného projektu: Podnikateľský IT projekt</h3>
                                                            </a>
                                                            <div class="icon-container">
                                                                <img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker">
                                                            </div>
                                                        </div>

                                                        <div class="preview-container col-container">
                                                            <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker"></span>
                                                            <div class="stars">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star-blank.svg" alt="star-blank">
                                                            </div>
                                                            <a href="#" class="button-preview"><img src="http://grantexpert.test/assets/img/svg/play.svg" alt="play"><span class="label">UKÁŽKA</span></a>
                                                        </div>

                                                        <div class="length-scope-container">
                                                            <div class="col-container">
                                                                <div class="col-length">
                                                                    <span class="label">Dĺžka:</span>3h 28m
                                                                </div>
                                                                <div class="col-scope">
                                                                    <span class="label">Rozsah:</span>22 tém + 4 testy
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </header>
                                                    <h4>Čo sa naučíte?</h4>
                                                    <ul class="check-list text-secondary">
                                                        <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                        <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                        <li>Aktuálne a plánované výzvy</li>
                                                    </ul>
                                                    <footer>
                                                        <div class="col-container">
                                                            <div class="price-container">
                                                                <div class="tax-included">Cena s DPH</div>
                                                                <div class="title-secondary color-sun price">19,90 €</div>
                                                            </div>
                                                            <div class="more-info">
                                                                <a href="#" class="arrow-link no-hover">
                                                                    <div class="icon-container">
                                                                        <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                                                    </div>
                                                                    <div class="label-container">
                                                                        <div class="title">Viac info</div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <div class="button-container">
                                                                <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                                            </div>

                                                        </div>
                                                    </footer>
                                                </div>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="card-event-container card-event-container--blue">
                                                <div class="card card-event">
                                                    <header>
                                                        <div class="col-container">
                                                            <a href="#" class="title-container">
                                                                <h3 class="title-secondary">Cesta úspešného projektu: Podnikateľský IT projekt</h3>
                                                            </a>
                                                            <div class="icon-container">
                                                                <img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker">
                                                            </div>
                                                        </div>

                                                        <div class="preview-container col-container">
                                                            <span class="icon-mobile"><img src="http://grantexpert.test/assets/img/svg/locker.svg" alt="locker"></span>
                                                            <div class="stars">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star.svg" alt="star">
                                                                <img src="http://grantexpert.test/assets/img/svg/star-blank.svg" alt="star-blank">
                                                            </div>
                                                            <a href="#" class="button-preview"><img src="http://grantexpert.test/assets/img/svg/play.svg" alt="play"><span class="label">UKÁŽKA</span></a>
                                                        </div>

                                                        <div class="length-scope-container">
                                                            <div class="col-container">
                                                                <div class="col-length">
                                                                    <span class="label">Dĺžka:</span>3h 28m
                                                                </div>
                                                                <div class="col-scope">
                                                                    <span class="label">Rozsah:</span>22 tém + 4 testy
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </header>
                                                    <h4>Čo sa naučíte?</h4>
                                                    <ul class="check-list text-secondary">
                                                        <li>Všeobecný prehľad grantových zdrojov pre školy</li>
                                                        <li>Podpora školstva a vzdelávania v programovom období 2021 - 2027</li>
                                                        <li>Aktuálne a plánované výzvy</li>
                                                    </ul>
                                                    <footer>
                                                        <div class="col-container">
                                                            <div class="price-container">
                                                                <div class="tax-included">Cena s DPH</div>
                                                                <div class="title-secondary color-sun price">19,90 €</div>
                                                            </div>
                                                            <div class="more-info">
                                                                <a href="#" class="arrow-link no-hover">
                                                                    <div class="icon-container">
                                                                        <div class="icon"><img src="http://grantexpert.test/assets/img/svg/arrow-blue.svg" alt="arrow"></div>
                                                                    </div>
                                                                    <div class="label-container">
                                                                        <div class="title">Viac info</div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <div class="button-container">
                                                                <a href="#" class="button-secondary">ZAKÚPIŤ TERAZ</a>
                                                            </div>

                                                        </div>
                                                    </footer>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <button id="education-carousel-prev" class="button-scroll button-scroll--left">
                                        <div class="button-border--gray"></div>
                                        <div class="button-border--white">
                                            <div class="icon-container"></div>
                                        </div>
                                    </button>
                                    <button id="education-carousel-next" class="button-scroll button-scroll--right">
                                        <div class="button-border--gray"></div>
                                        <div class="button-border--white">
                                            <div class="icon-container"></div>
                                        </div>
                                    </button>

                                </div>
                                <div class="button-center">
                                    <a href="" class="button-primary"><div class="vertical-center">Všetky <br>E-learningy</div></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </main>

    <?php include '../../_components/_footer.php';?>

