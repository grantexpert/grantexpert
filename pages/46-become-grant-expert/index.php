
<?php include '../../_components/_head.php';?>

<body class="page-become-expert page-grant-jobs">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li>
                    <a href="#">Pracovné ponuky</a>
                </li>
                <li>
                    <a href="#">Pridať pracovnú ponuku</a>
                </li>
                <li>
                    <a href="#">Databáza grantových expertov</a>
                </li>
                <li class="active">
                    <a href="#">Stať sa Grantovým expertom</a>
                </li>
                <li>
                    <a href="#">Blog</a>
                </li>
            </ul>
        </nav>

       <section class="description-benefit">
           <div class="container-fluid">
               <div class="section-container">
                   <div class="container">
                       <div class="row">
                           <div class="col-lg-6">
                               <div class="description">
                                   <img src="http://grantexpert.test/assets/img/svg/cogwheel-head.svg" alt="">
                                   <h1 class="title-primary">Stať sa grantovým expertom</h1>
                                   <p class="text-primary">
                                       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla suscipit accumsan urna eu ultricies. Quisque mauris erat, porta lacinia libero vitae, dapibus commodo augue.
                                   </p>

                                   <a href="#" class="button-tertiary button-tertiary--shadow">Mám záujem</a>
                               </div>
                           </div>
                           <div class="col-lg-6">
                               <div class="card">
                                   <div class="col-container">
                                       <div class="benefit">
                                           <h3>Benefit 1</h3>
                                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse elit elit,</p>
                                       </div>
                                       <div class="benefit">
                                           <h3>Benefit 1</h3>
                                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse elit elit,</p>
                                       </div>
                                       <div class="benefit">
                                           <h3>Benefit 1</h3>
                                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse elit elit,</p>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </section>

        <section class="grant-steps">
            <div class="container">
                <h2 class="title-secondary">Grantovým expertom v troch krokoch</h2>

                <div class="steps">
                    <div class="step-row col-container">
                        <div class="left-part">
                            <div class="col-container">
                                <div class="col-number">
                                    <div class="number">1</div>
                                </div>
                                <div class="col-label">
                                    Vyplnenie profilu
                                </div>
                            </div>
                        </div>
                        <div class="right-part">
                            Stači jednoducho vyplniť vaše základné údaje, zvoliť si expertné oblasti a priložiť svoje CV, alebo odkaz na vašu webstránku.
                        </div>
                    </div>
                    <div class="step-row col-container">
                        <div class="left-part">
                            <div class="col-container">
                                <div class="col-number">
                                    <div class="number">2</div>
                                </div>
                                <div class="col-label">
                                    Autorizácia
                                </div>
                            </div>
                        </div>
                        <div class="right-part">
                            Váš vytvorený profil  autorizujeme. V prípade nejasností vás budeme kontaktovať. Po autorizácii vás budeme informovať o zverejnení vášho profilu.
                        </div>
                    </div>
                    <div class="step-row col-container">
                        <div class="left-part">
                            <div class="col-container">
                                <div class="col-number">
                                    <div class="number">3</div>
                                </div>
                                <div class="col-label">
                                    Zverejnenie
                                </div>
                            </div>
                        </div>
                        <div class="right-part">
                            Váš profil sa zverejní na našej stránke ako anonymný. Prístup ku vašim osobným údajom a CV budú mať iba platiaci návštevníci nášho portálu. Pokaľ chcete, aby sa vaša ponuka zobrazovala všetkým, staňte sa naším Prémiovým grantovým špecialistom
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="advertising mt-1 mt-md-9">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 offset-lg-1 col-lg-5">
                        <div class="card-advertising card-advertising--sun">
                            <h3 class="title-primary">Free</h3>
                            <div class="price-container">
                                <div class="tax-included">Cena s DPH</div>
                                <div class="title-secondary">Zadarmo</div>
                            </div>
                            <ul class="check-list check-list--black-check text-secondary">
                                <li>Neobmedzený čas ponuky</li>
                                <li>Logo spoločnosti</li>
                            </ul>
                        </div>
                        <div class="button-secondary">OBJEDNAŤ</div>
                    </div>
                    <div class="col-md-6 col-lg-5">
                        <div class="card-advertising card-advertising--recommendation">
                            <div class="card-recommendation">
                                Najefektívnejší spôsob inzerovania
                            </div>
                            <h3 class="title-primary">Premium</h3>
                            <div class="price-container">
                                <div class="tax-included">Cena s DPH</div>
                                <div class="title-secondary">123 €/inzerát</div>
                            </div>
                            <ul class="check-list check-list--black-check text-secondary">
                                <li>Neobmedzený čas ponuky</li>
                                <li>Prémiové zobrazenie 14 dní</li>
                                <li>Facebook post</li>
                                <li>Logo spoločnosti</li>
                                <li>PPC kampaň v hodnote 10€</li>
                            </ul>
                        </div>
                        <div class="button-secondary">OBJEDNAŤ</div>
                    </div>
                </div>
            </div>
        </section>



    </main>

    <?php include '../../_components/_footer.php';?>

