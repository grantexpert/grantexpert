
<?php include '../../_components/_head.php';?>

<body class="page-grant-resources-detail page-grant-education">

<div id="site-container">

    <?php include '../../_components/_header.php';?>

    <main id="site-content">

        <nav class="page-submenu">
            <ul>
                <li>
                    <a href="#">E-LEANING KURZY</a>
                </li>
                <li>
                    <a href="#">WEBINÁRE</a>
                </li>
                <li>
                    <a href="#">ŠKOLENIA NA MIERU</a>
                </li>
                <li>
                    <a href="#">VIDEOSLOVNÍK</a>
                </li>
                <li class="active">
                    <a href="#">GRANTOVÉ ZDROJE</a>
                </li>
                <li>
                    <a href="#">BLOG</a>
                </li>
            </ul>
        </nav>


        <header class="card-title-header">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">
                        <div class="col-container">
                            <div class="col-card col-card--logo order-2 order-lg-1">
                                <div class="card-header-summary">
                                    <img src="http://grantexpert.test/assets/img/europa-pre-obcanov-logo.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-title order-1 order-lg-2">
                                <div class="vertical-center">
                                    <h1 class="page-title title-primary">Európa pre občanov</h1>
                                    <p class="description">
                                        Celková alokácia sedemročného komunitárneho programu EÚ „Európa pre občanov 2014 – 2020“ predstavuje takmer 185,5 mil. eur. Hlavným cieľom je zlepšovať podmienky pre občiansku participáciu v Európskej únii.
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="row-line-grid">
            <div class="container">
                <div class="col-container">
                    <div class="col-title">
                    </div>
                    <div class="col-content">
                        <p>
                            Hlavné ciele komunitárneho programu Európa pre občanov je prispievať k tomu, aby občania EÚ rozumeli Únii, jej histórii a rozmanitosti a posilňovať inštitúciu občianstva k Európskej únii a zlepšovať podmienky pre demokratickú a občiansku participáciu na úrovni EÚ. Rozpočet programu je vyše 185 miliónov eur na roky 2014 – 2020.
                        </p>
                        <p>
                            Za riadenie programu je zodpovedná Európska komisia, konkrétne Generálne riaditeľstvo pre vnútorné záležitosti a migráciu. Za implementáciu programu zodpovedá Výkonná agentúra pre vzdelávanie, audiovizuálny sektor a kultúru v Bruseli.
                        </p>
                        <p>
                            Miestne a regionálne orgány, mimovládne organizácie, vzdelávacie, výskumné, kultúrne a mládežnícke inštitúcie môžu získať podporu na diskusné podujatia, konferencie, workshopy, umelecké dielne, športové podujatia, štúdie a iné občianske aktivity rôznorodého charakteru.
                        </p>
                        <p>
                            Výzvy a špecifické priority sú vyhlasované každoročne.
                        </p>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Dotácie z komunitárneho programu Európa pre občanov je možné čerpať na nasledujúce aktivity:</h3>
                    </div>
                    <div class="col-content">
                        <p>
                            Akcia 1: Európska pamiatka – Zvyšovanie povedomia o pamiatke, spoločnej histórii a hodnotách a cieli Únie.
                        </p>
                        <p>
                            Akcia 2: Podpora demokracie a občianska participácia – Podpora demokratickej a občianskej účasti občanov na úrovni Únie. Medzi opatrenia v tejto oblasti patria: družobné partnerstvá miest, siete medzi mestami a projekty občianskej spoločnosti.
                            Prevádzkové granty – poskytujú finančnú podporu na náklady potrebné na riadne vykonávanie bežných a stálych činností organizácie.
                        </p>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Program Európa pre občanov poskytuje dva druhy grantov:</h3>
                    </div>
                    <div class="col-content">
                        <p>
                            A. PROJEKTOVÉ GRANTY: Financie sú určené na presne vymedzené aktivity s obmedzenou lehotou trvania. Plán činnosti stanovený v žiadosti je po jej schválení záväzný.
                        </p>
                        <p>
                            B. PREVÁDZKOVÉ GRANTY: Finančná podpora je určená na náklady potrebné na riadne vykonávanie bežných činností organizácie. (Predovšetkým administratívne a cestovné náklady vznikajúce pri realizácii pracovného programu, náklady na zamestnancov, vydávanie publikácií a ďalšie náklady priamo súvisiace s pracovným programom organizácie.)
                        </p>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Viac o dotáciách z komunitárneho programu Európa pre občanov nájdete na:</h3>
                    </div>
                    <div class="col-content">
                        <p>
                            Európa pre občanov 2014 – 2020<br>
                            Európsky kontaktný bod Slovensko
                        </p>
                    </div>
                </div>
                <div class="col-container">
                    <div class="col-title">
                        <h3>Dokumenty na stiahnutie</h3>
                    </div>
                    <div class="col-content">
                        <div class="file-download">
                            <div class="col-container">
                                <div class="col-text">
                                    Dokument s nazvom vyzva cislo 1 v ktorom sa dozviete vsetko o tomto grante
                                    <div class="filename">
                                        Dokument_vyzva_1.PDF
                                    </div>
                                </div>
                                <div class="col-button">
                                    <button class="button-tertiary button-tertiary--reverse button-icon--auto-width">
                                        <div class="col-container">
                                            <div class="icon-container">
                                                <svg class="arrow-download" xmlns="http://www.w3.org/2000/svg" width="13.128" height="15.706" viewBox="0 0 13.128 15.706">
                                                    <g data-name="Component 46 – 44">
                                                        <path data-name="Line 2008" transform="rotate(90 3.215 3.216)" style="stroke:#fff;fill:none" d="M15 0H0"/>
                                                        <path class="arrow" data-name="Path 1073" d="m460.161 104.154-5.861 5.856 5.857 5.857" transform="rotate(-90 182.929 286.375)" style="stroke:#299bd7;stroke-linejoin:bevel;stroke-width:2px;fill:none"/>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="label-container">
                                                Stiahnuť
                                            </div>
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>

        <section class="more-grants mt-6 mt-md-8">
            <div class="container-fluid">
                <div class="section-container">
                    <div class="container">

                        <h2 class="title-secondary text-center">Ďalšie podobné granty a výzvy, ktoré by vás mohli zaujímať</h2>

                        <div class="card-content-two-col card-border-hover card-shadow-hover">
                            <div class="card-container">

                                <div class="tag-container">
                                    <a href="#" class="tag-rectangle">Covid 19</a>
                                    <a href="" class="tag-rectangle"><img src="http://grantexpert.test/assets/img/svg/star-blank-white.svg" alt="star-blank"> Nová výzva</a>
                                </div>

                                <div class="row">
                                    <div class="col-xl-8">
                                        <a href="#"><h2>Podpora nových a existujúcich sociálnych služieb, sociálnoprávnej ochrany detí v zariadeniach na komunitnej úrovni (2021)</h2></a>
                                        <p>
                                            Cieľom výzvy je podporiť prechod poskytovania sociálnych služieb a zabezpečenia výkonu opatrení sociálnoprávnej ochrany detí a sociálnej kurately v zariadení z inštitucionálnej formy na komunitnú a podporiť rozvoj služieb starostlivosti o dieťa do troch rokov veku na ...
                                        </p>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="row">
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Výška dotácie
                                                        </div>
                                                        <div class="value">max. 2 000 000</div>
                                                    </div>
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Miera dotácie
                                                        </div>
                                                        <div class="value">max. 95%</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="row">
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Ostáva dní
                                                        </div>
                                                        <div class="value">48</div>
                                                    </div>
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="date">
                                                            <div class="date-from">Od 1.9.2021</div>
                                                            <div class="date-to">Do Vyčerpania financií</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="card-content-two-col card-border-hover card-shadow-hover">
                            <div class="card-container">

                                <div class="tag-container">
                                    <a href="#" class="tag-rectangle">Covid 19</a>
                                    <a href="" class="tag-rectangle"><img src="http://grantexpert.test/assets/img/svg/star-blank-white.svg" alt="star-blank"> Nová výzva</a>
                                </div>

                                <div class="row">
                                    <div class="col-xl-8">
                                        <a href="#"><h2>Podpora nových a existujúcich sociálnych služieb, sociálnoprávnej ochrany detí v zariadeniach na komunitnej úrovni (2021)</h2></a>
                                        <p>
                                            Cieľom výzvy je podporiť prechod poskytovania sociálnych služieb a zabezpečenia výkonu opatrení sociálnoprávnej ochrany detí a sociálnej kurately v zariadení z inštitucionálnej formy na komunitnú a podporiť rozvoj služieb starostlivosti o dieťa do troch rokov veku na ...
                                        </p>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="row">
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Výška dotácie
                                                        </div>
                                                        <div class="value">max. 2 000 000</div>
                                                    </div>
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Miera dotácie
                                                        </div>
                                                        <div class="value">max. 95%</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="row">
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="text-small-label-secondary">
                                                            Ostáva dní
                                                        </div>
                                                        <div class="value">48</div>
                                                    </div>
                                                    <div class="col-6 col-lg-6 col-xl-12">
                                                        <div class="date">
                                                            <div class="date-from">Od 1.9.2021</div>
                                                            <div class="date-to">Do Vyčerpania financií</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="button-center">
                            <a href="#" class="button-primary">Všetky granty</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </main>

    <?php include '../../_components/_footer.php';?>

